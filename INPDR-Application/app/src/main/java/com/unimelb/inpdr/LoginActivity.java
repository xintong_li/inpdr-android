package com.unimelb.inpdr;

import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.graphics.Typeface;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.Gravity;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.inputmethod.InputMethodManager;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.CompoundButton;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.unimelb.inpdr.Models.API.APIItem;
import com.unimelb.inpdr.Models.API.APIReceivable;
import com.unimelb.inpdr.Models.API.INPDRAPI;
import com.unimelb.inpdr.Models.API.ModelFacade;
import com.unimelb.inpdr.Models.ErrorMessage;
import com.unimelb.inpdr.Sources.FontsOverride;

import java.util.List;

import me.drakeet.materialdialog.MaterialDialog;

public class LoginActivity extends AppCompatActivity implements APIReceivable{
    EditText loginEmail;
    EditText loginPassword;
    TextView email;
    TextView password;
    Button buttonLogin;
    boolean saveLogin;
    private Typeface mTf;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        FontsOverride.setDefaultFont(this, "DEFAULT", "OpenSans-Regular.ttf");
        FontsOverride.setDefaultFont(this, "MONOSPACE", "Bitstream Vera Sans Mono Roman.ttf");

        setContentView(R.layout.activity_login);
        mTf = Typeface.createFromAsset(getAssets(), "OpenSans-Regular.ttf");

        email = (TextView) findViewById(R.id.text_email_login);
        password= (TextView) findViewById(R.id.text_password_login);
        email.setTypeface(mTf);
        password.setTypeface(mTf);


        loginEmail = (EditText) findViewById(R.id.text_login_email_info);
        loginEmail.setTypeface(mTf);
        InputMethodManager imm = (InputMethodManager) getSystemService(Context.INPUT_METHOD_SERVICE);
        imm.showSoftInput(loginEmail, InputMethodManager.SHOW_IMPLICIT);

        loginPassword = (EditText) findViewById(R.id.text_login_password_info);
        InputMethodManager imm2 = (InputMethodManager) getSystemService(Context.INPUT_METHOD_SERVICE);
        imm2.showSoftInput(loginPassword, InputMethodManager.SHOW_IMPLICIT);

        buttonLogin = (Button) findViewById(R.id.button_login);

        final INPDRAPI api = new INPDRAPI(getApplicationContext());
        final APIReceivable receivable = this;
        final Context context = this;

        SharedPreferences loginPreferences = getSharedPreferences("loginPrefs", MODE_PRIVATE);
        final SharedPreferences.Editor loginPrefsEditor = loginPreferences.edit();

        CheckBox remember_me = (CheckBox)findViewById(R.id.checkBox_remember_me_login);
        remember_me.setTypeface(mTf);

        saveLogin = loginPreferences.getBoolean("saveLogin", false);
        if (saveLogin == true) {
            loginEmail.setText(loginPreferences.getString("email", ""));
            loginPassword.setText(loginPreferences.getString("password", ""));
            remember_me.setChecked(true);
        }

        remember_me.setOnCheckedChangeListener(
                new CompoundButton.OnCheckedChangeListener() {

                       @Override
                       public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
                           if(isChecked){
                               loginPrefsEditor.putBoolean("saveLogin", true);
                               loginPrefsEditor.putString("email", loginEmail.getText().toString());
                               loginPrefsEditor.putString("password", loginPassword.getText().toString());
                               loginPrefsEditor.commit();
                           }else {
                               loginPrefsEditor.clear();
                               loginPrefsEditor.commit();
                           }

                       }
                   }
        );
        buttonLogin.setOnClickListener((new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                String email = loginEmail.getText().toString();
                String password = loginPassword.getText().toString();

                if(email.equals("") || password.equals("")){
                    final MaterialDialog mMaterialDialog = new MaterialDialog(context);
                    mMaterialDialog.setTitle("Empty Login Code");
                    mMaterialDialog.setMessage("The login code cannot be empty.\nPlease type in your login code.");
                    mMaterialDialog.setPositiveButton("Try Again", new View.OnClickListener() {
                        @Override
                        public void onClick(View v) {
                            mMaterialDialog.dismiss();
                        }
                    });
                    mMaterialDialog.show();
                }
                else {
                    ModelFacade.getInstance().login(receivable, api, email, password);
                }

//                if(loginCode.getText().toString().equals("1") || loginCode.getText().toString().equals("2")
//                        || loginCode.getText().toString().equals("3")){
//                    int user_id = Integer.parseInt(loginCode.getText().toString());
//                    Context context = v.getContext();
//                    CharSequence text = "succeed!";
//                    int duration = Toast.LENGTH_SHORT;
//
//                    Toast toast = Toast.makeText(context, text, duration);
//                    toast.show();
//                    intent.putExtra("user_id", user_id);
//                    startActivity(intent);
//                }
//                else if(loginCode.getText().toString().equals("")){
//                    final AlertDialog.Builder builder = new AlertDialog.Builder(context);
//
//                    builder.setTitle("Empty Login Code");
//                    builder.setMessage("The login code cannot be empty.\nPlease type in your login code.");
//                    builder.setNeutralButton("Try Again", null);
//
//                    builder.setIcon(R.drawable.icon_alert);
//                    final AlertDialog alertDialog = builder.show();
//                    TextView messageText = (TextView)alertDialog.findViewById(android.R.id.message);
//                    messageText.setGravity(Gravity.CENTER);
//
//                    Button okButton = alertDialog.getButton(AlertDialog.BUTTON_NEUTRAL);
//
//                    LinearLayout.LayoutParams okButtonL = (LinearLayout.LayoutParams) okButton.getLayoutParams();
//                    okButtonL.gravity = Gravity.CENTER;
//                    okButton.setLayoutParams(okButtonL);
//
//                    alertDialog.show();
//                }
//                else{
//                    final AlertDialog alertDialog = new AlertDialog.Builder(context).create();
//                    alertDialog.setTitle("Incorrect Login code");
//                    alertDialog.setMessage("The login code you typed is incorrect.\n\t\t\t\t\t\t\t\t\t\tPlease type again.");
//                    alertDialog.setIcon(R.drawable.icon_alert);
//
//                    alertDialog.setButton(AlertDialog.BUTTON_NEUTRAL, "Try Again",
//                            new DialogInterface.OnClickListener() {
//                                public void onClick(DialogInterface dialog, int which) {
//                                    alertDialog.dismiss();
//                                }
//                            });
//
//                    alertDialog.show();
//                }
            }
        }));
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_login, menu);
            return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == R.id.action_settings) {
            return true;
        }

        return super.onOptionsItemSelected(item);
    }

    @Override
    public <U extends APIItem> void receiveData(List<U> items, String type) {

    }

    @Override
    public <U extends APIItem> void receiveData(U item, String type) {
        Intent intent = new Intent(this, MainActivity.class);
        final Context context = this;
        switch (type){
            case "login":
                CharSequence text = "succeed!";
                int duration = Toast.LENGTH_SHORT;

                Toast toast = Toast.makeText(context, text, duration);
                toast.show();
                startActivity(intent);
                break;
            case "error":
                final MaterialDialog mMaterialDialog_c = new MaterialDialog(context);
                mMaterialDialog_c.setTitle("Error");
                ErrorMessage errorMessage = (ErrorMessage) item;
                mMaterialDialog_c.setMessage(errorMessage.getErrorMes());
                mMaterialDialog_c.setPositiveButton("Ok", new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        mMaterialDialog_c.dismiss();
                    }
                });
                mMaterialDialog_c.show();
                break;
        }

    }
}
