package com.unimelb.inpdr.Models;

import com.unimelb.inpdr.Models.API.APIItem;

/**
 * Created by Xintong on 20/05/2016.
 */
public class Authentication extends APIItem{
    private String status;
    private String token;


    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public String getToken() {
        return token;
    }

    public void setToken(String token) {
        this.token = token;
    }
}
