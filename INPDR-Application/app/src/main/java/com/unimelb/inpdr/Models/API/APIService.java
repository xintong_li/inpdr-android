package com.unimelb.inpdr.Models.API;

import android.app.Service;
import android.content.Intent;
import android.os.Binder;
import android.os.IBinder;
import android.util.Log;

/**
 * Created by Xintong on 20/04/2016.
 */
public class APIService extends Service {
    private final IBinder mBinder = new NetworkBinder();
    private INPDRAPI inpdrApi;

    @Override
    public void onCreate() {
        inpdrApi = new INPDRAPI(getApplicationContext());
        Log.d("inpdr api created", "" + inpdrApi);
    }

    public class NetworkBinder extends Binder {
        public APIService getService() {
            Log.d("service!!!!", "" + inpdrApi);
            return APIService.this;
        }
    }

    @Override
    public IBinder onBind(Intent intent) {
        return mBinder;
    }

    public INPDRAPI getINPDRAPI() {
        return inpdrApi;
    }

    public void setNetwork(INPDRAPI instagramAPI) {
        this.inpdrApi = instagramAPI;
    }
}
