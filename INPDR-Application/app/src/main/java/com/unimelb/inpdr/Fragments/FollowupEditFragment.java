package com.unimelb.inpdr.Fragments;

import android.content.Context;
import android.content.DialogInterface;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentTransaction;
import android.support.v7.app.AlertDialog;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.TextView;
import android.widget.Toast;

import com.jaredrummler.materialspinner.MaterialSpinner;
import com.unimelb.inpdr.MainActivity;
import com.unimelb.inpdr.Models.API.APIItem;
import com.unimelb.inpdr.Models.API.APIReceivable;
import com.unimelb.inpdr.Models.API.INPDRAPI;
import com.unimelb.inpdr.Models.API.ModelFacade;
import com.unimelb.inpdr.Models.BasicInfo;
import com.unimelb.inpdr.Models.ErrorMessage;
import com.unimelb.inpdr.Models.NpbApplicantFollowupCollection;
import com.unimelb.inpdr.Models.NpbFollowUp;
import com.unimelb.inpdr.Models.NpbFollowupCollection;
import com.unimelb.inpdr.Models.NpcApplicantFollowupCollection;
import com.unimelb.inpdr.Models.NpcFollowUp;
import com.unimelb.inpdr.Models.NpcFollowupCollection;
import com.unimelb.inpdr.Models.Selection;
import com.unimelb.inpdr.R;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import me.drakeet.materialdialog.MaterialDialog;

/**
 * Created by Xintong on 20/05/2016.
 */
public class FollowupEditFragment extends Fragment implements APIReceivable{
    int followable_id;
    String followable_type;
    int id;
    private ArrayList<NpbFollowUp> npbFollowUps = new ArrayList<>();
    private ArrayList<NpcFollowUp> npcFollowUps = new ArrayList<>();
    private ArrayList<NpbFollowUp> npbApplicantFollowUps = new ArrayList<>();
    private ArrayList<NpcFollowUp> npcApplicantFollowUps = new ArrayList<>();

    Map<String, Integer> npbCollection = new HashMap<>();
    Map<String, Integer> npbApplicantCollection = new HashMap<>();
    Map<String, Integer> npcCollection = new HashMap<>();
    Map<String, Integer> npcApplicantCollection = new HashMap<>();

    boolean isFirstTime = true;

    public FollowupEditFragment() {
        // Required empty public constructor
    }

    @Override
    public void onStart(){
        super.onStart();
        ((MainActivity)getActivity()).setActionBarTitle("Edit Followup");
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        return inflater.inflate(R.layout.fragment_followup_edit, container, false);
    }

     @Override
    public void onViewCreated(View view, Bundle savedInstanceState) {
         BasicInfo.getInstance();
         NpbFollowupCollection.getInstance();
         NpbApplicantFollowupCollection.getInstance();
         NpcFollowupCollection.getInstance();
         NpcApplicantFollowupCollection.getInstance();
         Selection.getInstance();

         final INPDRAPI api = new INPDRAPI(getContext());

         ((MainActivity)getActivity()).setActionBarTitle("Edit Followup");

         Bundle data = getArguments();

         if (data != null) {
             try {
                 followable_id = data.getInt("followable_id");
                 id = data.getInt("followup_id");
                 followable_type = data.getString("followable_type");
                 switch (followable_type) {
                     case "NpbParticipant":
                         ModelFacade.getInstance().getANpbFollowup(this, api, followable_type, followable_id, id);
                         break;
                     case "NpbApplicantParticipant":
                         ModelFacade.getInstance().getANpbFollowup(this, api, followable_type, followable_id, id);
                         break;
                     case "NpcParticipant":
                         ModelFacade.getInstance().getANpcFollowup(this, api, followable_type, followable_id, id);
                         break;
                     case "NpcApplicantParticipant":
                         ModelFacade.getInstance().getANpcFollowup(this, api, followable_type, followable_id, id);
                         break;
                 }
             } catch (Exception e) {
                 e.printStackTrace();
             }
         }
     }

    @Override
    public <U extends APIItem> void receiveData(List<U> items, String type) {
        switch (type){
            case "getANpbFollowup":
                npbFollowUps.clear();
                npbApplicantFollowUps.clear();
                for (U item : items) {
                    NpbFollowUp feed = (NpbFollowUp) item;
                    switch (feed.getFollowable_type()){
                        case "NpbParticipant":
                            npbCollection.put("symptom_bone", feed.getSymptom_bone());
                            npbCollection.put("symptom_abdominal", feed.getSymptom_abdominal());
                            npbCollection.put("symptom_pain", feed.getSymptom_pain());
                            npbCollection.put("symptom_cognition", feed.getSymptom_cognition());
                            npbCollection.put("symptom_breathlessness_rate", feed.getSymptom_breathlessness_rate());
                            npbCollection.put("symptom_breathlessness_impact", feed.getSymptom_breathlessness_impact());
                            npbCollection.put("symptom_bleeding_rate", feed.getSymptom_bleeding_rate());
                            npbCollection.put("symptom_bleeding_impact", feed.getSymptom_bleeding_impact());
                            npbCollection.put("symptom_fatigue_rate", feed.getSymptom_fatigue_rate());
                            npbCollection.put("symptom_fatigue_impact", feed.getSymptom_fatigue_impact());
                            npbCollection.put("symptom_enlarge_organ", feed.getSymptom_enlarge_organ());
                            npbCollection.put("symptom_enlarge_organ_impact", feed.getSymptom_enlarge_organ_impact());
                            npbCollection.put("symptom_slow_growth", feed.getSymptom_slow_growth());
                            npbCollection.put("symptom_slow_growth_impact", feed.getSymptom_slow_growth_impact());
                            npbCollection.put("symptom_infection", feed.getSymptom_infection());
                            npbCollection.put("symptom_fracture", feed.getSymptom_fracture());
                            npbCollection.put("impact_family_disappointed", feed.getImpact_family_disappointed());
                            npbCollection.put("impact_family_give_up", feed.getImpact_family_give_up());
                            npbCollection.put("impact_family_worry_future", feed.getImpact_family_worry_future());
                            npbCollection.put("impact_family_closer", feed.getImpact_family_closer());
                            npbCollection.put("impact_wider_self_miss_career", feed.getImpact_wider_self_miss_career());
                            npbCollection.put("impact_wider_carer_miss_career", feed.getImpact_wider_carer_miss_career());
                            npbCollection.put("impact_wider_emergency", feed.getImpact_wider_emergency());
                            npbCollection.put("impact_wider_appointment", feed.getImpact_wider_appointment());
                            EditNpbFollowup(feed);
                            npbFollowUps.add(feed);
                            break;
                        case "NpbApplicantParticipant":
                            npbApplicantCollection.put("symptom_bone", feed.getSymptom_bone());
                            npbApplicantCollection.put("symptom_abdominal", feed.getSymptom_abdominal());
                            npbApplicantCollection.put("symptom_pain", feed.getSymptom_pain());
                            npbApplicantCollection.put("symptom_cognition", feed.getSymptom_cognition());
                            npbApplicantCollection.put("symptom_breathlessness_rate", feed.getSymptom_breathlessness_rate());
                            npbApplicantCollection.put("symptom_breathlessness_impact", feed.getSymptom_breathlessness_impact());
                            npbApplicantCollection.put("symptom_bleeding_rate", feed.getSymptom_bleeding_rate());
                            npbApplicantCollection.put("symptom_bleeding_impact", feed.getSymptom_bleeding_impact());
                            npbApplicantCollection.put("symptom_fatigue_rate", feed.getSymptom_fatigue_rate());
                            npbApplicantCollection.put("symptom_fatigue_impact", feed.getSymptom_fatigue_impact());
                            npbApplicantCollection.put("symptom_enlarge_organ", feed.getSymptom_enlarge_organ());
                            npbApplicantCollection.put("symptom_enlarge_organ_impact", feed.getSymptom_enlarge_organ_impact());
                            npbApplicantCollection.put("symptom_slow_growth", feed.getSymptom_slow_growth());
                            npbApplicantCollection.put("symptom_slow_growth_impact", feed.getSymptom_slow_growth_impact());
                            npbApplicantCollection.put("symptom_infection", feed.getSymptom_infection());
                            npbApplicantCollection.put("symptom_fracture", feed.getSymptom_fracture());
                            npbApplicantCollection.put("impact_family_disappointed", feed.getImpact_family_disappointed());
                            npbApplicantCollection.put("impact_family_give_up", feed.getImpact_family_give_up());
                            npbApplicantCollection.put("impact_family_worry_future", feed.getImpact_family_worry_future());
                            npbApplicantCollection.put("impact_family_closer", feed.getImpact_family_closer());
                            npbApplicantCollection.put("impact_wider_self_miss_career", feed.getImpact_wider_self_miss_career());
                            npbApplicantCollection.put("impact_wider_carer_miss_career", feed.getImpact_wider_carer_miss_career());
                            npbApplicantCollection.put("impact_wider_emergency", feed.getImpact_wider_emergency());
                            npbApplicantCollection.put("impact_wider_appointment", feed.getImpact_wider_appointment());
                            EditApplicantNpbFollowup(feed);
                            npbApplicantFollowUps.add(feed);
                            break;
                    }

                    //Log.d("!!!!!data received!!!", feed.getUser().getEmail() + "");
                }
                break;
            case "getANpcFollowup":
                npcFollowUps.clear();
                npcApplicantFollowUps.clear();
                for (U item : items) {
                    NpcFollowUp feed = (NpcFollowUp) item;
                    switch (feed.getFollowable_type()){
                        case "NpcParticipant":
                            npcCollection.put("impact_ambulation", feed.getImpact_ambulation());
                            npcCollection.put("impact_manipulation", feed.getImpact_manipulation());
                            npcCollection.put("impact_speech", feed.getImpact_speech());
                            npcCollection.put("impact_swallowing", feed.getImpact_swallowing());
                            npcCollection.put("impact_eye_movement", feed.getImpact_eye_movement());
                            npcCollection.put("impact_seizure", feed.getImpact_seizure());
                            npcCollection.put("impact_cognitive_impaired", feed.getImpact_cognitive_impaired());
                            npcCollection.put("impact_family_disappointed", feed.getImpact_family_disappointed());
                            npcCollection.put("impact_family_give_up", feed.getImpact_family_give_up());
                            npcCollection.put("impact_family_worry_future", feed.getImpact_family_worry_future());
                            npcCollection.put("impact_family_closer_family", feed.getImpact_family_closer_family());
                            npcCollection.put("impact_wider_self_miss_career", feed.getImpact_wider_self_miss_career());
                            npcCollection.put("impact_wider_family_miss_career", feed.getImpact_wider_family_miss_career());
                            npcCollection.put("impact_wider_emergency", feed.getImpact_wider_emergency());
                            npcCollection.put("impact_wider_appointment", feed.getImpact_wider_appointment());
                            EditNpcFollowup(feed);
                            npcFollowUps.add(feed);
                            break;
                        case "NpcApplicantParticipant":
                            npcApplicantCollection.put("impact_ambulation", feed.getImpact_ambulation());
                            npcApplicantCollection.put("impact_manipulation", feed.getImpact_manipulation());
                            npcApplicantCollection.put("impact_speech", feed.getImpact_speech());
                            npcApplicantCollection.put("impact_swallowing", feed.getImpact_swallowing());
                            npcApplicantCollection.put("impact_eye_movement", feed.getImpact_eye_movement());
                            npcApplicantCollection.put("impact_seizure", feed.getImpact_seizure());
                            npcApplicantCollection.put("impact_cognitive_impaired", feed.getImpact_cognitive_impaired());
                            npcApplicantCollection.put("impact_family_disappointed", feed.getImpact_family_disappointed());
                            npcApplicantCollection.put("impact_family_give_up", feed.getImpact_family_give_up());
                            npcApplicantCollection.put("impact_family_worry_future", feed.getImpact_family_worry_future());
                            npcApplicantCollection.put("impact_family_closer_family", feed.getImpact_family_closer_family());
                            npcApplicantCollection.put("impact_wider_self_miss_career", feed.getImpact_wider_self_miss_career());
                            npcApplicantCollection.put("impact_wider_family_miss_career", feed.getImpact_wider_family_miss_career());
                            npcApplicantCollection.put("impact_wider_emergency", feed.getImpact_wider_emergency());
                            npcApplicantCollection.put("impact_wider_appointment", feed.getImpact_wider_appointment());
                            EditApplicantNpcFollowup(feed);
                            npcApplicantFollowUps.add(feed);
                            break;
                    }
                }
                break;
        }
    }

    @Override
    public <U extends APIItem> void receiveData(U item, String type) {
        switch (type){
            case "error":
                final MaterialDialog mMaterialDialog_e = new MaterialDialog(getContext());
                mMaterialDialog_e.setTitle("Error");
                ErrorMessage errorMessage = (ErrorMessage) item;
                mMaterialDialog_e.setMessage(errorMessage.getErrorMes());
                mMaterialDialog_e.setPositiveButton("Ok", new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        mMaterialDialog_e.dismiss();
                    }
                });
                mMaterialDialog_e.show();
        }
    }

    private void EditApplicantNpcFollowup(NpcFollowUp feed) {
        TextView textDescription = (TextView)getView().findViewById(R.id.text_explanation_edit_followup);
        textDescription.setText("This section contains questions regarding how NP-C has affected a person's health. " +
                "It relates to a wide range of symptoms associated with NP-C. Please note that NP-C is very " +
                "variable and not all patients experience the same symptoms. For example, some people with NP-C never have seizures. " +
                "You can only choose one of the given options. If none of them fit exactly, simply choose the one that is most appropriate.");

        TextView textNpcCreate = (TextView)getView().findViewById(R.id.text_edit_followup_title);
        textNpcCreate.setText("Edit NP-C Followup Questionnaire");

        TextView textNpcCategory = (TextView)getView().findViewById(R.id.text_q_category_edit_followup);
        textNpcCategory.setText("NP-C Impact Questions");

        int index = 0;
        ViewGroup insertPoint = (ViewGroup) getView().findViewById(R.id.lty_edit_followup);
        LayoutInflater vi = (LayoutInflater) getActivity().getSystemService(Context.LAYOUT_INFLATER_SERVICE);


        List<String> answers_impact_ambulation = new ArrayList<>();
        answers_impact_ambulation.add(" ");
        for(int i = 0; i < NpcApplicantFollowupCollection.getImpact_ambulation().size(); i++){
            answers_impact_ambulation.add(NpcApplicantFollowupCollection.getImpact_ambulation().get(i));
        }

        NpcInflater(vi, answers_impact_ambulation, "impact_ambulation", insertPoint, index, npcApplicantCollection, BasicInfo.getNpc_applicant_participant_followups_questions());
        index ++;

        List<String> answers_impact_manipulation = new ArrayList<>();
        answers_impact_manipulation.add(" ");
        for(int i = 0; i < NpcApplicantFollowupCollection.getImpact_manipulation().size(); i++){
            answers_impact_manipulation.add(NpcApplicantFollowupCollection.getImpact_manipulation().get(i));
        }

        NpcInflater(vi, answers_impact_manipulation, "impact_manipulation", insertPoint, index, npcApplicantCollection, BasicInfo.getNpc_applicant_participant_followups_questions());
        index ++;

        List<String> answers_impact_speech = new ArrayList<>();
        answers_impact_speech.add(" ");
        for(int i = 0; i < NpcApplicantFollowupCollection.getImpact_speech().size(); i++){
            answers_impact_speech.add(NpcApplicantFollowupCollection.getImpact_speech().get(i));
        }

        NpcInflater(vi, answers_impact_speech, "impact_speech", insertPoint, index, npcApplicantCollection, BasicInfo.getNpc_applicant_participant_followups_questions());
        index ++;

        List<String> answers_impact_swallowing = new ArrayList<>();
        answers_impact_swallowing.add(" ");
        for(int i = 0; i < NpcApplicantFollowupCollection.getImpact_swallowing().size(); i++){
            answers_impact_swallowing.add(NpcApplicantFollowupCollection.getImpact_swallowing().get(i));
        }

        NpcInflater(vi, answers_impact_swallowing, "impact_swallowing", insertPoint, index, npcApplicantCollection, BasicInfo.getNpc_applicant_participant_followups_questions());
        index ++;

        List<String> answers_impact_eye_movement = new ArrayList<>();
        answers_impact_eye_movement.add(" ");
        for(int i = 0; i < NpcApplicantFollowupCollection.getImpact_eye_movement().size(); i++){
            answers_impact_eye_movement.add(NpcApplicantFollowupCollection.getImpact_eye_movement().get(i));
        }

        NpcInflater(vi, answers_impact_eye_movement, "impact_eye_movement", insertPoint, index, npcApplicantCollection, BasicInfo.getNpc_applicant_participant_followups_questions());
        index ++;

        List<String> answers_impact_seizure = new ArrayList<>();
        answers_impact_seizure.add(" ");
        for(int i = 0; i < NpcApplicantFollowupCollection.getImpact_seizure().size(); i++){
            answers_impact_seizure.add(NpcApplicantFollowupCollection.getImpact_seizure().get(i));
        }

        NpcInflater(vi, answers_impact_seizure, "impact_seizure", insertPoint, index, npcApplicantCollection, BasicInfo.getNpc_applicant_participant_followups_questions());
        index ++;

        List<String> answers_impact_cognitive_impaired = new ArrayList<>();
        answers_impact_cognitive_impaired.add(" ");
        for(int i = 0; i < NpcApplicantFollowupCollection.getImpact_cognitive_impaired().size(); i++){
            answers_impact_cognitive_impaired.add(NpcApplicantFollowupCollection.getImpact_cognitive_impaired().get(i));
        }

        NpcInflater(vi, answers_impact_cognitive_impaired, "impact_cognitive_impaired", insertPoint, index, npcApplicantCollection, BasicInfo.getNpc_applicant_participant_followups_questions());
        index ++;


        Button buttonNext = (Button) getView().findViewById(R.id.button_followup_next);
        buttonNext.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Toast.makeText(getView().getContext(), "next!", Toast.LENGTH_SHORT).show();

                Bundle data = new Bundle();
                data.putInt("followable_id", followable_id);
                data.putString("followable_type", followable_type);
                data.putInt("id", id);
                data.putInt("impact_ambulation", npcApplicantCollection.get("impact_ambulation"));
                data.putInt("impact_manipulation", npcApplicantCollection.get("impact_manipulation"));
                data.putInt("impact_speech", npcApplicantCollection.get("impact_speech"));
                data.putInt("impact_swallowing", npcApplicantCollection.get("impact_swallowing"));
                data.putInt("impact_eye_movement", npcApplicantCollection.get("impact_eye_movement"));
                data.putInt("impact_seizure", npcApplicantCollection.get("impact_seizure"));
                data.putInt("impact_cognitive_impaired", npcApplicantCollection.get("impact_cognitive_impaired"));
                data.putInt("impact_family_disappointed", npcApplicantCollection.get("impact_family_disappointed"));
                data.putInt("impact_family_give_up", npcApplicantCollection.get("impact_family_give_up"));
                data.putInt("impact_family_worry_future", npcApplicantCollection.get("impact_family_worry_future"));
                data.putInt("impact_family_closer_family", npcApplicantCollection.get("impact_family_closer_family"));
                data.putInt("impact_wider_self_miss_career", npcApplicantCollection.get("impact_wider_self_miss_career"));
                data.putInt("impact_wider_family_miss_career", npcApplicantCollection.get("impact_wider_family_miss_career"));
                data.putInt("impact_wider_emergency", npcApplicantCollection.get("impact_wider_emergency"));
                data.putInt("impact_wider_appointment", npcApplicantCollection.get("impact_wider_appointment"));
                data.putBoolean("first_time", isFirstTime);

                Fragment followupEditFamilyFragment;

                FragmentManager fm = getActivity().getSupportFragmentManager();
                FragmentTransaction transaction = fm.beginTransaction();
//

                followupEditFamilyFragment= new FollowupEditFamilyFragment();


                followupEditFamilyFragment.setArguments(data);
//                ft.replace(R.id.main_view, followupFamilyFragment);
//                ft.addToBackStack(null);
//                ft.commit();
                Log.d("back", fm.getBackStackEntryCount() + "");

                transaction.add(R.id.main_view, followupEditFamilyFragment,
                        "FragmentEditFamily"); //now you have an instance of newFragment1//now you have an instance of newFragment2
                transaction.addToBackStack("FragmentEdit");
                transaction.show(followupEditFamilyFragment);
                transaction.setTransition(FragmentTransaction.TRANSIT_FRAGMENT_OPEN);
                transaction.commit();
                isFirstTime = false;

            }
        });
    }

    private void EditNpcFollowup(NpcFollowUp feed) {
        TextView textDiscription = (TextView)getView().findViewById(R.id.text_explanation_edit_followup);
        textDiscription.setText("This section contains questions regarding how NP-C has affected a person's health. " +
                "It relates to a wide range of symptoms associated with NP-C. Please note that NP-C is very " +
                "variable and not all patients experience the same symptoms. For example, some people with NP-C never have seizures. " +
                "You can only choose one of the given options. If none of them fit exactly, simply choose the one that is most appropriate.");

        TextView textNpcCreate = (TextView)getView().findViewById(R.id.text_edit_followup_title);
        textNpcCreate.setText("Edit NP-C Followup Questionnaire");

        TextView textNpcCategory = (TextView)getView().findViewById(R.id.text_q_category_edit_followup);
        textNpcCategory.setText("NP-C Impact Questions");

        int index = 0;
        ViewGroup insertPoint = (ViewGroup) getView().findViewById(R.id.lty_edit_followup);
        LayoutInflater vi = (LayoutInflater) getActivity().getSystemService(Context.LAYOUT_INFLATER_SERVICE);


        List<String> answers_impact_ambulation = new ArrayList<>();
        answers_impact_ambulation.add(" ");
        for(int i = 0; i < NpcFollowupCollection.getImpact_ambulation().size(); i++){
            answers_impact_ambulation.add(NpcFollowupCollection.getImpact_ambulation().get(i));
        }

        NpcInflater(vi, answers_impact_ambulation, "impact_ambulation", insertPoint, index, npcCollection, BasicInfo.getNpc_participant_followups_questions());
        index ++;

        List<String> answers_impact_manipulation = new ArrayList<>();
        answers_impact_manipulation.add(" ");
        for(int i = 0; i < NpcFollowupCollection.getImpact_manipulation().size(); i++){
            answers_impact_manipulation.add(NpcFollowupCollection.getImpact_manipulation().get(i));
        }

        NpcInflater(vi, answers_impact_manipulation, "impact_manipulation", insertPoint, index, npcCollection, BasicInfo.getNpc_participant_followups_questions());
        index ++;

        List<String> answers_impact_speech = new ArrayList<>();
        answers_impact_speech.add(" ");
        for(int i = 0; i < NpcFollowupCollection.getImpact_speech().size(); i++){
            answers_impact_speech.add(NpcFollowupCollection.getImpact_speech().get(i));
        }

        NpcInflater(vi, answers_impact_speech, "impact_speech", insertPoint, index, npcCollection, BasicInfo.getNpc_participant_followups_questions());
        index ++;

        List<String> answers_impact_swallowing = new ArrayList<>();
        answers_impact_swallowing.add(" ");
        for(int i = 0; i < NpcFollowupCollection.getImpact_swallowing().size(); i++){
            answers_impact_swallowing.add(NpcFollowupCollection.getImpact_swallowing().get(i));
        }

        NpcInflater(vi, answers_impact_swallowing, "impact_swallowing", insertPoint, index, npcCollection, BasicInfo.getNpc_participant_followups_questions());
        index ++;

        List<String> answers_impact_eye_movement = new ArrayList<>();
        answers_impact_eye_movement.add(" ");
        for(int i = 0; i < NpcFollowupCollection.getImpact_eye_movement().size(); i++){
            answers_impact_eye_movement.add(NpcFollowupCollection.getImpact_eye_movement().get(i));
        }

        NpcInflater(vi, answers_impact_eye_movement, "impact_eye_movement", insertPoint, index, npcCollection, BasicInfo.getNpc_participant_followups_questions());
        index ++;

        List<String> answers_impact_seizure = new ArrayList<>();
        answers_impact_seizure.add(" ");
        for(int i = 0; i < NpcFollowupCollection.getImpact_seizure().size(); i++){
            answers_impact_seizure.add(NpcFollowupCollection.getImpact_seizure().get(i));
        }

        NpcInflater(vi, answers_impact_seizure, "impact_seizure", insertPoint, index, npcCollection, BasicInfo.getNpc_participant_followups_questions());
        index ++;

        List<String> answers_impact_cognitive_impaired = new ArrayList<>();
        answers_impact_cognitive_impaired.add(" ");
        for(int i = 0; i < NpcFollowupCollection.getImpact_cognitive_impaired().size(); i++){
            answers_impact_cognitive_impaired.add(NpcFollowupCollection.getImpact_cognitive_impaired().get(i));
        }

        NpcInflater(vi, answers_impact_cognitive_impaired, "impact_cognitive_impaired", insertPoint, index, npcCollection, BasicInfo.getNpc_participant_followups_questions());
        index ++;


        Button buttonNext = (Button) getView().findViewById(R.id.button_followup_next);
        buttonNext.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Toast.makeText(getView().getContext(), "next!", Toast.LENGTH_SHORT).show();

                Bundle data = new Bundle();
                data.putInt("followable_id", followable_id);
                data.putString("followable_type", followable_type);
                data.putInt("id", id);
                data.putInt("impact_ambulation", npcCollection.get("impact_ambulation"));
                data.putInt("impact_manipulation", npcCollection.get("impact_manipulation"));
                data.putInt("impact_speech", npcCollection.get("impact_speech"));
                data.putInt("impact_swallowing", npcCollection.get("impact_swallowing"));
                data.putInt("impact_eye_movement", npcCollection.get("impact_eye_movement"));
                data.putInt("impact_seizure", npcCollection.get("impact_seizure"));
                data.putInt("impact_cognitive_impaired", npcCollection.get("impact_cognitive_impaired"));
                data.putInt("impact_family_disappointed", npcCollection.get("impact_family_disappointed"));
                data.putInt("impact_family_give_up", npcCollection.get("impact_family_give_up"));
                data.putInt("impact_family_worry_future", npcCollection.get("impact_family_worry_future"));
                data.putInt("impact_family_closer_family", npcCollection.get("impact_family_closer_family"));
                data.putInt("impact_wider_self_miss_career", npcCollection.get("impact_wider_self_miss_career"));
                data.putInt("impact_wider_family_miss_career", npcCollection.get("impact_wider_family_miss_career"));
                data.putInt("impact_wider_emergency", npcCollection.get("impact_wider_emergency"));
                data.putInt("impact_wider_appointment", npcCollection.get("impact_wider_appointment"));
                data.putBoolean("first_time", isFirstTime);

                Fragment followupEditFamilyFragment;

                FragmentManager fm = getActivity().getSupportFragmentManager();
                FragmentTransaction transaction = fm.beginTransaction();
//

                followupEditFamilyFragment= new FollowupEditFamilyFragment();


                followupEditFamilyFragment.setArguments(data);
//                ft.replace(R.id.main_view, followupFamilyFragment);
//                ft.addToBackStack(null);
//                ft.commit();
                Log.d("back", fm.getBackStackEntryCount() + "");

                transaction.add(R.id.main_view, followupEditFamilyFragment,
                        "FragmentEditFamily"); //now you have an instance of newFragment1//now you have an instance of newFragment2
                transaction.addToBackStack("FragmentEdit");
                transaction.show(followupEditFamilyFragment);
                transaction.setTransition(FragmentTransaction.TRANSIT_FRAGMENT_OPEN);
                transaction.commit();
                isFirstTime = false;

            }
        });
    }

    private void NpcInflater(LayoutInflater vi, final List<String> items, final String param, ViewGroup insertPoint, int index, final Map<String, Integer> answers, Map<String, String> questions) {
        View v = vi.inflate(R.layout.item_create_followup, null);
        TextView textQuestion = (TextView)v.findViewById(R.id.text_create_followup_question);

        textQuestion.setText(questions.get(param));
        MaterialSpinner spinner = (MaterialSpinner) v.findViewById(R.id.spinner);
        spinner.setItems(items);

        spinner.setSelectedIndex(answers.get(param) + 1);
        spinner.setTextColor(getResources().getColor(R.color.colorPrimaryText));

        spinner.setOnItemSelectedListener(new MaterialSpinner.OnItemSelectedListener<String>() {

            @Override
            public void onItemSelected(MaterialSpinner view, int position, long id, String item) {
                if (position != 0) {
                    answers.put(param, position - 1);
                }
//                Context context = view.getContext();
//                CharSequence text = answers.get(param) + " " + param;
//                int duration = Toast.LENGTH_SHORT;
//
//                Toast toast = Toast.makeText(context, text, duration);
//                toast.show();
            }
        });
        spinner.onSaveInstanceState();

        insertPoint.addView(
                v,
                index,
                new ViewGroup.LayoutParams(ViewGroup.LayoutParams.MATCH_PARENT, ViewGroup.LayoutParams.WRAP_CONTENT));
    }

    private void EditApplicantNpbFollowup(NpbFollowUp feed) {
        final APIReceivable receivable = this;
        final INPDRAPI api = new INPDRAPI(getContext());
        int index = 0;

        TextView textNpcCreate = (TextView)getView().findViewById(R.id.text_edit_followup_title);
        textNpcCreate.setText("Edit NP-B Followup Questionnaire");

        ViewGroup insertPoint = (ViewGroup) getView().findViewById(R.id.lty_edit_followup);
        LayoutInflater vi = (LayoutInflater) getActivity().getSystemService(Context.LAYOUT_INFLATER_SERVICE);


        List<String> answers_bone_pain = new ArrayList<>();
        answers_bone_pain.add(" ");
        for(int i = 0; i < NpbApplicantFollowupCollection.getSymptom_bone_collection().size(); i++){
            answers_bone_pain.add(NpbApplicantFollowupCollection.getSymptom_bone_collection().get(i));
        }

        NpbInflater(vi, answers_bone_pain, "symptom_bone", insertPoint, index, npbApplicantCollection, BasicInfo.getNpb_applicant_participant_followups_questions());
        index ++;

        List<String> answers_symptom_abdominal = new ArrayList<>();
        answers_symptom_abdominal.add(" ");
        for(int i = 0; i < NpbApplicantFollowupCollection.getSymptom_abdominal_collection().size(); i++){
            answers_symptom_abdominal.add(NpbApplicantFollowupCollection.getSymptom_abdominal_collection().get(i));
        }
        NpbInflater(vi, answers_symptom_abdominal, "symptom_abdominal", insertPoint, index, npbApplicantCollection, BasicInfo.getNpb_applicant_participant_followups_questions());
        index++;

        List<String> answers_symptom_pain = new ArrayList<>();
        answers_symptom_pain.add(" ");
        for(int i = 0; i < NpbApplicantFollowupCollection.getSymptom_pain_collection().size(); i++){
            answers_symptom_pain.add(NpbApplicantFollowupCollection.getSymptom_pain_collection().get(i));
        }
        NpbInflater(vi, answers_symptom_pain, "symptom_pain", insertPoint, index, npbApplicantCollection, BasicInfo.getNpb_applicant_participant_followups_questions());
        index++;

        List<String> answers_symptom_breathlessness_rate = new ArrayList<>();
        answers_symptom_breathlessness_rate.add(" ");
        for(int i = 0; i < NpbApplicantFollowupCollection.getSymptom_breathlessness_rate_collection().size(); i++){
            answers_symptom_breathlessness_rate.add(NpbApplicantFollowupCollection.getSymptom_breathlessness_rate_collection().get(i));
        }
        NpbInflater(vi, answers_symptom_breathlessness_rate, "symptom_breathlessness_rate", insertPoint, index, npbApplicantCollection, BasicInfo.getNpb_applicant_participant_followups_questions());
        index++;

        List<String> answers_symptom_breathlessness_impact = new ArrayList<>();
        answers_symptom_breathlessness_impact.add(" ");
        for(int i = 0; i < NpbApplicantFollowupCollection.getSymptom_breathlessness_impact_collection().size(); i++){
            answers_symptom_breathlessness_impact.add(NpbApplicantFollowupCollection.getSymptom_breathlessness_impact_collection().get(i));
        }
        NpbInflater(vi, answers_symptom_breathlessness_impact, "symptom_breathlessness_impact", insertPoint, index, npbApplicantCollection, BasicInfo.getNpb_applicant_participant_followups_questions());
        index++;

        List<String> answers_symptom_bleeding_rate = new ArrayList<>();
        answers_symptom_bleeding_rate.add(" ");
        for(int i = 0; i < NpbApplicantFollowupCollection.getSymptom_bleeding_rate_collection().size(); i++){
            answers_symptom_bleeding_rate.add(NpbApplicantFollowupCollection.getSymptom_bleeding_rate_collection().get(i));
        }
        NpbInflater(vi, answers_symptom_bleeding_rate, "symptom_bleeding_rate", insertPoint, index, npbApplicantCollection, BasicInfo.getNpb_applicant_participant_followups_questions());
        index++;

        List<String> answers_symptom_bleeding_impact = new ArrayList<>();
        answers_symptom_bleeding_impact.add(" ");
        for(int i = 0; i < NpbApplicantFollowupCollection.getSymptom_bleeding_impact_collection().size(); i++){
            answers_symptom_bleeding_impact.add(NpbApplicantFollowupCollection.getSymptom_bleeding_impact_collection().get(i));
        }
        NpbInflater(vi, answers_symptom_bleeding_impact, "symptom_bleeding_impact", insertPoint, index, npbApplicantCollection, BasicInfo.getNpb_applicant_participant_followups_questions());
        index++;

        List<String> answers_symptom_fatigue_rate = new ArrayList<>();
        answers_symptom_fatigue_rate.add(" ");
        for(int i = 0; i < NpbApplicantFollowupCollection.getSymptom_fatigue_rate_collection().size(); i++){
            answers_symptom_fatigue_rate.add(NpbApplicantFollowupCollection.getSymptom_fatigue_rate_collection().get(i));
        }
        NpbInflater(vi, answers_symptom_fatigue_rate, "symptom_fatigue_rate", insertPoint, index, npbApplicantCollection, BasicInfo.getNpb_applicant_participant_followups_questions());
        index++;

        List<String> answers_symptom_fatigue_impact = new ArrayList<>();
        answers_symptom_fatigue_impact.add(" ");
        for(int i = 0; i < NpbApplicantFollowupCollection.getSymptom_fatigue_impact_collection().size(); i++){
            answers_symptom_fatigue_impact.add(NpbApplicantFollowupCollection.getSymptom_fatigue_impact_collection().get(i));
        }
        NpbInflater(vi, answers_symptom_fatigue_impact, "symptom_fatigue_impact", insertPoint, index, npbApplicantCollection, BasicInfo.getNpb_applicant_participant_followups_questions());
        index++;

        List<String> answers_symptom_enlarge_organ = new ArrayList<>();
        answers_symptom_enlarge_organ.add(" ");
        for(int i = 0; i < NpbApplicantFollowupCollection.getSymptom_enlarge_organ_collection().size(); i++){
            answers_symptom_enlarge_organ.add(NpbApplicantFollowupCollection.getSymptom_enlarge_organ_collection().get(i));
        }
        NpbInflater(vi, answers_symptom_enlarge_organ, "symptom_enlarge_organ", insertPoint, index, npbApplicantCollection, BasicInfo.getNpb_applicant_participant_followups_questions());
        index++;

        List<String> answers_symptom_enlarge_organ_impact = new ArrayList<>();
        answers_symptom_enlarge_organ_impact.add(" ");
        for(int i = 0; i < NpbApplicantFollowupCollection.getSymptom_enlarge_organ_impact_collection().size(); i++){
            answers_symptom_enlarge_organ_impact.add(NpbApplicantFollowupCollection.getSymptom_enlarge_organ_impact_collection().get(i));
        }
        NpbInflater(vi, answers_symptom_enlarge_organ_impact, "symptom_enlarge_organ_impact", insertPoint, index, npbApplicantCollection, BasicInfo.getNpb_applicant_participant_followups_questions());
        index++;

        List<String> answers_symptom_slow_growth = new ArrayList<>();
        answers_symptom_slow_growth.add(" ");
        for(int i = 0; i < NpbApplicantFollowupCollection.getSymptom_slow_growth_collection().size(); i++){
            answers_symptom_slow_growth.add(NpbApplicantFollowupCollection.getSymptom_slow_growth_collection().get(i));
        }
        NpbInflater(vi, answers_symptom_slow_growth, "symptom_slow_growth", insertPoint, index, npbApplicantCollection, BasicInfo.getNpb_applicant_participant_followups_questions());
        index++;

        List<String> answers_symptom_slow_growth_impact = new ArrayList<>();
        answers_symptom_slow_growth_impact.add(" ");
        for(int i = 0; i < NpbApplicantFollowupCollection.getSymptom_slow_growth_impact_collection().size(); i++){
            answers_symptom_slow_growth_impact.add(NpbApplicantFollowupCollection.getSymptom_slow_growth_impact_collection().get(i));
        }
        NpbInflater(vi, answers_symptom_slow_growth_impact, "symptom_slow_growth_impact", insertPoint, index, npbApplicantCollection, BasicInfo.getNpb_applicant_participant_followups_questions());
        index++;

        List<String> answers_symptom_infection = new ArrayList<>();
        answers_symptom_infection.add(" ");
        for(int i = 0; i < NpbApplicantFollowupCollection.getSymptom_infection_collection().size(); i++){
            answers_symptom_infection.add(NpbApplicantFollowupCollection.getSymptom_infection_collection().get(i));
        }
        NpbInflater(vi, answers_symptom_infection, "symptom_infection", insertPoint, index, npbApplicantCollection, BasicInfo.getNpb_applicant_participant_followups_questions());
        index++;

        List<String> answers_symptom_fracture = new ArrayList<>();
        answers_symptom_fracture.add(" ");
        for(int i = 0; i < NpbApplicantFollowupCollection.getSymptom_fracture_collection().size(); i++){
            answers_symptom_fracture.add(NpbApplicantFollowupCollection.getSymptom_fracture_collection().get(i));
        }
        NpbInflater(vi, answers_symptom_fracture, "symptom_fracture", insertPoint, index, npbApplicantCollection, BasicInfo.getNpb_applicant_participant_followups_questions());
        index++;

        List<String> answers_symptom_cognition = new ArrayList<>();
        answers_symptom_cognition.add(" ");
        for(int i = 0; i < NpbApplicantFollowupCollection.getSymptom_cognition_collection().size(); i++) {
            answers_symptom_cognition.add(NpbFollowupCollection.getSymptom_cognition_collection().get(i));
        }
        NpbInflater(vi, answers_symptom_cognition, "symptom_cognition", insertPoint, index, npbApplicantCollection, BasicInfo.getNpb_applicant_participant_followups_questions());
        index++;

        Button buttonNext = (Button) getView().findViewById(R.id.button_followup_next);
        buttonNext.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Toast.makeText(getView().getContext(), "next!", Toast.LENGTH_SHORT).show();

                Bundle data = new Bundle();
                data.putInt("followable_id", followable_id);
                data.putString("followable_type", followable_type);
                data.putInt("id", id);
                data.putInt("symptom_bone", npbApplicantCollection.get("symptom_bone"));
                data.putInt("symptom_abdominal", npbApplicantCollection.get("symptom_abdominal"));
                data.putInt("symptom_pain", npbApplicantCollection.get("symptom_pain"));
                data.putInt("symptom_cognition", npbApplicantCollection.get("symptom_cognition"));
                data.putInt("symptom_breathlessness_rate", npbApplicantCollection.get("symptom_breathlessness_rate"));
                data.putInt("symptom_breathlessness_impact", npbApplicantCollection.get("symptom_breathlessness_impact"));
                data.putInt("symptom_bleeding_rate", npbApplicantCollection.get("symptom_bleeding_rate"));
                data.putInt("symptom_bleeding_impact", npbApplicantCollection.get("symptom_bleeding_impact"));
                data.putInt("symptom_fatigue_rate", npbApplicantCollection.get("symptom_fatigue_rate"));
                data.putInt("symptom_fatigue_impact", npbApplicantCollection.get("symptom_fatigue_impact"));
                data.putInt("symptom_enlarge_organ", npbApplicantCollection.get("symptom_enlarge_organ"));
                data.putInt("symptom_enlarge_organ_impact", npbApplicantCollection.get("symptom_enlarge_organ_impact"));
                data.putInt("symptom_slow_growth", npbApplicantCollection.get("symptom_slow_growth"));
                data.putInt("symptom_slow_growth_impact", npbApplicantCollection.get("symptom_slow_growth_impact"));
                data.putInt("symptom_infection", npbApplicantCollection.get("symptom_infection"));
                data.putInt("symptom_fracture", npbApplicantCollection.get("symptom_fracture"));
                data.putInt("impact_family_disappointed", npbApplicantCollection.get("impact_family_disappointed"));
                data.putInt("impact_family_give_up", npbApplicantCollection.get("impact_family_give_up"));
                data.putInt("impact_family_worry_future", npbApplicantCollection.get("impact_family_worry_future"));
                data.putInt("impact_family_closer", npbApplicantCollection.get("impact_family_closer"));
                data.putInt("impact_wider_self_miss_career", npbApplicantCollection.get("impact_wider_self_miss_career"));
                data.putInt("impact_wider_carer_miss_career", npbApplicantCollection.get("impact_wider_carer_miss_career"));
                data.putInt("impact_wider_emergency", npbApplicantCollection.get("impact_wider_emergency"));
                data.putInt("impact_wider_appointment", npbApplicantCollection.get("impact_wider_appointment"));
                data.putBoolean("first_time", isFirstTime);

                Fragment followupEditFramilyFragment;

                FragmentManager fm = getActivity().getSupportFragmentManager();
                FragmentTransaction transaction = fm.beginTransaction();
//

                followupEditFramilyFragment= new FollowupEditFamilyFragment();


                followupEditFramilyFragment.setArguments(data);
//                ft.replace(R.id.main_view, followupFamilyFragment);
//                ft.addToBackStack(null);
//                ft.commit();
                Log.d("back", fm.getBackStackEntryCount() + "");

                transaction.add(R.id.main_view, followupEditFramilyFragment,
                        "FragmentEditFamily"); //now you have an instance of newFragment1//now you have an instance of newFragment2
                transaction.addToBackStack("FragmentEdit");
                transaction.show(followupEditFramilyFragment);
                transaction.setTransition(FragmentTransaction.TRANSIT_FRAGMENT_OPEN);
                transaction.commit();
                isFirstTime = false;

            }
        });
    }

    private void EditNpbFollowup(NpbFollowUp feed) {
        int index = 0;
        TextView textNpcCreate = (TextView)getView().findViewById(R.id.text_edit_followup_title);
        textNpcCreate.setText("Edit NP-B Followup Questionnaire");

        ViewGroup insertPoint = (ViewGroup) getView().findViewById(R.id.lty_edit_followup);
        LayoutInflater vi = (LayoutInflater) getActivity().getSystemService(Context.LAYOUT_INFLATER_SERVICE);

        List<String> answers_bone_pain = new ArrayList<>();
        answers_bone_pain.add(" ");
        for(int i = 0; i < NpbFollowupCollection.getSymptom_bone_collection().size(); i++){
            answers_bone_pain.add(NpbFollowupCollection.getSymptom_bone_collection().get(i));
        }

        NpbInflater(vi, answers_bone_pain, "symptom_bone", insertPoint, index, npbCollection, BasicInfo.getNpb_followups_questions());
        index ++;

        List<String> answers_symptom_abdominal = new ArrayList<>();
        answers_symptom_abdominal.add(" ");
        for(int i = 0; i < NpbFollowupCollection.getSymptom_abdominal_collection().size(); i++){
            answers_symptom_abdominal.add(NpbFollowupCollection.getSymptom_abdominal_collection().get(i));
        }
        NpbInflater(vi, answers_symptom_abdominal, "symptom_abdominal", insertPoint, index, npbCollection, BasicInfo.getNpb_followups_questions());
        index++;

        List<String> answers_symptom_pain = new ArrayList<>();
        answers_symptom_pain.add(" ");
        for(int i = 0; i < NpbFollowupCollection.getSymptom_pain_collection().size(); i++){
            answers_symptom_pain.add(NpbFollowupCollection.getSymptom_pain_collection().get(i));
        }
        NpbInflater(vi, answers_symptom_pain, "symptom_pain", insertPoint, index, npbCollection, BasicInfo.getNpb_followups_questions());
        index++;

        List<String> answers_symptom_breathlessness_rate = new ArrayList<>();
        answers_symptom_breathlessness_rate.add(" ");
        for(int i = 0; i < NpbFollowupCollection.getSymptom_breathlessness_rate_collection().size(); i++){
            answers_symptom_breathlessness_rate.add(NpbFollowupCollection.getSymptom_breathlessness_rate_collection().get(i));
        }
        NpbInflater(vi, answers_symptom_breathlessness_rate, "symptom_breathlessness_rate", insertPoint, index, npbCollection, BasicInfo.getNpb_followups_questions());
        index++;

        List<String> answers_symptom_breathlessness_impact = new ArrayList<>();
        answers_symptom_breathlessness_impact.add(" ");
        for(int i = 0; i < NpbFollowupCollection.getSymptom_breathlessness_impact_collection().size(); i++){
            answers_symptom_breathlessness_impact.add(NpbFollowupCollection.getSymptom_breathlessness_impact_collection().get(i));
        }
        NpbInflater(vi, answers_symptom_breathlessness_impact, "symptom_breathlessness_impact", insertPoint, index, npbCollection, BasicInfo.getNpb_followups_questions());
        index++;

        List<String> answers_symptom_bleeding_rate = new ArrayList<>();
        answers_symptom_bleeding_rate.add(" ");
        for(int i = 0; i < NpbFollowupCollection.getSymptom_bleeding_rate_collection().size(); i++){
            answers_symptom_bleeding_rate.add(NpbFollowupCollection.getSymptom_bleeding_rate_collection().get(i));
        }
        NpbInflater(vi, answers_symptom_bleeding_rate, "symptom_bleeding_rate", insertPoint, index, npbCollection, BasicInfo.getNpb_followups_questions());
        index++;

        List<String> answers_symptom_bleeding_impact = new ArrayList<>();
        answers_symptom_bleeding_impact.add(" ");
        for(int i = 0; i < NpbFollowupCollection.getSymptom_bleeding_impact_collection().size(); i++){
            answers_symptom_bleeding_impact.add(NpbFollowupCollection.getSymptom_bleeding_impact_collection().get(i));
        }
        NpbInflater(vi, answers_symptom_bleeding_impact, "symptom_bleeding_impact", insertPoint, index, npbCollection, BasicInfo.getNpb_followups_questions());
        index++;

        List<String> answers_symptom_fatigue_rate = new ArrayList<>();
        answers_symptom_fatigue_rate.add(" ");
        for(int i = 0; i < NpbFollowupCollection.getSymptom_fatigue_rate_collection().size(); i++){
            answers_symptom_fatigue_rate.add(NpbFollowupCollection.getSymptom_fatigue_rate_collection().get(i));
        }
        NpbInflater(vi, answers_symptom_fatigue_rate, "symptom_fatigue_rate", insertPoint, index, npbCollection, BasicInfo.getNpb_followups_questions());
        index++;

        List<String> answers_symptom_fatigue_impact = new ArrayList<>();
        answers_symptom_fatigue_impact.add(" ");
        for(int i = 0; i < NpbFollowupCollection.getSymptom_fatigue_impact_collection().size(); i++){
            answers_symptom_fatigue_impact.add(NpbFollowupCollection.getSymptom_fatigue_impact_collection().get(i));
        }
        NpbInflater(vi, answers_symptom_fatigue_impact, "symptom_fatigue_impact", insertPoint, index, npbCollection, BasicInfo.getNpb_followups_questions());
        index++;

        List<String> answers_symptom_enlarge_organ = new ArrayList<>();
        answers_symptom_enlarge_organ.add(" ");
        for(int i = 0; i < NpbFollowupCollection.getSymptom_enlarge_organ_collection().size(); i++){
            answers_symptom_enlarge_organ.add(NpbFollowupCollection.getSymptom_enlarge_organ_collection().get(i));
        }
        NpbInflater(vi, answers_symptom_enlarge_organ, "symptom_enlarge_organ", insertPoint, index, npbCollection, BasicInfo.getNpb_followups_questions());
        index++;

        List<String> answers_symptom_enlarge_organ_impact = new ArrayList<>();
        answers_symptom_enlarge_organ_impact.add(" ");
        for(int i = 0; i < NpbFollowupCollection.getSymptom_enlarge_organ_impact_collection().size(); i++){
            answers_symptom_enlarge_organ_impact.add(NpbFollowupCollection.getSymptom_enlarge_organ_impact_collection().get(i));
        }
        NpbInflater(vi, answers_symptom_enlarge_organ_impact, "symptom_enlarge_organ_impact", insertPoint, index, npbCollection, BasicInfo.getNpb_followups_questions());
        index++;

        List<String> answers_symptom_slow_growth = new ArrayList<>();
        answers_symptom_slow_growth.add(" ");
        for(int i = 0; i < NpbFollowupCollection.getSymptom_slow_growth_collection().size(); i++){
            answers_symptom_slow_growth.add(NpbFollowupCollection.getSymptom_slow_growth_collection().get(i));
        }
        NpbInflater(vi, answers_symptom_slow_growth, "symptom_slow_growth", insertPoint, index, npbCollection, BasicInfo.getNpb_followups_questions());
        index++;

        List<String> answers_symptom_slow_growth_impact = new ArrayList<>();
        answers_symptom_slow_growth_impact.add(" ");
        for(int i = 0; i < NpbFollowupCollection.getSymptom_slow_growth_impact_collection().size(); i++){
            answers_symptom_slow_growth_impact.add(NpbFollowupCollection.getSymptom_slow_growth_impact_collection().get(i));
        }
        NpbInflater(vi, answers_symptom_slow_growth_impact, "symptom_slow_growth_impact", insertPoint, index, npbCollection, BasicInfo.getNpb_followups_questions());
        index++;

        List<String> answers_symptom_infection = new ArrayList<>();
        answers_symptom_infection.add(" ");
        for(int i = 0; i < NpbFollowupCollection.getSymptom_infection_collection().size(); i++){
            answers_symptom_infection.add(NpbFollowupCollection.getSymptom_infection_collection().get(i));
        }
        NpbInflater(vi, answers_symptom_infection, "symptom_infection", insertPoint, index, npbCollection, BasicInfo.getNpb_followups_questions());
        index++;

        List<String> answers_symptom_fracture = new ArrayList<>();
        answers_symptom_fracture.add(" ");
        for(int i = 0; i < NpbFollowupCollection.getSymptom_fracture_collection().size(); i++){
            answers_symptom_fracture.add(NpbFollowupCollection.getSymptom_fracture_collection().get(i));
        }
        NpbInflater(vi, answers_symptom_fracture, "symptom_fracture", insertPoint, index, npbCollection, BasicInfo.getNpb_followups_questions());
        index++;

        List<String> answers_symptom_cognition = new ArrayList<>();
        answers_symptom_cognition.add(" ");
        for(int i = 0; i < NpbFollowupCollection.getSymptom_cognition_collection().size(); i++){
            answers_symptom_cognition.add(NpbFollowupCollection.getSymptom_cognition_collection().get(i));
        }
        NpbInflater(vi, answers_symptom_cognition, "symptom_cognition", insertPoint, index, npbCollection, BasicInfo.getNpb_followups_questions());
        index++;

        Button buttonNext = (Button) getView().findViewById(R.id.button_followup_next);
        buttonNext.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Toast.makeText(getView().getContext(), "next!", Toast.LENGTH_SHORT).show();

                Bundle data = new Bundle();
                data.putInt("followable_id", followable_id);
                data.putString("followable_type", followable_type);
                data.putInt("id", id);
                data.putInt("symptom_bone", npbCollection.get("symptom_bone"));
                data.putInt("symptom_abdominal", npbCollection.get("symptom_abdominal"));
                data.putInt("symptom_pain", npbCollection.get("symptom_pain"));
                data.putInt("symptom_cognition", npbCollection.get("symptom_cognition"));
                data.putInt("symptom_breathlessness_rate", npbCollection.get("symptom_breathlessness_rate"));
                data.putInt("symptom_breathlessness_impact", npbCollection.get("symptom_breathlessness_impact"));
                data.putInt("symptom_bleeding_rate", npbCollection.get("symptom_bleeding_rate"));
                data.putInt("symptom_bleeding_impact", npbCollection.get("symptom_bleeding_impact"));
                data.putInt("symptom_fatigue_rate", npbCollection.get("symptom_fatigue_rate"));
                data.putInt("symptom_fatigue_impact", npbCollection.get("symptom_fatigue_impact"));
                data.putInt("symptom_enlarge_organ", npbCollection.get("symptom_enlarge_organ"));
                data.putInt("symptom_enlarge_organ_impact", npbCollection.get("symptom_enlarge_organ_impact"));
                data.putInt("symptom_slow_growth", npbCollection.get("symptom_slow_growth"));
                data.putInt("symptom_slow_growth_impact", npbCollection.get("symptom_slow_growth_impact"));
                data.putInt("symptom_infection", npbCollection.get("symptom_infection"));
                data.putInt("symptom_fracture", npbCollection.get("symptom_fracture"));
                data.putInt("impact_family_disappointed", npbCollection.get("impact_family_disappointed"));
                data.putInt("impact_family_give_up", npbCollection.get("impact_family_give_up"));
                data.putInt("impact_family_worry_future", npbCollection.get("impact_family_worry_future"));
                data.putInt("impact_family_closer", npbCollection.get("impact_family_closer"));
                data.putInt("impact_wider_self_miss_career", npbCollection.get("impact_wider_self_miss_career"));
                data.putInt("impact_wider_carer_miss_career", npbCollection.get("impact_wider_carer_miss_career"));
                data.putInt("impact_wider_emergency", npbCollection.get("impact_wider_emergency"));
                data.putInt("impact_wider_appointment", npbCollection.get("impact_wider_appointment"));
                data.putBoolean("first_time", isFirstTime);

                Fragment followupEditFramilyFragment;

                FragmentManager fm = getActivity().getSupportFragmentManager();
                FragmentTransaction transaction = fm.beginTransaction();
//

                followupEditFramilyFragment= new FollowupEditFamilyFragment();


                followupEditFramilyFragment.setArguments(data);
//                ft.replace(R.id.main_view, followupFamilyFragment);
//                ft.addToBackStack(null);
//                ft.commit();
                Log.d("back", fm.getBackStackEntryCount()+"");

                transaction.add(R.id.main_view, followupEditFramilyFragment,
                        "FragmentEditFamily"); //now you have an instance of newFragment1//now you have an instance of newFragment2
                transaction.addToBackStack("FragmentEdit");
                transaction.show(followupEditFramilyFragment);
                transaction.setTransition(FragmentTransaction.TRANSIT_FRAGMENT_OPEN);
                transaction.commit();
                isFirstTime = false;

            }
        });
    }

    private void NpbInflater(LayoutInflater vi, final List<String> items, final String param, ViewGroup insertPoint, int index, final Map<String, Integer> answers, Map<String, String> questions) {
        View v = vi.inflate(R.layout.item_create_followup, null);
        TextView textQuestion = (TextView)v.findViewById(R.id.text_create_followup_question);

        textQuestion.setText(questions.get(param));
        MaterialSpinner spinner = (MaterialSpinner) v.findViewById(R.id.spinner);
        spinner.setItems(items);

        spinner.setSelectedIndex(answers.get(param) + 1);

        spinner.setTextColor(getResources().getColor(R.color.colorPrimaryText));

        spinner.setOnItemSelectedListener(new MaterialSpinner.OnItemSelectedListener<String>() {

            @Override
            public void onItemSelected(MaterialSpinner view, int position, long id, String item) {
                if (position != 0) {
                    answers.put(param, position - 1);
                }
//                Context context = view.getContext();
//                CharSequence text = answers.get(param) + " " + param;
//                int duration = Toast.LENGTH_SHORT;
//
//                Toast toast = Toast.makeText(context, text, duration);
//                toast.show();
            }
        });
        spinner.onSaveInstanceState();

        insertPoint.addView(
                v,
                index,
                new ViewGroup.LayoutParams(ViewGroup.LayoutParams.MATCH_PARENT, ViewGroup.LayoutParams.WRAP_CONTENT));
    }


}
