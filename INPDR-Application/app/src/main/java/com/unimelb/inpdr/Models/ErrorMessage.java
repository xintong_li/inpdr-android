package com.unimelb.inpdr.Models;

import com.unimelb.inpdr.Models.API.APIItem;

/**
 * Created by Xintong on 17/05/2016.
 */
public class ErrorMessage extends APIItem{
    private int errorCode;
    private String errorMes;

    public int getErrorCode() {
        return errorCode;
    }

    public void setErrorCode(int errorCode) {
        this.errorCode = errorCode;
    }

    public String getErrorMes() {
        return errorMes;
    }

    public void setErrorMes(String errorMes) {
        this.errorMes = errorMes;
    }
}
