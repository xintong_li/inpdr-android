package com.unimelb.inpdr.Fragments;

import android.content.DialogInterface;
import android.content.Intent;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v7.app.AlertDialog;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ExpandableListView;

import com.unimelb.inpdr.LineColumnDependencyActivity;
import com.unimelb.inpdr.MainActivity;
import com.unimelb.inpdr.Models.API.APIItem;
import com.unimelb.inpdr.Models.API.APIReceivable;
import com.unimelb.inpdr.Models.API.INPDRAPI;
import com.unimelb.inpdr.Models.API.ModelFacade;
import com.unimelb.inpdr.Models.BasicInfo;
import com.unimelb.inpdr.Models.ErrorMessage;
import com.unimelb.inpdr.Models.NpbFollowUp;
import com.unimelb.inpdr.Models.NpbFollowupCollection;
import com.unimelb.inpdr.Models.NpcApplicantFollowupCollection;
import com.unimelb.inpdr.Models.NpcFollowUp;
import com.unimelb.inpdr.Models.NpcFollowupCollection;
import com.unimelb.inpdr.Models.Selection;
import com.unimelb.inpdr.R;
import com.unimelb.inpdr.Sources.ExpandableListAdapter;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import me.drakeet.materialdialog.MaterialDialog;

/**
 * Created by Xintong on 14/05/2016.
 */
public class FollowupDetailFragment extends Fragment implements APIReceivable {
    int followable_id;
    int id;
    String followable_type;
    private ArrayList<NpbFollowUp> npbFollowUps = new ArrayList<>();
    private ArrayList<NpcFollowUp> npcFollowUps = new ArrayList<>();

    ExpandableListAdapter listAdapter;
    ExpandableListView expListView;
    List<String> listDataHeader;
    HashMap<String, List<String>> listDataChild;

    public FollowupDetailFragment() {
        // Required empty public constructor
    }

    @Override
    public void onCreateOptionsMenu(
            Menu menu, MenuInflater inflater) {
        Log.d("action bar setting", "aaa");
        MenuItem item1 = menu.findItem(R.id.action_settings);
        item1.setVisible(true);

        MenuItem item2 = menu.findItem(R.id.action_chart);
        item2.setVisible(true);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        Log.d("item selected", "");
        switch (item.getItemId()) {
            case R.id.action_chart:
                Intent intent = new Intent(getContext(), LineColumnDependencyActivity.class);
                intent.putExtra("followable_type", followable_type);
                intent.putExtra("followable_id", followable_id);
                intent.putExtra("followup_id", id);
                startActivity(intent);
                // User chose the "Favorite" action, mark the current item
                return true;

//            case R.id.action_logout:
//                return true;
            default:
                Log.d("button pressed","");
                // If we got here, the user's action was not recognized.
                // Invoke the superclass to handle it.
                return super.onOptionsItemSelected(item);

        }
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setHasOptionsMenu(true);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        return inflater.inflate(R.layout.fragment_followup_detail, container, false);
    }

    @Override
    public void onStart(){
        super.onStart();
        ((MainActivity)getActivity()).setActionBarTitle("Followup");
    }

    @Override
    public void onViewCreated(View view, Bundle savedInstanceState) {
        final INPDRAPI api = new INPDRAPI(getContext());

        BasicInfo.getInstance();
        NpbFollowupCollection.getInstance();
        NpcFollowupCollection.getInstance();
        NpcApplicantFollowupCollection.getInstance();
        Selection.getInstance();

        ((MainActivity)getActivity()).setActionBarTitle("Followup");

        Bundle data = getArguments();

        if (data != null) {
            try {
                followable_id = data.getInt("followable_id");
                id = data.getInt("followup_id");
                followable_type = data.getString("followable_type");

                switch (followable_type){
                    case "NpbParticipant":case "NpbApplicantParticipant":
                        ModelFacade.getInstance().getANpbFollowup(this, api, followable_type, followable_id, id);
                        break;
                    case "NpcParticipant":case "NpcApplicantParticipant":
                        ModelFacade.getInstance().getANpcFollowup(this, api, followable_type, followable_id, id);
                        break;
                }
            }catch (Exception e){
                e.printStackTrace();
            }
        }

        expListView = (ExpandableListView) view.findViewById(R.id.followup_expandableListView);
    }
    @Override
    public <U extends APIItem> void receiveData(List<U> items, String type) {
        switch (type){
            case "getANpbFollowup":
                npbFollowUps.clear();
                for (U item : items) {
                    NpbFollowUp feed = (NpbFollowUp) item;
                    displayNpbFollowups(feed);
                    npbFollowUps.add(feed);
                    //Log.d("!!!!!data received!!!", feed.getUser().getEmail() + "");
                }
                break;
            case "getANpcFollowup":
                npcFollowUps.clear();
                for (U item : items) {
                    NpcFollowUp feed = (NpcFollowUp) item;
                    displayNpcFollowups(feed);
                    npcFollowUps.add(feed);
                }
                break;
        }
    }

    @Override
    public <U extends APIItem> void receiveData(U item, String type) {
        switch (type){
            case "error":
                ErrorMessage error = (ErrorMessage) item;
                final MaterialDialog mMaterialDialog_e = new MaterialDialog(getContext());
                mMaterialDialog_e.setTitle("Error");
                ErrorMessage errorMessage = (ErrorMessage) item;
                mMaterialDialog_e.setMessage(errorMessage.getErrorMes());
                mMaterialDialog_e.setPositiveButton("Ok", new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        mMaterialDialog_e.dismiss();
                    }
                });
                mMaterialDialog_e.show();
        }

    }

    private void displayNpcFollowups(NpcFollowUp followUp) {
        listDataHeader = new ArrayList<String>();
        listDataChild = new HashMap<String, List<String>>();

        listDataHeader.add("Patient Information");
        listDataHeader.add("NP-C Impact Questions");
        listDataHeader.add("Impact on Family");
        listDataHeader.add("Wider Impact");

        Map<String, String> followup_questions = new HashMap<>();
        switch (followable_type){
            case "NpcParticipant":
                followup_questions = BasicInfo.getNpc_participant_followups_questions();
                break;
            case "NpcApplicantParticipant":
                followup_questions = BasicInfo.getNpc_applicant_participant_followups_questions();
                break;
        }

        List<String> personalInfo = new ArrayList<String>();
        List<String> npc_imact_questions = new ArrayList<String>();
        List<String> impacts_on_family = new ArrayList<String>();
        List<String> wilder_impacts = new ArrayList<String>();

        String followup_id = String.format("%s%25s", "Patient id:", followable_id);
        String followup_type = String.format("%s%25s", "Patient Type:", followable_type);
        personalInfo.add(followup_id);
        personalInfo.add(followup_type);


        switch (followable_type){
            case "NpcParticipant":
                String impact_ambulation = followup_questions.get("impact_ambulation");
                impact_ambulation += String.format("\n\n%37s", NpcFollowupCollection.getImpact_ambulation().get(followUp.getImpact_ambulation()));
                npc_imact_questions.add(impact_ambulation);

                String impact_manipulation = followup_questions.get("impact_manipulation");
                impact_manipulation += String.format("\n\n%37s", NpcFollowupCollection.getImpact_manipulation().get(followUp.getImpact_ambulation()));
                npc_imact_questions.add(impact_manipulation);

                String impact_speech = followup_questions.get("impact_speech");
                impact_speech += String.format("\n\n%37s", NpcFollowupCollection.getImpact_speech().get(followUp.getImpact_speech()));
                npc_imact_questions.add(impact_speech);

                String impact_swallowing = followup_questions.get("impact_swallowing");
                impact_swallowing += String.format("\n\n%37s", NpcFollowupCollection.getImpact_swallowing().get(followUp.getImpact_swallowing()));
                npc_imact_questions.add(impact_swallowing);

                String impact_eye_movement = followup_questions.get("impact_eye_movement");
                impact_eye_movement += String.format("\n\n%37s", NpcFollowupCollection.getImpact_eye_movement().get(followUp.getImpact_eye_movement()));
                npc_imact_questions.add(impact_eye_movement);

                String impact_seizure = followup_questions.get("impact_seizure");
                impact_seizure += String.format("\n\n%37s", NpcFollowupCollection.getImpact_seizure().get(followUp.getImpact_seizure()));
                npc_imact_questions.add(impact_seizure);

                String impact_cognitive_impaired = followup_questions.get("impact_cognitive_impaired");
                impact_cognitive_impaired += String.format("\n\n%37s", NpcFollowupCollection.getImpact_cognitive_impaired().get(followUp.getImpact_cognitive_impaired()));
                npc_imact_questions.add(impact_cognitive_impaired);

                String impact_wider_self_miss_career = followup_questions.get("impact_wider_self_miss_career");
                impact_wider_self_miss_career += String.format("\n\n%37s", NpcFollowupCollection.getImpact_wider_self_miss_career().get(followUp.getImpact_wider_self_miss_career()));
                wilder_impacts.add(impact_wider_self_miss_career);

                String impact_wider_family_miss_career = followup_questions.get("impact_wider_family_miss_career");
                impact_wider_family_miss_career += String.format("\n\n%37s", NpcFollowupCollection.getImpact_wider_family_miss_career().get(followUp.getImpact_wider_family_miss_career()));
                wilder_impacts.add(impact_wider_family_miss_career);

                String impact_wider_emergency = followup_questions.get("impact_wider_emergency");
                impact_wider_emergency += String.format("\n\n%37s", NpcFollowupCollection.getImpact_wider_emergency().get(followUp.getImpact_wider_emergency()));
                wilder_impacts.add(impact_wider_emergency);

                String impact_wider_appointment = followup_questions.get("impact_wider_appointment");
                impact_wider_appointment += String.format("\n\n%37s", NpcFollowupCollection.getImpact_wider_appointment().get(followUp.getImpact_wider_appointment()));
                wilder_impacts.add(impact_wider_appointment);

                break;
            case "NpcApplicantParticipant":
                impact_ambulation = followup_questions.get("impact_ambulation");
                int length = NpcApplicantFollowupCollection.getImpact_ambulation().get(followUp.getImpact_ambulation()).length();
                impact_ambulation += String.format("\n\n%37s", NpcApplicantFollowupCollection.getImpact_ambulation().get(followUp.getImpact_ambulation()));
                npc_imact_questions.add(impact_ambulation);

                impact_manipulation = followup_questions.get("impact_manipulation");
                impact_manipulation += String.format("\n\n%37s", NpcApplicantFollowupCollection.getImpact_manipulation().get(followUp.getImpact_ambulation()));
                npc_imact_questions.add(impact_manipulation);

                impact_speech = followup_questions.get("impact_speech");
                impact_speech +=String.format("\n\n%37s", NpcApplicantFollowupCollection.getImpact_speech().get(followUp.getImpact_speech()));
                npc_imact_questions.add(impact_speech);

                impact_swallowing = followup_questions.get("impact_swallowing");
                impact_swallowing += String.format("\n\n%37s", NpcApplicantFollowupCollection.getImpact_swallowing().get(followUp.getImpact_swallowing()));
                npc_imact_questions.add(impact_swallowing);

                impact_eye_movement = followup_questions.get("impact_eye_movement");
                impact_eye_movement += String.format("\n\n%37s", NpcApplicantFollowupCollection.getImpact_eye_movement().get(followUp.getImpact_eye_movement()));
                npc_imact_questions.add(impact_eye_movement);

                impact_seizure = followup_questions.get("impact_seizure");
                impact_seizure += String.format("\n\n%37s", NpcApplicantFollowupCollection.getImpact_seizure().get(followUp.getImpact_seizure()));
                npc_imact_questions.add(impact_seizure);

                impact_cognitive_impaired = followup_questions.get("impact_cognitive_impaired");
                impact_cognitive_impaired += String.format("\n\n%37s", NpcApplicantFollowupCollection.getImpact_cognitive_impaired().get(followUp.getImpact_cognitive_impaired()));
                npc_imact_questions.add(impact_cognitive_impaired);

                impact_wider_self_miss_career = followup_questions.get("impact_wider_self_miss_career");
                impact_wider_self_miss_career += String.format("\n\n%37s", NpcApplicantFollowupCollection.getImpact_wider_self_miss_career().get(followUp.getImpact_wider_self_miss_career()));
                wilder_impacts.add(impact_wider_self_miss_career);

                impact_wider_family_miss_career = followup_questions.get("impact_wider_family_miss_career");
                impact_wider_family_miss_career += String.format("\n\n%37s", NpcApplicantFollowupCollection.getImpact_wider_family_miss_career().get(followUp.getImpact_wider_family_miss_career()));
                wilder_impacts.add(impact_wider_family_miss_career);

                impact_wider_emergency = followup_questions.get("impact_wider_emergency");
                impact_wider_emergency += String.format("\n\n%37s", NpcApplicantFollowupCollection.getImpact_wider_emergency().get(followUp.getImpact_wider_emergency()));
                wilder_impacts.add(impact_wider_emergency);

                impact_wider_appointment = followup_questions.get("impact_wider_appointment");
                impact_wider_appointment += String.format("\n\n%37s", NpcApplicantFollowupCollection.getImpact_wider_appointment().get(followUp.getImpact_wider_appointment()));
                wilder_impacts.add(impact_wider_appointment);

                break;
        }

        String impact_family_disappointed = followup_questions.get("impact_family_disappointed");
        impact_family_disappointed += String.format("\n\n%37s", NpcFollowupCollection.getImpact_family_collection().get(followUp.getImpact_family_disappointed()));
        impacts_on_family.add(impact_family_disappointed);

        String impact_family_give_up = followup_questions.get("impact_family_give_up");
        impact_family_give_up += String.format("\n\n%37s", NpcFollowupCollection.getImpact_family_collection().get(followUp.getImpact_family_give_up()));
        impacts_on_family.add(impact_family_give_up);

        String impact_family_worry_future = followup_questions.get("impact_family_worry_future");
        impact_family_worry_future += String.format("\n\n%37s", NpcFollowupCollection.getImpact_family_collection().get(followUp.getImpact_family_worry_future()));
        impacts_on_family.add(impact_family_worry_future);

        String impact_family_closer_family = followup_questions.get("impact_family_closer_family");
        impact_family_closer_family += String.format("\n\n%37s", NpcFollowupCollection.getImpact_family_collection().get(followUp.getImpact_family_closer_family()));
        impacts_on_family.add(impact_family_closer_family);

        listDataChild.put(listDataHeader.get(0), personalInfo); // Header, Child data
        listDataChild.put(listDataHeader.get(1), npc_imact_questions);
        listDataChild.put(listDataHeader.get(2), impacts_on_family);
        listDataChild.put(listDataHeader.get(3), wilder_impacts);

        listAdapter = new ExpandableListAdapter(getContext(), listDataHeader, listDataChild);
        expListView.setAdapter(listAdapter);
        expListView.setGroupIndicator(null);
    }

    private void displayNpbFollowups(NpbFollowUp followUp) {
        listDataHeader = new ArrayList<String>();
        listDataChild = new HashMap<String, List<String>>();

        listDataHeader.add("Patient Information");
        listDataHeader.add("ASMD NP-B Impact Questions");
        listDataHeader.add("Impact on Family");
        listDataHeader.add("Wider Impact");

        Map<String, String> followup_questions = new HashMap<>();
        switch (followable_type){
            case "NpbParticipant":
                followup_questions = BasicInfo.getNpb_followups_questions();
                break;
            case "NpbApplicantParticipant":
                followup_questions = BasicInfo.getNpb_applicant_participant_followups_questions();
                break;
        }

        List<String> personalInfo = new ArrayList<String>();
        List<String> npb_imact_questions = new ArrayList<String>();
        List<String> impacts_on_family = new ArrayList<String>();
        List<String> wilder_impacts = new ArrayList<String>();

        String followup_id = String.format("%s%25s", "Patient id:", followUp.getFollowable_id());
        String followup_type = String.format("%s%25s", "Patient Type:", followUp.getFollowable_type());
        personalInfo.add(followup_id);
        personalInfo.add(followup_type);

        String symptom_bone = followup_questions.get("symptom_bone");
        symptom_bone += String.format("\n\n%37s", NpbFollowupCollection.getSymptom_bone_collection().get(followUp.getSymptom_bone()));
        npb_imact_questions.add(symptom_bone);

        String symptom_abdominal = followup_questions.get("symptom_abdominal");
        symptom_abdominal += String.format("\n\n%37s", NpbFollowupCollection.getSymptom_abdominal_collection().get(followUp.getSymptom_abdominal()));
        npb_imact_questions.add(symptom_abdominal);

        String symptom_pain = followup_questions.get("symptom_pain");
        symptom_pain += String.format("\n\n%37s", NpbFollowupCollection.getSymptom_pain_collection().get(followUp.getSymptom_pain()));
        npb_imact_questions.add(symptom_pain);

        String symptom_breathlessness_rate = followup_questions.get("symptom_breathlessness_rate");
        symptom_breathlessness_rate += String.format("\n\n%37s", NpbFollowupCollection.getSymptom_breathlessness_rate_collection().get(followUp.getSymptom_breathlessness_rate()));
        npb_imact_questions.add(symptom_breathlessness_rate);

        String symptom_breathlessness_impact = followup_questions.get("symptom_breathlessness_impact");
        symptom_breathlessness_impact += String.format("\n\n%37s", NpbFollowupCollection.getSymptom_breathlessness_impact_collection().get(followUp.getSymptom_breathlessness_impact()));
        npb_imact_questions.add(symptom_breathlessness_impact);

        String symptom_bleeding_rate = followup_questions.get("symptom_bleeding_rate");
        symptom_bleeding_rate += String.format("\n\n%37s", NpbFollowupCollection.getSymptom_bleeding_rate_collection().get(followUp.getSymptom_bleeding_rate()));
        npb_imact_questions.add(symptom_bleeding_rate);

        String symptom_bleeding_impact = followup_questions.get("symptom_bleeding_impact");
        symptom_bleeding_impact += String.format("\n\n%37s", NpbFollowupCollection.getSymptom_bleeding_impact_collection().get(followUp.getSymptom_bleeding_impact()));
        npb_imact_questions.add(symptom_bleeding_impact);

        String symptom_fatigue_rate = followup_questions.get("symptom_fatigue_rate");
        symptom_fatigue_rate += String.format("\n\n%37s", NpbFollowupCollection.getSymptom_fatigue_rate_collection().get(followUp.getSymptom_fatigue_rate()));
        npb_imact_questions.add(symptom_fatigue_rate);

        String symptom_fatigue_impact = followup_questions.get("symptom_fatigue_impact");
        symptom_fatigue_impact += String.format("\n\n%37s", NpbFollowupCollection.getSymptom_fatigue_impact_collection().get(followUp.getSymptom_fatigue_impact()));
        npb_imact_questions.add(symptom_fatigue_impact);

        String symptom_enlarge_organ = followup_questions.get("symptom_enlarge_organ");
        symptom_enlarge_organ += String.format("\n\n%37s", NpbFollowupCollection.getSymptom_enlarge_organ_collection().get(followUp.getSymptom_enlarge_organ()));
        npb_imact_questions.add(symptom_enlarge_organ);

        String symptom_enlarge_organ_impact = followup_questions.get("symptom_enlarge_organ_impact");
        symptom_enlarge_organ_impact += String.format("\n\n%37s", NpbFollowupCollection.getSymptom_enlarge_organ_impact_collection().get(followUp.getSymptom_enlarge_organ_impact()));
        npb_imact_questions.add(symptom_enlarge_organ_impact);

        String symptom_slow_growth = followup_questions.get("symptom_slow_growth");
        symptom_slow_growth += String.format("\n\n%37s", NpbFollowupCollection.getSymptom_slow_growth_collection().get(followUp.getSymptom_slow_growth()));
        npb_imact_questions.add(symptom_slow_growth);

        String symptom_slow_growth_impact = followup_questions.get("symptom_slow_growth_impact");
        symptom_slow_growth_impact += String.format("\n\n%37s", NpbFollowupCollection.getSymptom_slow_growth_impact_collection().get(followUp.getSymptom_slow_growth_impact()));
        npb_imact_questions.add(symptom_slow_growth_impact);

        String symptom_infection = followup_questions.get("symptom_infection");
        symptom_infection += String.format("\n\n%37s", NpbFollowupCollection.getSymptom_infection_collection().get(followUp.getSymptom_infection()));
        npb_imact_questions.add(symptom_infection);

        String symptom_fracture = followup_questions.get("symptom_fracture");
        symptom_fracture += String.format("\n\n%37s", NpbFollowupCollection.getSymptom_fracture_collection().get(followUp.getSymptom_fracture()));
        npb_imact_questions.add(symptom_fracture);

        String symptom_cognition = followup_questions.get("symptom_cognition");
        symptom_cognition += String.format("\n\n%37s", NpbFollowupCollection.getSymptom_cognition_collection().get(followUp.getSymptom_cognition()));
        npb_imact_questions.add(symptom_cognition);

        String impact_family_disappointed = followup_questions.get("impact_family_disappointed");
        impact_family_disappointed += String.format("\n\n%37s", NpbFollowupCollection.getImpact_family_collection().get(followUp.getImpact_family_disappointed()));
        impacts_on_family.add(impact_family_disappointed);

        String impact_family_give_up = followup_questions.get("impact_family_give_up");
        impact_family_give_up += String.format("\n\n%37s", NpbFollowupCollection.getImpact_family_collection().get(followUp.getImpact_family_give_up()));
        impacts_on_family.add(impact_family_give_up);

        String impact_family_worry_future = followup_questions.get("impact_family_worry_future");
        impact_family_worry_future += String.format("\n\n%37s", NpbFollowupCollection.getImpact_family_collection().get(followUp.getImpact_family_worry_future()));
        impacts_on_family.add(impact_family_worry_future);

        String impact_family_closer = followup_questions.get("impact_family_closer");
        impact_family_closer += String.format("\n\n%37s", NpbFollowupCollection.getImpact_family_collection().get(followUp.getImpact_family_closer()));
        impacts_on_family.add(impact_family_closer);

        String impact_wider_self_miss_career = followup_questions.get("impact_wider_self_miss_career");
        impact_wider_self_miss_career += String.format("\n\n%37s", NpbFollowupCollection.getImpact_wider_self_miss_career().get(followUp.getImpact_wider_self_miss_career()));
        wilder_impacts.add(impact_wider_self_miss_career);

        String impact_wider_carer_miss_career = followup_questions.get("impact_wider_carer_miss_career");
        impact_wider_carer_miss_career += String.format("\n\n%37s", NpbFollowupCollection.getImpact_wider_carer_miss_career().get(followUp.getImpact_wider_carer_miss_career()));
        wilder_impacts.add(impact_wider_carer_miss_career);

        String impact_wider_emergency = followup_questions.get("impact_wider_emergency");
        impact_wider_emergency += String.format("\n\n%37s", NpbFollowupCollection.getImpact_wider_emergency().get(followUp.getImpact_wider_emergency()));
        wilder_impacts.add(impact_wider_emergency);

        String impact_wider_appointment = followup_questions.get("impact_wider_appointment");
        impact_wider_appointment += String.format("\n\n%37s", NpbFollowupCollection.getImpact_wider_appointment().get(followUp.getImpact_wider_appointment()));
        wilder_impacts.add(impact_wider_appointment);

        listDataChild.put(listDataHeader.get(0), personalInfo); // Header, Child data
        listDataChild.put(listDataHeader.get(1), npb_imact_questions);
        listDataChild.put(listDataHeader.get(2), impacts_on_family);
        listDataChild.put(listDataHeader.get(3), wilder_impacts);

        listAdapter = new ExpandableListAdapter(getContext(), listDataHeader, listDataChild);
        expListView.setAdapter(listAdapter);
        expListView.setGroupIndicator(null);
    }
}
