package com.unimelb.inpdr.Models;

import java.util.HashMap;
import java.util.Map;

/**
 * Created by Xintong on 2/05/2016.
 */
public final class BasicInfo {
    static Map genders = new HashMap<Integer, String>();
    private static Map applicant_relation = new HashMap<Integer, String>();
    private static Map<String, String> npb_participant_questions = new HashMap<>();
    private static Map<String, String> npc_participant_questions = new HashMap<>();
    private static Map<String, String> npa_applicant_participant_questions = new HashMap<>();
    private static Map<String, String> npb_applicant_participant_questions = new HashMap<>();
    private static Map<String, String> npc_applicant_participant_questions = new HashMap<>();
    private static Map<String, String> npb_participant_followups_questions = new HashMap<>();
    private static Map<String, String> npb_applicant_participant_followups_questions = new HashMap<>();
    private static Map<String, String> npc_participant_followups_questions = new HashMap<>();
    private static Map<String, String> npc_applicant_participant_followups_questions = new HashMap<>();


    private BasicInfo(){
        genders.put(0, "Male");
        genders.put(1, "Female");

        getApplicant_relation().put(0, "Self");
        getApplicant_relation().put(1, "Mother");
        getApplicant_relation().put(2, "Father");
        getApplicant_relation().put(3, "Guardian");
        getApplicant_relation().put(4, "Husband");
        getApplicant_relation().put(5, "Wife");
        getApplicant_relation().put(6, "Partner");
        getApplicant_relation().put(7, "Brother");
        getApplicant_relation().put(8, "Sister");
        getApplicant_relation().put(9, "Son");
        getApplicant_relation().put(10, "Daughter");
        getApplicant_relation().put(11, "Carer");
        getApplicant_relation().put(12, "Other");

        getNpb_participant_questions().put("symptom_age_known", "How old were they when symptoms of ASMD were first observed");
        getNpb_participant_questions().put("symptom_title", "Please tell us about these symptoms");
        getNpb_participant_questions().put("age_diagnosed_known", "How old were they when a diagnosis of ASMD NP-B was confirmed by a doctor");
        getNpb_participant_questions().put("other_family_diagnosed", "Other family members diagnosed with ASMD NP-B");
        getNpb_participant_questions().put("family_diagnosed", "Is there currently or has there been anyone else in the family diagnosed with ASMD NP-B");
        getNpb_participant_questions().put("family_diagnosed_relation", "If yes, please tell us their relationship to the person you are registering");
        getNpb_participant_questions().put("gene_report_explanation_html", "When this person was diagnosed with NP-B, you may have received information regarding their genetic diagnosis. This tells you what mutations were found in their DNA. This may have come from a diagnostic centre or from a doctor.\n" +
                "                                           Knowing the precise details of an individual's genetic mutation will add to our understanding of the condition and is likely to be important for developing treatments.  ");
        getNpb_participant_questions().put("gene_diagnosis", "If you have a copy of the genetic diagnosis yourself ");
        getNpb_participant_questions().put("dont_have_gene_report", "If you don’t have the genetic report yourself, please provide details of the hospital or diagnostic centre where the genetic test was performed.");
        getNpb_participant_questions().put("hospital_name", "Name of hospital or diagnostic centre");
        getNpb_participant_questions().put("unable_to_contact_specialist", "If you are unable to contact your specialist");
        getNpb_participant_questions().put("gene_test_performed", "If a genetic test has not been performed");
        getNpb_participant_questions().put("gene_test_performed","If a genetic test has not been performed, please tick this box");

        getNpc_participant_questions().put("baby_symptoms", "Did you have prolonged jaundice, liver problems, or an enlarged liver and/or spleen as a baby?");
        getNpc_participant_questions().put("symptom_age_known", "How old were you when other symptoms of NP-C were first observed (this may be before or after NP-C was diagnosed)?");
        getNpc_participant_questions().put("symptom_title", "Please tell us which of the following symptoms you FIRST noticed?");
        getNpc_participant_questions().put("symptom_delayed_milestone", "Delayed milestone development:");
        getNpc_participant_questions().put("symptom_ppers", "Problems at school, not keeping up with peers:");
        getNpc_participant_questions().put("symptom_coordication", "Problems with coordination and movement:");
        getNpc_participant_questions().put("symptom_eye", "Problems with eye movements:");
        getNpc_participant_questions().put("symptom_behavioural", "Behavioural problems:");
        getNpc_participant_questions().put("symptom_seizure", "Seizures:");
        getNpc_participant_questions().put("symptom_psychiatric", "Psychiatric issues:");
        getNpc_participant_questions().put("symptom_other", "Other/further information:");
        getNpc_participant_questions().put("symptom_other_specify", "Please specify:");
        getNpc_participant_questions().put("age_diagnosed_known", "How old were you when a diagnosis of NP-C was confirmed by a doctor? ");
        getNpc_participant_questions().put("other_family_diagnosed", "Other family members diagnosed with NP-C");
        getNpc_participant_questions().put("family_diagnosed", "Is there currently or has there been anyone else in the family diagnosed with NP-C?");
        getNpc_participant_questions().put("family_diagnosed_relation", "If yes, please tell us their relationship to you");
        getNpc_participant_questions().put("gene_report_explanation_html", "When you were diagnosed with NP-C, you may have received information regarding your genetic diagnosis. This tells you what mutations were found in your DNA. This may have come from a diagnostic centre or from a doctor.\n" +
                "                                      Knowing the precise details of an individual's genetic mutation will add to our understanding of the condition and is likely to be important for developing treatments.");

        getNpc_participant_questions().put("gene_diagnosis", "If you have a copy of the genetic diagnosis yourself ");
        getNpc_participant_questions().put("dont_have_gene_report", "If you do not have the genetic report yourself, you may be able to obtain a copy by speaking to your specialist. After your specialist has provided this to you, please come back this form and upload the genetic report.");
        getNpc_participant_questions().put("unable_to_contact_specialist", "If you are unable to contact your specialist");
        getNpc_participant_questions().put("gene_test_performed","If a genetic test has not been performed, please tick this box");


        getNpa_applicant_participant_questions().put("applicant_relation", "Please tell us your relationship to the person you are registering:");
        getNpa_applicant_participant_questions().put("applicant_relation_specify", "Please specify:");
        getNpa_applicant_participant_questions().put("symptom_age_known", "How old were they when symptoms of ASMD NP-A were first observed?");
        getNpa_applicant_participant_questions().put("age_diagnosed_known", "How old were they when a diagnosis of ASMD NP-A was confirmed by a doctor");
        getNpa_applicant_participant_questions().put("other_family_diagnosed", "Other family members diagnosed with ASMD NP-A");
        getNpa_applicant_participant_questions().put("family_diagnosed", "Is there currently or has there been anyone else in the family diagnosed with ASMD NP-A");
        getNpa_applicant_participant_questions().put("family_diagnosed_relation", "If yes, please tell us their relationship to the person you are registering");
        getNpa_applicant_participant_questions().put("gene_report_explanation_html", "When this person was diagnosed with NP-A, you may have received information regarding their genetic diagnosis. This tells you what mutations were found in their DNA. This may have come from a diagnostic centre or from a doctor.\n" +
                "                                           Knowing the precise details of an individual's genetic mutation will add to our understanding of the condition and is likely to be important for developing treatments.");
        getNpa_applicant_participant_questions().put("gene_diagnosis", "If you have a copy of the genetic diagnosis yourself?");
        getNpa_applicant_participant_questions().put("dont_have_gene_report", "If you do not have the genetic report yourself, you may be able to obtain a copy by speaking to your specialist. After your specialist has provided this to you, please come back this form and upload the genetic report.");
        getNpa_applicant_participant_questions().put("unable_to_contact_specialist", "If you are unable to contact your specialist");
        getNpa_applicant_participant_questions().put("gene_test_performed","If a genetic test has not been performed, please tick this box");

        getNpb_applicant_participant_questions().put("applicant_relation", "Please tell us your relationship to the person you are registering:");
        getNpb_applicant_participant_questions().put("applicant_relation_specify", "Please specify:");
        getNpb_applicant_participant_questions().put("symptom_age_known","How old were they when symptoms of ASMD were first observed?");
        getNpb_applicant_participant_questions().put("symptom_title","Please tell us about these symptoms.");
        getNpb_applicant_participant_questions().put("symptom_enlarged_liver","Enlarged liver:");
        getNpb_applicant_participant_questions().put("symptom_breathing","Breathing or lung problems:");
        getNpb_applicant_participant_questions().put("symptom_anaemia","Anaemia:");
        getNpb_applicant_participant_questions().put("symptom_bleeding","Problems with bleeding:");
        getNpb_applicant_participant_questions().put("symptom_growth_delay","Growth delay:");
        getNpb_applicant_participant_questions().put("symptom_developmental_delay","Developmental delay:");
        getNpb_applicant_participant_questions().put("symptom_bone_pain","Bone pain:");
        getNpb_applicant_participant_questions().put("symptom_abdominal_pain","Abdominal pain:");
        getNpb_applicant_participant_questions().put("symptom_other","Other/further information:");
        getNpb_applicant_participant_questions().put("symptom_other_specify","Please specify:");
        getNpb_applicant_participant_questions().put("age_diagnosed_known","How old were they when a diagnosis of ASMD NP-B was confirmed by a doctor?");
        getNpb_applicant_participant_questions().put("other_family_diagnosed","Other family members diagnosed with ASMD NP-B");
        getNpb_applicant_participant_questions().put("family_diagnosed","Is there currently or has there been anyone else in the family diagnosed with ASMD NP-B");
        getNpb_applicant_participant_questions().put("family_diagnosed_relation","If yes, please tell us their relationship to the person you are registering");
        getNpb_applicant_participant_questions().put("gene_report_explanation_html","When this person was diagnosed with NP-B, you may have received information regarding their genetic diagnosis. This tells you what mutations were found in their DNA. This may have come from a diagnostic centre or from a doctor.\n" +
                "                                           Knowing the precise details of an individual's genetic mutation will add to our understanding of the condition and is likely to be important for developing treatments.  ");
        getNpb_applicant_participant_questions().put("gene_diagnosis","If you have a copy of the genetic diagnosis yourself ");
        getNpb_applicant_participant_questions().put("dont_have_gene_report","If you do not have the genetic report yourself, you may be able to obtain a copy by speaking to your specialist. After your specialist has provided this to you, please come back this form and upload the genetic report.");
        getNpb_applicant_participant_questions().put("unable_to_contact_specialist","If you are unable to contact your specialist");
        getNpb_applicant_participant_questions().put("gene_test_performed","If a genetic test has not been performed, please tick this box");

        getNpc_applicant_participant_questions().put("applicant_relation", "Please tell us your relationship to the person you are registering:");
        getNpc_applicant_participant_questions().put("applicant_relation_specify", "Please specify:");
        getNpc_applicant_participant_questions().put("baby_symptoms","Did he/she have prolonged jaundice, liver problems, or an enlarged liver and/or spleen as a baby?");
        getNpc_applicant_participant_questions().put("symptom_age_known","Other than the problems mentioned above, how old was he/she when other symptoms of NP-C were first observed?");
        getNpc_applicant_participant_questions().put("symptom_title","please tell us which of the following symptoms you FIRST noticed.");
        getNpc_applicant_participant_questions().put("symptom_delayed_milestone", "Delayed milestone development:");
        getNpc_applicant_participant_questions().put("symptom_ppers", "Problems at school, not keeping up with peers:");
        getNpc_applicant_participant_questions().put("symptom_coordication", "Problems with coordination and movement:");
        getNpc_applicant_participant_questions().put("symptom_eye", "Problems with eye movements:");
        getNpc_applicant_participant_questions().put("symptom_behavioural", "Behavioural problems:");
        getNpc_applicant_participant_questions().put("symptom_seizure", "Seizures:");
        getNpc_applicant_participant_questions().put("symptom_psychiatric", "Psychiatric issues:");
        getNpc_applicant_participant_questions().put("symptom_other", "Other/further information:");
        getNpc_applicant_participant_questions().put("symptom_other_specify", "Please specify:");
        getNpc_applicant_participant_questions().put("age_diagnosed_known","How old were they when a diagnosis of NP-C was confirmed by a doctor?");
        getNpc_applicant_participant_questions().put("other_family_diagnosed","Other family members diagnosed with NP-C");
        getNpc_applicant_participant_questions().put("family_diagnosed","Is there currently or has there been anyone else in the family diagnosed with NP-C");
        getNpc_applicant_participant_questions().put("family_diagnosed_relation","If yes, please tell us their relationship to the person you are registering");
        getNpc_applicant_participant_questions().put("gene_report_explanation_html","When this person was diagnosed with NP-C, you may have received information regarding their genetic diagnosis. This tells you what mutations were found in their DNA. This may have come from a diagnostic centre or from a doctor.\n" +
                "                                           Knowing the precise details of an individual's genetic mutation will add to our understanding of the condition and is likely to be important for developing treatments.  ");
        getNpc_applicant_participant_questions().put("gene_diagnosis", "If you have a copy of the genetic diagnosis yourself");


        getNpb_applicant_participant_followups_questions().put("symptom_bone", "How often does he / she experience bone pain");
        getNpb_applicant_participant_followups_questions().put("symptom_abdominal", "How often does he / she experience abdominal pain");
        getNpb_applicant_participant_followups_questions().put("symptom_pain", "Please rate the impact that Pain has on his / her day to day life");
        getNpb_applicant_participant_followups_questions().put("symptom_breathlessness_rate", "When does he / she typically experience shortness of breath");
        getNpb_applicant_participant_followups_questions().put("symptom_breathlessness_impact", "Please rate the impact that Breathlessness has on his / her day to day life");
        getNpb_applicant_participant_followups_questions().put("symptom_bleeding_rate", "How often does he / she experience nosebleeds, bleeding gums, or easy bruising");
        getNpb_applicant_participant_followups_questions().put("symptom_bleeding_impact", "Please rate the impact that Bleeding has on his / her day to day life");
        getNpb_applicant_participant_followups_questions().put("symptom_fatigue_rate", "In the past 7 days, how often did they have to limit their activity because they were tired");
        getNpb_applicant_participant_followups_questions().put("symptom_fatigue_impact", "Please rate the impact that Fatigue has on his / her day to day life?");
        getNpb_applicant_participant_followups_questions().put("symptom_enlarge_organ", "Are their organs (liver / spleen) enlarged");
        getNpb_applicant_participant_followups_questions().put("symptom_enlarge_organ_impact", "Please rate the impact that enlarged organs (liver / spleen) has on their day to day life");
        getNpb_applicant_participant_followups_questions().put("symptom_slow_growth", "Do they experience, or have they in the past experienced, slow growth");
        getNpb_applicant_participant_followups_questions().put("symptom_slow_growth_impact", "Please rate the impact that Slow Growth has, or has had, on their day to day life");
        getNpb_applicant_participant_followups_questions().put("symptom_cognition", "Do you think their cognitive abilities are impacted? i.e. learning new skills, making decisions, following instructions, focusing their attention");
        getNpb_applicant_participant_followups_questions().put("impact_family_disappointed", "I don't have much time left over for other family members after caring for him/her");
        getNpb_applicant_participant_followups_questions().put("impact_family_give_up", "Our family gives up things because of their illness");
        getNpb_applicant_participant_followups_questions().put("symptom_fracture", "How many bone fractures has he / she had, in total?");
        getNpb_applicant_participant_followups_questions().put("symptom_infection", "In the last 6 months, how many infections has he / she had");
        getNpb_applicant_participant_followups_questions().put("impact_family_worry_future", "I worry about what will happen in the future");
        getNpb_applicant_participant_followups_questions().put("impact_family_closer", "Because of what we have shared we are a closer family");
        getNpb_applicant_participant_followups_questions().put("impact_wider_self_miss_career", "Over the past 12 months, did he/she miss any time from his/her job or his/her school due to their illness");
        getNpb_applicant_participant_followups_questions().put("impact_wider_carer_miss_career", "Over the past 12 months, did you miss any time from your job or your school (i.e. college or university) due to their illness");
        getNpb_applicant_participant_followups_questions().put("impact_wider_emergency", "How many times, during the past 12 months, did he/she visit the hospital in an emergency because of issues relating to their illness");
        getNpb_applicant_participant_followups_questions().put("impact_wider_appointment", "During the past 12 months, how many appointments has he/she had with a doctor (both specialist and family doctors) regarding their illness");

        getNpb_followups_questions().put("symptom_bone", "How often do you experience bone pain");
        getNpb_followups_questions().put("symptom_abdominal", "How often do you experience abdominal pain");
        getNpb_followups_questions().put("symptom_pain", "Please rate the impact that Pain has on your day to day life");
        getNpb_followups_questions().put("symptom_breathlessness_rate", "When do you typically experience shortness of breath");
        getNpb_followups_questions().put("symptom_breathlessness_impact", "Please rate the impact that Breathlessness has on your day to day life");
        getNpb_followups_questions().put("symptom_bleeding_rate", "How often do you experience nosebleeds, bleeding gums, or easy bruising");
        getNpb_followups_questions().put("symptom_bleeding_impact", "Please rate the impact that Bleeding has on your day to day life");
        getNpb_followups_questions().put("symptom_fatigue_rate", "In the past 7 days, how often did you have to limit their activity because you were tired");
        getNpb_followups_questions().put("symptom_fatigue_impact", "Please rate the impact that Fatigue has on your day to day life?");
        getNpb_followups_questions().put("symptom_enlarge_organ", "Are your organs (liver / spleen) enlarged");
        getNpb_followups_questions().put("symptom_enlarge_organ_impact", "Please rate the impact that enlarged organs (liver / spleen) has on your day to day life");
        getNpb_followups_questions().put("symptom_slow_growth", "Do you experience, or have you in the past experienced, slow growth");
        getNpb_followups_questions().put("symptom_slow_growth_impact", "Please rate the impact that Slow Growth has, or has had, on your day to day life");
        getNpb_followups_questions().put("symptom_cognition", "Do you think your cognitive abilities are impacted? i.e. learning new skills, making decisions, following instructions, focusing their attention");
        getNpb_followups_questions().put("impact_family_disappointed", "I am disappointed in my ability to keep up with family responsibilities");
        getNpb_followups_questions().put("impact_family_give_up", "Our family gives up things because of this illness");
        getNpb_followups_questions().put("symptom_fracture", "How many bone fractures have you had, in total?");
        getNpb_followups_questions().put("symptom_infection", "In the last 6 months, how many infections have you had");
        getNpb_followups_questions().put("impact_family_worry_future", "I worry about what will happen in the future");
        getNpb_followups_questions().put("impact_family_closer", "Because of what we have shared we are a closer family");
        getNpb_followups_questions().put("impact_wider_self_miss_career", "Over the past 12 months, did you miss any time from your job or your school due to your illness");
        getNpb_followups_questions().put("impact_wider_carer_miss_career", "Over the past 12 months, if you have a carer, did they miss any time from their job or their school (i.e. college or university) due to your illness");
        getNpb_followups_questions().put("impact_wider_emergency", "How many times, during the past 12 months, did you visit the hospital in an emergency because of issues relating to your illness");
        getNpb_followups_questions().put("impact_wider_appointment", "During the past 12 months, how many appointments have you had with a doctor (both specialist and family doctors) regarding your illness");

        getNpc_applicant_participant_followups_questions().put("impact_ambulation", "This section relates to the person's ability to walk about from place to place. Please select the option that best describes how they are generally");
        getNpc_applicant_participant_followups_questions().put("impact_manipulation", "This section relates to the person's ability to coordinate their movements. Please select the option that best describes how they are generally");
        getNpc_applicant_participant_followups_questions().put("impact_speech", "This section relates to the person's speech. Please select the option that best describes how they are generally");
        getNpc_applicant_participant_followups_questions().put("impact_swallowing", "This section relates to the person's ability to swallow. Please select the option that best describes how they are generally");
        getNpc_applicant_participant_followups_questions().put("impact_eye_movement", "This section relates to the person's eye movements. Please select the option that best describes how they are generally");
        getNpc_applicant_participant_followups_questions().put("impact_seizure", "This section relates to seizures. Please select the option that best describes how they are generally");
        getNpc_applicant_participant_followups_questions().put("impact_cognitive_impaired", "Do you think his/her cognitive abilities are impaired? i.e. learning new skills, remembering things, following instructions, focusing their attention");
        getNpc_applicant_participant_followups_questions().put("impact_family_disappointed", "Because of this illness, I don't have much time left over for other family members after caring for him/her");
        getNpc_applicant_participant_followups_questions().put("impact_family_give_up", "Our family gives up things because of their illness");
        getNpc_applicant_participant_followups_questions().put("impact_family_worry_future", "Because of their illness, I worry about what will happen in the future");
        getNpc_applicant_participant_followups_questions().put("impact_family_closer_family", "Because of what we have shared we are a closer family");
        getNpc_applicant_participant_followups_questions().put("impact_wider_self_miss_career", "Over the past 12 months, did he/she miss any time from his/her job or his/her school due to their illness");
        getNpc_applicant_participant_followups_questions().put("impact_wider_family_miss_career", "Over the past 12 months, did you miss any time from your job or your school (i.e. college or university) due to their illness");
        getNpc_applicant_participant_followups_questions().put("impact_wider_emergency", "How many times, during the past 12 months, did he/she visit the hospital in an emergency because of issues relating to their illness");
        getNpc_applicant_participant_followups_questions().put("impact_wider_appointment", "During the past 12 months, how many appointments has he/she had with a doctor (both specialist and family doctors) regarding their illness");

        getNpc_participant_followups_questions().put("impact_ambulation", "This section relates to your ability to walk about from place to place. Please select the option that best describes how you are generally");
        getNpc_participant_followups_questions().put("impact_manipulation", "This section relates to your ability to coordinate their movements. Please select the option that best describes how you are generally");
        getNpc_participant_followups_questions().put("impact_speech", "This section relates to your speech. Please select the option that best describes how you are generally");
        getNpc_participant_followups_questions().put("impact_swallowing", "This section relates to your ability to swallow. Please select the option that best describes how you are generally");
        getNpc_participant_followups_questions().put("impact_eye_movement", "This section relates to your eye movements. Please select the option that best describes how you are generally");
        getNpc_participant_followups_questions().put("impact_seizure", "This section relates to seizures. Please select the option that best describes how you are generally");
        getNpc_participant_followups_questions().put("impact_cognitive_impaired", "Do you think your cognitive abilities are impaired? i.e. learning new skills, remembering things, following instructions, focusing their attention");
        getNpc_participant_followups_questions().put("impact_family_disappointed", "Because of my illness, I am disappointed in my ability to keep up with family responsibilities");
        getNpc_participant_followups_questions().put("impact_family_give_up", "Our family gives up things because of their illness");
        getNpc_participant_followups_questions().put("impact_family_worry_future", "Because of my illness, I worry about what will happen in the future");
        getNpc_participant_followups_questions().put("impact_family_closer_family", "Because of what we have shared we are a closer family");
        getNpc_participant_followups_questions().put("impact_wider_self_miss_career", "Over the past 12 months, did you miss any time from your job or your school due to their illness");
        getNpc_participant_followups_questions().put("impact_wider_family_miss_career", "Over the past 12 months, if you have a carer, did they miss any time from their job or their school (i.e. college or university) due to your illness");
        getNpc_participant_followups_questions().put("impact_wider_emergency", "How many times, during the past 12 months, did you visit the hospital in an emergency because of issues relating to your illness");
        getNpc_participant_followups_questions().put("impact_wider_appointment", "During the past 12 months, how many appointments have you had with a doctor (both specialist and family doctors) regarding your illness");


    }

    public static BasicInfo getInstance(){
        BasicInfo pi = new BasicInfo();
        return pi;
    }

    public static Object getGender(Integer index){
        return genders.get(index);
    }


    public static Map getApplicant_relation() {
        return applicant_relation;
    }

    public static void setApplicant_relation(Map applicant_relation) {
        BasicInfo.applicant_relation = applicant_relation;
    }

    public static Map<String, String> getNpb_participant_questions() {
        return npb_participant_questions;
    }

    public static void setNpb_participant_questions(Map<String, String> npb_participant_questions) {
        BasicInfo.npb_participant_questions = npb_participant_questions;
    }

    public static Map<String, String> getNpc_participant_questions() {
        return npc_participant_questions;
    }

    public static void setNpc_participant_questions(Map<String, String> npc_participant_questions) {
        BasicInfo.npc_participant_questions = npc_participant_questions;
    }

    public static Map<String, String> getNpa_applicant_participant_questions() {
        return npa_applicant_participant_questions;
    }

    public static void setNpa_applicant_participant_questions(Map<String, String> npa_applicant_participant_questions) {
        BasicInfo.npa_applicant_participant_questions = npa_applicant_participant_questions;
    }

    public static Map<String, String> getNpb_applicant_participant_questions() {
        return npb_applicant_participant_questions;
    }

    public static void setNpb_applicant_participant_questions(Map<String, String> npb_applicant_participant_questions) {
        BasicInfo.npb_applicant_participant_questions = npb_applicant_participant_questions;
    }

    public static Map<String, String> getNpc_applicant_participant_questions() {
        return npc_applicant_participant_questions;
    }

    public static void setNpc_applicant_participant_questions(Map<String, String> npc_applicant_participant_questions) {
        BasicInfo.npc_applicant_participant_questions = npc_applicant_participant_questions;
    }

    public static Map<String, String> getNpb_followups_questions() {
        return npb_participant_followups_questions;
    }

    public static void setNpb_followups_questions(Map<String, String> npb_followups_questions) {
        BasicInfo.npb_participant_followups_questions = npb_followups_questions;
    }

    public static Map<String, String> getNpb_applicant_participant_followups_questions() {
        return npb_applicant_participant_followups_questions;
    }

    public static void setNpb_applicant_participant_followups_questions(Map<String, String> npb_applicant_participant_followups_questions) {
        BasicInfo.npb_applicant_participant_followups_questions = npb_applicant_participant_followups_questions;
    }

    public static Map<String, String> getNpc_participant_followups_questions() {
        return npc_participant_followups_questions;
    }

    public static void setNpc_participant_followups_questions(Map<String, String> npc_participant_followups_questions) {
        BasicInfo.npc_participant_followups_questions = npc_participant_followups_questions;
    }

    public static Map<String, String> getNpc_applicant_participant_followups_questions() {
        return npc_applicant_participant_followups_questions;
    }

    public static void setNpc_applicant_participant_followups_questions(Map<String, String> npc_applicant_participant_followups_questions) {
        BasicInfo.npc_applicant_participant_followups_questions = npc_applicant_participant_followups_questions;
    }
}