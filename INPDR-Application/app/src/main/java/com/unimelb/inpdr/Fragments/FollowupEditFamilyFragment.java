package com.unimelb.inpdr.Fragments;

import android.content.Context;
import android.content.DialogInterface;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentTransaction;
import android.support.v7.app.AlertDialog;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.TextView;
import android.widget.Toast;

import com.jaredrummler.materialspinner.MaterialSpinner;
import com.unimelb.inpdr.MainActivity;
import com.unimelb.inpdr.Models.API.APIItem;
import com.unimelb.inpdr.Models.API.APIReceivable;
import com.unimelb.inpdr.Models.API.INPDRAPI;
import com.unimelb.inpdr.Models.BasicInfo;
import com.unimelb.inpdr.Models.ErrorMessage;
import com.unimelb.inpdr.Models.NpbApplicantFollowupCollection;
import com.unimelb.inpdr.Models.NpbFollowupCollection;
import com.unimelb.inpdr.Models.NpcApplicantFollowupCollection;
import com.unimelb.inpdr.Models.NpcFollowupCollection;
import com.unimelb.inpdr.Models.Selection;
import com.unimelb.inpdr.R;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import me.drakeet.materialdialog.MaterialDialog;

/**
 * Created by Xintong on 20/05/2016.
 */
public class FollowupEditFamilyFragment extends Fragment implements APIReceivable{
    int patientId;
    String patientType;
    int id;
    Map<String, Integer> npbCollection = new HashMap<>();
    Map<String, Integer> npbApplicantCollection = new HashMap<>();
    Map<String, Integer> npcCollection = new HashMap<>();
    Map<String, Integer> npcApplicantCollection = new HashMap<>();
    Fragment currentFragment;
    boolean isFirstTime;
    public FollowupEditFamilyFragment() {
        // Required empty public constructor
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        return inflater.inflate(R.layout.fragment_followup_edit_family, container, false);
    }

    @Override
    public void onSaveInstanceState(Bundle outState) {
        super.onSaveInstanceState(outState);
        // do this for each or your Spinner
        // You might consider using Bundle.putStringArray() instead
    }

    @Override
    public void onStart(){
        super.onStart();
        ((MainActivity)getActivity()).setActionBarTitle("Edit Impact of Family");
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        // initialize all your visual fields
    }
    @Override
    public void onViewCreated(View view, Bundle savedInstanceState) {
        currentFragment = this;
        BasicInfo.getInstance();
        NpbFollowupCollection.getInstance();
        NpbApplicantFollowupCollection.getInstance();
        NpcFollowupCollection.getInstance();
        NpcApplicantFollowupCollection.getInstance();
        Selection.getInstance();
        ((MainActivity)getActivity()).setActionBarTitle("Edit Impact of Family");

        Bundle data = getArguments();

        if (data != null) {
            try {
                isFirstTime = data.getBoolean("first_time");
                patientId = data.getInt("followable_id");
                patientType = data.getString("followable_type");
                id = data.getInt("id");
                switch (patientType) {
                    case "NpbParticipant":
                        npbCollection.put("symptom_bone", data.getInt("symptom_bone"));
                        npbCollection.put("symptom_abdominal", data.getInt("symptom_abdominal"));
                        npbCollection.put("symptom_pain", data.getInt("symptom_pain"));
                        npbCollection.put("impact_wider_emergency", data.getInt("impact_wider_emergency"));
                        npbCollection.put("impact_wider_appointment", data.getInt("impact_wider_appointment"));
                        npbCollection.put("symptom_cognition", data.getInt("symptom_cognition"));
                        npbCollection.put("symptom_bleeding_rate", data.getInt("symptom_bleeding_rate"));
                        npbCollection.put("symptom_bleeding_impact", data.getInt("symptom_bleeding_impact"));
                        npbCollection.put("symptom_breathlessness_rate", data.getInt("symptom_breathlessness_rate"));
                        npbCollection.put("symptom_breathlessness_impact", data.getInt("symptom_breathlessness_impact"));
                        npbCollection.put("symptom_fatigue_rate", data.getInt("symptom_fatigue_rate"));
                        npbCollection.put("symptom_fatigue_impact", data.getInt("symptom_fatigue_impact"));
                        npbCollection.put("symptom_enlarge_organ", data.getInt("symptom_enlarge_organ"));
                        npbCollection.put("symptom_enlarge_organ_impact", data.getInt("symptom_enlarge_organ_impact"));
                        npbCollection.put("symptom_slow_growth", data.getInt("symptom_slow_growth"));
                        npbCollection.put("symptom_slow_growth_impact", data.getInt("symptom_slow_growth_impact"));
                        npbCollection.put("symptom_infection", data.getInt("symptom_infection"));
                        npbCollection.put("symptom_fracture", data.getInt("symptom_fracture"));
                        npbCollection.put("impact_family_give_up", data.getInt("impact_family_give_up"));
                        npbCollection.put("impact_family_disappointed", data.getInt("impact_family_disappointed"));
                        npbCollection.put("impact_family_worry_future", data.getInt("impact_family_worry_future"));
                        npbCollection.put("impact_family_closer", data.getInt("impact_family_closer"));
                        npbCollection.put("impact_wider_emergency", data.getInt("impact_wider_emergency"));
                        npbCollection.put("impact_wider_appointment", data.getInt("impact_wider_appointment"));
                        npbCollection.put("impact_wider_self_miss_career", data.getInt("impact_wider_self_miss_career"));
                        npbCollection.put("impact_wider_carer_miss_career", data.getInt("impact_wider_carer_miss_career"));
                        CreateNpbFollowup();
                        break;
                    case "NpbApplicantParticipant":
                        npbApplicantCollection.put("symptom_bone", data.getInt("symptom_bone"));
                        npbApplicantCollection.put("symptom_abdominal", data.getInt("symptom_abdominal"));
                        npbApplicantCollection.put("symptom_pain", data.getInt("symptom_pain"));
                        npbApplicantCollection.put("impact_wider_emergency", data.getInt("impact_wider_emergency"));
                        npbApplicantCollection.put("impact_wider_appointment", data.getInt("impact_wider_appointment"));
                        npbApplicantCollection.put("symptom_cognition", data.getInt("symptom_cognition"));
                        npbApplicantCollection.put("symptom_bleeding_rate", data.getInt("symptom_bleeding_rate"));
                        npbApplicantCollection.put("symptom_bleeding_impact", data.getInt("symptom_bleeding_impact"));
                        npbApplicantCollection.put("symptom_breathlessness_rate", data.getInt("symptom_breathlessness_rate"));
                        npbApplicantCollection.put("symptom_breathlessness_impact", data.getInt("symptom_breathlessness_impact"));
                        npbApplicantCollection.put("symptom_fatigue_rate", data.getInt("symptom_fatigue_rate"));
                        npbApplicantCollection.put("symptom_fatigue_impact", data.getInt("symptom_fatigue_impact"));
                        npbApplicantCollection.put("symptom_enlarge_organ", data.getInt("symptom_enlarge_organ"));
                        npbApplicantCollection.put("symptom_enlarge_organ_impact", data.getInt("symptom_enlarge_organ_impact"));
                        npbApplicantCollection.put("symptom_slow_growth", data.getInt("symptom_slow_growth"));
                        npbApplicantCollection.put("symptom_slow_growth_impact", data.getInt("symptom_slow_growth_impact"));
                        npbApplicantCollection.put("symptom_infection", data.getInt("symptom_infection"));
                        npbApplicantCollection.put("symptom_fracture", data.getInt("symptom_fracture"));
                        npbApplicantCollection.put("impact_family_give_up", data.getInt("impact_family_give_up"));
                        npbApplicantCollection.put("impact_family_disappointed", data.getInt("impact_family_disappointed"));
                        npbApplicantCollection.put("impact_family_worry_future", data.getInt("impact_family_worry_future"));
                        npbApplicantCollection.put("impact_family_closer", data.getInt("impact_family_closer"));
                        npbApplicantCollection.put("impact_wider_emergency", data.getInt("impact_wider_emergency"));
                        npbApplicantCollection.put("impact_wider_appointment", data.getInt("impact_wider_appointment"));
                        npbApplicantCollection.put("impact_wider_self_miss_career", data.getInt("impact_wider_self_miss_career"));
                        npbApplicantCollection.put("impact_wider_carer_miss_career", data.getInt("impact_wider_carer_miss_career"));
                        CreateApplicantNpbFollowup();
                        break;
                    case "NpcParticipant":
                        npcCollection.put("impact_ambulation", data.getInt("impact_ambulation"));
                        npcCollection.put("impact_manipulation", data.getInt("impact_manipulation"));
                        npcCollection.put("impact_speech", data.getInt("impact_speech"));
                        npcCollection.put("impact_swallowing", data.getInt("impact_swallowing"));
                        npcCollection.put("impact_eye_movement", data.getInt("impact_eye_movement"));
                        npcCollection.put("impact_seizure", data.getInt("impact_seizure"));
                        npcCollection.put("impact_cognitive_impaired", data.getInt("impact_cognitive_impaired"));
                        npcCollection.put("impact_family_disappointed", data.getInt("impact_family_disappointed"));
                        npcCollection.put("impact_family_give_up", data.getInt("impact_family_give_up"));
                        npcCollection.put("impact_family_worry_future", data.getInt("impact_family_worry_future"));
                        npcCollection.put("impact_family_closer_family", data.getInt("impact_family_closer_family"));
                        npcCollection.put("impact_wider_emergency", data.getInt("impact_wider_emergency"));
                        npcCollection.put("impact_wider_appointment", data.getInt("impact_wider_appointment"));
                        npcCollection.put("impact_wider_self_miss_career", data.getInt("impact_wider_self_miss_career"));
                        npcCollection.put("impact_wider_family_miss_career", data.getInt("impact_wider_family_miss_career"));
                        CreateNpcFollowup();
                        break;
                    case "NpcApplicantParticipant":
                        npcApplicantCollection.put("impact_ambulation", data.getInt("impact_ambulation"));
                        npcApplicantCollection.put("impact_manipulation", data.getInt("impact_manipulation"));
                        npcApplicantCollection.put("impact_speech", data.getInt("impact_speech"));
                        npcApplicantCollection.put("impact_swallowing", data.getInt("impact_swallowing"));
                        npcApplicantCollection.put("impact_eye_movement", data.getInt("impact_eye_movement"));
                        npcApplicantCollection.put("impact_seizure", data.getInt("impact_seizure"));
                        npcApplicantCollection.put("impact_cognitive_impaired", data.getInt("impact_cognitive_impaired"));
                        npcApplicantCollection.put("impact_family_disappointed", data.getInt("impact_family_disappointed"));
                        npcApplicantCollection.put("impact_family_give_up", data.getInt("impact_family_give_up"));
                        npcApplicantCollection.put("impact_family_worry_future", data.getInt("impact_family_worry_future"));
                        npcApplicantCollection.put("impact_family_closer_family", data.getInt("impact_family_closer_family"));
                        npcApplicantCollection.put("impact_wider_emergency", data.getInt("impact_wider_emergency"));
                        npcApplicantCollection.put("impact_wider_appointment", data.getInt("impact_wider_appointment"));
                        npcApplicantCollection.put("impact_wider_self_miss_career", data.getInt("impact_wider_self_miss_career"));
                        npcApplicantCollection.put("impact_wider_family_miss_career", data.getInt("impact_wider_family_miss_career"));
                        CreateApplicantNpcFollowup();
                        break;
                }
            }catch (Exception e){
                e.printStackTrace();
            }
        }


//        NiceSpinner niceSpinner = (NiceSpinner) view.findViewById(R.id.spinner);
//        List<String> dataset = new LinkedList<>(Arrays.asList("One", "Two", "Three", "Four", "Five"));
//        niceSpinner.attachDataSource(dataset);

        // ArrayAdapter<String> adapter = new ArrayAdapter<String>(getContext(), android.R.layout.simple_spinner_dropdown_item, countries);

//        NiceSpinnerAdapter adapter = new NiceSpinnerAdapter(getContext(), dataset, Color.DKGRAY, 2);
//        spinner.setAdapter(adapter);
        // Getting object reference to listview of main.xml
//        ListView listView = (ListView) view.findViewById(R.id.listView);
//
//        // Instantiating array adapter to populate the listView
//        // The layout android.R.layout.simple_list_item_single_choice creates radio button for each listview item
//
//        ArrayAdapter<String> adapter = new ArrayAdapter<String>(getContext(), android.R.layout.simple_list_item_single_choice, countries);
//
//        listView.setAdapter(adapter);

    }




    @Override
    public <U extends APIItem> void receiveData(List<U> items, String type) {

    }

    @Override
    public <U extends APIItem> void receiveData(U item, String type) {
        switch (type) {
            case "createANpbFollowup":
                break;
            case "createANpcFollowup":
                break;
            case "error":
                ErrorMessage error = (ErrorMessage) item;
                final MaterialDialog mMaterialDialog_e = new MaterialDialog(getContext());
                mMaterialDialog_e.setTitle("Error");
                ErrorMessage errorMessage = (ErrorMessage) item;
                mMaterialDialog_e.setMessage(errorMessage.getErrorMes());
                mMaterialDialog_e.setPositiveButton("Ok", new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        mMaterialDialog_e.dismiss();
                    }
                });
                mMaterialDialog_e.show();
                break;
        }
    }

    private void CreateNpcFollowup() {
        TextView textDiscription = (TextView)getView().findViewById(R.id.text_explaination_family_edit_followup);
        textDiscription.setText("The following section looks at how Niemann-Pick disease has impacted the family. " +
                "For each statement below, please tick whether at the present time you would strongly agree, agree, disagree or strongly disagree with the statement.");

        TextView textNpcCreate = (TextView)getView().findViewById(R.id.text_edit_family_followup_title);
        textNpcCreate.setText("Edit NP-C Followup Questionnaire");

        int index = 0;
        ViewGroup insertPoint = (ViewGroup) getView().findViewById(R.id.lty_edit_family_followup);
        LayoutInflater vi = (LayoutInflater) getActivity().getSystemService(Context.LAYOUT_INFLATER_SERVICE);

        List<String> answers_impact_family_disappointed = new ArrayList<>();
        answers_impact_family_disappointed.add(" ");
        for(int i = 0; i < NpcFollowupCollection.getImpact_family_collection().size(); i++){
            answers_impact_family_disappointed.add(NpcFollowupCollection.getImpact_family_collection().get(i));
        }
        NpcInflater(vi, answers_impact_family_disappointed, "impact_family_disappointed", insertPoint, index, npcCollection, BasicInfo.getNpc_participant_followups_questions());
        index++;

        List<String> answers_impact_family_give_up = new ArrayList<>();
        answers_impact_family_give_up.add(" ");
        for(int i = 0; i < NpcFollowupCollection.getImpact_family_collection().size(); i++){
            answers_impact_family_give_up.add(NpcFollowupCollection.getImpact_family_collection().get(i));
        }
        NpcInflater(vi, answers_impact_family_give_up, "impact_family_give_up", insertPoint, index, npcCollection, BasicInfo.getNpc_participant_followups_questions());
        index++;

        List<String> answers_impact_family_worry_future = new ArrayList<>();
        answers_impact_family_worry_future.add(" ");
        for(int i = 0; i < NpcFollowupCollection.getImpact_family_collection().size(); i++){
            answers_impact_family_worry_future.add(NpcFollowupCollection.getImpact_family_collection().get(i));
        }
        NpcInflater(vi, answers_impact_family_worry_future, "impact_family_worry_future", insertPoint, index, npcCollection, BasicInfo.getNpc_participant_followups_questions());
        index++;

        List<String> answers_impact_family_closer_family = new ArrayList<>();
        answers_impact_family_closer_family.add(" ");
        for(int i = 0; i < NpcFollowupCollection.getImpact_family_collection().size(); i++){
            answers_impact_family_closer_family.add(NpcFollowupCollection.getImpact_family_collection().get(i));
        }
        NpcInflater(vi, answers_impact_family_closer_family, "impact_family_closer_family", insertPoint, index, npcCollection, BasicInfo.getNpc_participant_followups_questions());
        index++;

        Button buttonNext = (Button) getView().findViewById(R.id.button_followup_next);
        buttonNext.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Toast.makeText(getView().getContext(), "next!", Toast.LENGTH_SHORT).show();
                Bundle data = new Bundle();
                data.putInt("followable_id", patientId);
                data.putString("followable_type", patientType);
                data.putInt("id", id);
                data.putInt("impact_ambulation", npcCollection.get("impact_ambulation"));
                data.putInt("impact_manipulation", npcCollection.get("impact_manipulation"));
                data.putInt("impact_speech", npcCollection.get("impact_speech"));
                data.putInt("impact_swallowing", npcCollection.get("impact_swallowing"));
                data.putInt("impact_eye_movement", npcCollection.get("impact_eye_movement"));
                data.putInt("impact_seizure", npcCollection.get("impact_seizure"));
                data.putInt("impact_cognitive_impaired", npcCollection.get("impact_cognitive_impaired"));
                data.putInt("impact_family_disappointed", npcCollection.get("impact_family_disappointed"));
                data.putInt("impact_family_give_up", npcCollection.get("impact_family_give_up"));
                data.putInt("impact_family_worry_future", npcCollection.get("impact_family_worry_future"));
                data.putInt("impact_family_closer_family", npcCollection.get("impact_family_closer_family"));
                data.putInt("impact_wider_self_miss_career", npcCollection.get("impact_wider_self_miss_career"));
                data.putInt("impact_wider_family_miss_career", npcCollection.get("impact_wider_family_miss_career"));
                data.putInt("impact_wider_emergency", npcCollection.get("impact_wider_emergency"));
                data.putInt("impact_wider_appointment", npcCollection.get("impact_wider_appointment"));
                data.putBoolean("first_time", isFirstTime);


                FragmentManager fm = getFragmentManager();
                FragmentTransaction transaction = fm.beginTransaction();
                Fragment followupEditWiderImpactFragment;

                followupEditWiderImpactFragment= new FollowupEditWiderImpactFragment();


                followupEditWiderImpactFragment.setArguments(data);
//                ft.replace(R.id.main_view, followupWiderImpactFragment);
//                ft.addToBackStack(null);
//                ft.commit();


//                FragmentTransaction transaction = getFragmentManager()
//                        .beginTransaction();
                transaction.add(R.id.main_view, followupEditWiderImpactFragment,
                        "FragmentWiderImpact");
                transaction.addToBackStack("FragmentFamily");
                transaction.show(followupEditWiderImpactFragment);
                transaction.setTransition(FragmentTransaction.TRANSIT_FRAGMENT_CLOSE);
                transaction.commit();
                isFirstTime = false;
            }
        });
    }

    private void CreateApplicantNpcFollowup() {
        TextView textDiscription = (TextView)getView().findViewById(R.id.text_explaination_family_edit_followup);
        textDiscription.setText("The following section looks at how Niemann-Pick disease has impacted the family. " +
                "For each statement below, please tick whether at the present time you would strongly agree, agree, disagree or strongly disagree with the statement.");

        TextView textNpcCreate = (TextView)getView().findViewById(R.id.text_edit_family_followup_title);
        textNpcCreate.setText("Edit NP-C Followup Questionnaire");

        int index = 0;
        ViewGroup insertPoint = (ViewGroup) getView().findViewById(R.id.lty_edit_family_followup);
        LayoutInflater vi = (LayoutInflater) getActivity().getSystemService(Context.LAYOUT_INFLATER_SERVICE);

        List<String> answers_impact_family_disappointed = new ArrayList<>();
        answers_impact_family_disappointed.add(" ");
        for(int i = 0; i < NpcApplicantFollowupCollection.getImpact_family_collection().size(); i++){
            answers_impact_family_disappointed.add(NpcApplicantFollowupCollection.getImpact_family_collection().get(i));
        }
        NpcInflater(vi, answers_impact_family_disappointed, "impact_family_disappointed", insertPoint, index, npcApplicantCollection, BasicInfo.getNpc_applicant_participant_followups_questions());
        index++;

        List<String> answers_impact_family_give_up = new ArrayList<>();
        answers_impact_family_give_up.add(" ");
        for(int i = 0; i < NpcApplicantFollowupCollection.getImpact_family_collection().size(); i++){
            answers_impact_family_give_up.add(NpcApplicantFollowupCollection.getImpact_family_collection().get(i));
        }
        NpcInflater(vi, answers_impact_family_give_up, "impact_family_give_up", insertPoint, index, npcApplicantCollection, BasicInfo.getNpc_applicant_participant_followups_questions());
        index++;

        List<String> answers_impact_family_worry_future = new ArrayList<>();
        answers_impact_family_worry_future.add(" ");
        for(int i = 0; i < NpcApplicantFollowupCollection.getImpact_family_collection().size(); i++){
            answers_impact_family_worry_future.add(NpcApplicantFollowupCollection.getImpact_family_collection().get(i));
        }
        NpcInflater(vi, answers_impact_family_worry_future, "impact_family_worry_future", insertPoint, index, npcApplicantCollection, BasicInfo.getNpc_applicant_participant_followups_questions());
        index++;

        List<String> answers_impact_family_closer_family = new ArrayList<>();
        answers_impact_family_closer_family.add(" ");
        for(int i = 0; i < NpcApplicantFollowupCollection.getImpact_family_collection().size(); i++){
            answers_impact_family_closer_family.add(NpcApplicantFollowupCollection.getImpact_family_collection().get(i));
        }
        NpcInflater(vi, answers_impact_family_closer_family, "impact_family_closer_family", insertPoint, index, npcApplicantCollection, BasicInfo.getNpc_applicant_participant_followups_questions());
        index++;

        Button buttonNext = (Button) getView().findViewById(R.id.button_followup_next);
        buttonNext.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Toast.makeText(getView().getContext(), "next!", Toast.LENGTH_SHORT).show();
                Log.d("click1!", "!!!!!!!");
                Bundle data = new Bundle();
                data.putInt("id", id);
                data.putInt("followable_id", patientId);
                data.putString("followable_type", patientType);
                data.putInt("impact_ambulation", npcApplicantCollection.get("impact_ambulation"));
                data.putInt("impact_manipulation", npcApplicantCollection.get("impact_manipulation"));
                data.putInt("impact_speech", npcApplicantCollection.get("impact_speech"));
                data.putInt("impact_swallowing", npcApplicantCollection.get("impact_swallowing"));
                data.putInt("impact_eye_movement", npcApplicantCollection.get("impact_eye_movement"));
                data.putInt("impact_seizure", npcApplicantCollection.get("impact_seizure"));
                data.putInt("impact_cognitive_impaired", npcApplicantCollection.get("impact_cognitive_impaired"));
                data.putInt("impact_family_disappointed", npcApplicantCollection.get("impact_family_disappointed"));
                data.putInt("impact_family_give_up", npcApplicantCollection.get("impact_family_give_up"));
                data.putInt("impact_family_worry_future", npcApplicantCollection.get("impact_family_worry_future"));
                data.putInt("impact_family_closer_family", npcApplicantCollection.get("impact_family_closer_family"));
                data.putInt("impact_wider_family_miss_career", npcApplicantCollection.get("impact_wider_family_miss_career"));
                data.putInt("impact_wider_self_miss_career", npcApplicantCollection.get("impact_wider_self_miss_career"));
                data.putInt("impact_wider_emergency", npcApplicantCollection.get("impact_wider_emergency"));
                data.putInt("impact_wider_appointment", npcApplicantCollection.get("impact_wider_appointment"));
                data.putBoolean("first_time", isFirstTime);

                Log.d("family_give_up", npcApplicantCollection.get("impact_family_give_up")+"");

                FragmentManager fm = getFragmentManager();
                FragmentTransaction transaction = fm.beginTransaction();
                Fragment followupEditWiderImpactFragment;

                followupEditWiderImpactFragment= new FollowupEditWiderImpactFragment();


                followupEditWiderImpactFragment.setArguments(data);
//                ft.replace(R.id.main_view, followupWiderImpactFragment);
//                ft.addToBackStack(null);
//                ft.commit();


//                FragmentTransaction transaction = getFragmentManager()
//                        .beginTransaction();
                transaction.add(R.id.main_view, followupEditWiderImpactFragment,
                        "FragmentWiderImpact");
                transaction.addToBackStack("FragmentFamily");
                transaction.show(followupEditWiderImpactFragment);
                transaction.setTransition(FragmentTransaction.TRANSIT_FRAGMENT_CLOSE);
                transaction.commit();
                isFirstTime = false;
            }
        });
    }

    private void NpcInflater(LayoutInflater vi, final List<String> items, final String param, ViewGroup insertPoint, int index, final Map<String, Integer> answers, Map<String, String> questions) {
        View v = vi.inflate(R.layout.item_create_followup, null);
        TextView textQuestion = (TextView)v.findViewById(R.id.text_create_followup_question);

        Log.d("flater!!","");
        textQuestion.setText(questions.get(param));
        final MaterialSpinner spinner = (MaterialSpinner) v.findViewById(R.id.spinner);
        spinner.setItems(items);

        spinner.setSelectedIndex(answers.get(param) + 1);
        spinner.setTextColor(getResources().getColor(R.color.colorPrimaryText));

        if(!isFirstTime){
            SharedPreferences myPrefs = getActivity().getSharedPreferences("myPrefs", 0);

            if(myPrefs.contains(param)){
                spinner.setSelectedIndex(myPrefs.getInt(param, 0));
                Log.d("get prefs", myPrefs.getInt(param, 0)+ " " + param);
            }
        }


        spinner.setOnItemSelectedListener(new MaterialSpinner.OnItemSelectedListener<String>() {

            @Override
            public void onItemSelected(MaterialSpinner view, int position, long id, String item) {
                if (position != 0) {
                    answers.put(param, position - 1);
                }

                int mSpnValue = position;
                SharedPreferences myPrefs = getActivity().getSharedPreferences("myPrefs", getContext().MODE_MULTI_PROCESS);
                SharedPreferences.Editor prefsEditor = myPrefs.edit();
                prefsEditor.putInt(param, mSpnValue);
                prefsEditor.commit();
                Log.d("valuee", " " + param + ", " + mSpnValue);

//                Context context = view.getContext();
//                CharSequence text = answers.get(param) + " " + param;
//                int duration = Toast.LENGTH_SHORT;
//
//                Toast toast = Toast.makeText(context, text, duration);
//                toast.show();
            }
        });
        answers.put(param, spinner.getSelectedIndex() - 1);

        spinner.onSaveInstanceState();

        insertPoint.addView(
                v,
                index,
                new ViewGroup.LayoutParams(ViewGroup.LayoutParams.MATCH_PARENT, ViewGroup.LayoutParams.WRAP_CONTENT));
    }


    private void CreateNpbFollowup() {
        final APIReceivable receivable = this;
        final INPDRAPI api = new INPDRAPI(getContext());
        int index = 0;

        TextView textNpcCreate = (TextView)getView().findViewById(R.id.text_edit_family_followup_title);
        textNpcCreate.setText("Edit NP-B Followup Questionnaire");

        ViewGroup insertPoint = (ViewGroup) getView().findViewById(R.id.lty_edit_family_followup);
        LayoutInflater vi = (LayoutInflater) getActivity().getSystemService(Context.LAYOUT_INFLATER_SERVICE);

        List<String> answers_impact_family_disappointed = new ArrayList<>();
        answers_impact_family_disappointed.add(" ");
        for(int i = 0; i < NpbFollowupCollection.getImpact_family_collection().size(); i++){
            answers_impact_family_disappointed.add(NpbFollowupCollection.getImpact_family_collection().get(i));
        }
        NpbInflater(vi, answers_impact_family_disappointed, "impact_family_disappointed", insertPoint, index, npbCollection, BasicInfo.getNpb_followups_questions());
        index++;

        List<String> answers_impact_family_give_up = new ArrayList<>();
        answers_impact_family_give_up.add(" ");
        for(int i = 0; i < NpbFollowupCollection.getImpact_family_collection().size(); i++){
            answers_impact_family_give_up.add(NpbFollowupCollection.getImpact_family_collection().get(i));
        }
        NpbInflater(vi, answers_impact_family_give_up, "impact_family_give_up", insertPoint, index, npbCollection, BasicInfo.getNpb_followups_questions());
        index++;

        List<String> answers_impact_family_worry_future = new ArrayList<>();
        answers_impact_family_worry_future.add(" ");
        for(int i = 0; i < NpbFollowupCollection.getImpact_family_collection().size(); i++){
            answers_impact_family_worry_future.add(NpbFollowupCollection.getImpact_family_collection().get(i));
        }
        NpbInflater(vi, answers_impact_family_worry_future, "impact_family_worry_future", insertPoint, index, npbCollection, BasicInfo.getNpb_followups_questions());
        index++;

        List<String> answers_impact_family_closer = new ArrayList<>();
        answers_impact_family_closer.add(" ");
        for(int i = 0; i < NpbFollowupCollection.getImpact_family_collection().size(); i++){
            answers_impact_family_closer.add(NpbFollowupCollection.getImpact_family_collection().get(i));
        }
        NpbInflater(vi, answers_impact_family_closer, "impact_family_closer", insertPoint, index, npbCollection, BasicInfo.getNpb_followups_questions());
        index++;

        Button buttonNext = (Button) getView().findViewById(R.id.button_followup_next);
        buttonNext.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Toast.makeText(getView().getContext(), "next!", Toast.LENGTH_SHORT).show();
                Bundle data = new Bundle();
                data.putInt("followable_id", patientId);
                data.putString("followable_type", patientType);
                data.putInt("id", id);
                data.putInt("symptom_bone", npbCollection.get("symptom_bone"));
                data.putInt("symptom_abdominal", npbCollection.get("symptom_abdominal"));
                data.putInt("symptom_pain", npbCollection.get("symptom_pain"));
                data.putInt("symptom_cognition", npbCollection.get("symptom_cognition"));
                data.putInt("symptom_breathlessness_rate", npbCollection.get("symptom_breathlessness_rate"));
                data.putInt("symptom_breathlessness_impact", npbCollection.get("symptom_breathlessness_impact"));
                data.putInt("symptom_bleeding_rate", npbCollection.get("symptom_bleeding_rate"));
                data.putInt("symptom_bleeding_impact", npbCollection.get("symptom_bleeding_impact"));
                data.putInt("symptom_fatigue_rate", npbCollection.get("symptom_fatigue_rate"));
                data.putInt("symptom_fatigue_impact", npbCollection.get("symptom_fatigue_impact"));
                data.putInt("symptom_enlarge_organ", npbCollection.get("symptom_enlarge_organ"));
                data.putInt("symptom_enlarge_organ_impact", npbCollection.get("symptom_enlarge_organ_impact"));
                data.putInt("symptom_slow_growth", npbCollection.get("symptom_slow_growth"));
                data.putInt("symptom_slow_growth_impact", npbCollection.get("symptom_slow_growth_impact"));
                data.putInt("symptom_infection", npbCollection.get("symptom_infection"));
                data.putInt("symptom_fracture", npbCollection.get("symptom_fracture"));
                data.putInt("impact_family_disappointed", npbCollection.get("impact_family_disappointed"));
                data.putInt("impact_family_give_up", npbCollection.get("impact_family_give_up"));
                data.putInt("impact_family_worry_future", npbCollection.get("impact_family_worry_future"));
                data.putInt("impact_family_closer", npbCollection.get("impact_family_closer"));
                data.putInt("impact_wider_self_miss_career", npbCollection.get("impact_wider_self_miss_career"));
                data.putInt("impact_wider_carer_miss_career", npbCollection.get("impact_wider_carer_miss_career"));
                data.putInt("impact_wider_emergency", npbCollection.get("impact_wider_emergency"));
                data.putInt("impact_wider_appointment", npbCollection.get("impact_wider_appointment"));
                data.putBoolean("first_time", isFirstTime);


                FragmentManager fm = getFragmentManager();
                FragmentTransaction transaction = fm.beginTransaction();
                Fragment followupEditWiderImpactFragment;

                followupEditWiderImpactFragment= new FollowupEditWiderImpactFragment();


                followupEditWiderImpactFragment.setArguments(data);
//                ft.replace(R.id.main_view, followupWiderImpactFragment);
//                ft.addToBackStack(null);
//                ft.commit();


//                FragmentTransaction transaction = getFragmentManager()
//                        .beginTransaction();
                transaction.add(R.id.main_view, followupEditWiderImpactFragment,
                        "FragmentWiderImpact");
                transaction.addToBackStack("FragmentFamily");
                transaction.show(followupEditWiderImpactFragment);
                transaction.setTransition(FragmentTransaction.TRANSIT_FRAGMENT_CLOSE);
                transaction.commit();
                isFirstTime = false;
            }
        });

//        Spinner spinner = (Spinner) v.findViewById(R.id.spinner);
//        ArrayAdapter<String> adapter = new ArrayAdapter<String>(getContext(), android.R.layout.simple_spinner_dropdown_item, answers_bone_pain);
//
//        spinner.setAdapter(adapter);

    }

    private void NpbInflater(LayoutInflater vi, final List<String> items, final String param, ViewGroup insertPoint, int index, final Map<String, Integer> answers, Map<String, String> questions) {
        View v = vi.inflate(R.layout.item_create_followup, null);
        TextView textQuestion = (TextView)v.findViewById(R.id.text_create_followup_question);

        Log.d("flater!!","");
        textQuestion.setText(questions.get(param));
        final MaterialSpinner spinner = (MaterialSpinner) v.findViewById(R.id.spinner);
        spinner.setItems(items);

        spinner.setSelectedIndex(answers.get(param) + 1);
        spinner.setTextColor(getResources().getColor(R.color.colorPrimaryText));

        if(!isFirstTime){
            SharedPreferences myPrefs = getActivity().getSharedPreferences("myPrefs", 0);

            if(myPrefs.contains(param)){
                spinner.setSelectedIndex(myPrefs.getInt(param, 0));
                Log.d("get prefs", myPrefs.getInt(param, 0)+ " " + param);
            }
        }


        spinner.setOnItemSelectedListener(new MaterialSpinner.OnItemSelectedListener<String>() {

            @Override
            public void onItemSelected(MaterialSpinner view, int position, long id, String item) {
                if (position != 0) {
                    answers.put(param, position - 1);
                }

                int mSpnValue = position;
                SharedPreferences myPrefs = getActivity().getSharedPreferences("myPrefs", getContext().MODE_MULTI_PROCESS);
                SharedPreferences.Editor prefsEditor = myPrefs.edit();
                prefsEditor.putInt(param, mSpnValue);
                prefsEditor.commit();
                Log.d("valuee", " " + param + ", " + mSpnValue);

//                Context context = view.getContext();
//                CharSequence text = answers.get(param) + " " + param;
//                int duration = Toast.LENGTH_SHORT;
//
//                Toast toast = Toast.makeText(context, text, duration);
//                toast.show();
            }
        });
        answers.put(param, spinner.getSelectedIndex() - 1);



        spinner.onSaveInstanceState();

        insertPoint.addView(
                v,
                index,
                new ViewGroup.LayoutParams(ViewGroup.LayoutParams.MATCH_PARENT, ViewGroup.LayoutParams.WRAP_CONTENT));
    }


    private void CreateApplicantNpbFollowup() {
        int index = 0;

        TextView textNpcCreate = (TextView)getView().findViewById(R.id.text_edit_family_followup_title);
        textNpcCreate.setText("Edit NP-B Followup Questionnaire");

        ViewGroup insertPoint = (ViewGroup) getView().findViewById(R.id.lty_edit_family_followup);
        LayoutInflater vi = (LayoutInflater) getActivity().getSystemService(Context.LAYOUT_INFLATER_SERVICE);

        List<String> answers_impact_family_disappointed = new ArrayList<>();
        answers_impact_family_disappointed.add(" ");
        for(int i = 0; i < NpbApplicantFollowupCollection.getImpact_family_collection().size(); i++){
            answers_impact_family_disappointed.add(NpbApplicantFollowupCollection.getImpact_family_collection().get(i));
        }
        NpbInflater(vi, answers_impact_family_disappointed, "impact_family_disappointed", insertPoint, index, npbApplicantCollection, BasicInfo.getNpb_followups_questions());
        index++;

        List<String> answers_impact_family_give_up = new ArrayList<>();
        answers_impact_family_give_up.add(" ");
        for(int i = 0; i < NpbApplicantFollowupCollection.getImpact_family_collection().size(); i++){
            answers_impact_family_give_up.add(NpbApplicantFollowupCollection.getImpact_family_collection().get(i));
        }
        NpbInflater(vi, answers_impact_family_give_up, "impact_family_give_up", insertPoint, index, npbApplicantCollection, BasicInfo.getNpb_followups_questions());
        index++;

        List<String> answers_impact_family_worry_future = new ArrayList<>();
        answers_impact_family_worry_future.add(" ");
        for(int i = 0; i < NpbApplicantFollowupCollection.getImpact_family_collection().size(); i++){
            answers_impact_family_worry_future.add(NpbApplicantFollowupCollection.getImpact_family_collection().get(i));
        }
        NpbInflater(vi, answers_impact_family_worry_future, "impact_family_worry_future", insertPoint, index, npbApplicantCollection, BasicInfo.getNpb_followups_questions());
        index++;

        List<String> answers_impact_family_closer = new ArrayList<>();
        answers_impact_family_closer.add(" ");
        for(int i = 0; i < NpbApplicantFollowupCollection.getImpact_family_collection().size(); i++){
            answers_impact_family_closer.add(NpbApplicantFollowupCollection.getImpact_family_collection().get(i));
        }
        NpbInflater(vi, answers_impact_family_closer, "impact_family_closer", insertPoint, index, npbApplicantCollection, BasicInfo.getNpb_followups_questions());
        index++;

        Button buttonNext = (Button) getView().findViewById(R.id.button_followup_next);
        buttonNext.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Toast.makeText(getView().getContext(), "next!", Toast.LENGTH_SHORT).show();
                Log.d("click1!", "!!!!!!!");
                Bundle data = new Bundle();
                data.putInt("followable_id", patientId);
                data.putString("followable_type", patientType);
                data.putInt("id", id);
                data.putInt("symptom_bone", npbApplicantCollection.get("symptom_bone"));
                data.putInt("symptom_abdominal", npbApplicantCollection.get("symptom_abdominal"));
                data.putInt("symptom_pain", npbApplicantCollection.get("symptom_pain"));
                data.putInt("symptom_cognition", npbApplicantCollection.get("symptom_cognition"));
                data.putInt("symptom_breathlessness_rate", npbApplicantCollection.get("symptom_breathlessness_rate"));
                data.putInt("symptom_breathlessness_impact", npbApplicantCollection.get("symptom_breathlessness_impact"));
                data.putInt("symptom_bleeding_rate", npbApplicantCollection.get("symptom_bleeding_rate"));
                data.putInt("symptom_bleeding_impact", npbApplicantCollection.get("symptom_bleeding_impact"));
                data.putInt("symptom_fatigue_rate", npbApplicantCollection.get("symptom_fatigue_rate"));
                data.putInt("symptom_fatigue_impact", npbApplicantCollection.get("symptom_fatigue_impact"));
                data.putInt("symptom_enlarge_organ", npbApplicantCollection.get("symptom_enlarge_organ"));
                data.putInt("symptom_enlarge_organ_impact", npbApplicantCollection.get("symptom_enlarge_organ_impact"));
                data.putInt("symptom_slow_growth", npbApplicantCollection.get("symptom_slow_growth"));
                data.putInt("symptom_slow_growth_impact", npbApplicantCollection.get("symptom_slow_growth_impact"));
                data.putInt("symptom_infection", npbApplicantCollection.get("symptom_infection"));
                data.putInt("symptom_fracture", npbApplicantCollection.get("symptom_fracture"));
                data.putInt("impact_family_disappointed", npbApplicantCollection.get("impact_family_disappointed"));
                data.putInt("impact_family_give_up", npbApplicantCollection.get("impact_family_give_up"));
                data.putInt("impact_family_worry_future", npbApplicantCollection.get("impact_family_worry_future"));
                data.putInt("impact_family_closer", npbApplicantCollection.get("impact_family_closer"));
                data.putInt("impact_wider_self_miss_career", npbApplicantCollection.get("impact_wider_self_miss_career"));
                data.putInt("impact_wider_carer_miss_career", npbApplicantCollection.get("impact_wider_carer_miss_career"));
                data.putInt("impact_wider_emergency", npbApplicantCollection.get("impact_wider_emergency"));
                data.putInt("impact_wider_appointment", npbApplicantCollection.get("impact_wider_appointment"));
                data.putBoolean("first_time", isFirstTime);


                FragmentManager fm = getFragmentManager();
                FragmentTransaction transaction = fm.beginTransaction();
                Fragment followupEditWiderImpactFragment;

                followupEditWiderImpactFragment= new FollowupEditWiderImpactFragment();

                followupEditWiderImpactFragment.setArguments(data);
//                ft.replace(R.id.main_view, followupWiderImpactFragment);
//                ft.addToBackStack(null);
//                ft.commit();


//                FragmentTransaction transaction = getFragmentManager()
//                        .beginTransaction();
                transaction.add(R.id.main_view, followupEditWiderImpactFragment,
                        "FragmentWiderImpact");
                transaction.addToBackStack("FragmentFamily");
                transaction.show(followupEditWiderImpactFragment);
                transaction.setTransition(FragmentTransaction.TRANSIT_FRAGMENT_CLOSE);
                transaction.commit();
                isFirstTime = false;
            }
        });

    }
}
