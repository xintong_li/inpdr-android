package com.unimelb.inpdr.Fragments;

import android.content.Context;
import android.content.DialogInterface;
import android.content.SharedPreferences;
import android.graphics.Color;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentTransaction;
import android.support.v7.app.AlertDialog;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;

import com.jaredrummler.materialspinner.MaterialSpinner;
import com.unimelb.inpdr.MainActivity;
import com.unimelb.inpdr.Models.API.APIItem;
import com.unimelb.inpdr.Models.API.APIReceivable;
import com.unimelb.inpdr.Models.API.INPDRAPI;
import com.unimelb.inpdr.Models.BasicInfo;
import com.unimelb.inpdr.Models.ErrorMessage;
import com.unimelb.inpdr.Models.NpbApplicantFollowupCollection;
import com.unimelb.inpdr.Models.NpbApplicantFollowupCollection;
import com.unimelb.inpdr.Models.NpbFollowupCollection;
import com.unimelb.inpdr.Models.NpcApplicantFollowupCollection;
import com.unimelb.inpdr.Models.NpcFollowupCollection;
import com.unimelb.inpdr.Models.Selection;
import com.unimelb.inpdr.R;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import me.drakeet.materialdialog.MaterialDialog;

/**
 * Created by Xintong on 17/05/2016.
 */
public class FollowupFamilyFragment extends Fragment implements APIReceivable {
    int patientId;
    String patientType;
    Map<String, String> npbCollection = new HashMap<>();
    Map<String, String> npbApplicantCollection = new HashMap<>();
    Map<String, String> npcCollection = new HashMap<>();
    Map<String, String> npcApplicantCollection = new HashMap<>();
    Fragment currentFragment;
    boolean isFirstTime;

    Map<String, Integer> npbIndexes = new HashMap<>();
    Map<String, Integer> npcIndexes = new HashMap<>();
    List<String> npbParams = new ArrayList<>();
    List<String> npcParams = new ArrayList<>();

    public FollowupFamilyFragment() {
        // Required empty public constructor
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        return inflater.inflate(R.layout.fragment_followup_family, container, false);
    }

    @Override
    public void onSaveInstanceState(Bundle outState) {
        super.onSaveInstanceState(outState);
        // do this for each or your Spinner
        // You might consider using Bundle.putStringArray() instead
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        // initialize all your visual fields
    }

    @Override
    public void onStart() {
        super.onStart();
        ((MainActivity) getActivity()).setActionBarTitle("Create Followup");
    }

    @Override
    public void onViewCreated(View view, Bundle savedInstanceState) {
        currentFragment = this;
        BasicInfo.getInstance();
        NpbFollowupCollection.getInstance();
        NpbApplicantFollowupCollection.getInstance();
        NpcFollowupCollection.getInstance();
        NpcApplicantFollowupCollection.getInstance();
        Selection.getInstance();

        MapInitialization();


        ((MainActivity) getActivity()).setActionBarTitle("Create Followup");

        Bundle data = getArguments();

        if (data != null) {
            try {
                isFirstTime = data.getBoolean("first_time");
                patientId = data.getInt("followable_id");
                patientType = data.getString("followable_type");
                switch (patientType) {
                    case "NpbParticipant":
                        npbCollection.put("symptom_bone", data.getString("symptom_bone"));
                        npbCollection.put("symptom_abdominal", data.getString("symptom_abdominal"));
                        npbCollection.put("symptom_pain", data.getString("symptom_pain"));
                        npbCollection.put("impact_wider_emergency", data.getString("impact_wider_emergency"));
                        npbCollection.put("impact_wider_appointment", data.getString("impact_wider_appointment"));
                        npbCollection.put("symptom_cognition", data.getString("symptom_cognition"));
                        npbCollection.put("symptom_bleeding_rate", data.getString("symptom_bleeding_rate"));
                        npbCollection.put("symptom_bleeding_impact", data.getString("symptom_bleeding_impact"));
                        npbCollection.put("symptom_breathlessness_rate", data.getString("symptom_breathlessness_rate"));
                        npbCollection.put("symptom_breathlessness_impact", data.getString("symptom_breathlessness_impact"));
                        npbCollection.put("symptom_fatigue_rate", data.getString("symptom_fatigue_rate"));
                        npbCollection.put("symptom_fatigue_impact", data.getString("symptom_fatigue_impact"));
                        npbCollection.put("symptom_enlarge_organ", data.getString("symptom_enlarge_organ"));
                        npbCollection.put("symptom_enlarge_organ_impact", data.getString("symptom_enlarge_organ_impact"));
                        npbCollection.put("symptom_slow_growth", data.getString("symptom_slow_growth"));
                        npbCollection.put("symptom_slow_growth_impact", data.getString("symptom_slow_growth_impact"));
                        npbCollection.put("symptom_infection", data.getString("symptom_infection"));
                        npbCollection.put("symptom_fracture", data.getString("symptom_fracture"));
                        CreateNpbFollowup();
                        break;
                    case "NpbApplicantParticipant":
                        npbApplicantCollection.put("symptom_bone", data.getString("symptom_bone"));
                        npbApplicantCollection.put("symptom_abdominal", data.getString("symptom_abdominal"));
                        npbApplicantCollection.put("symptom_pain", data.getString("symptom_pain"));
                        npbApplicantCollection.put("impact_wider_emergency", data.getString("impact_wider_emergency"));
                        npbApplicantCollection.put("impact_wider_appointment", data.getString("impact_wider_appointment"));
                        npbApplicantCollection.put("symptom_cognition", data.getString("symptom_cognition"));
                        npbApplicantCollection.put("symptom_bleeding_rate", data.getString("symptom_bleeding_rate"));
                        npbApplicantCollection.put("symptom_bleeding_impact", data.getString("symptom_bleeding_impact"));
                        npbApplicantCollection.put("symptom_breathlessness_rate", data.getString("symptom_breathlessness_rate"));
                        npbApplicantCollection.put("symptom_breathlessness_impact", data.getString("symptom_breathlessness_impact"));
                        npbApplicantCollection.put("symptom_fatigue_rate", data.getString("symptom_fatigue_rate"));
                        npbApplicantCollection.put("symptom_fatigue_impact", data.getString("symptom_fatigue_impact"));
                        npbApplicantCollection.put("symptom_enlarge_organ", data.getString("symptom_enlarge_organ"));
                        npbApplicantCollection.put("symptom_enlarge_organ_impact", data.getString("symptom_enlarge_organ_impact"));
                        npbApplicantCollection.put("symptom_slow_growth", data.getString("symptom_slow_growth"));
                        npbApplicantCollection.put("symptom_slow_growth_impact", data.getString("symptom_slow_growth_impact"));
                        npbApplicantCollection.put("symptom_infection", data.getString("symptom_infection"));
                        npbApplicantCollection.put("symptom_fracture", data.getString("symptom_fracture"));
                        CreateApplicantNpbFollowup();
                        break;
                    case "NpcParticipant":
                        npcCollection.put("impact_ambulation", data.getString("impact_ambulation"));
                        npcCollection.put("impact_manipulation", data.getString("impact_manipulation"));
                        npcCollection.put("impact_speech", data.getString("impact_speech"));
                        npcCollection.put("impact_swallowing", data.getString("impact_swallowing"));
                        npcCollection.put("impact_eye_movement", data.getString("impact_eye_movement"));
                        npcCollection.put("impact_seizure", data.getString("impact_seizure"));
                        npcCollection.put("impact_cognitive_impaired", data.getString("impact_cognitive_impaired"));
                        CreateNpcFollowup();
                        break;
                    case "NpcApplicantParticipant":
                        npcApplicantCollection.put("impact_ambulation", data.getString("impact_ambulation"));
                        npcApplicantCollection.put("impact_manipulation", data.getString("impact_manipulation"));
                        npcApplicantCollection.put("impact_speech", data.getString("impact_speech"));
                        npcApplicantCollection.put("impact_swallowing", data.getString("impact_swallowing"));
                        npcApplicantCollection.put("impact_eye_movement", data.getString("impact_eye_movement"));
                        npcApplicantCollection.put("impact_seizure", data.getString("impact_seizure"));
                        npcApplicantCollection.put("impact_cognitive_impaired", data.getString("impact_cognitive_impaired"));
                        CreateApplicantNpcFollowup();
                        break;
                }
            } catch (Exception e) {
                e.printStackTrace();
            }
        }


//        NiceSpinner niceSpinner = (NiceSpinner) view.findViewById(R.id.spinner);
//        List<String> dataset = new LinkedList<>(Arrays.asList("One", "Two", "Three", "Four", "Five"));
//        niceSpinner.attachDataSource(dataset);

        // ArrayAdapter<String> adapter = new ArrayAdapter<String>(getContext(), android.R.layout.simple_spinner_dropdown_item, countries);

//        NiceSpinnerAdapter adapter = new NiceSpinnerAdapter(getContext(), dataset, Color.DKGRAY, 2);
//        spinner.setAdapter(adapter);
        // Getting object reference to listview of main.xml
//        ListView listView = (ListView) view.findViewById(R.id.listView);
//
//        // Instantiating array adapter to populate the listView
//        // The layout android.R.layout.simple_list_item_single_choice creates radio button for each listview item
//
//        ArrayAdapter<String> adapter = new ArrayAdapter<String>(getContext(), android.R.layout.simple_list_item_single_choice, countries);
//
//        listView.setAdapter(adapter);

    }

    private void MapInitialization() {
//        npbIndexes.put("symptom_bone", 0);
//        npbIndexes.put("symptom_abdominal", 1);
//        npbIndexes.put("symptom_pain", 2);
//        npbIndexes.put("symptom_cognition", 3);
//        npbIndexes.put("symptom_breathlessness_rate", 4);
//        npbIndexes.put("symptom_breathlessness_impact", 5);
//        npbIndexes.put("symptom_bleeding_rate", 6);
//        npbIndexes.put("symptom_bleeding_impact", 7);
//        npbIndexes.put("symptom_fatigue_rate", 8);
//        npbIndexes.put("symptom_fatigue_impact", 9);
//        npbIndexes.put("symptom_enlarge_organ", 10);
//        npbIndexes.put("symptom_enlarge_organ_impact", 11);
//        npbIndexes.put("symptom_slow_growth", 12);
//        npbIndexes.put("symptom_slow_growth_impact", 13);
//        npbIndexes.put("symptom_infection", 14);
//        npbIndexes.put("symptom_fracture", 15);
        npbIndexes.put("impact_family_disappointed", 0);
        npbIndexes.put("impact_family_give_up", 1);
        npbIndexes.put("impact_family_worry_future", 2);
        npbIndexes.put("impact_family_closer", 3);
//        npbIndexes.put("impact_wider_self_miss_career", 20);
//        npbIndexes.put("impact_wider_carer_miss_career", 21);
//        npbIndexes.put("impact_wider_emergency", 22);
//        npbIndexes.put("impact_wider_appointment", 23);

        npbParams.add("impact_family_disappointed");
        npbParams.add("impact_family_give_up");
        npbParams.add("impact_family_worry_future");
        npbParams.add("impact_family_closer");
        npbParams.add("symptom_bone");
        npbParams.add("symptom_abdominal");
        npbParams.add("symptom_pain");
        npbParams.add("symptom_cognition");
        npbParams.add("symptom_breathlessness_rate");
        npbParams.add("symptom_breathlessness_impact");
        npbParams.add("symptom_bleeding_rate");
        npbParams.add("symptom_bleeding_impact");
        npbParams.add("symptom_fatigue_rate");
        npbParams.add("symptom_fatigue_impact");
        npbParams.add("symptom_enlarge_organ");
        npbParams.add("symptom_enlarge_organ_impact");
        npbParams.add("symptom_slow_growth");
        npbParams.add("symptom_slow_growth_impact");
        npbParams.add("symptom_infection");
        npbParams.add("symptom_fracture");
//        npbParams.add("impact_wider_self_miss_career");
//        npbParams.add("impact_wider_carer_miss_career");
//        npbParams.add("impact_wider_emergency");
//        npbParams.add("impact_wider_appointment");

//        npcIndexes.put("impact_ambulation", 0);
//        npcIndexes.put("impact_manipulation", 1);
//        npcIndexes.put("impact_speech", 2);
//        npcIndexes.put("impact_swallowing", 3);
//        npcIndexes.put("impact_eye_movement", 4);
//        npcIndexes.put("impact_seizure", 5);
//        npcIndexes.put("impact_cognitive_impaired", 6);
        npcIndexes.put("impact_family_disappointed", 0);
        npcIndexes.put("impact_family_give_up", 1);
        npcIndexes.put("impact_family_worry_future", 2);
        npcIndexes.put("impact_family_closer_family", 3);

        npcParams.add("impact_family_disappointed");
        npcParams.add("impact_family_give_up");
        npcParams.add("impact_family_worry_future");
        npcParams.add("impact_family_closer_family");
        npcParams.add("impact_ambulation");
        npcParams.add("impact_manipulation");
        npcParams.add("impact_speech");
        npcParams.add("impact_swallowing");
        npcParams.add("impact_eye_movement");
        npcParams.add("impact_seizure");
        npcParams.add("impact_cognitive_impaired");


    }


    @Override
    public <U extends APIItem> void receiveData(List<U> items, String type) {

    }

    @Override
    public <U extends APIItem> void receiveData(U item, String type) {
        switch (type) {
            case "createANpbFollowup":
                break;
            case "createANpcFollowup":
                break;
            case "error":
                ErrorMessage error = (ErrorMessage) item;
                final MaterialDialog mMaterialDialog_e = new MaterialDialog(getContext());
                mMaterialDialog_e.setTitle("Error");
                ErrorMessage errorMessage = (ErrorMessage) item;
                mMaterialDialog_e.setMessage(errorMessage.getErrorMes());
                mMaterialDialog_e.setPositiveButton("Ok", new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        mMaterialDialog_e.dismiss();
                    }
                });
                mMaterialDialog_e.show();
                break;
        }
    }

    private void CreateNpcFollowup() {
        TextView textDiscription = (TextView) getView().findViewById(R.id.text_explaination_family_followup);
        textDiscription.setText("The following section looks at how Niemann-Pick disease has impacted the family. " +
                "For each statement below, please tick whether at the present time you would strongly agree, agree, disagree or strongly disagree with the statement.");

        TextView textNpcCreate = (TextView) getView().findViewById(R.id.text_family_followup_title);
        textNpcCreate.setText("Create NP-C Followup Questionnaire");

        int index = 0;
        final ViewGroup insertPoint = (ViewGroup) getView().findViewById(R.id.lty_family_followup);
        LayoutInflater vi = (LayoutInflater) getActivity().getSystemService(Context.LAYOUT_INFLATER_SERVICE);

        List<String> answers_impact_family_disappointed = new ArrayList<>();
        answers_impact_family_disappointed.add(" ");
        for (int i = 0; i < NpcFollowupCollection.getImpact_family_collection().size(); i++) {
            answers_impact_family_disappointed.add(NpcFollowupCollection.getImpact_family_collection().get(i));
        }
        NpcInflater(vi, answers_impact_family_disappointed, "impact_family_disappointed", insertPoint, npcIndexes.get("impact_family_disappointed"), npcCollection, BasicInfo.getNpc_participant_followups_questions());
        index++;

        List<String> answers_impact_family_give_up = new ArrayList<>();
        answers_impact_family_give_up.add(" ");
        for (int i = 0; i < NpcFollowupCollection.getImpact_family_collection().size(); i++) {
            answers_impact_family_give_up.add(NpcFollowupCollection.getImpact_family_collection().get(i));
        }

        NpcInflater(vi, answers_impact_family_give_up, "impact_family_give_up", insertPoint, npcIndexes.get("impact_family_give_up"), npcCollection, BasicInfo.getNpc_participant_followups_questions());
        index++;

        List<String> answers_impact_family_worry_future = new ArrayList<>();
        answers_impact_family_worry_future.add(" ");
        for (int i = 0; i < NpcFollowupCollection.getImpact_family_collection().size(); i++) {
            answers_impact_family_worry_future.add(NpcFollowupCollection.getImpact_family_collection().get(i));
        }
        NpcInflater(vi, answers_impact_family_worry_future, "impact_family_worry_future", insertPoint, npcIndexes.get("impact_family_worry_future"), npcCollection, BasicInfo.getNpc_participant_followups_questions());
        index++;

        List<String> answers_impact_family_closer_family = new ArrayList<>();
        answers_impact_family_closer_family.add(" ");
        for (int i = 0; i < NpcFollowupCollection.getImpact_family_collection().size(); i++) {
            answers_impact_family_closer_family.add(NpcFollowupCollection.getImpact_family_collection().get(i));
        }
        NpcInflater(vi, answers_impact_family_closer_family, "impact_family_closer_family", insertPoint, npcIndexes.get("impact_family_closer_family"), npcCollection, BasicInfo.getNpc_participant_followups_questions());
        index++;

        Button buttonNext = (Button) getView().findViewById(R.id.button_followup_next);
        buttonNext.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                boolean isFullAnswer = true;
                for (int i = 0; i < npcIndexes.size(); i++) {
                    if (!npcCollection.containsKey(npcParams.get(i))) {
                        isFullAnswer = false;
                        Log.d("lack of params", npcParams.get(i) + " " + i);
                        TextView textQuestion = (TextView) insertPoint.getChildAt(i).findViewById(R.id.text_create_followup_question);
                        textQuestion.setHint(BasicInfo.getNpc_participant_followups_questions().get(npcParams.get(i)) + " cannot be blank");
                        textQuestion.setTextColor(Color.RED);
                    } else {
                        Log.d("got params", npcParams.get(i) + " " + i);
                        TextView textQuestion = (TextView) insertPoint.getChildAt(i).findViewById(R.id.text_create_followup_question);
                        textQuestion.setHint(BasicInfo.getNpc_participant_followups_questions().get(npcParams.get(i)) + " cannot be blank");
                        textQuestion.setTextColor(Color.BLACK);
                    }
                }
                if (isFullAnswer) {
                    Toast.makeText(getView().getContext(), "next!", Toast.LENGTH_SHORT).show();
                    Log.d("click1!", "!!!!!!!");
                    Bundle data = new Bundle();
                    data.putInt("followable_id", patientId);
                    data.putString("followable_type", patientType);
                    data.putString("impact_ambulation", npcCollection.get("impact_ambulation"));
                    data.putString("impact_manipulation", npcCollection.get("impact_manipulation"));
                    data.putString("impact_speech", npcCollection.get("impact_speech"));
                    data.putString("impact_swallowing", npcCollection.get("impact_swallowing"));
                    data.putString("impact_eye_movement", npcCollection.get("impact_eye_movement"));
                    data.putString("impact_seizure", npcCollection.get("impact_seizure"));
                    data.putString("impact_cognitive_impaired", npcCollection.get("impact_cognitive_impaired"));
                    data.putString("impact_family_disappointed", npcCollection.get("impact_family_disappointed"));
                    data.putString("impact_family_give_up", npcCollection.get("impact_family_give_up"));
                    data.putString("impact_family_worry_future", npcCollection.get("impact_family_worry_future"));
                    data.putString("impact_family_closer_family", npcCollection.get("impact_family_closer_family"));
                    data.putString("impact_wider_self_miss_career", npcCollection.get("impact_wider_self_miss_career"));
                    data.putString("impact_wider_family_miss_career", npcCollection.get("impact_wider_family_miss_career"));
                    data.putString("impact_wider_emergency", npcCollection.get("impact_wider_emergency"));
                    data.putString("impact_wider_appointment", npcCollection.get("impact_wider_appointment"));
                    data.putBoolean("first_time", isFirstTime);


                    FragmentManager fm = getFragmentManager();
                    FragmentTransaction transaction = fm.beginTransaction();
                    Fragment followupWiderImpactFragment;

                    followupWiderImpactFragment = new FollowupWiderImpactFragment();


                    followupWiderImpactFragment.setArguments(data);
//                ft.replace(R.id.main_view, followupWiderImpactFragment);
//                ft.addToBackStack(null);
//                ft.commit();


//                FragmentTransaction transaction = getFragmentManager()
//                        .beginTransaction();
                    transaction.add(R.id.main_view, followupWiderImpactFragment,
                            "FragmentWiderImpact");
                    transaction.addToBackStack("FragmentFamily");
                    transaction.show(followupWiderImpactFragment);
                    transaction.setTransition(FragmentTransaction.TRANSIT_FRAGMENT_CLOSE);
                    transaction.commit();
                    isFirstTime = false;
                } else {
                    Toast.makeText(getView().getContext(), "issue!", Toast.LENGTH_SHORT).show();
                }
            }
        });
    }


    private void CreateApplicantNpcFollowup() {
        TextView textDiscription = (TextView)getView().findViewById(R.id.text_explaination_family_followup);
        textDiscription.setText("The following section looks at how Niemann-Pick disease has impacted the family. " +
                "For each statement below, please tick whether at the present time you would strongly agree, agree, disagree or strongly disagree with the statement.");

        TextView textNpcCreate = (TextView)getView().findViewById(R.id.text_family_followup_title);
        textNpcCreate.setText("Create NP-C Followup Questionnaire");

        int index = 0;
        final ViewGroup insertPoint = (ViewGroup) getView().findViewById(R.id.lty_family_followup);
        LayoutInflater vi = (LayoutInflater) getActivity().getSystemService(Context.LAYOUT_INFLATER_SERVICE);

        List<String> answers_impact_family_disappointed = new ArrayList<>();
        answers_impact_family_disappointed.add(" ");
        for(int i = 0; i < NpcApplicantFollowupCollection.getImpact_family_collection().size(); i++){
            answers_impact_family_disappointed.add(NpcApplicantFollowupCollection.getImpact_family_collection().get(i));
        }
        NpcInflater(vi, answers_impact_family_disappointed, "impact_family_disappointed", insertPoint, npcIndexes.get("impact_family_disappointed"), npcApplicantCollection, BasicInfo.getNpc_applicant_participant_followups_questions());
        index++;

        List<String> answers_impact_family_give_up = new ArrayList<>();
        answers_impact_family_give_up.add(" ");
        for(int i = 0; i < NpcApplicantFollowupCollection.getImpact_family_collection().size(); i++){
            answers_impact_family_give_up.add(NpcApplicantFollowupCollection.getImpact_family_collection().get(i));
        }
        NpcInflater(vi, answers_impact_family_give_up, "impact_family_give_up", insertPoint, npcIndexes.get("impact_family_give_up"), npcApplicantCollection, BasicInfo.getNpc_applicant_participant_followups_questions());
        index++;

        List<String> answers_impact_family_worry_future = new ArrayList<>();
        answers_impact_family_worry_future.add(" ");
        for(int i = 0; i < NpcApplicantFollowupCollection.getImpact_family_collection().size(); i++){
            answers_impact_family_worry_future.add(NpcApplicantFollowupCollection.getImpact_family_collection().get(i));
        }
        NpcInflater(vi, answers_impact_family_worry_future, "impact_family_worry_future", insertPoint, npcIndexes.get("impact_family_worry_future"), npcApplicantCollection, BasicInfo.getNpc_applicant_participant_followups_questions());
        index++;

        List<String> answers_impact_family_closer_family = new ArrayList<>();
        answers_impact_family_closer_family.add(" ");
        for(int i = 0; i < NpcApplicantFollowupCollection.getImpact_family_collection().size(); i++){
            answers_impact_family_closer_family.add(NpcApplicantFollowupCollection.getImpact_family_collection().get(i));
        }
        NpcInflater(vi, answers_impact_family_closer_family, "impact_family_closer_family", insertPoint, npcIndexes.get("impact_family_closer_family"), npcApplicantCollection, BasicInfo.getNpc_applicant_participant_followups_questions());
        index++;

        Button buttonNext = (Button) getView().findViewById(R.id.button_followup_next);
        buttonNext.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                boolean isFullAnswer = true;
                for (int i = 0; i < npcIndexes.size(); i++) {
                    if(!npcApplicantCollection.containsKey(npcParams.get(i))){
                        isFullAnswer = false;
                        TextView textQuestion = (TextView)insertPoint.getChildAt(i).findViewById(R.id.text_create_followup_question);
                        textQuestion.setHint(BasicInfo.getNpc_applicant_participant_followups_questions().get(npcParams.get(i)) + " cannot be blank");
                        textQuestion.setTextColor(Color.RED);
                    }else {
                        TextView textQuestion = (TextView)insertPoint.getChildAt(i).findViewById(R.id.text_create_followup_question);
                        textQuestion.setHint(BasicInfo.getNpc_applicant_participant_followups_questions().get(npcParams.get(i))+ " cannot be blank");
                        textQuestion.setTextColor(Color.BLACK);
                    }
                }

                if(isFullAnswer) {
                    Toast.makeText(getView().getContext(), "next!", Toast.LENGTH_SHORT).show();
                    Log.d("click1!", "!!!!!!!");
                    Bundle data = new Bundle();
                    data.putInt("followable_id", patientId);
                    data.putString("followable_type", patientType);
                    data.putString("impact_ambulation", npcApplicantCollection.get("impact_ambulation"));
                    data.putString("impact_manipulation", npcApplicantCollection.get("impact_manipulation"));
                    data.putString("impact_speech", npcApplicantCollection.get("impact_speech"));
                    data.putString("impact_swallowing", npcApplicantCollection.get("impact_swallowing"));
                    data.putString("impact_eye_movement", npcApplicantCollection.get("impact_eye_movement"));
                    data.putString("impact_seizure", npcApplicantCollection.get("impact_seizure"));
                    data.putString("impact_cognitive_impaired", npcApplicantCollection.get("impact_cognitive_impaired"));
                    data.putString("impact_family_disappointed", npcApplicantCollection.get("impact_family_disappointed"));
                    data.putString("impact_family_give_up", npcApplicantCollection.get("impact_family_give_up"));
                    data.putString("impact_family_worry_future", npcApplicantCollection.get("impact_family_worry_future"));
                    data.putString("impact_family_closer_family", npcApplicantCollection.get("impact_family_closer_family"));
                    data.putString("impact_wider_self_miss_career", npcApplicantCollection.get("impact_wider_self_miss_career"));
                    data.putString("impact_wider_family_miss_career", npcApplicantCollection.get("impact_wider_family_miss_career"));
                    data.putString("impact_wider_emergency", npcApplicantCollection.get("impact_wider_emergency"));
                    data.putString("impact_wider_appointment", npcApplicantCollection.get("impact_wider_appointment"));
                    data.putBoolean("first_time", isFirstTime);

                    Log.d("family_give_up", npcApplicantCollection.get("impact_family_give_up") + "");

                    FragmentManager fm = getFragmentManager();
                    FragmentTransaction transaction = fm.beginTransaction();
                    Fragment followupWiderImpactFragment;

                    followupWiderImpactFragment = new FollowupWiderImpactFragment();


                    followupWiderImpactFragment.setArguments(data);
//                ft.replace(R.id.main_view, followupWiderImpactFragment);
//                ft.addToBackStack(null);
//                ft.commit();


//                FragmentTransaction transaction = getFragmentManager()
//                        .beginTransaction();
                    transaction.add(R.id.main_view, followupWiderImpactFragment,
                            "FragmentWiderImpact");
                    transaction.addToBackStack("FragmentFamily");
                    transaction.show(followupWiderImpactFragment);
                    transaction.setTransition(FragmentTransaction.TRANSIT_FRAGMENT_CLOSE);
                    transaction.commit();
                    isFirstTime = false;
                }else {
                    Toast.makeText(getView().getContext(), "issue!", Toast.LENGTH_SHORT).show();
                }
            }
        });
    }

    private void NpcInflater(LayoutInflater vi, final List<String> items, final String param, ViewGroup insertPoint, int index, final Map<String, String> answers, Map<String, String> questions) {
        View v = vi.inflate(R.layout.item_create_followup, null);
        final TextView textQuestion = (TextView)v.findViewById(R.id.text_create_followup_question);

        Log.d("flater!!","");
        textQuestion.setText(questions.get(param));
        final MaterialSpinner spinner = (MaterialSpinner) v.findViewById(R.id.spinner);
        spinner.setTextColor(getResources().getColor(R.color.colorPrimaryText));
        spinner.setItems(items);

        if(!isFirstTime){
            SharedPreferences myPrefs = getActivity().getSharedPreferences("myPrefs", 0);

            if(myPrefs.contains(param)){
                spinner.setSelectedIndex(myPrefs.getInt(param, 0));
                Log.d("get prefs", myPrefs.getInt(param, 0)+ " " + param);
            }
        }


        spinner.setOnItemSelectedListener(new MaterialSpinner.OnItemSelectedListener<String>() {

            @Override
            public void onItemSelected(MaterialSpinner view, int position, long id, String item) {
                if (position != 0) {
                    textQuestion.setTextColor(Color.BLACK);
                    answers.put(param, position - 1 + "");
                } else {
                    if (answers.containsKey(param)) {
                        answers.remove(param);
                    }
                }

                int mSpnValue = position;
                SharedPreferences myPrefs = getActivity().getSharedPreferences("myPrefs", getContext().MODE_MULTI_PROCESS);
                SharedPreferences.Editor prefsEditor = myPrefs.edit();
                prefsEditor.putInt(param, mSpnValue);
                prefsEditor.commit();
                Log.d("valuee", " " + param + ", " + mSpnValue);

//                Context context = view.getContext();
//                CharSequence text = answers.get(param) + " " + param;
//                int duration = Toast.LENGTH_SHORT;
//
//                Toast toast = Toast.makeText(context, text, duration);
//                toast.show();
            }
        });
        if(spinner.getSelectedIndex() != 0) {
            answers.put(param, spinner.getSelectedIndex() - 1 + "");
        }
        spinner.onSaveInstanceState();

        insertPoint.addView(
                v,
                index,
                new ViewGroup.LayoutParams(ViewGroup.LayoutParams.MATCH_PARENT, ViewGroup.LayoutParams.WRAP_CONTENT));
    }


    private void CreateNpbFollowup() {
        final APIReceivable receivable = this;
        final INPDRAPI api = new INPDRAPI(getContext());
        int index = 0;
        final ViewGroup insertPoint = (ViewGroup) getView().findViewById(R.id.lty_family_followup);
        LayoutInflater vi = (LayoutInflater) getActivity().getSystemService(Context.LAYOUT_INFLATER_SERVICE);


        List<String> answers_impact_family_disappointed = new ArrayList<>();
        answers_impact_family_disappointed.add(" ");
        for(int i = 0; i < NpbFollowupCollection.getImpact_family_collection().size(); i++){
            answers_impact_family_disappointed.add(NpbFollowupCollection.getImpact_family_collection().get(i));
        }
        NpbInflater(vi, answers_impact_family_disappointed, "impact_family_disappointed", insertPoint, npbIndexes.get("impact_family_disappointed"), npbCollection, BasicInfo.getNpb_followups_questions());
        index++;

        List<String> answers_impact_family_give_up = new ArrayList<>();
        answers_impact_family_give_up.add(" ");
        for(int i = 0; i < NpbFollowupCollection.getImpact_family_collection().size(); i++){
            answers_impact_family_give_up.add(NpbFollowupCollection.getImpact_family_collection().get(i));
        }
        NpbInflater(vi, answers_impact_family_give_up, "impact_family_give_up", insertPoint, npbIndexes.get("impact_family_give_up"), npbCollection, BasicInfo.getNpb_followups_questions());
        index++;

        List<String> answers_impact_family_worry_future = new ArrayList<>();
        answers_impact_family_worry_future.add(" ");
        for(int i = 0; i < NpbFollowupCollection.getImpact_family_collection().size(); i++){
            answers_impact_family_worry_future.add(NpbFollowupCollection.getImpact_family_collection().get(i));
        }
        NpbInflater(vi, answers_impact_family_worry_future, "impact_family_worry_future", insertPoint, npbIndexes.get("impact_family_worry_future"), npbCollection, BasicInfo.getNpb_followups_questions());
        index++;

        List<String> answers_impact_family_closer = new ArrayList<>();
        answers_impact_family_closer.add(" ");
        for(int i = 0; i < NpbFollowupCollection.getImpact_family_collection().size(); i++){
            answers_impact_family_closer.add(NpbFollowupCollection.getImpact_family_collection().get(i));
        }
        NpbInflater(vi, answers_impact_family_closer, "impact_family_closer", insertPoint, npbIndexes.get("impact_family_closer"), npbCollection, BasicInfo.getNpb_followups_questions());
        index++;

        Button buttonCreate = (Button) getView().findViewById(R.id.button_followup_next);
        buttonCreate.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                boolean isFullAnswer = true;
                for (int i = 0; i < npbIndexes.size(); i++) {
                    if(!npbCollection.containsKey(npbParams.get(i))){
                        isFullAnswer = false;
                        Log.d("lack of param", npbParams.get(i) + " " + i);
                        TextView textQuestion = (TextView)insertPoint.getChildAt(i).findViewById(R.id.text_create_followup_question);
                        textQuestion.setHint(BasicInfo.getNpb_followups_questions().get(npbParams.get(i)) + " cannot be blank");
                        textQuestion.setTextColor(Color.RED);
                    }else {
                        Log.d("got param", npbParams.get(i) + " " + i);
                        TextView textQuestion = (TextView)insertPoint.getChildAt(i).findViewById(R.id.text_create_followup_question);
                        textQuestion.setHint(BasicInfo.getNpb_followups_questions().get(npbParams.get(i)) + " cannot be blank");
                        textQuestion.setTextColor(Color.BLACK);
                    }
                }

                if(isFullAnswer) {
                    Toast.makeText(getView().getContext(), "next!", Toast.LENGTH_SHORT).show();
                    Log.d("click1!", "!!!!!!!");
                    Bundle data = new Bundle();
                    data.putInt("followable_id", patientId);
                    data.putString("followable_type", patientType);
                    data.putString("symptom_bone", npbCollection.get("symptom_bone"));
                    data.putString("symptom_abdominal", npbCollection.get("symptom_abdominal"));
                    data.putString("symptom_pain", npbCollection.get("symptom_pain"));
                    data.putString("impact_wider_emergency", npbCollection.get("impact_wider_emergency"));
                    data.putString("impact_wider_appointment", npbCollection.get("impact_wider_appointment"));
                    data.putString("symptom_cognition", npbCollection.get("symptom_cognition"));
                    data.putString("symptom_breathlessness_rate", npbCollection.get("symptom_breathlessness_rate"));
                    data.putString("symptom_breathlessness_impact", npbCollection.get("symptom_breathlessness_impact"));
                    data.putString("symptom_bleeding_rate", npbCollection.get("symptom_bleeding_rate"));
                    data.putString("symptom_bleeding_impact", npbCollection.get("symptom_bleeding_impact"));
                    data.putString("symptom_fatigue_rate", npbCollection.get("symptom_fatigue_rate"));
                    data.putString("symptom_fatigue_impact", npbCollection.get("symptom_fatigue_impact"));
                    data.putString("symptom_enlarge_organ", npbCollection.get("symptom_enlarge_organ"));
                    data.putString("symptom_enlarge_organ_impact", npbCollection.get("symptom_enlarge_organ_impact"));
                    data.putString("symptom_slow_growth", npbCollection.get("symptom_slow_growth"));
                    data.putString("symptom_slow_growth_impact", npbCollection.get("symptom_slow_growth_impact"));
                    data.putString("symptom_infection", npbCollection.get("symptom_infection"));
                    data.putString("symptom_fracture", npbCollection.get("symptom_fracture"));
                    data.putString("impact_family_disappointed", npbCollection.get("impact_family_disappointed"));
                    data.putString("impact_family_give_up", npbCollection.get("impact_family_give_up"));
                    data.putString("impact_family_worry_future", npbCollection.get("impact_family_worry_future"));
                    data.putString("impact_family_closer", npbCollection.get("impact_family_closer"));
                    data.putString("impact_wider_self_miss_career", npbCollection.get("impact_wider_self_miss_career"));
                    data.putString("impact_wider_carer_miss_career", npbCollection.get("impact_wider_carer_miss_career"));
                    data.putBoolean("first_time", isFirstTime);


                    FragmentManager fm = getFragmentManager();
                    FragmentTransaction transaction = fm.beginTransaction();
                    Fragment followupWiderImpactFragment;

                    followupWiderImpactFragment = new FollowupWiderImpactFragment();


                    followupWiderImpactFragment.setArguments(data);
//                ft.replace(R.id.main_view, followupWiderImpactFragment);
//                ft.addToBackStack(null);
//                ft.commit();


//                FragmentTransaction transaction = getFragmentManager()
//                        .beginTransaction();
                    transaction.add(R.id.main_view, followupWiderImpactFragment,
                            "FragmentWiderImpact");
                    transaction.addToBackStack("FragmentFamily");
                    transaction.show(followupWiderImpactFragment);
                    transaction.setTransition(FragmentTransaction.TRANSIT_FRAGMENT_CLOSE);
                    transaction.commit();
                    isFirstTime = false;
                }else {
                    Toast.makeText(getView().getContext(), "issue!", Toast.LENGTH_SHORT).show();
                }
            }
        });

//        Spinner spinner = (Spinner) v.findViewById(R.id.spinner);
//        ArrayAdapter<String> adapter = new ArrayAdapter<String>(getContext(), android.R.layout.simple_spinner_dropdown_item, answers_bone_pain);
//
//        spinner.setAdapter(adapter);

    }

    private void NpbInflater(LayoutInflater vi, final List<String> items, final String param, ViewGroup insertPoint, int index, final Map<String, String> answers, Map<String, String> questions) {
        View v = vi.inflate(R.layout.item_create_followup, null);
        final TextView textQuestion = (TextView)v.findViewById(R.id.text_create_followup_question);

        Log.d("flater!!","");
        textQuestion.setText(questions.get(param));
        final MaterialSpinner spinner = (MaterialSpinner) v.findViewById(R.id.spinner);
        spinner.setTextColor(getResources().getColor(R.color.colorPrimaryText));
        spinner.setItems(items);

        if(!isFirstTime){
            SharedPreferences myPrefs = getActivity().getSharedPreferences("myPrefs", 0);

            if(myPrefs.contains(param)){
                spinner.setSelectedIndex(myPrefs.getInt(param, 0));
                Log.d("get prefs", myPrefs.getInt(param, 0)+ " " + param);
            }
        }


        spinner.setOnItemSelectedListener(new MaterialSpinner.OnItemSelectedListener<String>() {

            @Override
            public void onItemSelected(MaterialSpinner view, int position, long id, String item) {
                if (position != 0) {
                    textQuestion.setTextColor(Color.BLACK);
                    answers.put(param, position - 1 + "");
                } else {
                    if (answers.containsKey(param)) {
                        answers.remove(param);
                    }
                }
                int mSpnValue = position;
                SharedPreferences myPrefs = getActivity().getSharedPreferences("myPrefs", getContext().MODE_MULTI_PROCESS);
                SharedPreferences.Editor prefsEditor = myPrefs.edit();
                prefsEditor.putInt(param, mSpnValue);
                prefsEditor.commit();
                Log.d("valuee", " " + param + ", " + mSpnValue);

//                Context context = view.getContext();
//                CharSequence text = answers.get(param) + " " + param;
//                int duration = Toast.LENGTH_SHORT;
//
//                Toast toast = Toast.makeText(context, text, duration);
//                toast.show();
            }
        });
        if(spinner.getSelectedIndex() != 0) {
            answers.put(param, spinner.getSelectedIndex() - 1 + "");
        }

        spinner.onSaveInstanceState();

        insertPoint.addView(
                v,
                index,
                new ViewGroup.LayoutParams(ViewGroup.LayoutParams.MATCH_PARENT, ViewGroup.LayoutParams.WRAP_CONTENT));
    }


    private void CreateApplicantNpbFollowup() {
        final APIReceivable receivable = this;
        final INPDRAPI api = new INPDRAPI(getContext());
        int index = 0;
        final ViewGroup insertPoint = (ViewGroup) getView().findViewById(R.id.lty_family_followup);
        LayoutInflater vi = (LayoutInflater) getActivity().getSystemService(Context.LAYOUT_INFLATER_SERVICE);


        List<String> answers_impact_family_disappointed = new ArrayList<>();
        answers_impact_family_disappointed.add(" ");
        for(int i = 0; i < NpbApplicantFollowupCollection.getImpact_family_collection().size(); i++){
            answers_impact_family_disappointed.add(NpbApplicantFollowupCollection.getImpact_family_collection().get(i));
        }
        NpbInflater(vi, answers_impact_family_disappointed, "impact_family_disappointed", insertPoint, npbIndexes.get("impact_family_disappointed"), npbApplicantCollection, BasicInfo.getNpb_followups_questions());
        index++;

        List<String> answers_impact_family_give_up = new ArrayList<>();
        answers_impact_family_give_up.add(" ");
        for(int i = 0; i < NpbApplicantFollowupCollection.getImpact_family_collection().size(); i++){
            answers_impact_family_give_up.add(NpbApplicantFollowupCollection.getImpact_family_collection().get(i));
        }
        NpbInflater(vi, answers_impact_family_give_up, "impact_family_give_up", insertPoint, npbIndexes.get("impact_family_give_up"), npbApplicantCollection, BasicInfo.getNpb_followups_questions());
        index++;

        List<String> answers_impact_family_worry_future = new ArrayList<>();
        answers_impact_family_worry_future.add(" ");
        for(int i = 0; i < NpbApplicantFollowupCollection.getImpact_family_collection().size(); i++){
            answers_impact_family_worry_future.add(NpbApplicantFollowupCollection.getImpact_family_collection().get(i));
        }
        NpbInflater(vi, answers_impact_family_worry_future, "impact_family_worry_future", insertPoint, npbIndexes.get("impact_family_worry_future"), npbApplicantCollection, BasicInfo.getNpb_followups_questions());
        index++;

        List<String> answers_impact_family_closer = new ArrayList<>();
        answers_impact_family_closer.add(" ");
        for(int i = 0; i < NpbApplicantFollowupCollection.getImpact_family_collection().size(); i++){
            answers_impact_family_closer.add(NpbApplicantFollowupCollection.getImpact_family_collection().get(i));
        }
        NpbInflater(vi, answers_impact_family_closer, "impact_family_closer", insertPoint, npbIndexes.get("impact_family_closer"), npbApplicantCollection, BasicInfo.getNpb_followups_questions());
        index++;

        Button buttonNext = (Button) getView().findViewById(R.id.button_followup_next);
        buttonNext.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                boolean isFullAnswer = true;
                for (int i = 0; i < npbIndexes.size(); i++) {
                    if(!npbApplicantCollection.containsKey(npbParams.get(i))){
                        isFullAnswer = false;
                        TextView textQuestion = (TextView)insertPoint.getChildAt(i).findViewById(R.id.text_create_followup_question);
                        textQuestion.setHint(BasicInfo.getNpb_applicant_participant_followups_questions().get(npbParams.get(i)) + " cannot be blank");
                        textQuestion.setTextColor(Color.RED);
                    } else {
                        TextView textQuestion = (TextView) insertPoint.getChildAt(i).findViewById(R.id.text_create_followup_question);
                        textQuestion.setHint(BasicInfo.getNpb_applicant_participant_followups_questions().get(npbParams.get(i)) + " cannot be blank");
                        textQuestion.setTextColor(Color.BLACK);
                    }
                }

                if(isFullAnswer) {
                    Toast.makeText(getView().getContext(), "next!", Toast.LENGTH_SHORT).show();
                    Log.d("click1!", "!!!!!!!");
                    Bundle data = new Bundle();
                    data.putInt("followable_id", patientId);
                    data.putString("followable_type", patientType);
                    data.putString("symptom_bone", npbApplicantCollection.get("symptom_bone"));
                    data.putString("symptom_abdominal", npbApplicantCollection.get("symptom_abdominal"));
                    data.putString("symptom_pain", npbApplicantCollection.get("symptom_pain"));
                    data.putString("impact_wider_emergency", npbApplicantCollection.get("impact_wider_emergency"));
                    data.putString("impact_wider_appointment", npbApplicantCollection.get("impact_wider_appointment"));
                    data.putString("symptom_cognition", npbApplicantCollection.get("symptom_cognition"));
                    data.putString("symptom_breathlessness_rate", npbApplicantCollection.get("symptom_breathlessness_rate"));
                    data.putString("symptom_breathlessness_impact", npbApplicantCollection.get("symptom_breathlessness_impact"));
                    data.putString("symptom_bleeding_rate", npbApplicantCollection.get("symptom_bleeding_rate"));
                    data.putString("symptom_bleeding_impact", npbApplicantCollection.get("symptom_bleeding_impact"));
                    data.putString("symptom_fatigue_rate", npbApplicantCollection.get("symptom_fatigue_rate"));
                    data.putString("symptom_fatigue_impact", npbApplicantCollection.get("symptom_fatigue_impact"));
                    data.putString("symptom_enlarge_organ", npbApplicantCollection.get("symptom_enlarge_organ"));
                    data.putString("symptom_enlarge_organ_impact", npbApplicantCollection.get("symptom_enlarge_organ_impact"));
                    data.putString("symptom_slow_growth", npbApplicantCollection.get("symptom_slow_growth"));
                    data.putString("symptom_slow_growth_impact", npbApplicantCollection.get("symptom_slow_growth_impact"));
                    data.putString("symptom_infection", npbApplicantCollection.get("symptom_infection"));
                    data.putString("symptom_fracture", npbApplicantCollection.get("symptom_fracture"));
                    data.putString("impact_family_disappointed", npbApplicantCollection.get("impact_family_disappointed"));
                    data.putString("impact_family_give_up", npbApplicantCollection.get("impact_family_give_up"));
                    data.putString("impact_family_worry_future", npbApplicantCollection.get("impact_family_worry_future"));
                    data.putString("impact_family_closer", npbApplicantCollection.get("impact_family_closer"));
                    data.putString("impact_wider_self_miss_career", npbApplicantCollection.get("impact_wider_self_miss_career"));
                    data.putString("impact_wider_carer_miss_career", npbApplicantCollection.get("impact_wider_carer_miss_career"));
                    data.putBoolean("first_time", isFirstTime);


                    FragmentManager fm = getFragmentManager();
                    FragmentTransaction transaction = fm.beginTransaction();
                    Fragment followupWiderImpactFragment;

                    followupWiderImpactFragment = new FollowupWiderImpactFragment();

                    followupWiderImpactFragment.setArguments(data);
//                ft.replace(R.id.main_view, followupWiderImpactFragment);
//                ft.addToBackStack(null);
//                ft.commit();


//                FragmentTransaction transaction = getFragmentManager()
//                        .beginTransaction();
                    transaction.add(R.id.main_view, followupWiderImpactFragment,
                            "FragmentWiderImpact");
                    transaction.addToBackStack("FragmentFamily");
                    transaction.show(followupWiderImpactFragment);
                    transaction.setTransition(FragmentTransaction.TRANSIT_FRAGMENT_CLOSE);
                    transaction.commit();
                    isFirstTime = false;
                }else {
                    Toast.makeText(getView().getContext(), "issue!", Toast.LENGTH_SHORT).show();
                }
            }
        });

    }

}
