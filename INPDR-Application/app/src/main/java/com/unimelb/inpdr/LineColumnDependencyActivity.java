package com.unimelb.inpdr;

import android.os.Bundle;
import android.os.Debug;
import android.support.v4.app.Fragment;
import android.support.v7.app.ActionBarActivity;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Toast;

import com.unimelb.inpdr.Models.API.APIItem;
import com.unimelb.inpdr.Models.API.APIReceivable;
import com.unimelb.inpdr.Models.API.INPDRAPI;
import com.unimelb.inpdr.Models.API.ModelFacade;
import com.unimelb.inpdr.Models.BasicInfo;
import com.unimelb.inpdr.Models.NpbApplicantFollowupCollection;
import com.unimelb.inpdr.Models.NpbFollowUp;
import com.unimelb.inpdr.Models.NpbFollowupCollection;
import com.unimelb.inpdr.Models.NpcApplicantFollowupCollection;
import com.unimelb.inpdr.Models.NpcFollowUp;
import com.unimelb.inpdr.Models.NpcFollowupCollection;
import com.unimelb.inpdr.Models.Selection;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import lecho.lib.hellocharts.gesture.ZoomType;
import lecho.lib.hellocharts.listener.ColumnChartOnValueSelectListener;
import lecho.lib.hellocharts.listener.LineChartOnValueSelectListener;
import lecho.lib.hellocharts.model.Axis;
import lecho.lib.hellocharts.model.AxisValue;
import lecho.lib.hellocharts.model.Column;
import lecho.lib.hellocharts.model.ColumnChartData;
import lecho.lib.hellocharts.model.Line;
import lecho.lib.hellocharts.model.LineChartData;
import lecho.lib.hellocharts.model.PointValue;
import lecho.lib.hellocharts.model.SubcolumnValue;
import lecho.lib.hellocharts.model.Viewport;
import lecho.lib.hellocharts.util.ChartUtils;
import lecho.lib.hellocharts.view.ColumnChartView;
import lecho.lib.hellocharts.view.LineChartView;

public class LineColumnDependencyActivity extends ActionBarActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_line_column_dependency);
        if (savedInstanceState == null) {
            getSupportFragmentManager().beginTransaction().add(R.id.container, new PlaceholderFragment()).commit();
        }
    }

    /**
     * A placeholder fragment containing a simple view.
     */
    public static class PlaceholderFragment extends Fragment implements APIReceivable{
        Map<Integer, Integer> npbIndexes = new HashMap<>();
        Map<Integer, Integer> npcIndexes = new HashMap<>();
        List<String> npbParams = new ArrayList<>();
        List<String> npcParams = new ArrayList<>();

        public void MapInitialization() {
            npbIndexes.put(0, 5);
            npbIndexes.put(1, 5);
            npbIndexes.put(2, 5);
            npbIndexes.put(3, 5);
            npbIndexes.put(4, 5);
            npbIndexes.put(5, 5);
            npbIndexes.put(6, 5);
            npbIndexes.put(7, 5);
            npbIndexes.put(8, 5);
            npbIndexes.put(9, 5);
            npbIndexes.put(10, 3);
            npbIndexes.put(11, 5);
            npbIndexes.put(12, 3);
            npbIndexes.put(13, 5);
            npbIndexes.put(14, 4);
            npbIndexes.put(15, 4);
            npbIndexes.put(16, 4);
            npbIndexes.put(17, 4);
            npbIndexes.put(18, 4);
            npbIndexes.put(19, 4);
            npbIndexes.put(20, 6);
            npbIndexes.put(21, 6);
            npbIndexes.put(22, 4);
            npbIndexes.put(23, 4);

            npbParams.add("symptom_bone");
            npbParams.add("symptom_abdominal");
            npbParams.add("symptom_pain");
            npbParams.add("symptom_cognition");
            npbParams.add("symptom_breathlessness_rate");
            npbParams.add("symptom_breathlessness_impact");
            npbParams.add("symptom_bleeding_rate");
            npbParams.add("symptom_bleeding_impact");
            npbParams.add("symptom_fatigue_rate");
            npbParams.add("symptom_fatigue_impact");
            npbParams.add("symptom_enlarge_organ");
            npbParams.add("symptom_enlarge_organ_impact");
            npbParams.add("symptom_slow_growth");
            npbParams.add("symptom_slow_growth_impact");
            npbParams.add("symptom_infection");
            npbParams.add("symptom_fracture");
            npbParams.add("impact_family_disappointed");
            npbParams.add("impact_family_give_up");
            npbParams.add("impact_family_worry_future");
            npbParams.add("impact_family_closer");
            npbParams.add("impact_wider_self_miss_career");
            npbParams.add("impact_wider_carer_miss_career");
            npbParams.add("impact_wider_emergency");
            npbParams.add("impact_wider_appointment");

            npcIndexes.put(0, 6);
            npcIndexes.put(1, 5);
            npcIndexes.put(2, 4);
            npcIndexes.put(3, 5);
            npcIndexes.put(4, 4);
            npcIndexes.put(5, 4);
            npcIndexes.put(6, 4);
            npcIndexes.put(7, 4);
            npcIndexes.put(8, 4);
            npcIndexes.put(9, 4);
            npcIndexes.put(10, 4);
            npcIndexes.put(11, 6);
            npcIndexes.put(12, 6);
            npcIndexes.put(13, 4);
            npcIndexes.put(14, 4);


            npcParams.add("impact_ambulation");
            npcParams.add("impact_manipulation");
            npcParams.add("impact_speech");
            npcParams.add("impact_swallowing");
            npcParams.add("impact_eye_movement");
            npcParams.add("impact_seizure");
            npcParams.add("impact_cognitive_impaired");
            npcParams.add("impact_family_disappointed");
            npcParams.add("impact_family_give_up");
            npcParams.add("impact_family_worry_future");
            npcParams.add("impact_family_closer_family");
            npcParams.add("impact_wider_self_miss_career");
            npcParams.add("impact_wider_family_miss_career");
            npcParams.add("impact_wider_emergency");
            npcParams.add("impact_wider_appointment");
            return;
        }

        final Map<Integer, String> monthIndexes = new HashMap<Integer, String>(){{
            put(0, "Jan");
            put(1, "Feb");
            put(2, "Mar");
            put(3, "Apr");
            put(4, "May");
            put(5, "Jun");
            put(6, "Jul");
            put(7, "Aug");
            put(8, "Sep");
            put(9, "Oct");
            put(10, "Nov");
            put(11, "Dec");
        }

        };


        public final static String[] questions_npc = new String[]{"Q1", "Q2", "Q3", "Q4", "Q5", "Q6", "Q7", "Q8",
                "Q9", "Q10", "Q11", "Q12", "Q13", "Q14", "Q15",};

        public final static String[] questions_npb = new String[]{"Q1", "Q2", "Q3", "Q4", "Q5", "Q6", "Q7", "Q8",
                "Q9", "Q10", "Q11", "Q12", "Q13", "Q14", "Q15", "Q17", "Q18", "Q19", "Q20", "Q21", "Q22", "Q23"};

        public final static String[] months = new String[]{"Jan", "Feb", "Mar", "Apr", "May", "Jun", "Jul", "Aug",
                "Sep", "Oct", "Nov", "Dec",};

        private String patientType;
        private int patientId;
        private int followupId;

        private LineChartView chartTop;
        private ColumnChartView chartBottom;

        private LineChartData lineData;
        private ColumnChartData columnData;

        private ArrayList<NpbFollowUp> npbFollowUps = new ArrayList<>();
        private ArrayList<NpcFollowUp> npcFollowUps = new ArrayList<>();

        public PlaceholderFragment() {
        }

        @Override
        public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
            View rootView = inflater.inflate(R.layout.fragment_line_column_dependency, container, false);

            MapInitialization();

            BasicInfo.getInstance();
            NpbFollowupCollection.getInstance();
            NpbApplicantFollowupCollection.getInstance();
            NpcFollowupCollection.getInstance();
            NpcApplicantFollowupCollection.getInstance();
            Selection.getInstance();

            Bundle extras = getActivity().getIntent().getExtras();
            final INPDRAPI api = new INPDRAPI(getContext());
            if(extras != null){
                patientType = extras.getString("followable_type");
                patientId = extras.getInt("followable_id");
                followupId = extras.getInt("followup_id");

                switch (patientType){
                    case "NpbParticipant":case "NpbApplicantParticipant":
                        ModelFacade.getInstance().getANpbFollowup(this, api, patientType, patientId, followupId);
                        break;
                    case "NpcParticipant":case "NpcApplicantParticipant":
                        ModelFacade.getInstance().getANpcFollowup(this, api, patientType, patientId, followupId);
                        break;
                }
                Log.d("extras ", patientType + " id " + patientId + " followup " + followupId);
            }
            // *** TOP LINE CHART ***
            chartTop = (LineChartView) rootView.findViewById(R.id.chart_top);
            chartTop.setOnValueTouchListener(new ValueTouchListener_line());

            // Generate and set data for line chart
            generateInitialLineData();

            // *** BOTTOM COLUMN CHART ***

            chartBottom = (ColumnChartView) rootView.findViewById(R.id.chart_bottom);

            return rootView;
        }

        private void generateColumnData(APIItem feed) {
            int numColumns = 0;
            NpbFollowUp npbFollowUp;
            NpcFollowUp npcFollowUp;

            List npbAnswers = new ArrayList<Integer>();
            List npcAnswers = new ArrayList<Integer>();

            String[] questions = {""};
            switch (patientType){
                case "NpbParticipant":case "NpbApplicantParticipant":
                    numColumns = questions_npb.length;
                    questions = questions_npb;
                    npbFollowUp = (NpbFollowUp) feed;
                    npbAnswers.add(npbFollowUp.getSymptom_bone());
                    npbAnswers.add(npbFollowUp.getSymptom_abdominal());
                    npbAnswers.add(npbFollowUp.getSymptom_pain());
                    npbAnswers.add(npbFollowUp.getSymptom_cognition());
                    npbAnswers.add(npbFollowUp.getSymptom_breathlessness_rate());
                    npbAnswers.add(npbFollowUp.getSymptom_breathlessness_impact());
                    npbAnswers.add(npbFollowUp.getSymptom_bleeding_rate());
                    npbAnswers.add(npbFollowUp.getSymptom_bleeding_impact());
                    npbAnswers.add(npbFollowUp.getSymptom_fatigue_rate());
                    npbAnswers.add(npbFollowUp.getSymptom_fatigue_impact());
                    npbAnswers.add(npbFollowUp.getSymptom_enlarge_organ());
                    npbAnswers.add(npbFollowUp.getSymptom_enlarge_organ_impact());
                    npbAnswers.add(npbFollowUp.getSymptom_slow_growth());
                    npbAnswers.add(npbFollowUp.getSymptom_slow_growth_impact());
                    npbAnswers.add(npbFollowUp.getSymptom_infection());
                    npbAnswers.add(npbFollowUp.getSymptom_fracture());
                    npbAnswers.add(npbFollowUp.getImpact_family_disappointed());
                    npbAnswers.add(npbFollowUp.getImpact_family_give_up());
                    npbAnswers.add(npbFollowUp.getImpact_family_worry_future());
                    npbAnswers.add(npbFollowUp.getImpact_family_closer());
                    npbAnswers.add(npbFollowUp.getImpact_wider_self_miss_career());
                    npbAnswers.add(npbFollowUp.getImpact_wider_carer_miss_career());
                    npbAnswers.add(npbFollowUp.getImpact_wider_emergency());
                    npbAnswers.add(npbFollowUp.getImpact_wider_appointment());
                    break;
                case "NpcParticipant":case "NpcApplicantParticipant":
                    numColumns = questions_npc.length;
                    questions = questions_npc;
                    npcFollowUp = (NpcFollowUp) feed;
                    npcAnswers.add(npcFollowUp.getImpact_ambulation());
                    npcAnswers.add(npcFollowUp.getImpact_manipulation());
                    npcAnswers.add(npcFollowUp.getImpact_speech());
                    npcAnswers.add(npcFollowUp.getImpact_swallowing());
                    npcAnswers.add(npcFollowUp.getImpact_eye_movement());
                    npcAnswers.add(npcFollowUp.getImpact_seizure());
                    npcAnswers.add(npcFollowUp.getImpact_cognitive_impaired());
                    npcAnswers.add(npcFollowUp.getImpact_family_disappointed());
                    npcAnswers.add(npcFollowUp.getImpact_family_give_up());
                    npcAnswers.add(npcFollowUp.getImpact_family_worry_future());
                    npcAnswers.add(npcFollowUp.getImpact_family_closer_family());
                    npcAnswers.add(npcFollowUp.getImpact_wider_self_miss_career());
                    npcAnswers.add(npcFollowUp.getImpact_wider_family_miss_career());
                    npcAnswers.add(npcFollowUp.getImpact_wider_emergency());
                    npcAnswers.add(npcFollowUp.getImpact_wider_appointment());
                    break;
            }
            int numSubcolumns = 1;

            List<AxisValue> axisValues = new ArrayList<AxisValue>();
            List<Column> columns = new ArrayList<Column>();
            List<SubcolumnValue> values;
            for (int i = 0; i < numColumns; ++i) {

                values = new ArrayList<SubcolumnValue>();
                for (int j = 0; j < numSubcolumns; ++j) {
                    int answer = 0;
                    float value = 0;
                    switch (patientType){
                        case "NpbParticipant":case "NpbApplicantParticipant":
                            answer = (Integer)npbAnswers.get(i) + 1;
                            //Log.d("getdata ", npbAnswers.get(i) + " " + i + " out of" + npbIndexes.get(i));
                            value = (float)answer/(float)npbIndexes.get(i) * 100;
                            values.add(new SubcolumnValue(value, ChartUtils.pickColor()));
                            break;
                        case "NpcParticipant":case "NpcApplicantParticipant":
                            answer = (Integer)npcAnswers.get(i) + 1;
                            value = (float)answer/(float)npcIndexes.get(i) * 100;
                            //Log.d("getdata ", npcAnswers.get(i) + " " + i + "");
                            values.add(new SubcolumnValue(value, ChartUtils.pickColor()));
                            break;
                    }

                }

                axisValues.add(new AxisValue(i).setLabel(questions[i]));

                columns.add(new Column(values).setHasLabelsOnlyForSelected(true));
            }

            List<AxisValue> axisValues_1 = new ArrayList<AxisValue>();

            for(int i = 0; i < 10; i++){
                axisValues_1.add((new AxisValue(i).setLabel(i * 10+"")));
            }
            columnData = new ColumnChartData(columns);
            columnData.setStacked(true);

            columnData.setAxisXBottom(new Axis(axisValues).setHasLines(true));
            columnData.setAxisYLeft(new Axis().setHasLines(true).setMaxLabelChars(3));

            chartBottom.setColumnChartData(columnData);

            Viewport v ;
            switch (patientType){
                case "NpbParticipant":case "NpbApplicantParticipant":
                    v = new Viewport(-1, 110, 24, 0);
                    break;
                case "NpcParticipant":case "NpcApplicantParticipant":
                    v = new Viewport(-1, 110, 15, 0);
                    break;
                default:
                    v = new Viewport(-1, 110, 15, 0);
            }

            chartBottom.setMaximumViewport(v);
            chartBottom.setCurrentViewport(v);

            // Set value touch listener that will trigger changes for chartTop.
            chartBottom.setOnValueTouchListener(new ValueTouchListener());

            // Set selection mode to keep selected month column highlighted.
            chartBottom.setValueSelectionEnabled(true);

            chartBottom.setZoomType(ZoomType.HORIZONTAL);

            // chartBottom.setOnClickListener(new View.OnClickListener() {
            //
            // @Override
            // public void onClick(View v) {
            // SelectedValue sv = chartBottom.getSelectedValue();
            // if (!sv.isSet()) {
            // generateInitialLineData();
            // }
            //
            // }
            // });

        }

        /**
         * Generates initial data for line chart. At the begining all Y values are equals 0. That will change when user
         * will select value on column chart.
         */
        private void generateInitialLineData() {
            int numValues = months.length;

            List<AxisValue> axisValues = new ArrayList<AxisValue>();
            List<PointValue> values = new ArrayList<PointValue>();
            for (int i = 0; i < numValues; ++i) {
                values.add(new PointValue(i, 0));
                axisValues.add(new AxisValue(i).setLabel(months[i]));
            }

            Line line = new Line(values);
            line.setColor(ChartUtils.COLOR_GREEN).setCubic(true);

            List<Line> lines = new ArrayList<Line>();
            lines.add(line);

            lineData = new LineChartData(lines);
            lineData.setAxisXBottom(new Axis(axisValues).setHasLines(true));
            lineData.setAxisYLeft(new Axis().setHasLines(true).setMaxLabelChars(3));

            chartTop.setLineChartData(lineData);
            chartTop.canScrollHorizontally(1);
            chartTop.setClickable(true);
            chartTop.dispatchDisplayHint(View.VISIBLE);

            // For build-up animation you have to disable viewport recalculation.
            chartTop.setViewportCalculationEnabled(false);

            // And set initial max viewport and current viewport- remember to set viewports after data.
            Viewport v = new Viewport(0, 100, 11, 0);
            chartTop.setMaximumViewport(v);
            chartTop.setCurrentViewport(v);

            chartTop.setZoomType(ZoomType.HORIZONTAL);
        }

        private void generateLineData(int color, float range) {
            // Cancel last animation if not finished.
            chartTop.cancelDataAnimation();

            // Modify data targets
            Line line = lineData.getLines().get(0);// For this example there is always only one line.
            line.setColor(color);
            for (PointValue value : line.getValues()) {
                // Change target only for Y value.
                value.setTarget(value.getX(), (float) Math.random() * range);
            }

            // Start new data animation with 300ms duration;
            chartTop.startDataAnimation(300);
        }

        @Override
        public <U extends APIItem> void receiveData(List<U> items, String type) {
            switch (type){
                case "getANpbFollowup":
                    npbFollowUps.clear();
                    for (U item : items) {
                        NpbFollowUp feed = (NpbFollowUp) item;
                        generateColumnData(feed);
                        npbFollowUps.add(feed);
                        //Log.d("!!!!!data received!!!", feed.getUser().getEmail() + "");
                    }
                    break;
                case "getANpcFollowup":
                    npcFollowUps.clear();
                    for (U item : items) {
                        NpcFollowUp feed = (NpcFollowUp) item;
                        generateColumnData(feed);
                        npcFollowUps.add(feed);
                    }
                    break;
            }
        }

        @Override
        public <U extends APIItem> void receiveData(U item, String type) {

        }

        private class ValueTouchListener implements ColumnChartOnValueSelectListener {

            @Override
            public void onValueSelected(int columnIndex, int subcolumnIndex, SubcolumnValue value) {
                int finalValue = 0;
                String answer = "";
                switch (patientType){
                    case "NpbParticipant":case "NpbApplicantParticipant":
                        finalValue = Math.round(value.getValue()/100 * npbIndexes.get(columnIndex)) - 1;
                        //Log.d("value ", "colum " + columnIndex + " "+value.getValue()/100 * npbIndexes.get(columnIndex));
                        answer = getAnswers(npbParams.get(columnIndex), finalValue);
                        Toast.makeText(getActivity(), String.format("%s:  [%d, %s]", npbParams.get(columnIndex), finalValue, answer), Toast.LENGTH_SHORT).show();
                        break;
                    case "NpcParticipant":case "NpcApplicantParticipant":
                        finalValue = Math.round(value.getValue()/100 * npcIndexes.get(columnIndex)) - 1;
                        //Log.d("value ", "colum " + columnIndex + " "+value.getValue()/100 * npcIndexes.get(columnIndex));
                        answer = getAnswers(npcParams.get(columnIndex), finalValue);
                        Toast.makeText(getActivity(), String.format("%s:  [%d, %s]", npcParams.get(columnIndex), finalValue, answer), Toast.LENGTH_SHORT).show();
                        break;
                }
                generateLineData(value.getColor(), 100);
            }

            @Override
            public void onValueDeselected() {

                generateLineData(ChartUtils.COLOR_GREEN, 0);

            }
        }

        private String getAnswers(String param, int index) {
            String answer = "";
            switch (patientType){
                case "NpbParticipant":
                    switch (param){
                        case "symptom_bone":
                            answer = NpbFollowupCollection.getSymptom_bone_collection().get(index);
                            break;
                        case "symptom_abdominal":
                            answer = NpbFollowupCollection.getSymptom_abdominal_collection().get(index);
                            break;
                        case "symptom_pain":
                            answer = NpbFollowupCollection.getSymptom_pain_collection().get(index);
                            break;
                        case "symptom_breathlessness_rate":
                            answer = NpbFollowupCollection.getSymptom_breathlessness_rate_collection().get(index);
                            break;
                        case "symptom_breathlessness_impact":
                            answer = NpbFollowupCollection.getSymptom_breathlessness_impact_collection().get(index);
                            break;
                        case "symptom_bleeding_rate":
                            answer = NpbFollowupCollection.getSymptom_bleeding_rate_collection().get(index);
                            break;
                        case "symptom_bleeding_impact":
                            answer = NpbFollowupCollection.getSymptom_bleeding_impact_collection().get(index);
                            break;
                        case "symptom_fatigue_rate":
                            answer = NpbFollowupCollection.getSymptom_fatigue_rate_collection().get(index);
                            break;
                        case "symptom_fatigue_impact":
                            answer = NpbFollowupCollection.getSymptom_fatigue_impact_collection().get(index);
                            break;
                        case "symptom_enlarge_organ":
                            answer = NpbFollowupCollection.getSymptom_enlarge_organ_collection().get(index);
                            break;
                        case "symptom_enlarge_organ_impact":
                            answer = NpbFollowupCollection.getSymptom_enlarge_organ_impact_collection().get(index);
                            break;
                        case "symptom_slow_growth":
                            answer = NpbFollowupCollection.getSymptom_slow_growth_collection().get(index);
                            break;
                        case "symptom_slow_growth_impact":
                            answer = NpbFollowupCollection.getSymptom_slow_growth_impact_collection().get(index);
                            break;
                        case "symptom_infection":
                            answer = NpbFollowupCollection.getSymptom_infection_collection().get(index);
                            break;
                        case "symptom_fracture":
                            answer = NpbFollowupCollection.getSymptom_fracture_collection().get(index);
                            break;
                        case "symptom_cognition":
                            answer = NpbFollowupCollection.getSymptom_cognition_collection().get(index);
                            break;
                        case "impact_family_disappointed":
                            answer = NpbFollowupCollection.getImpact_family_collection().get(index);
                            break;
                        case "impact_family_give_up":
                            answer = NpbFollowupCollection.getImpact_family_collection().get(index);
                            break;
                        case "impact_family_worry_future":
                            answer = NpbFollowupCollection.getImpact_family_collection().get(index);
                            break;
                        case "impact_family_closer":
                            answer = NpbFollowupCollection.getImpact_family_collection().get(index);
                            break;
                        case "impact_wider_self_miss_career":
                            answer = NpbFollowupCollection.getImpact_wider_self_miss_career().get(index);
                            break;
                        case "impact_wider_carer_miss_career":
                            answer = NpbFollowupCollection.getImpact_wider_carer_miss_career().get(index);
                            break;
                        case "impact_wider_emergency":
                            answer = NpbFollowupCollection.getImpact_wider_emergency().get(index);
                            break;
                        case "impact_wider_appointment":
                            answer = NpbFollowupCollection.getImpact_wider_appointment().get(index);
                            break;
                    }
                    break;
                case "NpbApplicantParticipant":
                    switch (param){
                        case "symptom_bone":
                            answer = NpbApplicantFollowupCollection.getSymptom_bone_collection().get(index);
                            break;
                        case "symptom_abdominal":
                            answer = NpbApplicantFollowupCollection.getSymptom_abdominal_collection().get(index);
                            break;
                        case "symptom_pain":
                            answer = NpbApplicantFollowupCollection.getSymptom_pain_collection().get(index);
                            break;
                        case "symptom_breathlessness_rate":
                            answer = NpbApplicantFollowupCollection.getSymptom_breathlessness_rate_collection().get(index);
                            break;
                        case "symptom_breathlessness_impact":
                            answer = NpbApplicantFollowupCollection.getSymptom_breathlessness_impact_collection().get(index);
                            break;
                        case "symptom_bleeding_rate":
                            answer = NpbApplicantFollowupCollection.getSymptom_bleeding_rate_collection().get(index);
                            break;
                        case "symptom_bleeding_impact":
                            answer = NpbApplicantFollowupCollection.getSymptom_bleeding_impact_collection().get(index);
                            break;
                        case "symptom_fatigue_rate":
                            answer = NpbApplicantFollowupCollection.getSymptom_fatigue_rate_collection().get(index);
                            break;
                        case "symptom_fatigue_impact":
                            answer = NpbApplicantFollowupCollection.getSymptom_fatigue_impact_collection().get(index);
                            break;
                        case "symptom_enlarge_organ":
                            answer = NpbApplicantFollowupCollection.getSymptom_enlarge_organ_collection().get(index);
                            break;
                        case "symptom_enlarge_organ_impact":
                            answer = NpbApplicantFollowupCollection.getSymptom_enlarge_organ_impact_collection().get(index);
                            break;
                        case "symptom_slow_growth":
                            answer = NpbApplicantFollowupCollection.getSymptom_slow_growth_collection().get(index);
                            break;
                        case "symptom_slow_growth_impact":
                            answer = NpbApplicantFollowupCollection.getSymptom_slow_growth_impact_collection().get(index);
                            break;
                        case "symptom_infection":
                            answer = NpbApplicantFollowupCollection.getSymptom_infection_collection().get(index);
                            break;
                        case "symptom_fracture":
                            answer = NpbApplicantFollowupCollection.getSymptom_fracture_collection().get(index);
                            break;
                        case "symptom_cognition":
                            answer = NpbApplicantFollowupCollection.getSymptom_cognition_collection().get(index);
                            break;
                        case "impact_family_disappointed":
                            answer = NpbApplicantFollowupCollection.getImpact_family_collection().get(index);
                            break;
                        case "impact_family_give_up":
                            answer = NpbApplicantFollowupCollection.getImpact_family_collection().get(index);
                            break;
                        case "impact_family_worry_future":
                            answer = NpbApplicantFollowupCollection.getImpact_family_collection().get(index);
                            break;
                        case "impact_family_closer":
                            answer = NpbApplicantFollowupCollection.getImpact_family_collection().get(index);
                            break;
                        case "impact_wider_self_miss_career":
                            answer = NpbApplicantFollowupCollection.getImpact_wider_self_miss_career().get(index);
                            break;
                        case "impact_wider_carer_miss_career":
                            answer = NpbApplicantFollowupCollection.getImpact_wider_carer_miss_career().get(index);
                            break;
                        case "impact_wider_emergency":
                            answer = NpbApplicantFollowupCollection.getImpact_wider_emergency().get(index);
                            break;
                        case "impact_wider_appointment":
                            answer = NpbApplicantFollowupCollection.getImpact_wider_appointment().get(index);
                            break;
                    }
                    break;
                case "NpcParticipant":
                    switch (param){
                        case "impact_ambulation":
                            answer = NpcFollowupCollection.getImpact_ambulation().get(index);
                            break;
                        case "impact_manipulation":
                            answer = NpcFollowupCollection.getImpact_manipulation().get(index);
                            break;
                        case "impact_speech":
                            answer = NpcFollowupCollection.getImpact_speech().get(index);
                            break;
                        case "impact_swallowing":
                            answer = NpcFollowupCollection.getImpact_swallowing().get(index);
                            break;
                        case "impact_eye_movement":
                            answer = NpcFollowupCollection.getImpact_eye_movement().get(index);
                            break;
                        case "impact_seizure":
                            answer = NpcFollowupCollection.getImpact_seizure().get(index);
                            break;
                        case "impact_cognitive_impaired":
                            answer = NpcFollowupCollection.getImpact_cognitive_impaired().get(index);
                            break;
                        case "impact_family_disappointed":
                            answer = NpcFollowupCollection.getImpact_family_collection().get(index);
                            break;
                        case "impact_family_give_up":
                            answer = NpcFollowupCollection.getImpact_family_collection().get(index);
                            break;
                        case "impact_family_worry_future":
                            answer = NpcFollowupCollection.getImpact_family_collection().get(index);
                            break;
                        case "impact_family_closer_family":
                            answer = NpcFollowupCollection.getImpact_family_collection().get(index);
                            break;
                        case "impact_wider_self_miss_career":
                            answer = NpcFollowupCollection.getImpact_wider_self_miss_career().get(index);
                            break;
                        case "impact_wider_family_miss_career":
                            answer = NpcFollowupCollection.getImpact_wider_family_miss_career().get(index);
                            break;
                        case "impact_wider_emergency":
                            answer = NpcFollowupCollection.getImpact_wider_emergency().get(index);
                            break;
                        case "impact_wider_appointment":
                            answer = NpcFollowupCollection.getImpact_wider_appointment().get(index);
                            break;
                    }
                    break;
                case "NpcApplicantParticipant":
                    switch (param) {
                        case "impact_ambulation":
                            answer = NpcApplicantFollowupCollection.getImpact_ambulation().get(index);
                            break;
                        case "impact_manipulation":
                            answer = NpcApplicantFollowupCollection.getImpact_manipulation().get(index);
                            break;
                        case "impact_speech":
                            answer = NpcApplicantFollowupCollection.getImpact_speech().get(index);
                            break;
                        case "impact_swallowing":
                            answer = NpcApplicantFollowupCollection.getImpact_swallowing().get(index);
                            break;
                        case "impact_eye_movement":
                            answer = NpcApplicantFollowupCollection.getImpact_eye_movement().get(index);
                            break;
                        case "impact_seizure":
                            answer = NpcApplicantFollowupCollection.getImpact_seizure().get(index);
                            break;
                        case "impact_cognitive_impaired":
                            answer = NpcApplicantFollowupCollection.getImpact_cognitive_impaired().get(index);
                            break;
                        case "impact_family_disappointed":
                            answer = NpcApplicantFollowupCollection.getImpact_family_collection().get(index);
                            break;
                        case "impact_family_give_up":
                            answer = NpcApplicantFollowupCollection.getImpact_family_collection().get(index);
                            break;
                        case "impact_family_worry_future":
                            answer = NpcApplicantFollowupCollection.getImpact_family_collection().get(index);
                            break;
                        case "impact_family_closer_family":
                            answer = NpcApplicantFollowupCollection.getImpact_family_collection().get(index);
                            break;
                        case "impact_wider_self_miss_career":
                            answer = NpcApplicantFollowupCollection.getImpact_wider_self_miss_career().get(index);
                            break;
                        case "impact_wider_family_miss_career":
                            answer = NpcApplicantFollowupCollection.getImpact_wider_family_miss_career().get(index);
                            break;
                        case "impact_wider_emergency":
                            answer = NpcApplicantFollowupCollection.getImpact_wider_emergency().get(index);
                            break;
                        case "impact_wider_appointment":
                            answer = NpcApplicantFollowupCollection.getImpact_wider_appointment().get(index);
                            break;
                    }
                    break;
            }
            return answer;

        }

        private class ValueTouchListener_line implements LineChartOnValueSelectListener {

            @Override
            public void onValueSelected(int lineIndex, int pointIndex, PointValue value) {
                Toast.makeText(getActivity(), String.format("%s:  %.1f", months[pointIndex], value.getY()), Toast.LENGTH_SHORT).show();
            }

            @Override
            public void onValueDeselected() {
                // TODO Auto-generated method stub
            }

        }
    }
}
