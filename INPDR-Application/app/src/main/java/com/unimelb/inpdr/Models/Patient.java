package com.unimelb.inpdr.Models;

import android.util.Log;

import com.unimelb.inpdr.Models.API.APIItem;

import org.json.JSONArray;
import org.json.JSONObject;

/**
 * Created by Xintong on 2/05/2016.
 */
public class Patient extends APIItem{
    private int id;
    private String registrant_first_name;
    private String registrant_last_name;
    private int relation_to_patient;
    private int addresses_id;
    private String patient_first_name;
    private String patient_last_name;
    private int gender;
    private String date_of_birth;
    private String created_at;
    private String updated_at;
    private int user_id;
    private User user;
    private boolean verified;
    private String unique_id;
    private boolean consent;
    private String consent_name;
    private int enrollable_id;
    private String enrollable_type;
    private boolean not_finish;
    private NpcParticipant npcParticipant;
    private NpbParticipant npbParticipant;
    private NpaApplicantParticipant npaApplicantParticipant;
    private NpcApplicantParticipant npcApplicantParticipant;
    private NpbApplicantParticipant npbApplicantParticipant;
    private APIItem item;

    public Patient(JSONObject obj){
        try {
            setId(obj.optInt("id"));
            setRegistrant_first_name(obj.optString("registrant_first_name"));
            setRegistrant_last_name(obj.optString("registrant_last_name"));
            setRelation_to_patient(obj.optInt("relation_to_patient"));
            setAddresses_id(obj.optInt("addresses_id"));
            setPatient_first_name(obj.optString("patient_first_name"));
            setPatient_last_name(obj.optString("patient_last_name"));
            setGender(obj.optInt("gender"));
            setDate_of_birth(obj.optString("date_of_birth"));
            setCreated_at(obj.optString("created_at"));
            setUpdated_at(obj.optString("updated_at"));
            setUser_id(obj.optInt("user_id"));
            setConsent(obj.optBoolean("consent"));
            setConsent_name(obj.optString("consent_name"));
//            JSONArray aData = new JSONArray(obj.optString("user"));

            JSONObject userObject = new JSONObject(obj.optString("user"));
            user = new User(userObject);
            Log.d("user ", user.getEmail());
            setUser(user);
            setVerified(obj.optBoolean("verified"));
            setUnique_id(obj.optString("unique_id"));
            setEnrollable_id(obj.optInt("enrollable_id"));
            setEnrollable_type(obj.optString("enrollable_type"));
            setNot_finish(obj.optBoolean("not_finish"));

            JSONObject patientObject = new JSONObject(obj.optString("enrollable"));
            if(getEnrollable_type().equals("NpcParticipant")){
                setItem(new NpcParticipant(patientObject));
            }
            else if(getEnrollable_type().equals("NpbParticipant")){
                setItem(new NpbParticipant(patientObject));
            }
            else if(getEnrollable_type().equals("NpaApplicantParticipant")){
                setItem(new NpaApplicantParticipant(patientObject));
            }
            else if(getEnrollable_type().equals("NpbApplicantParticipant")) {
                setItem(new NpbApplicantParticipant(patientObject));
            }
            else if(getEnrollable_type().equals("NpcApplicantParticipant")) {
                setItem(new NpcApplicantParticipant(patientObject));
            }
            else {
                setItem(null);
            }
            setItem(item);
        }catch (Exception e){
            e.printStackTrace();
        }
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getRegistrant_first_name() {
        return registrant_first_name;
    }

    public void setRegistrant_first_name(String registrant_first_name) {
        this.registrant_first_name = registrant_first_name;
    }

    public String getRegistrant_last_name() {
        return registrant_last_name;
    }

    public void setRegistrant_last_name(String registrant_last_name) {
        this.registrant_last_name = registrant_last_name;
    }

    public int getRelation_to_patient() {
        return relation_to_patient;
    }

    public void setRelation_to_patient(int relation_to_patient) {
        this.relation_to_patient = relation_to_patient;
    }

    public int getAddresses_id() {
        return addresses_id;
    }

    public void setAddresses_id(int addresses_id) {
        this.addresses_id = addresses_id;
    }

    public String getPatient_first_name() {
        return patient_first_name;
    }

    public void setPatient_first_name(String patient_first_name) {
        this.patient_first_name = patient_first_name;
    }

    public String getPatient_last_name() {
        return patient_last_name;
    }

    public void setPatient_last_name(String patient_last_name) {
        this.patient_last_name = patient_last_name;
    }

    public int getGender() {
        return gender;
    }

    public void setGender(int gender) {
        this.gender = gender;
    }

    public String getDate_of_birth() {
        return date_of_birth;
    }

    public void setDate_of_birth(String date_of_birth) {
        this.date_of_birth = date_of_birth;
    }

    public String getCreated_at() {
        return created_at;
    }

    public void setCreated_at(String created_at) {
        this.created_at = created_at;
    }

    public String getUpdated_at() {
        return updated_at;
    }

    public void setUpdated_at(String updated_at) {
        this.updated_at = updated_at;
    }

    public int getUser_id() {
        return user_id;
    }

    public void setUser_id(int user_id) {
        this.user_id = user_id;
    }


    public boolean isVerified() {
        return verified;
    }

    public void setVerified(boolean verified) {
        this.verified = verified;
    }

    public String getUnique_id() {
        return unique_id;
    }

    public void setUnique_id(String unique_id) {
        this.unique_id = unique_id;
    }

    public int getEnrollable_id() {
        return enrollable_id;
    }

    public void setEnrollable_id(int enrollable_id) {
        this.enrollable_id = enrollable_id;
    }

    public String getEnrollable_type() {
        return enrollable_type;
    }

    public void setEnrollable_type(String enrollable_type) {
        this.enrollable_type = enrollable_type;
    }

    public boolean isNot_finish() {
        return not_finish;
    }

    public void setNot_finish(boolean not_finish) {
        this.not_finish = not_finish;
    }

    public User getUser() {
        return user;
    }

    public void setUser(User user) {
        this.user = user;
    }

    public APIItem getItem() {
        return item;
    }

    public void setItem(APIItem item) {
        this.item = item;
    }

    public boolean isConsent() {
        return consent;
    }

    public void setConsent(boolean consent) {
        this.consent = consent;
    }

    public String isConsent_name() {
        return consent_name;
    }

    public void setConsent_name(String consent_name) {
        this.consent_name = consent_name;
    }
}
