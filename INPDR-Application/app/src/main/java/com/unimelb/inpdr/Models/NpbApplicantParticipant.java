package com.unimelb.inpdr.Models;

import android.util.Log;

import com.unimelb.inpdr.Models.API.APIItem;

import org.json.JSONException;
import org.json.JSONObject;

/**
 * Created by Xintong on 20/04/2016.
 */
public class NpbApplicantParticipant extends APIItem{
    private int id;
    private String created_at;
    private String updated_at;
    private boolean age_diagnosed_known;
    private int age_diagnosed_year;
    private int age_diagnosed_month;
    private int family_diagnosed;
    private String family_diagnosed_relation;
    private int applicant_relation;
    private String applicant_relation_specify;
    private boolean gene_test_performed;
    private int symptom_age_year;
    private int symptom_age_month;
    private boolean symptom_age_unknown;
    private boolean symptom_age_na;
    private boolean symptom_enlarged_liver;
    private boolean symptom_breathing;
    private boolean symptom_anaemia;
    private boolean symptom_bleeding;
    private boolean symptom_growth_delay;
    private boolean symptom_developmental_delay;
    private boolean symptom_bone_pain;
    private boolean symptom_abdominal_pain;
    private boolean symptom_other;
    private String symptom_other_specify;
    private String hospital_clinician_name;
    private String gene_diagnosis;
    private boolean unable_to_contact_specialist;
    private boolean not_finish;

    public NpbApplicantParticipant (JSONObject obj) {
        setId(obj.optInt("id"));
        setCreated_at(obj.optString("created_at"));
        setUpdated_at(obj.optString("updated_at"));
        setAge_diagnosed_known(obj.optBoolean("age_diagnosed_known"));
        setAge_diagnosed_year(obj.optInt("age_diagnosed_year"));
        setAge_diagnosed_month(obj.optInt("age_diagnosed_month"));
        setFamily_diagnosed(obj.optInt("family_diagnosed"));
        setFamily_diagnosed_relation(obj.optString("family_diagnosed_relation"));
        setApplicant_relation(obj.optInt("applicant_relation"));
        setApplicant_relation_specify(obj.optString("applicant_relation_specify"));
        setGene_test_performed(obj.optBoolean("gene_test_performed"));
        setSymptom_age_year(obj.optInt("symptom_age_year"));
        setSymptom_age_month(obj.optInt("symptom_age_month"));
        setSymptom_age_unknown(obj.optBoolean("symptom_age_unknown"));
        setSymptom_age_na(obj.optBoolean("symptom_age_na"));
        setSymptom_enlarged_liver(obj.optBoolean("symptom_enlarged_liver"));
        setSymptom_breathing(obj.optBoolean("symptom_breathing"));
        setSymptom_anaemia(obj.optBoolean("symptom_anaemia"));
        setSymptom_bleeding(obj.optBoolean("symptom_bleeding"));
        setSymptom_growth_delay(obj.optBoolean("symptom_growth_delay"));
        setSymptom_developmental_delay(obj.optBoolean("symptom_developmental_delay"));
        setSymptom_bone_pain(obj.optBoolean("symptom_bone_pain"));
        setSymptom_abdominal_pain(obj.optBoolean("symptom_abdominal_pain"));
        setSymptom_other(obj.optBoolean("symptom_other"));
        setSymptom_other_specify(obj.optString("symptom_other_specify"));
        setHospital_clinician_name(obj.optString("hospital_clinician_name"));
        try {
            JSONObject urlObject = new JSONObject(obj.optString("gene_diagnosis"));
            String url = urlObject.optString("url");
            setGene_diagnosis(url);
        } catch (JSONException e) {
            e.printStackTrace();
        }
        setUnable_to_contact_specialist(obj.optBoolean("unable_to_contact_specialist"));
        setNot_finish(obj.optBoolean("not_finish"));
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getCreated_at() {
        return created_at;
    }

    public void setCreated_at(String created_at) {
        this.created_at = created_at;
    }

    public String getUpdated_at() {
        return updated_at;
    }

    public void setUpdated_at(String updated_at) {
        this.updated_at = updated_at;
    }

    public boolean isAge_diagnosed_known() {
        return age_diagnosed_known;
    }

    public void setAge_diagnosed_known(boolean age_diagnosed_known) {
        this.age_diagnosed_known = age_diagnosed_known;
    }

    public int getAge_diagnosed_year() {
        return age_diagnosed_year;
    }

    public void setAge_diagnosed_year(int age_diagnosed_year) {
        this.age_diagnosed_year = age_diagnosed_year;
    }

    public int getAge_diagnosed_month() {
        return age_diagnosed_month;
    }

    public void setAge_diagnosed_month(int age_diagnosed_month) {
        this.age_diagnosed_month = age_diagnosed_month;
    }

    public int getFamily_diagnosed() {
        return family_diagnosed;
    }

    public void setFamily_diagnosed(int family_diagnosed) {
        this.family_diagnosed = family_diagnosed;
    }

    public String getFamily_diagnosed_relation() {
        return family_diagnosed_relation;
    }

    public void setFamily_diagnosed_relation(String family_diagnosed_relation) {
        this.family_diagnosed_relation = family_diagnosed_relation;
    }

    public int getApplicant_relation() {
        return applicant_relation;
    }

    public void setApplicant_relation(int applicant_relation) {
        this.applicant_relation = applicant_relation;
    }

    public String getApplicant_relation_specify() {
        return applicant_relation_specify;
    }

    public void setApplicant_relation_specify(String applicant_relation_specify) {
        this.applicant_relation_specify = applicant_relation_specify;
    }

    public boolean isGene_test_performed() {
        return gene_test_performed;
    }

    public void setGene_test_performed(boolean gene_test_performed) {
        this.gene_test_performed = gene_test_performed;
    }

    public int getSymptom_age_year() {
        return symptom_age_year;
    }

    public void setSymptom_age_year(int symptom_age_year) {
        this.symptom_age_year = symptom_age_year;
    }

    public int getSymptom_age_month() {
        return symptom_age_month;
    }

    public void setSymptom_age_month(int symptom_age_month) {
        this.symptom_age_month = symptom_age_month;
    }

    public boolean isSymptom_age_unknown() {
        return symptom_age_unknown;
    }

    public void setSymptom_age_unknown(boolean symptom_age_unknown) {
        this.symptom_age_unknown = symptom_age_unknown;
    }

    public boolean isSymptom_age_na() {
        return symptom_age_na;
    }

    public void setSymptom_age_na(boolean symptom_age_na) {
        this.symptom_age_na = symptom_age_na;
    }

    public boolean isSymptom_enlarged_liver() {
        return symptom_enlarged_liver;
    }

    public void setSymptom_enlarged_liver(boolean symptom_enlarged_liver) {
        this.symptom_enlarged_liver = symptom_enlarged_liver;
    }

    public boolean isSymptom_breathing() {
        return symptom_breathing;
    }

    public void setSymptom_breathing(boolean symptom_breathing) {
        this.symptom_breathing = symptom_breathing;
    }

    public boolean isSymptom_anaemia() {
        return symptom_anaemia;
    }

    public void setSymptom_anaemia(boolean symptom_anaemia) {
        this.symptom_anaemia = symptom_anaemia;
    }

    public boolean isSymptom_bleeding() {
        return symptom_bleeding;
    }

    public void setSymptom_bleeding(boolean symptom_bleeding) {
        this.symptom_bleeding = symptom_bleeding;
    }

    public boolean isSymptom_growth_delay() {
        return symptom_growth_delay;
    }

    public void setSymptom_growth_delay(boolean symptom_growth_delay) {
        this.symptom_growth_delay = symptom_growth_delay;
    }

    public boolean isSymptom_developmental_delay() {
        return symptom_developmental_delay;
    }

    public void setSymptom_developmental_delay(boolean symptom_developmental_delay) {
        this.symptom_developmental_delay = symptom_developmental_delay;
    }

    public boolean isSymptom_bone_pain() {
        return symptom_bone_pain;
    }

    public void setSymptom_bone_pain(boolean symptom_bone_pain) {
        this.symptom_bone_pain = symptom_bone_pain;
    }

    public boolean isSymptom_abdominal_pain() {
        return symptom_abdominal_pain;
    }

    public void setSymptom_abdominal_pain(boolean symptom_abdominal_pain) {
        this.symptom_abdominal_pain = symptom_abdominal_pain;
    }

    public boolean isSymptom_other() {
        return symptom_other;
    }

    public void setSymptom_other(boolean symptom_other) {
        this.symptom_other = symptom_other;
    }

    public String getSymptom_other_specify() {
        return symptom_other_specify;
    }

    public void setSymptom_other_specify(String symptom_other_specify) {
        this.symptom_other_specify = symptom_other_specify;
    }

    public String getHospital_clinician_name() {
        return hospital_clinician_name;
    }

    public void setHospital_clinician_name(String hospital_clinician_name) {
        this.hospital_clinician_name = hospital_clinician_name;
    }

    public String getGene_diagnosis() {
        return gene_diagnosis;
    }

    public void setGene_diagnosis(String gene_diagnosis) {
        this.gene_diagnosis = gene_diagnosis;
    }

    public boolean isUnable_to_contact_specialist() {
        return unable_to_contact_specialist;
    }

    public void setUnable_to_contact_specialist(boolean unable_to_contact_specialist) {
        this.unable_to_contact_specialist = unable_to_contact_specialist;
    }

    public boolean isNot_finish() {
        return not_finish;
    }

    public void setNot_finish(boolean not_finish) {
        this.not_finish = not_finish;
    }
}
