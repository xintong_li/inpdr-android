package com.unimelb.inpdr.Models.API;

/**
 * Created by Xintong on 20/04/2016.
 */
public class Params {
    public static final String ALL_PATIENTS = "http://10.0.3.2:3000/api/v1/patients/";
    public static final String A_PATIENT = "http://10.0.3.2:3000/api/v1/patients/%d";
    public static final String ALL_NPC_PARTICIPANTS = "http://10.0.3.2:3000/api/v1/npc_participants";
    public static final String A_NPC_PARTICIPANT = "http://10.0.3.2:3000/api/v1/npc_participants/%d";
    public static final String ALL_APPLICANT_NPC_PARTICIPANTS = "";
    public static final String A_APPLICANT_NPC_PARTICIPANTS = "";
    public static final String ALL_NPB_PARTICIPANTS = "";
    public static final String A_NPB_PARTICIPANT = "";
    public static final String ALL_APPLICANT_NPB_PARTICIPANTS = "http://10.0.3.2:3000/api/v1/npb_applicant_participants";
    public static final String A_APPLICANT_NPB_PARTICIPANT = "http://10.0.3.2:3000/api/v1/npb_applicant_participants/%d";
    public static final String ALL_APPLICANT_NPA_PARTICIPANTS = "";
    public static final String A_APPLICANT_NPA_PARTICIPANT = "";
    public static final String GET_A_NPA_FOLLOWUP = "";
    public static final String GET_ALL_NPA_FOLLOWUPS = "";
    public static final String GET_A_NPB_FOLLOWUP = "http://10.0.3.2:3000/api/v1/%s/%d/npb_followups/%d";
    public static final String GET_ALL_NPB_FOLLOWUPS = "http://10.0.3.2:3000/api/v1/%s/%d/npb_followups";
    public static final String GET_A_NPC_FOLLOWUP = "http://10.0.3.2:3000/api/v1/%s/%d/npc_followups/%d";
    public static final String GET_ALL_NPC_FOLLOWUPS = "http://10.0.3.2:3000/api/v1/%s/%d/npc_followups";
    public static final String DELETE_A_NPA_FOLLOWUP = "";
    public static final String DELETE_A_NPB_FOLLOWUP = "http://10.0.3.2:3000/api/v1/%s/%d/npb_followups/%d";
    public static final String DELETE_A_NPC_FOLLOWUP = "http://10.0.3.2:3000/api/v1/%s/%d/npc_followups/%d";
    public static final String UPDATE_A_NPA_FOLLOWUP = "";
    public static final String UPDATE_A_NPB_FOLLOWUP = "";
    public static final String UPDATE_A_NPC_FOLLOWUP = "";
    public static final String LOGIN = "http://10.0.3.2:3000/api/login?email=%s&password=%s";
    public static final String LOGOUT = "http://10.0.3.2:3000/api/logout";

    public static String ACCESS_TOKEN = "";
}
