package com.unimelb.inpdr;

/**
 * Created by Xintong on 31/05/2016.
 */

import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v7.app.ActionBarActivity;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Toast;

import com.jaredrummler.materialspinner.MaterialSpinner;
import com.unimelb.inpdr.Models.API.APIItem;
import com.unimelb.inpdr.Models.API.APIReceivable;
import com.unimelb.inpdr.Models.API.INPDRAPI;
import com.unimelb.inpdr.Models.API.ModelFacade;
import com.unimelb.inpdr.Models.BasicInfo;
import com.unimelb.inpdr.Models.NpbApplicantFollowupCollection;
import com.unimelb.inpdr.Models.NpbFollowUp;
import com.unimelb.inpdr.Models.NpbFollowupCollection;
import com.unimelb.inpdr.Models.NpcApplicantFollowupCollection;
import com.unimelb.inpdr.Models.NpcFollowUp;
import com.unimelb.inpdr.Models.NpcFollowupCollection;
import com.unimelb.inpdr.Models.Patient;
import com.unimelb.inpdr.Models.Selection;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import lecho.lib.hellocharts.animation.ChartAnimationListener;
import lecho.lib.hellocharts.gesture.ZoomType;
import lecho.lib.hellocharts.listener.LineChartOnValueSelectListener;
import lecho.lib.hellocharts.model.Axis;
import lecho.lib.hellocharts.model.Line;
import lecho.lib.hellocharts.model.LineChartData;
import lecho.lib.hellocharts.model.PointValue;
import lecho.lib.hellocharts.model.ValueShape;
import lecho.lib.hellocharts.model.Viewport;
import lecho.lib.hellocharts.util.ChartUtils;
import lecho.lib.hellocharts.view.Chart;
import lecho.lib.hellocharts.view.LineChartView;

public class LineChartActivity extends ActionBarActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_line_chart);
        if (savedInstanceState == null) {
            getSupportFragmentManager().beginTransaction().add(R.id.container, new PlaceholderFragment()).commit();
        }
    }

    /**
     * A fragment containing a line chart.
     */
    public static class PlaceholderFragment extends Fragment implements APIReceivable{

        private LineChartView chart;
        private LineChartData data;
        private int maxNumberOfLines = 24;
        
        private int numberOfPoints = 0;
        private int numberOfLines = 0;

        private boolean hasAxes = true;
        private boolean hasAxesNames = true;
        private boolean hasLines = true;
        private boolean hasPoints = true;
        private ValueShape shape = ValueShape.CIRCLE;
        private boolean isFilled = false;
        private boolean hasLabels = false;
        private boolean isCubic = false;
        private boolean hasLabelForSelected = false;
        private boolean pointsHaveDifferentColor;

        private ArrayList<NpbFollowUp> npbFollowUps = new ArrayList<>();
        private ArrayList<NpcFollowUp> npcFollowUps = new ArrayList<>();
        private List patientNames = new ArrayList<String>();
        private ArrayList<Patient> patients = new ArrayList<>();
        private String selectedPatientType;
        private int selectedPatientId;

        Map<Integer, Integer> npbIndexes = new HashMap<>();
        Map<Integer, Integer> npcIndexes = new HashMap<>();
        List<String> npbParams = new ArrayList<>();
        List<String> npcParams = new ArrayList<>();

        public void MapInitialization() {
            npbIndexes.put(0, 5);
            npbIndexes.put(1, 5);
            npbIndexes.put(2, 5);
            npbIndexes.put(3, 5);
            npbIndexes.put(4, 5);
            npbIndexes.put(5, 5);
            npbIndexes.put(6, 5);
            npbIndexes.put(7, 5);
            npbIndexes.put(8, 5);
            npbIndexes.put(9, 5);
            npbIndexes.put(10, 3);
            npbIndexes.put(11, 5);
            npbIndexes.put(12, 3);
            npbIndexes.put(13, 5);
            npbIndexes.put(14, 4);
            npbIndexes.put(15, 4);
            npbIndexes.put(16, 4);
            npbIndexes.put(17, 4);
            npbIndexes.put(18, 4);
            npbIndexes.put(19, 4);
            npbIndexes.put(20, 6);
            npbIndexes.put(21, 6);
            npbIndexes.put(22, 4);
            npbIndexes.put(23, 4);

            npbParams.add("symptom_bone");
            npbParams.add("symptom_abdominal");
            npbParams.add("symptom_pain");
            npbParams.add("symptom_cognition");
            npbParams.add("symptom_breathlessness_rate");
            npbParams.add("symptom_breathlessness_impact");
            npbParams.add("symptom_bleeding_rate");
            npbParams.add("symptom_bleeding_impact");
            npbParams.add("symptom_fatigue_rate");
            npbParams.add("symptom_fatigue_impact");
            npbParams.add("symptom_enlarge_organ");
            npbParams.add("symptom_enlarge_organ_impact");
            npbParams.add("symptom_slow_growth");
            npbParams.add("symptom_slow_growth_impact");
            npbParams.add("symptom_infection");
            npbParams.add("symptom_fracture");
            npbParams.add("impact_family_disappointed");
            npbParams.add("impact_family_give_up");
            npbParams.add("impact_family_worry_future");
            npbParams.add("impact_family_closer");
            npbParams.add("impact_wider_self_miss_career");
            npbParams.add("impact_wider_carer_miss_career");
            npbParams.add("impact_wider_emergency");
            npbParams.add("impact_wider_appointment");

            npcIndexes.put(0, 6);
            npcIndexes.put(1, 5);
            npcIndexes.put(2, 4);
            npcIndexes.put(3, 5);
            npcIndexes.put(4, 4);
            npcIndexes.put(5, 4);
            npcIndexes.put(6, 4);
            npcIndexes.put(7, 4);
            npcIndexes.put(8, 4);
            npcIndexes.put(9, 4);
            npcIndexes.put(10, 4);
            npcIndexes.put(11, 6);
            npcIndexes.put(12, 6);
            npcIndexes.put(13, 4);
            npcIndexes.put(14, 4);


            npcParams.add("impact_ambulation");
            npcParams.add("impact_manipulation");
            npcParams.add("impact_speech");
            npcParams.add("impact_swallowing");
            npcParams.add("impact_eye_movement");
            npcParams.add("impact_seizure");
            npcParams.add("impact_cognitive_impaired");
            npcParams.add("impact_family_disappointed");
            npcParams.add("impact_family_give_up");
            npcParams.add("impact_family_worry_future");
            npcParams.add("impact_family_closer_family");
            npcParams.add("impact_wider_self_miss_career");
            npcParams.add("impact_wider_family_miss_career");
            npcParams.add("impact_wider_emergency");
            npcParams.add("impact_wider_appointment");
            return;
        }

        public PlaceholderFragment() {
        }

        @Override
        public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
            setHasOptionsMenu(true);
            View rootView = inflater.inflate(R.layout.fragment_line_chart, container, false);

            chart = (LineChartView) rootView.findViewById(R.id.chart);
            chart.setOnValueTouchListener(new ValueTouchListener());

            MapInitialization();
            BasicInfo.getInstance();
            NpbFollowupCollection.getInstance();
            NpbApplicantFollowupCollection.getInstance();
            NpcFollowupCollection.getInstance();
            NpcApplicantFollowupCollection.getInstance();
            Selection.getInstance();


            final INPDRAPI api = new INPDRAPI(getContext());

            ModelFacade.getInstance().getAllPatients(this, api);

            // Disable viewport recalculations, see toggleCubic() method for more info.
            chart.setViewportCalculationEnabled(false);
            resetViewport();

            return rootView;
        }

        // MENU
        @Override
        public void onCreateOptionsMenu(Menu menu, MenuInflater inflater) {
            inflater.inflate(R.menu.line_chart, menu);
        }

        @Override
        public boolean onOptionsItemSelected(MenuItem item) {
            int id = item.getItemId();
            if (id == R.id.action_toggle_lines) {
                toggleLines();
                return true;
            }
            if (id == R.id.action_toggle_points) {
                togglePoints();
                return true;
            }
            if (id == R.id.action_toggle_cubic) {
                toggleCubic();
                return true;
            }
            if (id == R.id.action_toggle_area) {
                toggleFilled();
                return true;
            }
            if (id == R.id.action_point_color) {
                togglePointColor();
                return true;
            }
            if (id == R.id.action_shape_circles) {
                setCircles();
                return true;
            }
            if (id == R.id.action_shape_square) {
                setSquares();
                return true;
            }
            if (id == R.id.action_shape_diamond) {
                setDiamonds();
                return true;
            }
            if (id == R.id.action_toggle_labels) {
                toggleLabels();
                return true;
            }
            if (id == R.id.action_toggle_axes) {
                toggleAxes();
                return true;
            }
            if (id == R.id.action_toggle_axes_names) {
                toggleAxesNames();
                return true;
            }
            if (id == R.id.action_animate) {
                prepareDataAnimation();
                chart.startDataAnimation();
                return true;
            }
            if (id == R.id.action_toggle_selection_mode) {
                toggleLabelForSelected();

                Toast.makeText(getActivity(),
                        "Selection mode set to " + chart.isValueSelectionEnabled() + " select any point.",
                        Toast.LENGTH_SHORT).show();
                return true;
            }
            if (id == R.id.action_toggle_touch_zoom) {
                chart.setZoomEnabled(!chart.isZoomEnabled());
                Toast.makeText(getActivity(), "IsZoomEnabled " + chart.isZoomEnabled(), Toast.LENGTH_SHORT).show();
                return true;
            }
            if (id == R.id.action_zoom_both) {
                chart.setZoomType(ZoomType.HORIZONTAL_AND_VERTICAL);
                return true;
            }
            if (id == R.id.action_zoom_horizontal) {
                chart.setZoomType(ZoomType.HORIZONTAL);
                return true;
            }
            if (id == R.id.action_zoom_vertical) {
                chart.setZoomType(ZoomType.VERTICAL);
                return true;
            }
            return super.onOptionsItemSelected(item);
        }

        private float[][] generateNpbValues(List<NpbFollowUp> npbFollowUps) {
            float[][] values = new float[numberOfLines][numberOfPoints];

            for (int i = 0; i < numberOfLines; ++i) {
                List npbAnswers = new ArrayList<Integer>();
                NpbFollowUp npbFollowUp = npbFollowUps.get(i);
                npbAnswers.add(npbFollowUp.getSymptom_bone());
                npbAnswers.add(npbFollowUp.getSymptom_abdominal());
                npbAnswers.add(npbFollowUp.getSymptom_pain());
                npbAnswers.add(npbFollowUp.getSymptom_cognition());
                npbAnswers.add(npbFollowUp.getSymptom_breathlessness_rate());
                npbAnswers.add(npbFollowUp.getSymptom_breathlessness_impact());
                npbAnswers.add(npbFollowUp.getSymptom_bleeding_rate());
                npbAnswers.add(npbFollowUp.getSymptom_bleeding_impact());
                npbAnswers.add(npbFollowUp.getSymptom_fatigue_rate());
                npbAnswers.add(npbFollowUp.getSymptom_fatigue_impact());
                npbAnswers.add(npbFollowUp.getSymptom_enlarge_organ());
                npbAnswers.add(npbFollowUp.getSymptom_enlarge_organ_impact());
                npbAnswers.add(npbFollowUp.getSymptom_slow_growth());
                npbAnswers.add(npbFollowUp.getSymptom_slow_growth_impact());
                npbAnswers.add(npbFollowUp.getSymptom_infection());
                npbAnswers.add(npbFollowUp.getSymptom_fracture());
                npbAnswers.add(npbFollowUp.getImpact_family_disappointed());
                npbAnswers.add(npbFollowUp.getImpact_family_give_up());
                npbAnswers.add(npbFollowUp.getImpact_family_worry_future());
                npbAnswers.add(npbFollowUp.getImpact_family_closer());
                npbAnswers.add(npbFollowUp.getImpact_wider_self_miss_career());
                npbAnswers.add(npbFollowUp.getImpact_wider_carer_miss_career());
                npbAnswers.add(npbFollowUp.getImpact_wider_emergency());
                npbAnswers.add(npbFollowUp.getImpact_wider_appointment());
                for (int j = 0; j < numberOfPoints; ++j) {
                    int answer =  (Integer)npbAnswers.get(j) + 1;
                    float value = (float)answer/(float)npbIndexes.get(i) * 100;
                    values[i][j] = value;
                }
            }
            return values;
        }

        private float[][] generateNpcValues(List<NpcFollowUp> npcFollowUps) {
            float[][] values = new float[numberOfLines][numberOfPoints];

            for (int i = 0; i < numberOfLines; ++i) {
                List npcAnswers = new ArrayList<Integer>();
                NpcFollowUp npcFollowUp = npcFollowUps.get(i);
                npcAnswers.add(npcFollowUp.getImpact_ambulation());
                npcAnswers.add(npcFollowUp.getImpact_manipulation());
                npcAnswers.add(npcFollowUp.getImpact_speech());
                npcAnswers.add(npcFollowUp.getImpact_swallowing());
                npcAnswers.add(npcFollowUp.getImpact_eye_movement());
                npcAnswers.add(npcFollowUp.getImpact_seizure());
                npcAnswers.add(npcFollowUp.getImpact_cognitive_impaired());
                npcAnswers.add(npcFollowUp.getImpact_family_disappointed());
                npcAnswers.add(npcFollowUp.getImpact_family_give_up());
                npcAnswers.add(npcFollowUp.getImpact_family_worry_future());
                npcAnswers.add(npcFollowUp.getImpact_family_closer_family());
                npcAnswers.add(npcFollowUp.getImpact_wider_self_miss_career());
                npcAnswers.add(npcFollowUp.getImpact_wider_family_miss_career());
                npcAnswers.add(npcFollowUp.getImpact_wider_emergency());
                npcAnswers.add(npcFollowUp.getImpact_wider_appointment());
                for (int j = 0; j < numberOfPoints; ++j) {
                    int answer =  (Integer)npcAnswers.get(j) + 1;
                    float value = (float)answer/(float)npcIndexes.get(i) * 100;
                    values[i][j] = value;
                }
            }
            return values;
        }

        private void resetViewport() {
            // Reset viewport height range to (0,100)
            final Viewport v = new Viewport(chart.getMaximumViewport());
            v.bottom = 0;
            v.top = 100;
            v.left = 0;
            v.right = numberOfPoints - 1;
            chart.setMaximumViewport(v);
            chart.setCurrentViewport(v);
        }

        private void generateData(float[][] answers) {

            List<Line> lines = new ArrayList<Line>();
            Log.d("lines ", numberOfLines + " " + numberOfPoints + " " + selectedPatientType);
            for (int i = 0; i < numberOfLines; ++i) {

                List<PointValue> values = new ArrayList<PointValue>();
                for (int j = 0; j < numberOfPoints; ++j) {
                    values.add(new PointValue(j, answers[i][j]));
                }

                Line line = new Line(values);
                line.setColor(ChartUtils.COLORS[i % 5]);
                line.setShape(shape);
                line.setCubic(isCubic);
                line.setFilled(isFilled);
                line.setHasLabels(hasLabels);
                line.setHasLabelsOnlyForSelected(hasLabelForSelected);
                line.setHasLines(hasLines);
                line.setHasPoints(hasPoints);
                if (pointsHaveDifferentColor){
                    line.setPointColor(ChartUtils.COLORS[(i + 1) % ChartUtils.COLORS.length]);
                }
                lines.add(line);
            }

            data = new LineChartData(lines);
            Log.d("line number", lines.size()+"");

            if (hasAxes) {
                Axis axisX = new Axis();
                Axis axisY = new Axis().setHasLines(true);
                if (hasAxesNames) {
                    axisX.setName("Question Number");
                    axisY.setName("Answer");
                }
                data.setAxisXBottom(axisX);
                data.setAxisYLeft(axisY);
            } else {
                data.setAxisXBottom(null);
                data.setAxisYLeft(null);
            }

            data.setBaseValue(Float.NEGATIVE_INFINITY);
            chart.setLineChartData(data);

        }

        private void toggleLines() {
            hasLines = !hasLines;
            final INPDRAPI api = new INPDRAPI(getContext());
            final APIReceivable receivable = this;
            switch (selectedPatientType){
                case "NpbParticipant":case "NpbApplicantParticipant":
                    ModelFacade.getInstance().getAllNpbFollowups(receivable, api, selectedPatientType, selectedPatientId);
                    break;
                case "NpcParticipant":case "NpcApplicantParticipant":
                    ModelFacade.getInstance().getAllNpcFollowups(receivable, api, selectedPatientType, selectedPatientId);
                    break;
            }
        }

        private void togglePoints() {
            hasPoints = !hasPoints;

            final INPDRAPI api = new INPDRAPI(getContext());
            final APIReceivable receivable = this;
            switch (selectedPatientType){
                case "NpbParticipant":case "NpbApplicantParticipant":
                    ModelFacade.getInstance().getAllNpbFollowups(receivable, api, selectedPatientType, selectedPatientId);
                    break;
                case "NpcParticipant":case "NpcApplicantParticipant":
                    ModelFacade.getInstance().getAllNpcFollowups(receivable, api, selectedPatientType, selectedPatientId);
                    break;
            }
        }

        private void toggleCubic() {
            isCubic = !isCubic;

            final INPDRAPI api = new INPDRAPI(getContext());
            final APIReceivable receivable = this;
            switch (selectedPatientType){
                case "NpbParticipant":case "NpbApplicantParticipant":
                    ModelFacade.getInstance().getAllNpbFollowups(receivable, api, selectedPatientType, selectedPatientId);
                    break;
                case "NpcParticipant":case "NpcApplicantParticipant":
                    ModelFacade.getInstance().getAllNpcFollowups(receivable, api, selectedPatientType, selectedPatientId);
                    break;
            }

            if (isCubic) {
                // It is good idea to manually set a little higher max viewport for cubic lines because sometimes line
                // go above or below max/min. To do that use Viewport.inest() method and pass negative value as dy
                // parameter or just set top and bottom values manually.
                // In this example I know that Y values are within (0,100) range so I set viewport height range manually
                // to (-5, 105).
                // To make this works during animations you should use Chart.setViewportCalculationEnabled(false) before
                // modifying viewport.
                // Remember to set viewport after you call setLineChartData().
                final Viewport v = new Viewport(chart.getMaximumViewport());
                v.bottom = -5;
                v.top = 105;
                // You have to set max and current viewports separately.
                chart.setMaximumViewport(v);
                // I changing current viewport with animation in this case.
                chart.setCurrentViewportWithAnimation(v);
            } else {
                // If not cubic restore viewport to (0,100) range.
                final Viewport v = new Viewport(chart.getMaximumViewport());
                v.bottom = 0;
                v.top = 100;

                // You have to set max and current viewports separately.
                // In this case, if I want animation I have to set current viewport first and use animation listener.
                // Max viewport will be set in onAnimationFinished method.
                chart.setViewportAnimationListener(new ChartAnimationListener() {

                    @Override
                    public void onAnimationStarted() {
                        // TODO Auto-generated method stub

                    }

                    @Override
                    public void onAnimationFinished() {
                        // Set max viewpirt and remove listener.
                        chart.setMaximumViewport(v);
                        chart.setViewportAnimationListener(null);

                    }
                });
                // Set current viewpirt with animation;
                chart.setCurrentViewportWithAnimation(v);
            }

        }

        private void toggleFilled() {
            isFilled = !isFilled;

            final INPDRAPI api = new INPDRAPI(getContext());
            final APIReceivable receivable = this;
            switch (selectedPatientType){
                case "NpbParticipant":case "NpbApplicantParticipant":
                    ModelFacade.getInstance().getAllNpbFollowups(receivable, api, selectedPatientType, selectedPatientId);
                    break;
                case "NpcParticipant":case "NpcApplicantParticipant":
                    ModelFacade.getInstance().getAllNpcFollowups(receivable, api, selectedPatientType, selectedPatientId);
                    break;
            }
        }

        private void togglePointColor() {
            pointsHaveDifferentColor = !pointsHaveDifferentColor;

            final INPDRAPI api = new INPDRAPI(getContext());
            final APIReceivable receivable = this;
            switch (selectedPatientType){
                case "NpbParticipant":case "NpbApplicantParticipant":
                    ModelFacade.getInstance().getAllNpbFollowups(receivable, api, selectedPatientType, selectedPatientId);
                    break;
                case "NpcParticipant":case "NpcApplicantParticipant":
                    ModelFacade.getInstance().getAllNpcFollowups(receivable, api, selectedPatientType, selectedPatientId);
                    break;
            }
        }

        private void setCircles() {
            shape = ValueShape.CIRCLE;

            final INPDRAPI api = new INPDRAPI(getContext());
            final APIReceivable receivable = this;
            switch (selectedPatientType){
                case "NpbParticipant":case "NpbApplicantParticipant":
                    ModelFacade.getInstance().getAllNpbFollowups(receivable, api, selectedPatientType, selectedPatientId);
                    break;
                case "NpcParticipant":case "NpcApplicantParticipant":
                    ModelFacade.getInstance().getAllNpcFollowups(receivable, api, selectedPatientType, selectedPatientId);
                    break;
            }
        }

        private void setSquares() {
            shape = ValueShape.SQUARE;

            final INPDRAPI api = new INPDRAPI(getContext());
            final APIReceivable receivable = this;
            switch (selectedPatientType){
                case "NpbParticipant":case "NpbApplicantParticipant":
                    ModelFacade.getInstance().getAllNpbFollowups(receivable, api, selectedPatientType, selectedPatientId);
                    break;
                case "NpcParticipant":case "NpcApplicantParticipant":
                    ModelFacade.getInstance().getAllNpcFollowups(receivable, api, selectedPatientType, selectedPatientId);
                    break;
            }
        }

        private void setDiamonds() {
            shape = ValueShape.DIAMOND;

            final INPDRAPI api = new INPDRAPI(getContext());
            final APIReceivable receivable = this;
            switch (selectedPatientType){
                case "NpbParticipant":case "NpbApplicantParticipant":
                    ModelFacade.getInstance().getAllNpbFollowups(receivable, api, selectedPatientType, selectedPatientId);
                    break;
                case "NpcParticipant":case "NpcApplicantParticipant":
                    ModelFacade.getInstance().getAllNpcFollowups(receivable, api, selectedPatientType, selectedPatientId);
                    break;
            }
        }

        private void toggleLabels() {
            hasLabels = !hasLabels;

            if (hasLabels) {
                hasLabelForSelected = false;
                chart.setValueSelectionEnabled(hasLabelForSelected);
            }

            final INPDRAPI api = new INPDRAPI(getContext());
            final APIReceivable receivable = this;
            switch (selectedPatientType){
                case "NpbParticipant":case "NpbApplicantParticipant":
                    ModelFacade.getInstance().getAllNpbFollowups(receivable, api, selectedPatientType, selectedPatientId);
                    break;
                case "NpcParticipant":case "NpcApplicantParticipant":
                    ModelFacade.getInstance().getAllNpcFollowups(receivable, api, selectedPatientType, selectedPatientId);
                    break;
            }
        }

        private void toggleLabelForSelected() {
            hasLabelForSelected = !hasLabelForSelected;

            chart.setValueSelectionEnabled(hasLabelForSelected);

            if (hasLabelForSelected) {
                hasLabels = false;
            }

            final INPDRAPI api = new INPDRAPI(getContext());
            final APIReceivable receivable = this;
            switch (selectedPatientType){
                case "NpbParticipant":case "NpbApplicantParticipant":
                    ModelFacade.getInstance().getAllNpbFollowups(receivable, api, selectedPatientType, selectedPatientId);
                    break;
                case "NpcParticipant":case "NpcApplicantParticipant":
                    ModelFacade.getInstance().getAllNpcFollowups(receivable, api, selectedPatientType, selectedPatientId);
                    break;
            }
        }

        private void toggleAxes() {
            hasAxes = !hasAxes;

            final INPDRAPI api = new INPDRAPI(getContext());
            final APIReceivable receivable = this;
            switch (selectedPatientType){
                case "NpbParticipant":case "NpbApplicantParticipant":
                    ModelFacade.getInstance().getAllNpbFollowups(receivable, api, selectedPatientType, selectedPatientId);
                    break;
                case "NpcParticipant":case "NpcApplicantParticipant":
                    ModelFacade.getInstance().getAllNpcFollowups(receivable, api, selectedPatientType, selectedPatientId);
                    break;
            }
        }

        private void toggleAxesNames() {
            hasAxesNames = !hasAxesNames;

            final INPDRAPI api = new INPDRAPI(getContext());
            final APIReceivable receivable = this;
            switch (selectedPatientType){
                case "NpbParticipant":case "NpbApplicantParticipant":
                    ModelFacade.getInstance().getAllNpbFollowups(receivable, api, selectedPatientType, selectedPatientId);
                    break;
                case "NpcParticipant":case "NpcApplicantParticipant":
                    ModelFacade.getInstance().getAllNpcFollowups(receivable, api, selectedPatientType, selectedPatientId);
                    break;
            }
        }

        /**
         * To animate values you have to change targets values and then call {@link Chart#startDataAnimation()}
         * method(don't confuse with View.animate()). If you operate on data that was set before you don't have to call
         * {@link LineChartView#setLineChartData(LineChartData)} again.
         */
        private void prepareDataAnimation() {
            for (Line line : data.getLines()) {
                for (PointValue value : line.getValues()) {
                    // Here I modify target only for Y values but it is OK to modify X targets as well.
                    value.setTarget(value.getX(), (float) Math.random() * 100);
                }
            }
        }

        @Override
        public <U extends APIItem> void receiveData(List<U> items, String type) {
            final INPDRAPI api = new INPDRAPI(getContext());
            final APIReceivable receivable = this;
            float[][] values;
            switch (type){
                case "getAllPatients":
                    patients.clear();
                    for (U item : items) {
                        Patient feed = (Patient) item;
                        int patientId = feed.getEnrollable_id();
                        String patientType = feed.getEnrollable_type();
                        switch (patientType){
                            case "NpbParticipant":case "NpbApplicantParticipant":
                                patientNames.add( "NPB" + feed.getUnique_id() + " " + feed.getEnrollable_type());
                                ModelFacade.getInstance().getAllNpbFollowups(this, api, patientType, patientId);
                                break;
                            case "NpcParticipant":case "NpcApplicantParticipant":
                                patientNames.add( "NPC" + feed.getUnique_id() + " " + feed.getEnrollable_type());
                                ModelFacade.getInstance().getAllNpcFollowups(this, api, patientType, patientId);
                                break;
                        }
                        patients.add(feed);
                    }
                    MaterialSpinner spinner = (MaterialSpinner) getView().findViewById(R.id.spinner);
                    spinner.setItems(patientNames);
                    spinner.setSelectedIndex(0);

                    spinner.setTextColor(getResources().getColor(R.color.colorPrimaryText));

                    spinner.setOnItemSelectedListener(new MaterialSpinner.OnItemSelectedListener<String>() {
                        @Override
                        public void onItemSelected(MaterialSpinner view, int position, long id, String item) {
                            selectedPatientType = patients.get(position).getEnrollable_type();
                            selectedPatientId = patients.get(position).getEnrollable_id();
                            switch (selectedPatientType) {
                                case "NpbParticipant":
                                case "NpbApplicantParticipant":
                                    ModelFacade.getInstance().getAllNpbFollowups(receivable, api, selectedPatientType, selectedPatientId);
                                    break;
                                case "NpcParticipant":
                                case "NpcApplicantParticipant":
                                    ModelFacade.getInstance().getAllNpcFollowups(receivable, api, selectedPatientType, selectedPatientId);
                                    break;
                            }
                        }
                    });

                    selectedPatientType = patients.get(spinner.getSelectedIndex()).getEnrollable_type();
                    selectedPatientId = patients.get(spinner.getSelectedIndex()).getEnrollable_id();
                    spinner.onSaveInstanceState();
                    //Log.d("!!!!!data received!!!", feed.getUser().getEmail() + "");
                    break;
                case "getAllNpbFollowups":
                    npbFollowUps.clear();
                    for (U item : items) {
                        NpbFollowUp feed = (NpbFollowUp) item;
                        npbFollowUps.add(feed);
                        //Log.d("!!!!!data received!!!", feed.getUser().getEmail() + "");
                    }
                    numberOfLines = npbFollowUps.size();
                    numberOfPoints  = 24;
                    maxNumberOfLines = npbFollowUps.size();

                    values = generateNpbValues(npbFollowUps);
                    generateData(values);
                    resetViewport();
                    break;
                case "getAllNpcFollowups":
                    npcFollowUps.clear();
                    for (U item : items) {
                        NpcFollowUp feed = (NpcFollowUp) item;
                        npcFollowUps.add(feed);
                        //Log.d("!!!!!data received!!!", feed.getUser().getEmail() + "");
                    }
                    numberOfLines = npcFollowUps.size();
                    numberOfPoints  = 15;
                    maxNumberOfLines = npcFollowUps.size();
                    values = generateNpcValues(npcFollowUps);
                    generateData(values);
                    resetViewport();
                    break;
            }
        }

        @Override
        public <U extends APIItem> void receiveData(U item, String type) {

        }

        private class ValueTouchListener implements LineChartOnValueSelectListener {

            @Override
            public void onValueSelected(int lineIndex, int pointIndex, PointValue value) {
                int finalValue = 0;
                int x_value = 0;
                String answer = "";
                switch (selectedPatientType){
                    case "NpbParticipant":case "NpbApplicantParticipant":
                        x_value = (int)value.getX();
                        finalValue = Math.round(value.getY()/100 * npbIndexes.get(pointIndex)) - 1;
                        Log.d("value ", "x " + value.getX() + " "+value.getY()/100 * npbIndexes.get(pointIndex));
                        Log.d("index ", "point in " + pointIndex + "line " + lineIndex);
                        answer = getAnswers(npbParams.get(pointIndex), finalValue);
                        Toast.makeText(getActivity(), String.format("%s:  [%d, %s]", npbParams.get(pointIndex), x_value, answer), Toast.LENGTH_SHORT).show();
                        break;
                    case "NpcParticipant":case "NpcApplicantParticipant":
                        x_value = (int)value.getX();
                        finalValue = Math.round(value.getY()/100 * npcIndexes.get(pointIndex)) - 1;
                        answer = getAnswers(npcParams.get(pointIndex), finalValue);
                        Toast.makeText(getActivity(), String.format("%s:  [%d, %s]", npcParams.get(pointIndex), x_value, answer), Toast.LENGTH_SHORT).show();
                        break;
                }
              }

            @Override
            public void onValueDeselected() {
                // TODO Auto-generated method stub

            }

        }

        private String getAnswers(String param, int index) {
            String answer = "";
            switch (selectedPatientType){
                case "NpbParticipant":
                    switch (param){
                        case "symptom_bone":
                            answer = NpbFollowupCollection.getSymptom_bone_collection().get(index);
                            break;
                        case "symptom_abdominal":
                            answer = NpbFollowupCollection.getSymptom_abdominal_collection().get(index);
                            break;
                        case "symptom_pain":
                            answer = NpbFollowupCollection.getSymptom_pain_collection().get(index);
                            break;
                        case "symptom_breathlessness_rate":
                            answer = NpbFollowupCollection.getSymptom_breathlessness_rate_collection().get(index);
                            break;
                        case "symptom_breathlessness_impact":
                            answer = NpbFollowupCollection.getSymptom_breathlessness_impact_collection().get(index);
                            break;
                        case "symptom_bleeding_rate":
                            answer = NpbFollowupCollection.getSymptom_bleeding_rate_collection().get(index);
                            break;
                        case "symptom_bleeding_impact":
                            answer = NpbFollowupCollection.getSymptom_bleeding_impact_collection().get(index);
                            break;
                        case "symptom_fatigue_rate":
                            answer = NpbFollowupCollection.getSymptom_fatigue_rate_collection().get(index);
                            break;
                        case "symptom_fatigue_impact":
                            answer = NpbFollowupCollection.getSymptom_fatigue_impact_collection().get(index);
                            break;
                        case "symptom_enlarge_organ":
                            answer = NpbFollowupCollection.getSymptom_enlarge_organ_collection().get(index);
                            break;
                        case "symptom_enlarge_organ_impact":
                            answer = NpbFollowupCollection.getSymptom_enlarge_organ_impact_collection().get(index);
                            break;
                        case "symptom_slow_growth":
                            answer = NpbFollowupCollection.getSymptom_slow_growth_collection().get(index);
                            break;
                        case "symptom_slow_growth_impact":
                            answer = NpbFollowupCollection.getSymptom_slow_growth_impact_collection().get(index);
                            break;
                        case "symptom_infection":
                            answer = NpbFollowupCollection.getSymptom_infection_collection().get(index);
                            break;
                        case "symptom_fracture":
                            answer = NpbFollowupCollection.getSymptom_fracture_collection().get(index);
                            break;
                        case "symptom_cognition":
                            answer = NpbFollowupCollection.getSymptom_cognition_collection().get(index);
                            break;
                        case "impact_family_disappointed":
                            answer = NpbFollowupCollection.getImpact_family_collection().get(index);
                            break;
                        case "impact_family_give_up":
                            answer = NpbFollowupCollection.getImpact_family_collection().get(index);
                            break;
                        case "impact_family_worry_future":
                            answer = NpbFollowupCollection.getImpact_family_collection().get(index);
                            break;
                        case "impact_family_closer":
                            answer = NpbFollowupCollection.getImpact_family_collection().get(index);
                            break;
                        case "impact_wider_self_miss_career":
                            answer = NpbFollowupCollection.getImpact_wider_self_miss_career().get(index);
                            break;
                        case "impact_wider_carer_miss_career":
                            answer = NpbFollowupCollection.getImpact_wider_carer_miss_career().get(index);
                            break;
                        case "impact_wider_emergency":
                            answer = NpbFollowupCollection.getImpact_wider_emergency().get(index);
                            break;
                        case "impact_wider_appointment":
                            answer = NpbFollowupCollection.getImpact_wider_appointment().get(index);
                            break;
                    }
                    break;
                case "NpbApplicantParticipant":
                    switch (param){
                        case "symptom_bone":
                            answer = NpbApplicantFollowupCollection.getSymptom_bone_collection().get(index);
                            break;
                        case "symptom_abdominal":
                            answer = NpbApplicantFollowupCollection.getSymptom_abdominal_collection().get(index);
                            break;
                        case "symptom_pain":
                            answer = NpbApplicantFollowupCollection.getSymptom_pain_collection().get(index);
                            break;
                        case "symptom_breathlessness_rate":
                            answer = NpbApplicantFollowupCollection.getSymptom_breathlessness_rate_collection().get(index);
                            break;
                        case "symptom_breathlessness_impact":
                            answer = NpbApplicantFollowupCollection.getSymptom_breathlessness_impact_collection().get(index);
                            break;
                        case "symptom_bleeding_rate":
                            answer = NpbApplicantFollowupCollection.getSymptom_bleeding_rate_collection().get(index);
                            break;
                        case "symptom_bleeding_impact":
                            answer = NpbApplicantFollowupCollection.getSymptom_bleeding_impact_collection().get(index);
                            break;
                        case "symptom_fatigue_rate":
                            answer = NpbApplicantFollowupCollection.getSymptom_fatigue_rate_collection().get(index);
                            break;
                        case "symptom_fatigue_impact":
                            answer = NpbApplicantFollowupCollection.getSymptom_fatigue_impact_collection().get(index);
                            break;
                        case "symptom_enlarge_organ":
                            answer = NpbApplicantFollowupCollection.getSymptom_enlarge_organ_collection().get(index);
                            break;
                        case "symptom_enlarge_organ_impact":
                            answer = NpbApplicantFollowupCollection.getSymptom_enlarge_organ_impact_collection().get(index);
                            break;
                        case "symptom_slow_growth":
                            answer = NpbApplicantFollowupCollection.getSymptom_slow_growth_collection().get(index);
                            break;
                        case "symptom_slow_growth_impact":
                            answer = NpbApplicantFollowupCollection.getSymptom_slow_growth_impact_collection().get(index);
                            break;
                        case "symptom_infection":
                            answer = NpbApplicantFollowupCollection.getSymptom_infection_collection().get(index);
                            break;
                        case "symptom_fracture":
                            answer = NpbApplicantFollowupCollection.getSymptom_fracture_collection().get(index);
                            break;
                        case "symptom_cognition":
                            answer = NpbApplicantFollowupCollection.getSymptom_cognition_collection().get(index);
                            break;
                        case "impact_family_disappointed":
                            answer = NpbApplicantFollowupCollection.getImpact_family_collection().get(index);
                            break;
                        case "impact_family_give_up":
                            answer = NpbApplicantFollowupCollection.getImpact_family_collection().get(index);
                            break;
                        case "impact_family_worry_future":
                            answer = NpbApplicantFollowupCollection.getImpact_family_collection().get(index);
                            break;
                        case "impact_family_closer":
                            answer = NpbApplicantFollowupCollection.getImpact_family_collection().get(index);
                            break;
                        case "impact_wider_self_miss_career":
                            answer = NpbApplicantFollowupCollection.getImpact_wider_self_miss_career().get(index);
                            break;
                        case "impact_wider_carer_miss_career":
                            answer = NpbApplicantFollowupCollection.getImpact_wider_carer_miss_career().get(index);
                            break;
                        case "impact_wider_emergency":
                            answer = NpbApplicantFollowupCollection.getImpact_wider_emergency().get(index);
                            break;
                        case "impact_wider_appointment":
                            answer = NpbApplicantFollowupCollection.getImpact_wider_appointment().get(index);
                            break;
                    }
                    break;
                case "NpcParticipant":
                    switch (param){
                        case "impact_ambulation":
                            answer = NpcFollowupCollection.getImpact_ambulation().get(index);
                            break;
                        case "impact_manipulation":
                            answer = NpcFollowupCollection.getImpact_manipulation().get(index);
                            break;
                        case "impact_speech":
                            answer = NpcFollowupCollection.getImpact_speech().get(index);
                            break;
                        case "impact_swallowing":
                            answer = NpcFollowupCollection.getImpact_swallowing().get(index);
                            break;
                        case "impact_eye_movement":
                            answer = NpcFollowupCollection.getImpact_eye_movement().get(index);
                            break;
                        case "impact_seizure":
                            answer = NpcFollowupCollection.getImpact_seizure().get(index);
                            break;
                        case "impact_cognitive_impaired":
                            answer = NpcFollowupCollection.getImpact_cognitive_impaired().get(index);
                            break;
                        case "impact_family_disappointed":
                            answer = NpcFollowupCollection.getImpact_family_collection().get(index);
                            break;
                        case "impact_family_give_up":
                            answer = NpcFollowupCollection.getImpact_family_collection().get(index);
                            break;
                        case "impact_family_worry_future":
                            answer = NpcFollowupCollection.getImpact_family_collection().get(index);
                            break;
                        case "impact_family_closer_family":
                            answer = NpcFollowupCollection.getImpact_family_collection().get(index);
                            break;
                        case "impact_wider_self_miss_career":
                            answer = NpcFollowupCollection.getImpact_wider_self_miss_career().get(index);
                            break;
                        case "impact_wider_family_miss_career":
                            answer = NpcFollowupCollection.getImpact_wider_family_miss_career().get(index);
                            break;
                        case "impact_wider_emergency":
                            answer = NpcFollowupCollection.getImpact_wider_emergency().get(index);
                            break;
                        case "impact_wider_appointment":
                            answer = NpcFollowupCollection.getImpact_wider_appointment().get(index);
                            break;
                    }
                    break;
                case "NpcApplicantParticipant":
                    switch (param) {
                        case "impact_ambulation":
                            answer = NpcApplicantFollowupCollection.getImpact_ambulation().get(index);
                            break;
                        case "impact_manipulation":
                            answer = NpcApplicantFollowupCollection.getImpact_manipulation().get(index);
                            break;
                        case "impact_speech":
                            answer = NpcApplicantFollowupCollection.getImpact_speech().get(index);
                            break;
                        case "impact_swallowing":
                            answer = NpcApplicantFollowupCollection.getImpact_swallowing().get(index);
                            break;
                        case "impact_eye_movement":
                            answer = NpcApplicantFollowupCollection.getImpact_eye_movement().get(index);
                            break;
                        case "impact_seizure":
                            answer = NpcApplicantFollowupCollection.getImpact_seizure().get(index);
                            break;
                        case "impact_cognitive_impaired":
                            answer = NpcApplicantFollowupCollection.getImpact_cognitive_impaired().get(index);
                            break;
                        case "impact_family_disappointed":
                            answer = NpcApplicantFollowupCollection.getImpact_family_collection().get(index);
                            break;
                        case "impact_family_give_up":
                            answer = NpcApplicantFollowupCollection.getImpact_family_collection().get(index);
                            break;
                        case "impact_family_worry_future":
                            answer = NpcApplicantFollowupCollection.getImpact_family_collection().get(index);
                            break;
                        case "impact_family_closer_family":
                            answer = NpcApplicantFollowupCollection.getImpact_family_collection().get(index);
                            break;
                        case "impact_wider_self_miss_career":
                            answer = NpcApplicantFollowupCollection.getImpact_wider_self_miss_career().get(index);
                            break;
                        case "impact_wider_family_miss_career":
                            answer = NpcApplicantFollowupCollection.getImpact_wider_family_miss_career().get(index);
                            break;
                        case "impact_wider_emergency":
                            answer = NpcApplicantFollowupCollection.getImpact_wider_emergency().get(index);
                            break;
                        case "impact_wider_appointment":
                            answer = NpcApplicantFollowupCollection.getImpact_wider_appointment().get(index);
                            break;
                    }
                    break;
            }
            Log.d("answer", param);
            return answer;

        }
    }
}
