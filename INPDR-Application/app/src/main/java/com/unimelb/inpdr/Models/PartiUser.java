package com.unimelb.inpdr.Models;

import com.unimelb.inpdr.Models.API.APIItem;

import org.json.JSONObject;

/**
 * Created by Xintong on 20/04/2016.
 */
public class PartiUser extends APIItem{
    private int id;
    private User user;
    private String participant_type;
    private String created_at;
    private String updated_at;

    public PartiUser (JSONObject obj) {
        setParticipant_type(obj.optString("participant_type"));
        setCreated_at(obj.optString("created_at"));
        setUpdated_at(obj.optString("updated_at"));
    }


    public String getParticipant_type() {
        return participant_type;
    }

    public void setParticipant_type(String participant_type) {
        this.participant_type = participant_type;
    }

    public String getCreated_at() {
        return created_at;
    }

    public void setCreated_at(String created_at) {
        this.created_at = created_at;
    }

    public String getUpdated_at() {
        return updated_at;
    }

    public void setUpdated_at(String updated_at) {
        this.updated_at = updated_at;
    }
}
