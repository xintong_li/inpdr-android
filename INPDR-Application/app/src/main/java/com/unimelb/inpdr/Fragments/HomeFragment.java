package com.unimelb.inpdr.Fragments;

import android.content.Context;
import android.content.DialogInterface;
import android.graphics.Typeface;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentTransaction;
import android.support.v4.widget.SwipeRefreshLayout;
import android.support.v7.app.AlertDialog;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.TextView;
import android.widget.Toast;

import com.unimelb.inpdr.MainActivity;
import com.unimelb.inpdr.Models.API.APIItem;
import com.unimelb.inpdr.Models.API.APIReceivable;
import com.unimelb.inpdr.Models.API.INPDRAPI;
import com.unimelb.inpdr.Models.API.ModelFacade;
import com.unimelb.inpdr.Models.ErrorMessage;
import com.unimelb.inpdr.Models.Patient;
import com.unimelb.inpdr.R;

import java.util.ArrayList;
import java.util.List;

import me.drakeet.materialdialog.MaterialDialog;

public class HomeFragment extends Fragment implements APIReceivable {
    private SwipeRefreshLayout swipeRefreshLayout;
    private ArrayList<Patient> patients = new ArrayList<>();
    private Typeface mTf;
    public HomeFragment() {
        // Required empty public constructor
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setHasOptionsMenu(true);
        mTf = Typeface.createFromAsset(getActivity().getAssets(), "OpenSans-Regular.ttf");
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        return inflater.inflate(R.layout.fragment_home, container, false);
    }

    @Override
    public void onStart(){
        super.onStart();
        ((MainActivity)getActivity()).setActionBarTitle("INPDR");
    }
    /**
     * initialize the menu options
     */
    @Override
    public void onCreateOptionsMenu(
            Menu menu, MenuInflater inflater) {
        inflater.inflate(R.menu.menu_main, menu);

        Log.d("action bar setting", "aaa");
        MenuItem item1 = menu.findItem(R.id.action_settings);
        item1.setVisible(true);

        MenuItem item2 = menu.findItem(R.id.action_chart);
        item2.setVisible(false);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        final APIReceivable receivable = this;
        final INPDRAPI api = new INPDRAPI(getContext());
        Log.d("item selected", "");
        switch (item.getItemId()) {
            case R.id.action_settings:
                // User chose the "Favorite" action, mark the current item
                return true;

//            case R.id.action_logout:
//                return true;
            default:
                Log.d("button pressed","");
                // If we got here, the user's action was not recognized.
                // Invoke the superclass to handle it.
                return super.onOptionsItemSelected(item);

        }
    }

    @Override
    public void onViewCreated(View view, Bundle savedInstanceState) {
        final INPDRAPI api = new INPDRAPI(getContext());
        final APIReceivable receivable = this;

        ModelFacade.getInstance().getAllPatients(this, api);

        swipeRefreshLayout = (SwipeRefreshLayout) getView().findViewById(R.id.swipe_container);
        // the refresh listner. this would be called when the layout is pulled down
        swipeRefreshLayout.setOnRefreshListener(new SwipeRefreshLayout.OnRefreshListener() {

            @Override
            public void onRefresh() {
                swipeRefreshLayout.setRefreshing(true);
                ModelFacade.getInstance().getAllPatients(receivable, api);
                Log.e("refreshing", swipeRefreshLayout.isRefreshing() + "");
                //displayPatients();
                swipeRefreshLayout.setRefreshing(false);
                Toast.makeText(getView().getContext(), "refreshed!", Toast.LENGTH_SHORT).show();
            }
        });
    }
    @Override
    public <U extends APIItem> void receiveData(List<U> items, String type) {
        if(type.equals("getAllPatients")){
            patients.clear();
            for (U item : items) {
                Patient feed = (Patient) item;
                patients.add(feed);
                //Log.d("!!!!!data received!!!", feed.getUser().getEmail() + "");
            }
            displayPatients(patients);
        }

//        else if(type.equals("getANpcParticipants")){
//            npcParticipants.clear();
//            for (U item : items) {
//                NpcParticipant feed = (NpcParticipant) item;
//                npcParticipants.add(feed);
//                Log.d("!!!!NpcPar", feed.getDate_of_birth() + "");
//            }
//            displayPatients(npcParticipants);
//        }
    }

    @Override
    public <U extends APIItem> void receiveData(U item, String type) {
        switch (type){
            case "error":
                final MaterialDialog mMaterialDialog_e = new MaterialDialog(getContext());
                mMaterialDialog_e.setTitle("Error");
                ErrorMessage errorMessage = (ErrorMessage) item;
                mMaterialDialog_e.setMessage(errorMessage.getErrorMes());
                mMaterialDialog_e.setPositiveButton("Ok", new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        mMaterialDialog_e.dismiss();
                    }
                });
                mMaterialDialog_e.show();
        }

    }

    public void displayPatients(ArrayList<Patient> feeds){
        ViewGroup insertPoint = (ViewGroup) getView().findViewById(R.id.lytHome);

        TextView textUserEmail = (TextView)getView().findViewById(R.id.text_useremail);
        LayoutInflater vi = (LayoutInflater) getActivity().getSystemService(Context.LAYOUT_INFLATER_SERVICE);

        for (final Patient feed : feeds) {

            View v = vi.inflate(R.layout.item_home, null);

            TextView textId = (TextView) v.findViewById(R.id.text_ID_detail);
            textId.setTypeface(mTf);
            if(feed.getEnrollable_type().equals("NpcParticipant")){
                textId.setText("NPC" + feed.getUnique_id());
            }
            else if(feed.getEnrollable_type().equals("NpbParticipant")){
                textId.setText("NPB" + feed.getUnique_id());
            }
            else if(feed.getEnrollable_type().equals("NpaApplicantParticipant")){
                textId.setText("NPA" + feed.getUnique_id());
                Button buttonInDevelopment = (Button)v.findViewById(R.id.button_followup);
                buttonInDevelopment.setText("In Development");
                buttonInDevelopment.setEnabled(false);
            }
            else if(feed.getEnrollable_type().equals("NpbApplicantParticipant")){
                textId.setText("NPB" + feed.getUnique_id());
            }
            else if(feed.getEnrollable_type().equals("NpcApplicantParticipant")){
                textId.setText("NPC" + feed.getUnique_id());
            }

            if(!feed.isVerified()){
                Button buttonUnverified = (Button)v.findViewById(R.id.button_followup);
                buttonUnverified.setText("Unverified");
                buttonUnverified.setEnabled(false);
            }

            if(feed.isNot_finish()){
                Button buttonSaved = (Button)v.findViewById(R.id.button_view);
                buttonSaved.setText("Unfinished");
                buttonSaved.setEnabled(false);
            }
            textUserEmail.setText(feed.getUser().getEmail() + "");
            textUserEmail.setTypeface(mTf);

            TextView textType = (TextView) v.findViewById(R.id.text_patient_type_detail_home);
            textType.setText(feed.getEnrollable_type());


            Button buttonView = (Button) v.findViewById(R.id.button_view);

            buttonView.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    Bundle data = new Bundle();
                    data.putInt("patientID", feed.getId());
                    Fragment PatientFragment = new PatientDetailFragment();
                    PatientFragment.setArguments(data);

                    FragmentManager fm = getFragmentManager();
                    FragmentTransaction ft = fm.beginTransaction();

                    ft.replace(R.id.main_view, PatientFragment);
                    ft.addToBackStack("HomeFragment");
                    ft.commit();
                }
            });

            Button buttonFollowup = (Button) v.findViewById(R.id.button_followup);

            buttonFollowup.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    Bundle data = new Bundle();
                    data.putInt("followable_id", feed.getEnrollable_id());
                    data.putString("followable_type", feed.getEnrollable_type());
                    data.putString("unique_id", feed.getUnique_id());
                    Fragment followupFragment = new FollowupFragment();
                    followupFragment.setArguments(data);

                    FragmentTransaction transaction = getFragmentManager()
                            .beginTransaction();
                    transaction.add(R.id.main_view, followupFragment,
                            "FragmentFollowup"); //now you have an instance of newFragment1//now you have an instance of newFragment2
                    transaction.addToBackStack("HomeFragment");
                    transaction.show(followupFragment);
                    transaction.setTransition(FragmentTransaction.TRANSIT_FRAGMENT_OPEN);
                    transaction.commit();

//                    FragmentManager fm = getFragmentManager();
//                    FragmentTransaction ft = fm.beginTransaction();
//
//                    ft.replace(R.id.main_view, followupDetailFragment);
//                    ft.addToBackStack(null);
//                    ft.commit();
                }
            });

            insertPoint.addView(
                    v,
                    2,
                    new ViewGroup.LayoutParams(ViewGroup.LayoutParams.MATCH_PARENT, ViewGroup.LayoutParams.WRAP_CONTENT));


        }


    }


}
