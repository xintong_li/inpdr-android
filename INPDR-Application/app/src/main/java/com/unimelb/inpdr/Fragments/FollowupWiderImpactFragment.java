package com.unimelb.inpdr.Fragments;

import android.content.Context;
import android.content.DialogInterface;
import android.content.SharedPreferences;
import android.graphics.Color;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentTransaction;
import android.support.v7.app.AlertDialog;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.TextView;
import android.widget.Toast;

import com.jaredrummler.materialspinner.MaterialSpinner;
import com.unimelb.inpdr.MainActivity;
import com.unimelb.inpdr.Models.API.APIItem;
import com.unimelb.inpdr.Models.API.APIReceivable;
import com.unimelb.inpdr.Models.API.INPDRAPI;
import com.unimelb.inpdr.Models.API.ModelFacade;
import com.unimelb.inpdr.Models.BasicInfo;
import com.unimelb.inpdr.Models.ErrorMessage;
import com.unimelb.inpdr.Models.NpbApplicantFollowupCollection;
import com.unimelb.inpdr.Models.NpbFollowUp;
import com.unimelb.inpdr.Models.NpbFollowupCollection;
import com.unimelb.inpdr.Models.NpcApplicantFollowupCollection;
import com.unimelb.inpdr.Models.NpcFollowUp;
import com.unimelb.inpdr.Models.NpcFollowupCollection;
import com.unimelb.inpdr.Models.Selection;
import com.unimelb.inpdr.R;

import org.apache.http.client.utils.URIBuilder;

import java.net.URISyntaxException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import me.drakeet.materialdialog.MaterialDialog;

/**
 * Created by Xintong on 17/05/2016.
 */
public class FollowupWiderImpactFragment extends Fragment implements APIReceivable{
    int patientId;
    String patientType;
    boolean isFirstTime;
    Map<String, String> npbCollection = new HashMap<>();
    Map<String, String> npbApplicantCollection = new HashMap<>();
    Map<String, String> npcCollection = new HashMap<>();
    Map<String, String> npcApplicantCollection = new HashMap<>();

    Map<String, Integer> npbIndexes = new HashMap<>();
    Map<String, Integer> npcIndexes = new HashMap<>();
    List<String> npbParams = new ArrayList<>();
    List<String> npcParams = new ArrayList<>();


    public FollowupWiderImpactFragment() {
        // Required empty public constructor
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        return inflater.inflate(R.layout.fragment_followup_wider_impact, container, false);
    }

    @Override
    public void onStart(){
        super.onStart();
        ((MainActivity)getActivity()).setActionBarTitle("Create Followup");
    }

    @Override
    public void onViewCreated(View view, Bundle savedInstanceState) {
        BasicInfo.getInstance();
        NpbFollowupCollection.getInstance();
        NpbApplicantFollowupCollection.getInstance();
        NpcFollowupCollection.getInstance();
        NpcApplicantFollowupCollection.getInstance();
        Selection.getInstance();

        ((MainActivity)getActivity()).setActionBarTitle("Create Followup");

        MapInitialization();

        Bundle data = getArguments();

        if (data != null) {
            try {
                isFirstTime = data.getBoolean("first_time");
                patientId = data.getInt("followable_id");
                patientType = data.getString("followable_type");
                switch (patientType) {
                    case "NpbParticipant":
                        npbCollection.put("symptom_bone", data.getString("symptom_bone"));
                        npbCollection.put("symptom_abdominal", data.getString("symptom_abdominal"));
                        npbCollection.put("symptom_pain", data.getString("symptom_pain"));
                        npbCollection.put("impact_wider_emergency", data.getString("impact_wider_emergency"));
                        npbCollection.put("impact_wider_appointment", data.getString("impact_wider_appointment"));
                        npbCollection.put("symptom_cognition", data.getString("symptom_cognition"));
                        npbCollection.put("symptom_bleeding_rate", data.getString("symptom_bleeding_rate"));
                        npbCollection.put("symptom_bleeding_impact", data.getString("symptom_bleeding_impact"));
                        npbCollection.put("symptom_breathlessness_rate", data.getString("symptom_breathlessness_rate"));
                        npbCollection.put("symptom_breathlessness_impact", data.getString("symptom_breathlessness_impact"));
                        npbCollection.put("symptom_fatigue_rate", data.getString("symptom_fatigue_rate"));
                        npbCollection.put("symptom_fatigue_impact", data.getString("symptom_fatigue_impact"));
                        npbCollection.put("symptom_enlarge_organ", data.getString("symptom_enlarge_organ"));
                        npbCollection.put("symptom_enlarge_organ_impact", data.getString("symptom_enlarge_organ_impact"));
                        npbCollection.put("symptom_slow_growth", data.getString("symptom_slow_growth"));
                        npbCollection.put("symptom_slow_growth_impact", data.getString("symptom_slow_growth_impact"));
                        npbCollection.put("symptom_infection", data.getString("symptom_infection"));
                        npbCollection.put("symptom_fracture", data.getString("symptom_fracture"));
                        npbCollection.put("impact_family_give_up", data.getString("impact_family_give_up"));
                        npbCollection.put("impact_family_disappointed", data.getString("impact_family_disappointed"));
                        npbCollection.put("impact_family_worry_future", data.getString("impact_family_worry_future"));
                        npbCollection.put("impact_family_closer", data.getString("impact_family_closer"));
                        CreateNpbFollowup();
                        break;
                    case "NpbApplicantParticipant":
                        npbApplicantCollection.put("symptom_bone", data.getString("symptom_bone"));
                        npbApplicantCollection.put("symptom_abdominal", data.getString("symptom_abdominal"));
                        npbApplicantCollection.put("symptom_pain", data.getString("symptom_pain"));
                        npbApplicantCollection.put("impact_wider_emergency", data.getString("impact_wider_emergency"));
                        npbApplicantCollection.put("impact_wider_appointment", data.getString("impact_wider_appointment"));
                        npbApplicantCollection.put("symptom_cognition", data.getString("symptom_cognition"));
                        npbApplicantCollection.put("symptom_bleeding_rate", data.getString("symptom_bleeding_rate"));
                        npbApplicantCollection.put("symptom_bleeding_impact", data.getString("symptom_bleeding_impact"));
                        npbApplicantCollection.put("symptom_breathlessness_rate", data.getString("symptom_breathlessness_rate"));
                        npbApplicantCollection.put("symptom_breathlessness_impact", data.getString("symptom_breathlessness_impact"));
                        npbApplicantCollection.put("symptom_fatigue_rate", data.getString("symptom_fatigue_rate"));
                        npbApplicantCollection.put("symptom_fatigue_impact", data.getString("symptom_fatigue_impact"));
                        npbApplicantCollection.put("symptom_enlarge_organ", data.getString("symptom_enlarge_organ"));
                        npbApplicantCollection.put("symptom_enlarge_organ_impact", data.getString("symptom_enlarge_organ_impact"));
                        npbApplicantCollection.put("symptom_slow_growth", data.getString("symptom_slow_growth"));
                        npbApplicantCollection.put("symptom_slow_growth_impact", data.getString("symptom_slow_growth_impact"));
                        npbApplicantCollection.put("symptom_infection", data.getString("symptom_infection"));
                        npbApplicantCollection.put("symptom_fracture", data.getString("symptom_fracture"));
                        npbApplicantCollection.put("impact_family_give_up", data.getString("impact_family_give_up"));
                        npbApplicantCollection.put("impact_family_disappointed", data.getString("impact_family_disappointed"));
                        npbApplicantCollection.put("impact_family_worry_future", data.getString("impact_family_worry_future"));
                        npbApplicantCollection.put("impact_family_closer", data.getString("impact_family_closer"));
                        CreateApplicantNpbFollowup();
                        break;
                    case "NpcParticipant":
                        npcCollection.put("impact_ambulation", data.getString("impact_ambulation"));
                        npcCollection.put("impact_manipulation", data.getString("impact_manipulation"));
                        npcCollection.put("impact_speech", data.getString("impact_speech"));
                        npcCollection.put("impact_swallowing", data.getString("impact_swallowing"));
                        npcCollection.put("impact_eye_movement", data.getString("impact_eye_movement"));
                        npcCollection.put("impact_seizure", data.getString("impact_seizure"));
                        npcCollection.put("impact_cognitive_impaired", data.getString("impact_cognitive_impaired"));
                        npcCollection.put("impact_family_disappointed", data.getString("impact_family_disappointed"));
                        npcCollection.put("impact_family_give_up", data.getString("impact_family_give_up"));
                        npcCollection.put("impact_family_worry_future", data.getString("impact_family_worry_future"));
                        npcCollection.put("impact_family_closer_family", data.getString("impact_family_closer_family"));
                        CreateNpcFollowup();
                        break;
                    case "NpcApplicantParticipant":
                        npcApplicantCollection.put("impact_ambulation", data.getString("impact_ambulation"));
                        npcApplicantCollection.put("impact_manipulation", data.getString("impact_manipulation"));
                        npcApplicantCollection.put("impact_speech", data.getString("impact_speech"));
                        npcApplicantCollection.put("impact_swallowing", data.getString("impact_swallowing"));
                        npcApplicantCollection.put("impact_eye_movement", data.getString("impact_eye_movement"));
                        npcApplicantCollection.put("impact_seizure", data.getString("impact_seizure"));
                        npcApplicantCollection.put("impact_cognitive_impaired", data.getString("impact_cognitive_impaired"));
                        npcApplicantCollection.put("impact_family_disappointed", data.getString("impact_family_disappointed"));
                        npcApplicantCollection.put("impact_family_give_up", data.getString("impact_family_give_up"));
                        npcApplicantCollection.put("impact_family_worry_future", data.getString("impact_family_worry_future"));
                        npcApplicantCollection.put("impact_family_closer_family", data.getString("impact_family_closer_family"));
                        CreateApplicantNpcFollowup();
                        break;
                }
            }catch (Exception e){
                e.printStackTrace();
            }
        }


//        NiceSpinner niceSpinner = (NiceSpinner) view.findViewById(R.id.spinner);
//        List<String> dataset = new LinkedList<>(Arrays.asList("One", "Two", "Three", "Four", "Five"));
//        niceSpinner.attachDataSource(dataset);

        // ArrayAdapter<String> adapter = new ArrayAdapter<String>(getContext(), android.R.layout.simple_spinner_dropdown_item, countries);

//        NiceSpinnerAdapter adapter = new NiceSpinnerAdapter(getContext(), dataset, Color.DKGRAY, 2);
//        spinner.setAdapter(adapter);
        // Getting object reference to listview of main.xml
//        ListView listView = (ListView) view.findViewById(R.id.listView);
//
//        // Instantiating array adapter to populate the listView
//        // The layout android.R.layout.simple_list_item_single_choice creates radio button for each listview item
//
//        ArrayAdapter<String> adapter = new ArrayAdapter<String>(getContext(), android.R.layout.simple_list_item_single_choice, countries);
//
//        listView.setAdapter(adapter);

    }

    private void MapInitialization() {
//        npbIndexes.put("symptom_bone", 0);
//        npbIndexes.put("symptom_abdominal", 1);
//        npbIndexes.put("symptom_pain", 2);
//        npbIndexes.put("symptom_cognition", 3);
//        npbIndexes.put("symptom_breathlessness_rate", 4);
//        npbIndexes.put("symptom_breathlessness_impact", 5);
//        npbIndexes.put("symptom_bleeding_rate", 6);
//        npbIndexes.put("symptom_bleeding_impact", 7);
//        npbIndexes.put("symptom_fatigue_rate", 8);
//        npbIndexes.put("symptom_fatigue_impact", 9);
//        npbIndexes.put("symptom_enlarge_organ", 10);
//        npbIndexes.put("symptom_enlarge_organ_impact", 11);
//        npbIndexes.put("symptom_slow_growth", 12);
//        npbIndexes.put("symptom_slow_growth_impact", 13);
//        npbIndexes.put("symptom_infection", 14);
//        npbIndexes.put("symptom_fracture", 15);
//        npbIndexes.put("impact_family_disappointed", 0);
//        npbIndexes.put("impact_family_give_up", 1);
//        npbIndexes.put("impact_family_worry_future", 2);
//        npbIndexes.put("impact_family_closer", 3);
        npbIndexes.put("impact_wider_self_miss_career", 0);
        npbIndexes.put("impact_wider_carer_miss_career", 1);
        npbIndexes.put("impact_wider_emergency", 2);
        npbIndexes.put("impact_wider_appointment", 3);

        npbParams.add("impact_wider_self_miss_career");
        npbParams.add("impact_wider_carer_miss_career");
        npbParams.add("impact_wider_emergency");
        npbParams.add("impact_wider_appointment");
        npbParams.add("symptom_bone");
        npbParams.add("symptom_abdominal");
        npbParams.add("symptom_pain");
        npbParams.add("symptom_cognition");
        npbParams.add("symptom_breathlessness_rate");
        npbParams.add("symptom_breathlessness_impact");
        npbParams.add("symptom_bleeding_rate");
        npbParams.add("symptom_bleeding_impact");
        npbParams.add("symptom_fatigue_rate");
        npbParams.add("symptom_fatigue_impact");
        npbParams.add("symptom_enlarge_organ");
        npbParams.add("symptom_enlarge_organ_impact");
        npbParams.add("symptom_slow_growth");
        npbParams.add("symptom_slow_growth_impact");
        npbParams.add("symptom_infection");
        npbParams.add("symptom_fracture");
        npbParams.add("impact_family_disappointed");
        npbParams.add("impact_family_give_up");
        npbParams.add("impact_family_worry_future");
        npbParams.add("impact_family_closer");


//        npcIndexes.put("impact_ambulation", 0);
//        npcIndexes.put("impact_manipulation", 1);
//        npcIndexes.put("impact_speech", 2);
//        npcIndexes.put("impact_swallowing", 3);
//        npcIndexes.put("impact_eye_movement", 4);
//        npcIndexes.put("impact_seizure", 5);
//        npcIndexes.put("impact_cognitive_impaired", 6);
//        npcIndexes.put("impact_family_disappointed", 0);
//        npcIndexes.put("impact_family_give_up", 1);
//        npcIndexes.put("impact_family_worry_future", 2);
//        npcIndexes.put("impact_family_closer_family", 3);
        npcIndexes.put("impact_wider_self_miss_career", 0);
        npcIndexes.put("impact_wider_family_miss_career", 1);
        npcIndexes.put("impact_wider_emergency", 2);
        npcIndexes.put("impact_wider_appointment", 3);

        npcParams.add("impact_wider_self_miss_career");
        npcParams.add("impact_wider_family_miss_career");
        npcParams.add("impact_wider_emergency");
        npcParams.add("impact_wider_appointment");
        npcParams.add("impact_family_disappointed");
        npcParams.add("impact_family_give_up");
        npcParams.add("impact_family_worry_future");
        npcParams.add("impact_family_closer_family");
        npcParams.add("impact_ambulation");
        npcParams.add("impact_manipulation");
        npcParams.add("impact_speech");
        npcParams.add("impact_swallowing");
        npcParams.add("impact_eye_movement");
        npcParams.add("impact_seizure");
        npcParams.add("impact_cognitive_impaired");
    }


    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

    }

    @Override
    public <U extends APIItem> void receiveData(List<U> items, String type) {

    }

    @Override
    public <U extends APIItem> void receiveData(U item, String type) {
        switch (type) {
            case "createANpbFollowup":
                final AlertDialog alertDialog_npb = new AlertDialog.Builder(getContext()).create();
                alertDialog_npb.setTitle("Congratulations");
                alertDialog_npb.setMessage("The new questionnaire is successfully created.");
                alertDialog_npb.setIcon(R.drawable.icon_congras);

                alertDialog_npb.setButton(AlertDialog.BUTTON_NEUTRAL, "Ok",
                        new DialogInterface.OnClickListener() {
                            public void onClick(DialogInterface dialog, int which) {
                                alertDialog_npb.dismiss();
                            }
                        });

                alertDialog_npb.show();

                NpbFollowUp npbFollowUp = (NpbFollowUp)item;
                Bundle data = new Bundle();

                data.putInt("followable_id", npbFollowUp.getFollowable_id());
                data.putString("followable_type", npbFollowUp.getFollowable_type());
                data.putInt("followup_id", npbFollowUp.getId());

                Fragment followupDetailFragment = new FollowupDetailFragment();
                Fragment followupFragment = new FollowupFragment();
                followupDetailFragment.setArguments(data);

                FragmentTransaction transaction = getFragmentManager()
                        .beginTransaction();

                transaction.add(R.id.main_view, followupFragment,
                        "FragmentFollowup");
                transaction.add(R.id.main_view, followupDetailFragment,
                        "FragmentFollowupDetail");
                transaction.addToBackStack("FragmentFollowup");
                transaction.show(followupDetailFragment);
                transaction.setTransition(FragmentTransaction.TRANSIT_FRAGMENT_CLOSE);
                transaction.commit();
                break;
            case "createANpcFollowup":
                final AlertDialog alertDialog_npc = new AlertDialog.Builder(getContext()).create();
                alertDialog_npc.setTitle("Congratulations");
                alertDialog_npc.setMessage("The new questionnaire is successfully created.");
                alertDialog_npc.setIcon(R.drawable.icon_congras);

                alertDialog_npc.setButton(AlertDialog.BUTTON_NEUTRAL, "Ok",
                        new DialogInterface.OnClickListener() {
                            public void onClick(DialogInterface dialog, int which) {
                                alertDialog_npc.dismiss();
                            }
                        });

                alertDialog_npc.show();

                NpcFollowUp npcFollowUp = (NpcFollowUp)item;
                data = new Bundle();

                data.putInt("followable_id", npcFollowUp.getFollowable_id());
                data.putString("followable_type", npcFollowUp.getFollowable_type());
                data.putInt("followup_id", npcFollowUp.getId());

                followupDetailFragment = new FollowupDetailFragment();
                followupFragment = new FollowupFragment();
                followupDetailFragment.setArguments(data);

                FragmentManager fm = getFragmentManager();
                fm.getBackStackEntryAt(0);
                transaction = fm.beginTransaction();

                transaction.add(R.id.main_view, followupDetailFragment,
                        "FragmentFollowupDetail");
                transaction.addToBackStack("FragmentFollowup");
                transaction.show(followupDetailFragment);
                transaction.setTransition(FragmentTransaction.TRANSIT_FRAGMENT_FADE);
                transaction.commit();
                break;
            case "error":
                ErrorMessage error = (ErrorMessage) item;
                final MaterialDialog mMaterialDialog_e = new MaterialDialog(getContext());
                mMaterialDialog_e.setTitle("Error");
                ErrorMessage errorMessage = (ErrorMessage) item;
                mMaterialDialog_e.setMessage(errorMessage.getErrorMes());
                mMaterialDialog_e.setPositiveButton("Ok", new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        mMaterialDialog_e.dismiss();
                    }
                });
                mMaterialDialog_e.show();
                break;
        }
    }



    private void CreateNpcFollowup() {
        final APIReceivable receivable = this;
        final INPDRAPI api = new INPDRAPI(getContext());

        TextView textDiscription = (TextView)getView().findViewById(R.id.text_explaination_wider_impact_followup);
        textDiscription.setText("The following section looks at the wide impact of Niemann-Pick disease. " +
                "For each question, please select the option that you feel is most accurate.");

        TextView textNpcCreate = (TextView)getView().findViewById(R.id.text_wider_impact_followup_title);
        textNpcCreate.setText("Create NP-C Followup Questionnaire");

        int index = 0;
        final ViewGroup insertPoint = (ViewGroup) getView().findViewById(R.id.lty_wider_impact_followup);
        LayoutInflater vi = (LayoutInflater) getActivity().getSystemService(Context.LAYOUT_INFLATER_SERVICE);

        List<String> answers_impact_wider_self_miss_career = new ArrayList<>();
        answers_impact_wider_self_miss_career.add(" ");
        for(int i = 0; i < NpcFollowupCollection.getImpact_wider_self_miss_career().size(); i++){
            answers_impact_wider_self_miss_career.add(NpcFollowupCollection.getImpact_wider_self_miss_career().get(i));
        }
        NpcInflater(vi, answers_impact_wider_self_miss_career, "impact_wider_self_miss_career", insertPoint, index, npcCollection, BasicInfo.getNpc_participant_followups_questions());
        index++;

        List<String> answers_impact_wider_family_miss_career = new ArrayList<>();
        answers_impact_wider_family_miss_career.add(" ");
        for(int i = 0; i < NpcFollowupCollection.getImpact_wider_family_miss_career().size(); i++){
            answers_impact_wider_family_miss_career.add(NpcFollowupCollection.getImpact_wider_family_miss_career().get(i));
        }
        NpcInflater(vi, answers_impact_wider_family_miss_career, "impact_wider_family_miss_career", insertPoint, index, npcCollection, BasicInfo.getNpc_participant_followups_questions());
        index++;

        List<String> answers_impact_wider_emergency = new ArrayList<>();
        answers_impact_wider_emergency.add(" ");
        for(int i = 0; i < NpcFollowupCollection.getImpact_wider_emergency().size(); i++){
            answers_impact_wider_emergency.add(NpcFollowupCollection.getImpact_wider_emergency().get(i));
        }
        NpcInflater(vi, answers_impact_wider_emergency, "impact_wider_emergency", insertPoint, index, npcCollection, BasicInfo.getNpc_participant_followups_questions());
        index++;

        List<String> answers_impact_wider_appointment = new ArrayList<>();
        answers_impact_wider_appointment.add(" ");
        for(int i = 0; i < NpcFollowupCollection.getImpact_wider_appointment().size(); i++){
            answers_impact_wider_appointment.add(NpcFollowupCollection.getImpact_wider_appointment().get(i));
        }
        NpcInflater(vi, answers_impact_wider_appointment, "impact_wider_appointment", insertPoint, index, npcCollection, BasicInfo.getNpc_participant_followups_questions());


        Button buttonDone = (Button) getView().findViewById(R.id.button_done);

        buttonDone.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                boolean isFullAnswer = true;
                for (int i = 0; i < npcIndexes.size(); i++) {
                    if (!npcCollection.containsKey(npcParams.get(i))) {
                        isFullAnswer = false;
                        Log.d("lack of params", npcParams.get(i) + " " + i);
                        TextView textQuestion = (TextView) insertPoint.getChildAt(i).findViewById(R.id.text_create_followup_question);
                        textQuestion.setHint(BasicInfo.getNpc_participant_followups_questions().get(npcParams.get(i)) + " cannot be blank");
                        textQuestion.setTextColor(Color.RED);
                    } else {
                        Log.d("got params", npcParams.get(i) + " " + i);
                        TextView textQuestion = (TextView) insertPoint.getChildAt(i).findViewById(R.id.text_create_followup_question);
                        textQuestion.setHint(BasicInfo.getNpc_participant_followups_questions().get(npcParams.get(i)) + " cannot be blank");
                        textQuestion.setTextColor(Color.BLACK);
                    }
                }
                if (isFullAnswer) {
                    String path = String.format("/api/v1/%s/%d/npc_followups",
                            patientType, patientId);

                    URIBuilder builder = new URIBuilder();
                    try {
                        builder.setScheme("http")
                                .setHost("10.0.3.2")
                                .setPort(3000)
                                .setPath(path)
                                .addParameter("impact_ambulation", npcCollection.get("impact_ambulation"))
                                .addParameter("impact_manipulation", npcCollection.get("impact_manipulation"))
                                .addParameter("impact_speech", npcCollection.get("impact_speech"))
                                .addParameter("impact_swallowing", npcCollection.get("impact_swallowing"))
                                .addParameter("impact_eye_movement", npcCollection.get("impact_eye_movement"))
                                .addParameter("impact_seizure", npcCollection.get("impact_seizure"))
                                .addParameter("impact_cognitive_impaired", npcCollection.get("impact_cognitive_impaired"))
                                .addParameter("impact_family_disappointed", npcCollection.get("impact_family_disappointed"))
                                .addParameter("impact_family_give_up", npcCollection.get("impact_family_give_up"))
                                .addParameter("impact_family_worry_future", npcCollection.get("impact_family_worry_future"))
                                .addParameter("impact_family_closer_family", npcCollection.get("impact_family_closer_family"))
                                .addParameter("impact_wider_self_miss_career", npcCollection.get("impact_wider_self_miss_career"))
                                .addParameter("impact_wider_family_miss_career", npcCollection.get("impact_wider_family_miss_career"))
                                .addParameter("impact_wider_emergency", npcCollection.get("impact_wider_emergency"))
                                .addParameter("impact_wider_appointment", npcCollection.get("impact_wider_appointment"))
                                .build();
                        // .fragment("section-name");
                        final String url = builder.toString();
                        Log.d("url=>", url);
                        AlertDialog alertDialog = new AlertDialog.Builder(getContext()).create();
                        alertDialog.setTitle("Alert");
                        alertDialog.setMessage("Ready to submit?");
                        alertDialog.setIcon(R.drawable.icon_alert);

                        alertDialog.setButton(AlertDialog.BUTTON_POSITIVE, "Yes",
                                new DialogInterface.OnClickListener() {
                                    public void onClick(DialogInterface dialog, int which) {
                                        ModelFacade.getInstance().createANpcFollowup(receivable, api, url);
                                    }
                                });

                        alertDialog.setButton(AlertDialog.BUTTON_NEGATIVE, "No",
                                new DialogInterface.OnClickListener() {
                                    public void onClick(DialogInterface dialog, int which) {
                                        dialog.dismiss();
                                    }
                                });

                        alertDialog.show();

                    } catch (URISyntaxException e) {
                        e.printStackTrace();
                    }
                }else{
                    Toast.makeText(getView().getContext(), "issue!", Toast.LENGTH_SHORT).show();
               }

            }
        });

    }

    private void NpcInflater(LayoutInflater vi, final List<String> items, final String param, ViewGroup insertPoint, int index, final Map<String, String> answers, Map<String, String> questions) {
        View v = vi.inflate(R.layout.item_create_followup, null);
        final TextView textQuestion = (TextView)v.findViewById(R.id.text_create_followup_question);

        textQuestion.setText(questions.get(param));
        MaterialSpinner spinner = (MaterialSpinner) v.findViewById(R.id.spinner);
        spinner.setTextColor(getResources().getColor(R.color.colorPrimaryText));
        spinner.setItems(items);

        if(!isFirstTime){
            SharedPreferences myPrefs = getActivity().getSharedPreferences("myPrefs", 0);
            Log.d("not first time", "");
            if(myPrefs.contains(param)){
                spinner.setSelectedIndex(myPrefs.getInt(param, 0));
                Log.d("get prefs", myPrefs.getInt(param, 0)+ " " + param);
            }
        }

        spinner.setOnItemSelectedListener(new MaterialSpinner.OnItemSelectedListener<String>() {

            @Override
            public void onItemSelected(MaterialSpinner view, int position, long id, String item) {
                if (position != 0) {
                    textQuestion.setTextColor(Color.BLACK);
                    answers.put(param, position - 1 + "");
                } else {
                    if (answers.containsKey(param)) {
                        answers.remove(param);
                    }
                }
                int mSpnValue = position;
                SharedPreferences myPrefs = getActivity().getSharedPreferences("myPrefs", getContext().MODE_MULTI_PROCESS);
                SharedPreferences.Editor prefsEditor = myPrefs.edit();
                prefsEditor.putInt(param, mSpnValue);
                prefsEditor.commit();
                Log.d("valuee", " " + param + ", " + mSpnValue);
//
//                Context context = view.getContext();
//                CharSequence text = answers.get(param) + " " + param;
//                int duration = Toast.LENGTH_SHORT;
//
//                Toast toast = Toast.makeText(context, text, duration);
//                toast.show();
            }
        });
        spinner.onSaveInstanceState();
        if(spinner.getSelectedIndex() != 0) {
            answers.put(param, spinner.getSelectedIndex() - 1 + "");
        }

        insertPoint.addView(
                v,
                index,
                new ViewGroup.LayoutParams(ViewGroup.LayoutParams.MATCH_PARENT, ViewGroup.LayoutParams.WRAP_CONTENT));
    }

    private void CreateApplicantNpcFollowup() {
        final APIReceivable receivable = this;
        final INPDRAPI api = new INPDRAPI(getContext());

        TextView textDiscription = (TextView)getView().findViewById(R.id.text_explaination_wider_impact_followup);
        textDiscription.setText("The following section looks at the wide impact of Niemann-Pick disease. " +
                "For each question, please select the option that you feel is most accurate.");

        TextView textNpcCreate = (TextView)getView().findViewById(R.id.text_wider_impact_followup_title);
        textNpcCreate.setText("Create NP-C Followup Questionnaire");

        int index = 0;
        final ViewGroup insertPoint = (ViewGroup) getView().findViewById(R.id.lty_wider_impact_followup);
        LayoutInflater vi = (LayoutInflater) getActivity().getSystemService(Context.LAYOUT_INFLATER_SERVICE);

        List<String> answers_impact_wider_self_miss_career = new ArrayList<>();
        answers_impact_wider_self_miss_career.add(" ");
        for(int i = 0; i < NpcApplicantFollowupCollection.getImpact_wider_self_miss_career().size(); i++){
            answers_impact_wider_self_miss_career.add(NpcApplicantFollowupCollection.getImpact_wider_self_miss_career().get(i));
        }
        NpcInflater(vi, answers_impact_wider_self_miss_career, "impact_wider_self_miss_career", insertPoint, index, npcApplicantCollection, BasicInfo.getNpc_applicant_participant_followups_questions());
        index++;

        List<String> answers_impact_wider_family_miss_career = new ArrayList<>();
        answers_impact_wider_family_miss_career.add(" ");
        for(int i = 0; i < NpcApplicantFollowupCollection.getImpact_wider_family_miss_career().size(); i++){
            answers_impact_wider_family_miss_career.add(NpcApplicantFollowupCollection.getImpact_wider_family_miss_career().get(i));
        }
        NpcInflater(vi, answers_impact_wider_family_miss_career, "impact_wider_family_miss_career", insertPoint, index, npcApplicantCollection, BasicInfo.getNpc_applicant_participant_followups_questions());
        index++;

        List<String> answers_impact_wider_emergency = new ArrayList<>();
        answers_impact_wider_emergency.add(" ");
        for(int i = 0; i < NpcApplicantFollowupCollection.getImpact_wider_emergency().size(); i++){
            answers_impact_wider_emergency.add(NpcApplicantFollowupCollection.getImpact_wider_emergency().get(i));
        }
        NpcInflater(vi, answers_impact_wider_emergency, "impact_wider_emergency", insertPoint, index, npcApplicantCollection, BasicInfo.getNpc_applicant_participant_followups_questions());
        index++;

        List<String> answers_impact_wider_appointment = new ArrayList<>();
        answers_impact_wider_appointment.add(" ");
        for(int i = 0; i < NpcApplicantFollowupCollection.getImpact_wider_appointment().size(); i++){
            answers_impact_wider_appointment.add(NpcApplicantFollowupCollection.getImpact_wider_appointment().get(i));
        }
        NpcInflater(vi, answers_impact_wider_appointment, "impact_wider_appointment", insertPoint, index, npcApplicantCollection, BasicInfo.getNpc_applicant_participant_followups_questions());


        Button buttonDone = (Button) getView().findViewById(R.id.button_done);

        buttonDone.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                boolean isFullAnswer = true;
                for (int i = 0; i < npcIndexes.size(); i++) {
                    if(!npcApplicantCollection.containsKey(npcParams.get(i))){
                        isFullAnswer = false;
                        TextView textQuestion = (TextView)insertPoint.getChildAt(i).findViewById(R.id.text_create_followup_question);
                        textQuestion.setHint(BasicInfo.getNpc_applicant_participant_followups_questions().get(npcParams.get(i)) + " cannot be blank");
                        textQuestion.setTextColor(Color.RED);
                    }else {
                        TextView textQuestion = (TextView)insertPoint.getChildAt(i).findViewById(R.id.text_create_followup_question);
                        textQuestion.setHint(BasicInfo.getNpc_applicant_participant_followups_questions().get(npcParams.get(i))+ " cannot be blank");
                        textQuestion.setTextColor(Color.BLACK);
                    }
                }

                if(isFullAnswer) {
                    String path = String.format("/api/v1/%s/%d/npc_followups",
                            patientType, patientId);

                    URIBuilder builder = new URIBuilder();
                    try {
                        builder.setScheme("http")
                                .setHost("10.0.3.2")
                                .setPort(3000)
                                .setPath(path)
                                .addParameter("impact_ambulation", npcApplicantCollection.get("impact_ambulation"))
                                .addParameter("impact_manipulation", npcApplicantCollection.get("impact_manipulation"))
                                .addParameter("impact_speech", npcApplicantCollection.get("impact_speech"))
                                .addParameter("impact_swallowing", npcApplicantCollection.get("impact_swallowing"))
                                .addParameter("impact_eye_movement", npcApplicantCollection.get("impact_eye_movement"))
                                .addParameter("impact_seizure", npcApplicantCollection.get("impact_seizure"))
                                .addParameter("impact_cognitive_impaired", npcApplicantCollection.get("impact_cognitive_impaired"))
                                .addParameter("impact_family_disappointed", npcApplicantCollection.get("impact_family_disappointed"))
                                .addParameter("impact_family_give_up", npcApplicantCollection.get("impact_family_give_up"))
                                .addParameter("impact_family_worry_future", npcApplicantCollection.get("impact_family_worry_future"))
                                .addParameter("impact_family_closer_family", npcApplicantCollection.get("impact_family_closer_family"))
                                .addParameter("impact_wider_self_miss_career", npcApplicantCollection.get("impact_wider_self_miss_career"))
                                .addParameter("impact_wider_family_miss_career", npcApplicantCollection.get("impact_wider_family_miss_career"))
                                .addParameter("impact_wider_emergency", npcApplicantCollection.get("impact_wider_emergency"))
                                .addParameter("impact_wider_appointment", npcApplicantCollection.get("impact_wider_appointment"))
                                .build();
                        // .fragment("section-name");
                        final String url = builder.toString();
                        Log.d("url=>", url);
                        AlertDialog alertDialog = new AlertDialog.Builder(getContext()).create();
                        alertDialog.setTitle("Alert");
                        alertDialog.setMessage("Ready to submit?");
                        alertDialog.setIcon(R.drawable.icon_alert);

                        alertDialog.setButton(AlertDialog.BUTTON_POSITIVE, "Yes",
                                new DialogInterface.OnClickListener() {
                                    public void onClick(DialogInterface dialog, int which) {
                                        ModelFacade.getInstance().createANpcFollowup(receivable, api, url);
                                    }
                                });

                        alertDialog.setButton(AlertDialog.BUTTON_NEGATIVE, "No",
                                new DialogInterface.OnClickListener() {
                                    public void onClick(DialogInterface dialog, int which) {
                                        dialog.dismiss();
                                    }
                                });

                        alertDialog.show();

                    } catch (URISyntaxException e) {
                        e.printStackTrace();
                    }
                }else {
                    Toast.makeText(getView().getContext(), "issue!", Toast.LENGTH_SHORT).show();
                }
            }
        });
    }


    private void CreateNpbFollowup() {
        final APIReceivable receivable = this;
        final INPDRAPI api = new INPDRAPI(getContext());
        int index = 0;
        final ViewGroup insertPoint = (ViewGroup) getView().findViewById(R.id.lty_wider_impact_followup);
        LayoutInflater vi = (LayoutInflater) getActivity().getSystemService(Context.LAYOUT_INFLATER_SERVICE);


        List<String> answers_impact_wider_self_miss_career = new ArrayList<>();
        answers_impact_wider_self_miss_career.add(" ");
        for(int i = 0; i < NpbFollowupCollection.getImpact_wider_self_miss_career().size(); i++){
            answers_impact_wider_self_miss_career.add(NpbFollowupCollection.getImpact_wider_self_miss_career().get(i));
        }
        NpbInflater(vi, answers_impact_wider_self_miss_career, "impact_wider_self_miss_career", insertPoint, index, npbCollection, BasicInfo.getNpb_followups_questions());
        index++;

        List<String> answers_impact_wider_carer_miss_career = new ArrayList<>();
        answers_impact_wider_carer_miss_career.add(" ");
        for(int i = 0; i < NpbFollowupCollection.getImpact_wider_carer_miss_career().size(); i++){
            answers_impact_wider_carer_miss_career.add(NpbFollowupCollection.getImpact_wider_carer_miss_career().get(i));
        }
        NpbInflater(vi, answers_impact_wider_carer_miss_career, "impact_wider_carer_miss_career", insertPoint, index, npbCollection, BasicInfo.getNpb_followups_questions());
        index++;

        List<String> answers_impact_wider_emergency = new ArrayList<>();
        answers_impact_wider_emergency.add(" ");
        for(int i = 0; i < NpbFollowupCollection.getImpact_wider_emergency().size(); i++){
            answers_impact_wider_emergency.add(NpbFollowupCollection.getImpact_wider_emergency().get(i));
        }
        NpbInflater(vi, answers_impact_wider_emergency, "impact_wider_emergency", insertPoint, index, npbCollection, BasicInfo.getNpb_followups_questions());
        index++;

        List<String> answers_impact_wider_appointment = new ArrayList<>();
        answers_impact_wider_appointment.add(" ");
        for(int i = 0; i < NpbFollowupCollection.getImpact_wider_appointment().size(); i++){
            answers_impact_wider_appointment.add(NpbFollowupCollection.getImpact_wider_appointment().get(i));
        }
        NpbInflater(vi, answers_impact_wider_appointment, "impact_wider_appointment", insertPoint, index, npbCollection, BasicInfo.getNpb_followups_questions());



        Button buttonDone = (Button) getView().findViewById(R.id.button_done);

        buttonDone.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                boolean isFullAnswer = true;
                for (int i = 0; i < npbIndexes.size(); i++) {
                    if(!npbCollection.containsKey(npbParams.get(i))){
                        isFullAnswer = false;
                        Log.d("lack of param", npbParams.get(i) + " " + i);
                        TextView textQuestion = (TextView)insertPoint.getChildAt(i).findViewById(R.id.text_create_followup_question);
                        textQuestion.setHint(BasicInfo.getNpb_followups_questions().get(npbParams.get(i)) + " cannot be blank");
                        textQuestion.setTextColor(Color.RED);
                    }else {
                        Log.d("got param", npbParams.get(i) + " " + i);
                        TextView textQuestion = (TextView)insertPoint.getChildAt(i).findViewById(R.id.text_create_followup_question);
                        textQuestion.setHint(BasicInfo.getNpb_followups_questions().get(npbParams.get(i)) + " cannot be blank");
                        textQuestion.setTextColor(Color.BLACK);
                    }
                }

                if(isFullAnswer) {
                    String path = String.format("/api/v1/%s/%d/npb_followups",
                            patientType, patientId);
                    URIBuilder builder = new URIBuilder();
                    try {
                        builder.setScheme("http")
                                .setHost("10.0.3.2")
                                .setPort(3000)
                                .setPath(path)
                                .addParameter("followable_id", patientId + "")
                                .addParameter("followable_type", patientType)
                                .addParameter("symptom_bone", npbCollection.get("symptom_bone"))
                                .addParameter("symptom_abdominal", npbCollection.get("symptom_abdominal"))
                                .addParameter("symptom_pain", npbCollection.get("symptom_pain"))
                                .addParameter("impact_wider_emergency", npbCollection.get("impact_wider_emergency"))
                                .addParameter("impact_wider_appointment", npbCollection.get("impact_wider_appointment"))
                                .addParameter("symptom_cognition", npbCollection.get("symptom_cognition"))
                                .addParameter("symptom_breathlessness_rate", npbCollection.get("symptom_breathlessness_rate"))
                                .addParameter("symptom_breathlessness_impact", npbCollection.get("symptom_breathlessness_impact"))
                                .addParameter("symptom_bleeding_rate", npbCollection.get("symptom_bleeding_rate"))
                                .addParameter("symptom_bleeding_impact", npbCollection.get("symptom_bleeding_impact"))
                                .addParameter("symptom_fatigue_rate", npbCollection.get("symptom_fatigue_rate"))
                                .addParameter("symptom_fatigue_impact", npbCollection.get("symptom_fatigue_impact"))
                                .addParameter("symptom_enlarge_organ", npbCollection.get("symptom_enlarge_organ"))
                                .addParameter("symptom_enlarge_organ_impact", npbCollection.get("symptom_enlarge_organ_impact"))
                                .addParameter("symptom_slow_growth", npbCollection.get("symptom_slow_growth"))
                                .addParameter("symptom_slow_growth_impact", npbCollection.get("symptom_slow_growth_impact"))
                                .addParameter("symptom_infection", npbCollection.get("symptom_infection"))
                                .addParameter("symptom_fracture", npbCollection.get("symptom_fracture"))
                                .addParameter("impact_family_disappointed", npbCollection.get("impact_family_disappointed"))
                                .addParameter("impact_family_give_up", npbCollection.get("impact_family_give_up"))
                                .addParameter("impact_family_worry_future", npbCollection.get("impact_family_worry_future"))
                                .addParameter("impact_family_closer", npbCollection.get("impact_family_closer"))
                                .addParameter("impact_wider_self_miss_career", npbCollection.get("impact_wider_self_miss_career"))
                                .addParameter("impact_wider_carer_miss_career", npbCollection.get("impact_wider_carer_miss_career"))
                                .build();
                        // .fragment("section-name");
                        final String url = builder.toString();
                        Log.d("url=>", url);

                        AlertDialog alertDialog = new AlertDialog.Builder(getContext()).create();
                        alertDialog.setTitle("Alert");
                        alertDialog.setMessage("Ready to submit?");
                        alertDialog.setIcon(R.drawable.icon_alert);

                        alertDialog.setButton(AlertDialog.BUTTON_POSITIVE, "Yes",
                                new DialogInterface.OnClickListener() {
                                    public void onClick(DialogInterface dialog, int which) {
                                        ModelFacade.getInstance().createANpbFollowup(receivable, api, url);
                                    }
                                });

                        alertDialog.setButton(AlertDialog.BUTTON_NEGATIVE, "No",
                                new DialogInterface.OnClickListener() {
                                    public void onClick(DialogInterface dialog, int which) {
                                        dialog.dismiss();
                                    }
                                });

                        alertDialog.show();

                    } catch (URISyntaxException e) {
                        e.printStackTrace();
                    }
                }else {
                    Toast.makeText(getView().getContext(), "issue!", Toast.LENGTH_SHORT).show();
                }
            }
        });

//        Spinner spinner = (Spinner) v.findViewById(R.id.spinner);
//        ArrayAdapter<String> adapter = new ArrayAdapter<String>(getContext(), android.R.layout.simple_spinner_dropdown_item, answers_bone_pain);
//
//        spinner.setAdapter(adapter);

    }

    private void NpbInflater(LayoutInflater vi, final List<String> items, final String param, ViewGroup insertPoint, int index, final Map<String, String> answers, Map<String, String> questions) {
        View v = vi.inflate(R.layout.item_create_followup, null);
        final TextView textQuestion = (TextView)v.findViewById(R.id.text_create_followup_question);

        textQuestion.setText(questions.get(param));
        MaterialSpinner spinner = (MaterialSpinner) v.findViewById(R.id.spinner);
        spinner.setTextColor(getResources().getColor(R.color.colorPrimaryText));
        spinner.setItems(items);

        if(!isFirstTime){
            SharedPreferences myPrefs = getActivity().getSharedPreferences("myPrefs", 0);
            Log.d("not first time", "");
            if(myPrefs.contains(param)){
                spinner.setSelectedIndex(myPrefs.getInt(param, 0));
                Log.d("get prefs", myPrefs.getInt(param, 0)+ " " + param);
            }
        }

        spinner.setOnItemSelectedListener(new MaterialSpinner.OnItemSelectedListener<String>() {

            @Override
            public void onItemSelected(MaterialSpinner view, int position, long id, String item) {
                if (position != 0) {
                    textQuestion.setTextColor(Color.BLACK);
                    answers.put(param, position - 1 + "");
                }else {
                    if(answers.containsKey(param)){
                        answers.remove(param);
                    }
                }
                int mSpnValue = position;
                SharedPreferences myPrefs = getActivity().getSharedPreferences("myPrefs", getContext().MODE_MULTI_PROCESS);
                SharedPreferences.Editor prefsEditor = myPrefs.edit();
                prefsEditor.putInt(param, mSpnValue);
                prefsEditor.commit();
                Log.d("valuee", " " + param + ", " + mSpnValue);

//                Context context = view.getContext();
//                CharSequence text = answers.get(param) + " " + param;
//                int duration = Toast.LENGTH_SHORT;
//
//                Toast toast = Toast.makeText(context, text, duration);
//                toast.show();
            }
        });
        spinner.onSaveInstanceState();
        if(spinner.getSelectedIndex() != 0) {
            answers.put(param, spinner.getSelectedIndex() - 1 + "");
        }

        insertPoint.addView(
                v,
                index,
                new ViewGroup.LayoutParams(ViewGroup.LayoutParams.MATCH_PARENT, ViewGroup.LayoutParams.WRAP_CONTENT));
    }


    private void CreateApplicantNpbFollowup() {
        final APIReceivable receivable = this;
        final INPDRAPI api = new INPDRAPI(getContext());
        int index = 0;
        final ViewGroup insertPoint = (ViewGroup) getView().findViewById(R.id.lty_wider_impact_followup);
        LayoutInflater vi = (LayoutInflater) getActivity().getSystemService(Context.LAYOUT_INFLATER_SERVICE);


        List<String> answers_impact_wider_self_miss_career = new ArrayList<>();
        answers_impact_wider_self_miss_career.add(" ");
        for(int i = 0; i < NpbApplicantFollowupCollection.getImpact_wider_self_miss_career().size(); i++){
            answers_impact_wider_self_miss_career.add(NpbApplicantFollowupCollection.getImpact_wider_self_miss_career().get(i));
        }
        NpbInflater(vi, answers_impact_wider_self_miss_career, "impact_wider_self_miss_career", insertPoint, index, npbApplicantCollection, BasicInfo.getNpb_applicant_participant_followups_questions());
        index++;

        List<String> answers_impact_wider_carer_miss_career = new ArrayList<>();
        answers_impact_wider_carer_miss_career.add(" ");
        for(int i = 0; i < NpbApplicantFollowupCollection.getImpact_wider_carer_miss_career().size(); i++){
            answers_impact_wider_carer_miss_career.add(NpbApplicantFollowupCollection.getImpact_wider_carer_miss_career().get(i));
        }
        NpbInflater(vi, answers_impact_wider_carer_miss_career, "impact_wider_carer_miss_career", insertPoint, index, npbApplicantCollection, BasicInfo.getNpb_applicant_participant_followups_questions());
        index++;

        List<String> answers_impact_wider_emergency = new ArrayList<>();
        answers_impact_wider_emergency.add(" ");
        for(int i = 0; i < NpbApplicantFollowupCollection.getImpact_wider_emergency().size(); i++){
            answers_impact_wider_emergency.add(NpbApplicantFollowupCollection.getImpact_wider_emergency().get(i));
        }
        NpbInflater(vi, answers_impact_wider_emergency, "impact_wider_emergency", insertPoint, index, npbApplicantCollection, BasicInfo.getNpb_applicant_participant_followups_questions());
        index++;

        List<String> answers_impact_wider_appointment = new ArrayList<>();
        answers_impact_wider_appointment.add(" ");
        for(int i = 0; i < NpbApplicantFollowupCollection.getImpact_wider_appointment().size(); i++){
            answers_impact_wider_appointment.add(NpbApplicantFollowupCollection.getImpact_wider_appointment().get(i));
        }
        NpbInflater(vi, answers_impact_wider_appointment, "impact_wider_appointment", insertPoint, index, npbApplicantCollection, BasicInfo.getNpb_applicant_participant_followups_questions());



        Button buttonDone = (Button) getView().findViewById(R.id.button_done);

        buttonDone.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                boolean isFullAnswer = true;
                for (int i = 0; i < npbIndexes.size(); i++) {
                    if(!npbApplicantCollection.containsKey(npbParams.get(i))){
                        isFullAnswer = false;
                        TextView textQuestion = (TextView)insertPoint.getChildAt(i).findViewById(R.id.text_create_followup_question);
                        textQuestion.setHint(BasicInfo.getNpb_applicant_participant_followups_questions().get(npbParams.get(i)) + " cannot be blank");
                        textQuestion.setTextColor(Color.RED);
                    } else {
                        TextView textQuestion = (TextView) insertPoint.getChildAt(i).findViewById(R.id.text_create_followup_question);
                        textQuestion.setHint(BasicInfo.getNpb_applicant_participant_followups_questions().get(npbParams.get(i)) + " cannot be blank");
                        textQuestion.setTextColor(Color.BLACK);
                    }
                }

                if(isFullAnswer) {
                    String path = String.format("/api/v1/%s/%d/npb_followups",
                            patientType, patientId);

                    URIBuilder builder = new URIBuilder();
                    try {
                        builder.setScheme("http")
                                .setHost("10.0.3.2")
                                .setPort(3000)
                                .setPath(path)
                                .addParameter("symptom_bone", npbApplicantCollection.get("symptom_bone"))
                                .addParameter("symptom_abdominal", npbApplicantCollection.get("symptom_abdominal"))
                                .addParameter("symptom_pain", npbApplicantCollection.get("symptom_pain"))
                                .addParameter("impact_wider_emergency", npbApplicantCollection.get("impact_wider_emergency"))
                                .addParameter("impact_wider_appointment", npbApplicantCollection.get("impact_wider_appointment"))
                                .addParameter("symptom_cognition", npbApplicantCollection.get("symptom_cognition"))
                                .addParameter("symptom_breathlessness_rate", npbApplicantCollection.get("symptom_breathlessness_rate"))
                                .addParameter("symptom_breathlessness_impact", npbApplicantCollection.get("symptom_breathlessness_impact"))
                                .addParameter("symptom_bleeding_rate", npbApplicantCollection.get("symptom_bleeding_rate"))
                                .addParameter("symptom_bleeding_impact", npbApplicantCollection.get("symptom_bleeding_impact"))
                                .addParameter("symptom_fatigue_rate", npbApplicantCollection.get("symptom_fatigue_rate"))
                                .addParameter("symptom_fatigue_impact", npbApplicantCollection.get("symptom_fatigue_impact"))
                                .addParameter("symptom_enlarge_organ", npbApplicantCollection.get("symptom_enlarge_organ"))
                                .addParameter("symptom_enlarge_organ_impact", npbApplicantCollection.get("symptom_enlarge_organ_impact"))
                                .addParameter("symptom_slow_growth", npbApplicantCollection.get("symptom_slow_growth"))
                                .addParameter("symptom_slow_growth_impact", npbApplicantCollection.get("symptom_slow_growth_impact"))
                                .addParameter("symptom_infection", npbApplicantCollection.get("symptom_infection"))
                                .addParameter("symptom_fracture", npbApplicantCollection.get("symptom_fracture"))
                                .addParameter("impact_family_disappointed", npbApplicantCollection.get("impact_family_disappointed"))
                                .addParameter("impact_family_give_up", npbApplicantCollection.get("impact_family_give_up"))
                                .addParameter("impact_family_worry_future", npbApplicantCollection.get("impact_family_worry_future"))
                                .addParameter("impact_family_closer", npbApplicantCollection.get("impact_family_closer"))
                                .addParameter("impact_wider_self_miss_career", npbApplicantCollection.get("impact_wider_self_miss_career"))
                                .addParameter("impact_wider_carer_miss_career", npbApplicantCollection.get("impact_wider_carer_miss_career"))
                                .build();
                        // .fragment("section-name");
                        final String url = builder.toString();
                        Log.d("url=>", url);

                        AlertDialog alertDialog = new AlertDialog.Builder(getContext()).create();
                        alertDialog.setTitle("Alert");
                        alertDialog.setMessage("Ready to submit?");
                        alertDialog.setIcon(R.drawable.icon_alert);

                        alertDialog.setButton(AlertDialog.BUTTON_POSITIVE, "Yes",
                                new DialogInterface.OnClickListener() {
                                    public void onClick(DialogInterface dialog, int which) {
                                        ModelFacade.getInstance().createANpbFollowup(receivable, api, url);
                                    }
                                });

                        alertDialog.setButton(AlertDialog.BUTTON_NEGATIVE, "No",
                                new DialogInterface.OnClickListener() {
                                    public void onClick(DialogInterface dialog, int which) {
                                        dialog.dismiss();
                                    }
                                });

                        alertDialog.show();

                    } catch (URISyntaxException e) {
                        e.printStackTrace();
                    }
                }else {
                    Toast.makeText(getView().getContext(), "issue!", Toast.LENGTH_SHORT).show();
                }
            }
        });

    }
}
