package com.unimelb.inpdr.Models;

import com.unimelb.inpdr.Models.API.APIItem;

import org.json.JSONObject;

/**
 * Created by Xintong on 1/05/2016.
 */
public class User extends APIItem{
    private int id;
    private String email;
    private String encrypted_password;
    private String reset_password_token;
    private String reset_password_sent_at;
    private String remember_created_at;
    private int sign_in_count;
    private String current_sign_in_at;
    private String last_sign_in_at;
    private String current_sign_in_ip;
    private String last_sign_in_ip;
    private String confirmation_token;
    private String confirmed_at;
    private String confirmation_sent_at;
    private String unconfirmed_email;
    private int failed_attempts;
    private String unlock_token;
    private String locked_at;
    private String created_at;
    private String updated_at;
    private boolean admin;
    private boolean study_team;
    private String first_name;
    private String last_name;

    public User(JSONObject obj){
        setId(obj.optInt("id"));
        setEmail(obj.optString("email"));
        setEncrypted_password(obj.optString("encrypted_password"));
        setReset_password_token(obj.optString("reset_password_token"));
        setReset_password_sent_at(obj.optString("reset_password_sent_at"));
        setRemember_created_at(obj.optString("remember_created_at"));
        setSign_in_count(obj.optInt("sign_in_count"));
        setCurrent_sign_in_at(obj.optString("current_sign_in_at"));
        setLast_sign_in_at(obj.optString("last_sign_in_at"));
        setCurrent_sign_in_ip(obj.optString("current_sign_in_ip"));
        setLast_sign_in_ip(obj.optString("last_sign_in_ip"));
        setConfirmation_token(obj.optString("confirmation_token"));
        setConfirmed_at(obj.optString("confirmed_at"));
        setConfirmation_sent_at(obj.optString("confirmation_sent_at"));
        setUnconfirmed_email(obj.optString("unconfirmed_email"));
        setFailed_attempts(obj.optInt("failed_attempts"));
        setUnlock_token(obj.optString("unlock_token"));
        setLocked_at(obj.optString("locked_at"));
        setCreated_at(obj.optString("created_at"));
        setUpdated_at(obj.optString("updated_at"));
        setAdmin(obj.optBoolean("admin"));
        setStudy_team(obj.optBoolean("study_team"));
        setFirst_name(obj.optString("first_name"));
        setLast_name(obj.optString("last_name"));
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getEncrypted_password() {
        return encrypted_password;
    }

    public void setEncrypted_password(String encrypted_password) {
        this.encrypted_password = encrypted_password;
    }

    public String getReset_password_token() {
        return reset_password_token;
    }

    public void setReset_password_token(String reset_password_token) {
        this.reset_password_token = reset_password_token;
    }

    public String getReset_password_sent_at() {
        return reset_password_sent_at;
    }

    public void setReset_password_sent_at(String reset_password_sent_at) {
        this.reset_password_sent_at = reset_password_sent_at;
    }

    public String getRemember_created_at() {
        return remember_created_at;
    }

    public void setRemember_created_at(String remember_created_at) {
        this.remember_created_at = remember_created_at;
    }

    public int getSign_in_count() {
        return sign_in_count;
    }

    public void setSign_in_count(int sign_in_count) {
        this.sign_in_count = sign_in_count;
    }

    public String getCurrent_sign_in_at() {
        return current_sign_in_at;
    }

    public void setCurrent_sign_in_at(String current_sign_in_at) {
        this.current_sign_in_at = current_sign_in_at;
    }

    public String getLast_sign_in_at() {
        return last_sign_in_at;
    }

    public void setLast_sign_in_at(String last_sign_in_at) {
        this.last_sign_in_at = last_sign_in_at;
    }

    public String getCurrent_sign_in_ip() {
        return current_sign_in_ip;
    }

    public void setCurrent_sign_in_ip(String current_sign_in_ip) {
        this.current_sign_in_ip = current_sign_in_ip;
    }

    public String getLast_sign_in_ip() {
        return last_sign_in_ip;
    }

    public void setLast_sign_in_ip(String last_sign_in_ip) {
        this.last_sign_in_ip = last_sign_in_ip;
    }

    public String getConfirmation_token() {
        return confirmation_token;
    }

    public void setConfirmation_token(String confirmation_token) {
        this.confirmation_token = confirmation_token;
    }

    public String getConfirmed_at() {
        return confirmed_at;
    }

    public void setConfirmed_at(String confirmed_at) {
        this.confirmed_at = confirmed_at;
    }

    public String getConfirmation_sent_at() {
        return confirmation_sent_at;
    }

    public void setConfirmation_sent_at(String confirmation_sent_at) {
        this.confirmation_sent_at = confirmation_sent_at;
    }

    public String getUnconfirmed_email() {
        return unconfirmed_email;
    }

    public void setUnconfirmed_email(String unconfirmed_email) {
        this.unconfirmed_email = unconfirmed_email;
    }

    public int getFailed_attempts() {
        return failed_attempts;
    }

    public void setFailed_attempts(int failed_attempts) {
        this.failed_attempts = failed_attempts;
    }

    public String getUnlock_token() {
        return unlock_token;
    }

    public void setUnlock_token(String unlock_token) {
        this.unlock_token = unlock_token;
    }

    public String getLocked_at() {
        return locked_at;
    }

    public void setLocked_at(String locked_at) {
        this.locked_at = locked_at;
    }

    public String getCreated_at() {
        return created_at;
    }

    public void setCreated_at(String created_at) {
        this.created_at = created_at;
    }

    public String getUpdated_at() {
        return updated_at;
    }

    public void setUpdated_at(String updated_at) {
        this.updated_at = updated_at;
    }

    public boolean isAdmin() {
        return admin;
    }

    public void setAdmin(boolean admin) {
        this.admin = admin;
    }

    public boolean isStudy_team() {
        return study_team;
    }

    public void setStudy_team(boolean study_team) {
        this.study_team = study_team;
    }

    public String getFirst_name() {
        return first_name;
    }

    public void setFirst_name(String first_name) {
        this.first_name = first_name;
    }

    public String getLast_name() {
        return last_name;
    }

    public void setLast_name(String last_name) {
        this.last_name = last_name;
    }
}
