package com.unimelb.inpdr.Fragments;

import android.content.DialogInterface;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v7.app.AlertDialog;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ExpandableListView;

import com.unimelb.inpdr.MainActivity;
import com.unimelb.inpdr.Models.API.APIItem;
import com.unimelb.inpdr.Models.API.APIReceivable;
import com.unimelb.inpdr.Models.API.INPDRAPI;
import com.unimelb.inpdr.Models.API.ModelFacade;
import com.unimelb.inpdr.Models.ErrorMessage;
import com.unimelb.inpdr.Models.NpaApplicantParticipant;
import com.unimelb.inpdr.Models.NpbApplicantParticipant;
import com.unimelb.inpdr.Models.NpbFollowupCollection;
import com.unimelb.inpdr.Models.NpbParticipant;
import com.unimelb.inpdr.Models.NpcApplicantParticipant;
import com.unimelb.inpdr.Models.NpcFollowupCollection;
import com.unimelb.inpdr.Models.NpcParticipant;
import com.unimelb.inpdr.Models.Patient;
import com.unimelb.inpdr.Models.BasicInfo;
import com.unimelb.inpdr.Models.Selection;
import com.unimelb.inpdr.R;
import com.unimelb.inpdr.Sources.ExpandableListAdapter;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

import me.drakeet.materialdialog.MaterialDialog;

import static com.unimelb.inpdr.Models.BasicInfo.*;
import static com.unimelb.inpdr.Models.Selection.*;

public class PatientDetailFragment extends Fragment implements APIReceivable{
    private ArrayList<Patient> patients = new ArrayList<>();
    ExpandableListAdapter listAdapter;
    ExpandableListView expListView;
    List<String> listDataHeader;
    HashMap<String, List<String>> listDataChild;

    public PatientDetailFragment() {
        // Required empty public constructor
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        return inflater.inflate(R.layout.fragment_patient_detail, container, false);

    }

    @Override
    public void onStart(){
        super.onStart();
        ((MainActivity)getActivity()).setActionBarTitle("Patient");
    }

    @Override
    public void onViewCreated(View view, Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);

        final INPDRAPI api = new INPDRAPI(getContext());

        ((MainActivity)getActivity()).setActionBarTitle("Patient");

        BasicInfo.getInstance();
        NpbFollowupCollection.getInstance();
        NpcFollowupCollection.getInstance();
        Selection.getInstance();

        Bundle data = getArguments();
        if(data != null){
            int id = data.getInt("patientID");
            ModelFacade.getInstance().getAPatient(this, api, id);
        }

        expListView = (ExpandableListView) view.findViewById(R.id.expandableListView);

    }

    @Override
    public <U extends APIItem> void receiveData(List<U> items, String type) {
        if(type.equals("getAPatient")){
            patients.clear();
            for (U item : items) {
                Patient feed = (Patient) item;
                displayPatients(feed);
                //Log.d("!!!!!data received!!!", feed.getUser().getEmail() + "");
            }

        }
    }

    @Override
    public <U extends APIItem> void receiveData(U item, String type) {
        switch (type){
            case "error":
                ErrorMessage error = (ErrorMessage) item;
                final MaterialDialog mMaterialDialog_e = new MaterialDialog(getContext());
                mMaterialDialog_e.setTitle("Error");
                ErrorMessage errorMessage = (ErrorMessage) item;
                mMaterialDialog_e.setMessage(errorMessage.getErrorMes());
                mMaterialDialog_e.setPositiveButton("Ok", new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        mMaterialDialog_e.dismiss();
                    }
                });
                mMaterialDialog_e.show();
        }

    }

    public void displayPatients(Patient feed){
        listDataHeader = new ArrayList<String>();
        listDataChild = new HashMap<String, List<String>>();

        listDataHeader.add("PERSONAL INFORMATION");
        listDataHeader.add("ENROLMENT INFORMATION");
        listDataHeader.add("GENETIC DIAGNOSIS");
        listDataHeader.add("CONSENT");

        List<String> personalInfo = new ArrayList<String>();

        String id = String.format("%-30s%-30s","Patient ID:", feed.getEnrollable_id());
        personalInfo.add(id);

        String patient_type = String.format("%-30s%-30s","Patient Type:", feed.getEnrollable_type());
        personalInfo.add(patient_type);

        if(!feed.isVerified()) {

            String firstName;
            if (!feed.getPatient_first_name().equals("")) {
                firstName = String.format("%-30s%-30s", "First Name:", feed.getPatient_first_name());
                personalInfo.add(firstName);
            }

            if (!feed.getPatient_last_name().equals("")) {
                String lastName = String.format("%-30s%-30s", "Last Name:", feed.getPatient_last_name());
                personalInfo.add(lastName);
            }

            String gender = String.format("%-30s%-30s", "Gender:", getGender(feed.getGender()));
            personalInfo.add(gender);

            if (!feed.getDate_of_birth().equals("null")) {
                String dob = String.format("%-30s%-30s", "Date of Birth:", feed.getDate_of_birth());
                personalInfo.add(dob);
            }
        }

        List<String> enrolmentInfo = new ArrayList<String>();
        List<String> geneticDiag = new ArrayList<String>();

        /*
        ** NpbParticipant
         */
        if(feed.getEnrollable_type().equals("NpbParticipant")){
            NpbParticipant patient = (NpbParticipant) feed.getItem();
            String symptom_age = getNpb_participant_questions().get("symptom_age_known");
            if(patient.isSymptom_age_unknown()){
                symptom_age += String.format("\n\n%-30s%-30s", "Unknown:" , getSelection(patient.isSymptom_age_unknown()));
                enrolmentInfo.add(symptom_age);
            }
            else {
                if (patient.isSymptom_age_na()) {
                    symptom_age += String.format("\n\n%-30s%-30s", "No Symptoms:", getSelection(patient.isSymptom_age_na()));
                    enrolmentInfo.add(symptom_age);
                } else {
                    symptom_age += String.format("\n\n%-30s%-30s", "Years:", patient.getSymptom_age_year()) + String.format("\n%-30s%-30s", "Months:", patient.getSymptom_age_month());
                    enrolmentInfo.add(symptom_age);
                }
            }

            String symptom_title = getNpb_participant_questions().get("symptom_title");
            symptom_title += String.format("\n\n%-30s%-30s", "Enlarged liver:", getSelection(patient.isSymptom_enlarged_liver()));
            symptom_title += String.format("\n%-30s%-30s", "Breathing or lung problems:", getSelection(patient.isSymptom_breathing()));
            symptom_title += String.format("\n%-30s%-30s", "Anaemia:", getSelection(patient.isSymptom_anaemia()));
            symptom_title += String.format("\n%-30s%-30s", "Problems with bleeding:" , Selection.getSelection(patient.isSymptom_bleeding()));
            symptom_title += String.format("\n%-30s%-30s", "Growth delay:" , Selection.getSelection(patient.isSymptom_growth_delay()));
            symptom_title += String.format("\n%-30s%-30s", "Developmental delay:" , Selection.getSelection(patient.isSymptom_developmental_delay()));
            symptom_title += String.format("\n%-30s%-30s", "Bone pain:" , Selection.getSelection(patient.isSymptom_bone_pain()));
            symptom_title += String.format("\n%-30s%-30s", "Abdominal pain:" , Selection.getSelection(patient.isSymptom_abdominal_pain()));
            symptom_title += String.format("\n%-30s%-30s", "Other/further information:" , Selection.getSelection(patient.isSymptom_other()));
            if(patient.isSymptom_other()){
                symptom_title += String.format("\n%-30s%-30s", "Please specify:" , patient.getSymptom_other_specify());
            }
            enrolmentInfo.add(symptom_title);

            String age_diagnosed_known = getNpb_participant_questions().get("age_diagnosed_known");
            if(patient.isAge_diagnosed_known()){
                age_diagnosed_known += String.format("\n\n%-30s%-30s","Unknown:", getSelection(patient.isAge_diagnosed_known()));
            }
            else {
                age_diagnosed_known += String.format("\n\n%-30s%-30s","Years:", patient.getAge_diagnosed_year());
                age_diagnosed_known += String.format("\n%-30s%-30s","Month:", patient.getAge_diagnosed_month());
            }
            enrolmentInfo.add(age_diagnosed_known);

            String other_family_diagnosed = getNpb_participant_questions().get("other_family_diagnosed");
            other_family_diagnosed += String.format("\n\n%s", getNpb_participant_questions().get("family_diagnosed"));
            other_family_diagnosed += String.format("\n\n%-30s%-30s","", getYes(patient.getFamily_diagnosed()));
            if(patient.getFamily_diagnosed() == 0){
                other_family_diagnosed += String.format("\n\n%s", getNpb_participant_questions().get("family_diagnosed_relation"));
                other_family_diagnosed += String.format("\n\n%-30s%-30s","", patient.getFamily_diagnosed_relation());
            }
            enrolmentInfo.add(other_family_diagnosed);


            String intro = getNpb_participant_questions().get("gene_report_explanation_html");
            geneticDiag.add(intro);

            String gene_diagnosis = getNpb_participant_questions().get("gene_diagnosis");
            if(!patient.getGene_diagnosis().equals("null")){
                gene_diagnosis += String.format("\n\n%-30s%-30s","", patient.getGene_diagnosis());
            }
            else {
                gene_diagnosis += String.format("\n\n%-30s%-30s","", "No");
            }
            geneticDiag.add(gene_diagnosis);

            String dont_have_gene_report = getNpb_participant_questions().get("dont_have_gene_report");
            geneticDiag.add(dont_have_gene_report);

            String unable_to_contact_specialist = getNpb_participant_questions().get("unable_to_contact_specialist");
            unable_to_contact_specialist +=String.format("\n\n%-30s%-30s","", getSelection(patient.isUnable_to_contact_specialist()));
            geneticDiag.add(unable_to_contact_specialist);

            String gene_test_performed = getNpb_participant_questions().get("gene_test_performed");
            gene_test_performed += String.format("\n\n%-30s%-30s","", getSelection(patient.isGene_test_performed()));
            geneticDiag.add(gene_test_performed);
        }

        /**
         * NpcParticipant
         */
        if(feed.getEnrollable_type().equals("NpcParticipant")){
            NpcParticipant patient = (NpcParticipant) feed.getItem();
            String baby_symptoms = getNpc_participant_questions().get("baby_symptoms");
            baby_symptoms += String.format("\n\n%-30s%-30s", "", getYes(patient.getBaby_symptoms()));
            enrolmentInfo.add(baby_symptoms);

            String symptom_age = getNpc_participant_questions().get("symptom_age_known");
            if(patient.isSymptom_age_unknown()){
                symptom_age += String.format("\n\n%-30s%-30s", "Unknown:" , getSelection(patient.isSymptom_age_unknown()));
                enrolmentInfo.add(symptom_age);
            }
            else if(patient.isSymptom_age_na()){
                symptom_age += String.format("\n\n%-30s%-30s", "No Symptoms:" , getSelection(patient.isSymptom_age_na()));
                enrolmentInfo.add(symptom_age);
            }
            else {
                symptom_age += String.format("\n\n%-30s%-30s", "Years:" , patient.getSymptom_age_year()) + String.format("\n%-30s%-30s", "Months:" , patient.getSymptom_age_month());
                enrolmentInfo.add(symptom_age);
            }

            String symptom = getNpc_participant_questions().get("symptom_title");

            symptom +=  "\n\n" + getNpc_participant_questions().get("symptom_delayed_milestone");
            symptom += String.format("\n\n%-30s%-30s", "", getSelection(patient.isSymptom_delayed_milestone()));
            symptom += "\n\n" + getNpc_participant_questions().get("symptom_ppers");
            symptom += String.format("\n\n%-30s%-30s", "", getSelection(patient.isSymptom_ppers()));
            symptom += "\n\n" + getNpc_participant_questions().get("symptom_coordication");
            symptom += String.format("\n\n%-30s%-30s", "", getSelection(patient.isSymptom_coordication()));
            symptom += String.format("\n\n%-30s%-30s", getNpc_participant_questions().get("symptom_eye"), getSelection(patient.isSymptom_eye()));
            symptom += String.format("\n\n%-30s%-30s", getNpc_participant_questions().get("symptom_behavioural"), getSelection(patient.isSymptom_behavioural()));
            symptom += String.format("\n\n%-30s%-30s", getNpc_participant_questions().get("symptom_seizure"), getSelection(patient.isSymptom_seizure()));
            symptom += String.format("\n\n%-30s%-30s", getNpc_participant_questions().get("symptom_psychiatric"), getSelection(patient.isSymptom_psychiatric()));
            symptom += String.format("\n\n%-30s%-30s", getNpc_participant_questions().get("symptom_other"), getSelection(patient.isSymptom_other()));

            if(patient.isSymptom_other()){
                symptom += String.format("\n%-30s%-30s", getNpc_participant_questions().get("symptom_other_specify") , patient.getSymptom_other_specify());
            }
            enrolmentInfo.add(symptom);

            String age_diagnosed_known = getNpc_participant_questions().get("age_diagnosed_known");
            if(patient.isAge_diagnosed_known()){
                age_diagnosed_known += String.format("\n\n%-30s%-30s","Unknown:", getSelection(patient.isAge_diagnosed_known()));
            }
            else {
                age_diagnosed_known += String.format("\n\n%-30s%-30s","Years:", patient.getAge_diagnosed_year());
                age_diagnosed_known += String.format("\n%-30s%-30s","Month:", patient.getAge_diagnosed_month());
            }
            enrolmentInfo.add(age_diagnosed_known);

            String other_family_diagnosed = getNpc_participant_questions().get("other_family_diagnosed");
            other_family_diagnosed += String.format("\n\n%s", getNpc_participant_questions().get("family_diagnosed"));
            other_family_diagnosed += String.format("\n\n%-30s%-30s","", getYes(patient.getFamily_diagnosed()));
            if(patient.getFamily_diagnosed() == 0){
                other_family_diagnosed += String.format("\n\n%s", getNpc_participant_questions().get("family_diagnosed_relation"));
                other_family_diagnosed += String.format("\n\n%-30s%-30s","", patient.getFamily_diagnosed_relation());
            }
            enrolmentInfo.add(other_family_diagnosed);

            String intro = getNpc_participant_questions().get("gene_report_explanation_html");
            geneticDiag.add(intro);

            String gene_diagnosis = getNpc_participant_questions().get("gene_diagnosis");
            if(!patient.getGene_diagnosis().equals("null")){
                gene_diagnosis += String.format("\n\n%-30s%-30s","", patient.getGene_diagnosis());
            }
            else {
                gene_diagnosis += String.format("\n\n%-30s%-30s","", "No");
            }
            geneticDiag.add(gene_diagnosis);

            String dont_have_gene_report = getNpc_participant_questions().get("dont_have_gene_report");
            geneticDiag.add(dont_have_gene_report);

            String unable_to_contact_specialist = getNpc_participant_questions().get("unable_to_contact_specialist");
            unable_to_contact_specialist += String.format("\n\n%-30s%-30s","", getSelection(patient.isUnable_to_contact_specialist()));
            geneticDiag.add(unable_to_contact_specialist);

            String gene_test_performed = getNpc_participant_questions().get("gene_test_performed");
            gene_test_performed += "\n\n" + getSelection(patient.isGene_test_performed());
            geneticDiag.add(gene_test_performed);
        }

        /**
         * NpaApplicantParticipant
         */
        if(feed.getEnrollable_type().equals("NpaApplicantParticipant")){
            NpaApplicantParticipant patient = (NpaApplicantParticipant) feed.getItem();
            if(BasicInfo.getNpc_applicant_participant_questions().get("applicant_relation")!= null){
                String applicant_relation = BasicInfo.getNpa_applicant_participant_questions().get("applicant_relation");
                applicant_relation += String.format("\n\n%-30s%-30s", "", BasicInfo.getApplicant_relation().get(patient.getApplicant_relation()));
                enrolmentInfo.add(applicant_relation);

                if(!patient.getApplicant_relation_specify().equals("")){
                    String applicant_relation_specify = BasicInfo.getNpa_applicant_participant_questions().get("applicant_relation_specify");
                    enrolmentInfo.add(applicant_relation_specify);
                }
            }
            String symptom_age = BasicInfo.getNpa_applicant_participant_questions().get("symptom_age_known");
            if(patient.isSymptom_age_unknown()){
                symptom_age += String.format("\n\n%-30s%-30s", "Unknown:" , getSelection(patient.isSymptom_age_unknown()));
                enrolmentInfo.add(symptom_age);
            }
            else if(patient.isSymptom_age_na()){
                symptom_age += String.format("\n\n%-30s%-30s", "No Symptoms:" , getSelection(patient.isSymptom_age_na()));
                enrolmentInfo.add(symptom_age);
            }
            else {
                symptom_age += String.format("\n\n%-30s%-30s", "Years:" , patient.getSymptom_age_year()) + String.format("\n%-30s%-30s", "Months:" , patient.getSymptom_age_month());
                enrolmentInfo.add(symptom_age);
            }

            String age_diagnosed_known = BasicInfo.getNpa_applicant_participant_questions().get("age_diagnosed_known");
            if(patient.isAge_diagnosed_known()){
                age_diagnosed_known += String.format("\n\n%-30s%-30s","Unknown:", getSelection(patient.isAge_diagnosed_known()));
            }
            else {
                age_diagnosed_known += String.format("\n\n%-30s%-30s","Years:", patient.getAge_diagnosed_year());
                age_diagnosed_known += String.format("\n%-30s%-30s","Month:", patient.getAge_diagnosed_month());
            }
            enrolmentInfo.add(age_diagnosed_known);

            String other_family_diagnosed = BasicInfo.getNpa_applicant_participant_questions().get("other_family_diagnosed");
            other_family_diagnosed += String.format("\n\n%s", BasicInfo.getNpa_applicant_participant_questions().get("family_diagnosed"));
            other_family_diagnosed += String.format("\n\n%-30s%-30s","", getYes(patient.getFamily_diagnosed()));
            if(patient.getFamily_diagnosed() == 0){
                other_family_diagnosed += String.format("\n\n%s", BasicInfo.getNpa_applicant_participant_questions().get("family_diagnosed_relation"));
                other_family_diagnosed += String.format("\n\n%-30s%-30s", "", patient.getFamily_diagnosed_relation());
            }
            enrolmentInfo.add(other_family_diagnosed);


            String intro = BasicInfo.getNpa_applicant_participant_questions().get("gene_report_explanation_html");
            geneticDiag.add(intro);

            String gene_diagnosis = BasicInfo.getNpa_applicant_participant_questions().get("gene_diagnosis");
            if(patient.getGene_diagnosis() != null){
                gene_diagnosis += String.format("\n\n%-30s%-30s","", patient.getGene_diagnosis());
            }
            else {
                gene_diagnosis += String.format("\n\n%-30s%-30s","", "No");
            }
            geneticDiag.add(gene_diagnosis);

            String dont_have_gene_report = BasicInfo.getNpa_applicant_participant_questions().get("dont_have_gene_report");
            geneticDiag.add(dont_have_gene_report);

            String unable_to_contact_specialist = BasicInfo.getNpa_applicant_participant_questions().get("unable_to_contact_specialist");
            unable_to_contact_specialist += String.format("\n\n%-30s%-30s","", getSelection(patient.isUnable_to_contact_specialist()));
            geneticDiag.add(unable_to_contact_specialist);

            String gene_test_performed = BasicInfo.getNpa_applicant_participant_questions().get("gene_test_performed");
            gene_test_performed += String.format("\n\n%-30s%-30s","", getSelection(patient.isGene_test_performed()));
            geneticDiag.add(gene_test_performed);
        }

        /**
         * NpbApplicantParticipant
         */
        if(feed.getEnrollable_type().equals("NpbApplicantParticipant")){
            NpbApplicantParticipant patient = (NpbApplicantParticipant) feed.getItem();
            if(BasicInfo.getNpc_applicant_participant_questions().get("applicant_relation")!= null){
                String applicant_relation = BasicInfo.getNpa_applicant_participant_questions().get("applicant_relation");
                applicant_relation += String.format("\n\n%-30s%-30s", "", BasicInfo.getApplicant_relation().get(patient.getApplicant_relation()));
                enrolmentInfo.add(applicant_relation);

                if(!patient.getApplicant_relation_specify().equals("")){
                    String applicant_relation_specify = BasicInfo.getNpa_applicant_participant_questions().get("applicant_relation_specify");
                    enrolmentInfo.add(applicant_relation_specify);
                }
            }

            String symptom_age = getNpb_applicant_participant_questions().get("symptom_age_known");
            if(patient.isSymptom_age_unknown()){
                symptom_age += String.format("\n\n%-30s%-30s", "Unknown:" , getSelection(patient.isSymptom_age_unknown()));
                enrolmentInfo.add(symptom_age);
            }
            else if(patient.isSymptom_age_na()){
                symptom_age += String.format("\n\n%-30s%-30s", "No Symptoms:" , getSelection(patient.isSymptom_age_na()));
                enrolmentInfo.add(symptom_age);
            }
            else {
                symptom_age += String.format("\n\n%-30s%-30s", "Years:" , patient.getSymptom_age_year()) + String.format("\n%-30s%-30s", "Months:" , patient.getSymptom_age_month());
                enrolmentInfo.add(symptom_age);
            }

            String symptom_title = getNpb_applicant_participant_questions().get("symptom_title");
            symptom_title += String.format("\n\n%-30s%-30s", getNpb_applicant_participant_questions().get("symptom_enlarged_liver"), getSelection(patient.isSymptom_enlarged_liver()));
            symptom_title += String.format("\n%-30s%-30s", getNpb_applicant_participant_questions().get("symptom_breathing"), getSelection(patient.isSymptom_breathing()));
            symptom_title += String.format("\n%-30s%-30s", getNpb_applicant_participant_questions().get("symptom_anaemia"), getSelection(patient.isSymptom_anaemia()));
            symptom_title += String.format("\n%-30s%-30s", getNpb_applicant_participant_questions().get("symptom_bleeding") , getSelection(patient.isSymptom_bleeding()));
            symptom_title += String.format("\n%-30s%-30s", getNpb_applicant_participant_questions().get("symptom_growth_delay") , getSelection(patient.isSymptom_growth_delay()));
            symptom_title += String.format("\n%-30s%-30s", getNpb_applicant_participant_questions().get("symptom_developmental_delay") , getSelection(patient.isSymptom_developmental_delay()));
            symptom_title += String.format("\n%-30s%-30s", getNpb_applicant_participant_questions().get("symptom_bone_pain") , getSelection(patient.isSymptom_bone_pain()));
            symptom_title += String.format("\n%-30s%-30s", getNpb_applicant_participant_questions().get("symptom_abdominal_pain") , getSelection(patient.isSymptom_abdominal_pain()));
            symptom_title += String.format("\n%-30s%-30s", getNpb_applicant_participant_questions().get("symptom_other") , getSelection(patient.isSymptom_other()));
            if(patient.isSymptom_other()){
                symptom_title += String.format("\n%-30s%-30s", getNpb_applicant_participant_questions().get("symptom_other_specify") , patient.getSymptom_other_specify());
            }
            enrolmentInfo.add(symptom_title);

            String age_diagnosed_known = getNpb_applicant_participant_questions().get("age_diagnosed_known");
            if(patient.isAge_diagnosed_known()){
                age_diagnosed_known += String.format("\n\n%-30s%-30s","Unknown:", getSelection(patient.isAge_diagnosed_known()));
            }
            else {
                age_diagnosed_known += String.format("\n\n%-30s%-30s","Years:", patient.getAge_diagnosed_year());
                age_diagnosed_known += String.format("\n%-30s%-30s","Month:", patient.getAge_diagnosed_month());
            }
            enrolmentInfo.add(age_diagnosed_known);

            String other_family_diagnosed = getNpb_applicant_participant_questions().get("other_family_diagnosed");
            other_family_diagnosed += String.format("\n\n%s", getNpb_applicant_participant_questions().get("family_diagnosed"));
            other_family_diagnosed += String.format("\n\n%-30s%-30s","", getYes(patient.getFamily_diagnosed()));
            if(patient.getFamily_diagnosed() == 0){
                other_family_diagnosed += String.format("\n\n%s", getNpb_applicant_participant_questions().get("family_diagnosed_relation"));
                other_family_diagnosed += String.format("\n\n%-30s%-30s","", patient.getFamily_diagnosed_relation());
            }
            enrolmentInfo.add(other_family_diagnosed);

            String intro = getNpb_applicant_participant_questions().get("gene_report_explanation_html");
            geneticDiag.add(intro);

            String gene_diagnosis = getNpb_applicant_participant_questions().get("gene_diagnosis");
            if(!patient.getGene_diagnosis().equals("null")){
                gene_diagnosis += String.format("\n\n%-30s%-30s","", patient.getGene_diagnosis());
                geneticDiag.add(gene_diagnosis);
            }
            else {
                gene_diagnosis += String.format("\n\n%-30s%-30s","", "No");
                String dont_have_gene_report = getNpb_applicant_participant_questions().get("dont_have_gene_report");
                geneticDiag.add(gene_diagnosis);
                geneticDiag.add(dont_have_gene_report);
            }


            String unable_to_contact_specialist = getNpb_applicant_participant_questions().get("unable_to_contact_specialist");
            unable_to_contact_specialist += String.format("\n\n%-30s%-30s","", getSelection(patient.isUnable_to_contact_specialist()));
            geneticDiag.add(unable_to_contact_specialist);

            String gene_test_performed = getNpb_applicant_participant_questions().get("gene_test_performed");
            gene_test_performed += String.format("\n\n%-30s%-30s","", getSelection(patient.isGene_test_performed()));
            geneticDiag.add(gene_test_performed);
        }

        /**
         * NpcApplicantParticipant
         */
        if(feed.getEnrollable_type().equals("NpcApplicantParticipant")){
            NpcApplicantParticipant patient = (NpcApplicantParticipant) feed.getItem();
            if(BasicInfo.getNpc_applicant_participant_questions().get("applicant_relation")!= null){
                String applicant_relation = BasicInfo.getNpa_applicant_participant_questions().get("applicant_relation");
                applicant_relation += String.format("\n\n%-30s%-30s", "", BasicInfo.getApplicant_relation().get(patient.getApplicant_relation()));
                enrolmentInfo.add(applicant_relation);

                if(!patient.getApplicant_relation_specify().equals("")){
                    String applicant_relation_specify = BasicInfo.getNpa_applicant_participant_questions().get("applicant_relation_specify");
                    enrolmentInfo.add(applicant_relation_specify);
                }
            }

            String baby_symptoms = getNpc_applicant_participant_questions().get("baby_symptoms");
            baby_symptoms += String.format("\n\n%-30s%-30s", "", getYes(patient.getBaby_symptoms()));
            enrolmentInfo.add(baby_symptoms);

            String symptom_age = getNpc_applicant_participant_questions().get("symptom_age_known");
            if(patient.isSymptom_age_unknown()){
                symptom_age += String.format("\n\n%-30s%-30s", "Unknown:" , getSelection(patient.isSymptom_age_unknown()));
                enrolmentInfo.add(symptom_age);
            }
            else if(patient.isSymptom_age_na()){
                symptom_age += String.format("\n\n%-30s%-30s", "No Symptoms:" , getSelection(patient.isSymptom_age_na()));
                enrolmentInfo.add(symptom_age);
            }
            else {
                symptom_age += String.format("\n\n%-30s%-30s", "Years:" , patient.getSymptom_age_year()) + String.format("\n%-30s%-30s", "Months:" , patient.getSymptom_age_month());
                enrolmentInfo.add(symptom_age);
            }

            String symptom = getNpc_applicant_participant_questions().get("symptom_title");

            symptom +=  "\n\n" + getNpc_applicant_participant_questions().get("symptom_delayed_milestone");
            symptom += String.format("\n\n%-30s%-30s", "", getSelection(patient.isSymptom_delayed_milestone()));
            symptom += "\n\n" + getNpc_applicant_participant_questions().get("symptom_ppers");
            symptom += String.format("\n\n%-30s%-30s", "", getSelection(patient.isSymptom_ppers()));
            symptom += "\n\n" + getNpc_applicant_participant_questions().get("symptom_coordication");
            symptom += String.format("\n\n%-30s%-30s", "", getSelection(patient.isSymptom_coordication()));
            symptom += String.format("\n\n%-30s%-30s", getNpc_applicant_participant_questions().get("symptom_eye"), getSelection(patient.isSymptom_eye()));
            symptom += String.format("\n\n%-30s%-30s", getNpc_applicant_participant_questions().get("symptom_behavioural"), getSelection(patient.isSymptom_behavioural()));
            symptom += String.format("\n\n%-30s%-30s", getNpc_applicant_participant_questions().get("symptom_seizure"), getSelection(patient.isSymptom_seizure()));
            symptom += String.format("\n\n%-30s%-30s", getNpc_applicant_participant_questions().get("symptom_psychiatric"), getSelection(patient.isSymptom_psychiatric()));
            symptom += String.format("\n\n%-30s%-30s", getNpc_applicant_participant_questions().get("symptom_other"), getSelection(patient.isSymptom_other()));

            if(patient.isSymptom_other()){
                symptom += String.format("\n%-30s%-30s", getNpc_applicant_participant_questions().get("symptom_other_specify") , patient.getSymptom_other_specify());
            }
            enrolmentInfo.add(symptom);

            String age_diagnosed_known = getNpc_applicant_participant_questions().get("age_diagnosed_known");
            if(patient.isAge_diagnosed_known()){
                age_diagnosed_known += String.format("\n\n%-30s%-30s","Unknown:", getSelection(patient.isAge_diagnosed_known()));
            }
            else {
                age_diagnosed_known += String.format("\n\n%-30s%-30s","Years:", patient.getAge_diagnosed_year());
                age_diagnosed_known += String.format("\n%-30s%-30s","Month:", patient.getAge_diagnosed_month());
            }
            enrolmentInfo.add(age_diagnosed_known);

            String other_family_diagnosed = getNpc_applicant_participant_questions().get("other_family_diagnosed");
            other_family_diagnosed += String.format("\n\n%s", getNpc_applicant_participant_questions().get("family_diagnosed"));
            other_family_diagnosed += String.format("\n\n%-30s%-30s","", getYes(patient.getFamily_diagnosed()));
            if(patient.getFamily_diagnosed() == 0){
                other_family_diagnosed += String.format("\n\n%s", getNpc_applicant_participant_questions().get("family_diagnosed_relation"));
                other_family_diagnosed += String.format("\n\n%-30s%-30s","", patient.getFamily_diagnosed_relation());
            }
            enrolmentInfo.add(other_family_diagnosed);

            String intro = getNpc_applicant_participant_questions().get("gene_report_explanation_html");
            geneticDiag.add(intro);

            String gene_diagnosis = getNpc_applicant_participant_questions().get("gene_diagnosis");
            if(!patient.getGene_diagnosis().equals("null")){
                gene_diagnosis += String.format("\n\n%-30s%-30s","", patient.getGene_diagnosis());
            }
            else {
                gene_diagnosis += String.format("\n\n%-30s%-30s","", "No");
            }
            geneticDiag.add(gene_diagnosis);

            String dont_have_gene_report = getNpc_applicant_participant_questions().get("dont_have_gene_report");
            geneticDiag.add(dont_have_gene_report);

            String unable_to_contact_specialist = getNpc_applicant_participant_questions().get("unable_to_contact_specialist");
            unable_to_contact_specialist += String.format("\n\n%-30s%-30s","", getSelection(patient.isUnable_to_contact_specialist()));
            geneticDiag.add(unable_to_contact_specialist);

            String gene_test_performed = getNpc_applicant_participant_questions().get("gene_test_performed");
            gene_test_performed +=String.format("\n\n%-30s%-30s","" , getSelection(patient.isGene_test_performed()));
            geneticDiag.add(gene_test_performed);
        }

        List<String> consentInfo = new ArrayList<String>();
        String consent = String.format("%-30s%-30s", "Consent", feed.isConsent());
        String consentName = String.format("%-30s%-30s", "Consent Name", feed.isConsent_name());

        consentInfo.add(consent);
        consentInfo.add(consentName);


        listDataChild.put(listDataHeader.get(0), personalInfo); // Header, Child data
        listDataChild.put(listDataHeader.get(1), enrolmentInfo);
        listDataChild.put(listDataHeader.get(2), geneticDiag);
        listDataChild.put(listDataHeader.get(3), consentInfo);

        listAdapter = new ExpandableListAdapter(getContext(), listDataHeader, listDataChild);
        expListView.setAdapter(listAdapter);
        expListView.setGroupIndicator(null);
    }
}
