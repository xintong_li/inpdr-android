package com.unimelb.inpdr.Models.API;

import android.app.Activity;
import android.content.ComponentName;
import android.content.Context;
import android.content.Intent;
import android.content.ServiceConnection;
import android.os.Debug;
import android.os.IBinder;
import android.util.Log;

/**
 * ModelFacade uses the concept of the Facade design pattern to be the model's
 * contact point with the rest of the MVC. ModelFacade is a singleton object to
 * be able to use it throughout the application. Every call to INPDR or any
 * model-related request someone may have must be done through this singleton
 * object.
 *
 * The main task is to connect to the INPDR servers using the INPDR API.
 *
 * This class also create a service so that these types of requests keep running
 * even if the user exits the application.
 *
 * Created by Xintong Li on 20/04/2016.
 */
public class ModelFacade {

    /**
     * Private instance of this class to act as the singleton object
     */
    private static ModelFacade _instance;

    /**
     * INPDR object to call every time we need to request information
     * from INPDR
     */
    private INPDRAPI inpdrApi;


    /**
     * Make the constructor private to help with the singleton implementation
     */
    private ModelFacade(){
    }

    /**
     * Method to get the singleton object
     *
     * @return the singleton object
     */
    public static ModelFacade getInstance(){
        if(_instance == null)
            _instance = new ModelFacade();
        return _instance;
    }

    /**
     * Binds the provided Activity to the INPDR API service. Every Activity
     * that wants to use the service must call this method
     *
     * @param activity - The activity that wants to use the service
     */
    public void bindService(Activity activity){
        Intent intent = new Intent(activity, INPDRAPI.class);
        activity.bindService(intent, mConnection, Context.BIND_AUTO_CREATE);
        Log.d("mconnection !!", "" + mConnection);
    }


    public void clearCache(){
        inpdrApi.clearCache();
    }

    /**
     * This is the ServiceConnection object used to create the INPDR API
     * service.
     */
    private ServiceConnection mConnection = new ServiceConnection() {
        @Override
        public void onServiceConnected(ComponentName className, IBinder service) {
            Log.d("!!!! ", "");
            APIService.NetworkBinder binder = (APIService.NetworkBinder) service;
            APIService mService = binder.getService();
            inpdrApi = mService.getINPDRAPI();
            Log.d(" !!!!!! get service!!!!", "" + inpdrApi);
        }

        @Override
        public void onServiceDisconnected(ComponentName arg0) {
            //mBound = false;
        };
    };

    public void getAllPatients(APIReceivable receivable, INPDRAPI api){
        inpdrApi = api;
        inpdrApi.getAllPatients(receivable);
    }

    public void getANpcParticipant(APIReceivable receivable, INPDRAPI api, int id){
        inpdrApi = api;
        inpdrApi.getANpcParticipant(id, receivable);
    }

    public void getAPatient(APIReceivable receivable, INPDRAPI api, int id){
        inpdrApi = api;
        inpdrApi.getAPatient(id, receivable);
    }

    public void getANpbFollowup(APIReceivable receivable, INPDRAPI api, String followable_type, int followable_id, int id){
        inpdrApi = api;
        inpdrApi.getANpbFollowup(receivable, followable_type, followable_id, id);
    }

    public void getAllNpbFollowups(APIReceivable receivable, INPDRAPI api, String followable_type, int followable_id){
        inpdrApi = api;
        inpdrApi.getAllNpbFollowups(receivable, followable_type, followable_id);
    }

    public void getAllNpcFollowups(APIReceivable receivable, INPDRAPI api, String followable_type, int followable_id){
        inpdrApi = api;
        inpdrApi.getAllNpcFollowups(receivable, followable_type, followable_id);
    }

    public void getANpcFollowup(APIReceivable receivable, INPDRAPI api, String followable_type, int followable_id, int id){
        inpdrApi = api;
        inpdrApi.getANpcFollowup(receivable, followable_type, followable_id, id);
    }

    public void deleteANpbFollowup(APIReceivable receivable, INPDRAPI api, String followable_type, int followable_id, int id){
        inpdrApi = api;
        inpdrApi.deleteANpbFollowup(receivable, followable_type, followable_id, id);
    }

    public void deleteANpcFollowup(APIReceivable receivable, INPDRAPI api, String followable_type, int followable_id, int id){
        inpdrApi = api;
        inpdrApi.deleteANpcFollowup(receivable, followable_type, followable_id, id);
    }

    public void createANpbFollowup(APIReceivable receivable, INPDRAPI api, String url){
        inpdrApi = api;
        inpdrApi.createANpbFollowup(receivable, url);
    }

    public void createANpcFollowup(APIReceivable receivable, INPDRAPI api, String url){
        inpdrApi = api;
        inpdrApi.createANpcFollowup(receivable, url);
    }

    public void updateANpcFollowup(APIReceivable receivable, INPDRAPI api, String url){
        inpdrApi = api;
        inpdrApi.updateANpcFollowup(receivable, url);
    }

    public void updateANpbFollowup(APIReceivable receivable, INPDRAPI api, String url){
        inpdrApi = api;
        inpdrApi.updateANpbFollowup(receivable, url);
    }

    public void login(APIReceivable receivable, INPDRAPI api, String email, String password){
        inpdrApi = api;
        inpdrApi.login(receivable, email, password);
    }

    public void logout(APIReceivable receivable, INPDRAPI api){
        inpdrApi = api;
        inpdrApi.logout(receivable);
    }
}

