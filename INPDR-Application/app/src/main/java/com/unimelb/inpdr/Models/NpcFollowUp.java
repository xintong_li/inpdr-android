package com.unimelb.inpdr.Models;

import com.unimelb.inpdr.Models.API.APIItem;

import org.json.JSONObject;

/**
 * Created by Xintong on 22/04/2016.
 */
public class NpcFollowUp extends APIItem{
    private int id;
    private int impact_ambulation;
    private int impact_manipulation;
    private int impact_speech;
    private int impact_swallowing;
    private int impact_eye_movement;
    private int impact_seizure;
    private int impact_cognitive_impaired;
    private int impact_family_disappointed;
    private int impact_family_give_up;
    private int impact_family_worry_future;
    private int impact_family_closer_family;
    private int impact_wider_self_miss_career;
    private int impact_wider_family_miss_career;
    private int impact_wider_emergency;
    private int impact_wider_appointment;
    private String created_at;
    private String updated_at;
    private int followable_id;
    private String followable_type;

    public NpcFollowUp(JSONObject obj){
        setId(obj.optInt("id"));
        setImpact_ambulation(obj.optInt("impact_ambulation"));
        setImpact_manipulation(obj.optInt("impact_manipulation"));
        setImpact_speech(obj.optInt("impact_speech"));
        setImpact_swallowing(obj.optInt("impact_swallowing"));
        setImpact_eye_movement(obj.optInt("impact_eye_movement"));
        setImpact_seizure(obj.optInt("impact_seizure"));
        setImpact_cognitive_impaired(obj.optInt("impact_cognitive_impaired"));
        setImpact_family_disappointed(obj.optInt("impact_family_disappointed"));
        setImpact_family_give_up(obj.optInt("impact_family_give_up"));
        setImpact_family_worry_future(obj.optInt("impact_family_worry_future"));
        setImpact_family_closer_family(obj.optInt("impact_family_closer_family"));
        setImpact_wider_self_miss_career(obj.optInt("impact_wider_self_miss_career"));
        setImpact_wider_family_miss_career(obj.optInt("impact_wider_family_miss_career"));
        setImpact_wider_emergency(obj.optInt("impact_wider_emergency"));
        setImpact_wider_appointment(obj.optInt("impact_wider_appointment"));
        setCreated_at(obj.optString("created_at"));
        setUpdated_at(obj.optString("updated_at"));
        setFollowable_id(obj.optInt("followable_id"));
        setFollowable_type(obj.optString("followable_type"));
    }

    public int getImpact_ambulation() {
        return impact_ambulation;
    }

    public void setImpact_ambulation(int impact_ambulation) {
        this.impact_ambulation = impact_ambulation;
    }

    public int getImpact_manipulation() {
        return impact_manipulation;
    }

    public void setImpact_manipulation(int impact_manipulation) {
        this.impact_manipulation = impact_manipulation;
    }

    public int getImpact_speech() {
        return impact_speech;
    }

    public void setImpact_speech(int impact_speech) {
        this.impact_speech = impact_speech;
    }

    public int getImpact_swallowing() {
        return impact_swallowing;
    }

    public void setImpact_swallowing(int impact_swallowing) {
        this.impact_swallowing = impact_swallowing;
    }

    public int getImpact_eye_movement() {
        return impact_eye_movement;
    }

    public void setImpact_eye_movement(int impact_eye_movement) {
        this.impact_eye_movement = impact_eye_movement;
    }

    public int getImpact_seizure() {
        return impact_seizure;
    }

    public void setImpact_seizure(int impact_seizure) {
        this.impact_seizure = impact_seizure;
    }

    public int getImpact_cognitive_impaired() {
        return impact_cognitive_impaired;
    }

    public void setImpact_cognitive_impaired(int impact_cognitive_impaired) {
        this.impact_cognitive_impaired = impact_cognitive_impaired;
    }

    public int getImpact_family_disappointed() {
        return impact_family_disappointed;
    }

    public void setImpact_family_disappointed(int impact_family_disappointed) {
        this.impact_family_disappointed = impact_family_disappointed;
    }

    public int getImpact_family_give_up() {
        return impact_family_give_up;
    }

    public void setImpact_family_give_up(int impact_family_give_up) {
        this.impact_family_give_up = impact_family_give_up;
    }

    public int getImpact_family_worry_future() {
        return impact_family_worry_future;
    }

    public void setImpact_family_worry_future(int impact_family_worry_future) {
        this.impact_family_worry_future = impact_family_worry_future;
    }

    public int getImpact_family_closer_family() {
        return impact_family_closer_family;
    }

    public void setImpact_family_closer_family(int impact_family_closer_family) {
        this.impact_family_closer_family = impact_family_closer_family;
    }

    public int getImpact_wider_self_miss_career() {
        return impact_wider_self_miss_career;
    }

    public void setImpact_wider_self_miss_career(int impact_wider_self_miss_career) {
        this.impact_wider_self_miss_career = impact_wider_self_miss_career;
    }

    public int getImpact_wider_family_miss_career() {
        return impact_wider_family_miss_career;
    }

    public void setImpact_wider_family_miss_career(int impact_wider_family_miss_career) {
        this.impact_wider_family_miss_career = impact_wider_family_miss_career;
    }

    public int getImpact_wider_emergency() {
        return impact_wider_emergency;
    }

    public void setImpact_wider_emergency(int impact_wider_emergency) {
        this.impact_wider_emergency = impact_wider_emergency;
    }

    public int getImpact_wider_appointment() {
        return impact_wider_appointment;
    }

    public void setImpact_wider_appointment(int impact_wider_appointment) {
        this.impact_wider_appointment = impact_wider_appointment;
    }

    public String getCreated_at() {
        return created_at;
    }

    public void setCreated_at(String created_at) {
        this.created_at = created_at;
    }

    public String getUpdated_at() {
        return updated_at;
    }

    public void setUpdated_at(String updated_at) {
        this.updated_at = updated_at;
    }

    public int getFollowable_id() {
        return followable_id;
    }

    public void setFollowable_id(int followable_id) {
        this.followable_id = followable_id;
    }

    public String getFollowable_type() {
        return followable_type;
    }

    public void setFollowable_type(String followable_type) {
        this.followable_type = followable_type;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }
}
