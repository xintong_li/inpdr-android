package com.unimelb.inpdr.Fragments;

import android.content.Context;
import android.content.DialogInterface;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentTransaction;
import android.support.v7.app.AlertDialog;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.TextView;
import android.widget.Toast;

import com.jaredrummler.materialspinner.MaterialSpinner;
import com.unimelb.inpdr.MainActivity;
import com.unimelb.inpdr.Models.API.APIItem;
import com.unimelb.inpdr.Models.API.APIReceivable;
import com.unimelb.inpdr.Models.API.INPDRAPI;
import com.unimelb.inpdr.Models.API.ModelFacade;
import com.unimelb.inpdr.Models.API.Params;
import com.unimelb.inpdr.Models.BasicInfo;
import com.unimelb.inpdr.Models.ErrorMessage;
import com.unimelb.inpdr.Models.NpbApplicantFollowupCollection;
import com.unimelb.inpdr.Models.NpbFollowUp;
import com.unimelb.inpdr.Models.NpbFollowupCollection;
import com.unimelb.inpdr.Models.NpcApplicantFollowupCollection;
import com.unimelb.inpdr.Models.NpcFollowUp;
import com.unimelb.inpdr.Models.NpcFollowupCollection;
import com.unimelb.inpdr.Models.Selection;
import com.unimelb.inpdr.R;

import org.apache.http.client.utils.URIBuilder;

import java.net.URISyntaxException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * Created by Xintong on 20/05/2016.
 */
public class FollowupEditWiderImpactFragment extends Fragment implements APIReceivable{
    int patientId;
    String patientType;
    boolean isFirstTime;
    int id;
    Map<String, Integer> npbCollection = new HashMap<>();
    Map<String, Integer> npbApplicantCollection = new HashMap<>();
    Map<String, Integer> npcCollection = new HashMap<>();
    Map<String, Integer> npcApplicantCollection = new HashMap<>();

    public FollowupEditWiderImpactFragment() {
        // Required empty public constructor
    }

    @Override
    public void onStart(){
        super.onStart();
        ((MainActivity)getActivity()).setActionBarTitle("Edit Wider Impact");
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        return inflater.inflate(R.layout.fragment_followup_edit_wider_impact, container, false);
    }

    @Override
    public void onViewCreated(View view, Bundle savedInstanceState) {
        BasicInfo.getInstance();
        NpbFollowupCollection.getInstance();
        NpbApplicantFollowupCollection.getInstance();
        NpcFollowupCollection.getInstance();
        NpcApplicantFollowupCollection.getInstance();
        Selection.getInstance();

        ((MainActivity)getActivity()).setActionBarTitle("Edit Wider Impact");

        Bundle data = getArguments();

        if (data != null) {
            try {
                isFirstTime = data.getBoolean("first_time");
                patientId = data.getInt("followable_id");
                patientType = data.getString("followable_type");
                id = data.getInt("id");
                switch (patientType) {
                    case "NpbParticipant":
                        npbCollection.put("symptom_bone", data.getInt("symptom_bone"));
                        npbCollection.put("symptom_abdominal", data.getInt("symptom_abdominal"));
                        npbCollection.put("symptom_pain", data.getInt("symptom_pain"));
                        npbCollection.put("impact_wider_emergency", data.getInt("impact_wider_emergency"));
                        npbCollection.put("impact_wider_appointment", data.getInt("impact_wider_appointment"));
                        npbCollection.put("symptom_cognition", data.getInt("symptom_cognition"));
                        npbCollection.put("symptom_bleeding_rate", data.getInt("symptom_bleeding_rate"));
                        npbCollection.put("symptom_bleeding_impact", data.getInt("symptom_bleeding_impact"));
                        npbCollection.put("symptom_breathlessness_rate", data.getInt("symptom_breathlessness_rate"));
                        npbCollection.put("symptom_breathlessness_impact", data.getInt("symptom_breathlessness_impact"));
                        npbCollection.put("symptom_fatigue_rate", data.getInt("symptom_fatigue_rate"));
                        npbCollection.put("symptom_fatigue_impact", data.getInt("symptom_fatigue_impact"));
                        npbCollection.put("symptom_enlarge_organ", data.getInt("symptom_enlarge_organ"));
                        npbCollection.put("symptom_enlarge_organ_impact", data.getInt("symptom_enlarge_organ_impact"));
                        npbCollection.put("symptom_slow_growth", data.getInt("symptom_slow_growth"));
                        npbCollection.put("symptom_slow_growth_impact", data.getInt("symptom_slow_growth_impact"));
                        npbCollection.put("symptom_infection", data.getInt("symptom_infection"));
                        npbCollection.put("symptom_fracture", data.getInt("symptom_fracture"));
                        npbCollection.put("impact_family_give_up", data.getInt("impact_family_give_up"));
                        npbCollection.put("impact_family_disappointed", data.getInt("impact_family_disappointed"));
                        npbCollection.put("impact_family_worry_future", data.getInt("impact_family_worry_future"));
                        npbCollection.put("impact_family_closer", data.getInt("impact_family_closer"));
                        npbCollection.put("impact_wider_emergency", data.getInt("impact_wider_emergency"));
                        npbCollection.put("impact_wider_appointment", data.getInt("impact_wider_appointment"));
                        npbCollection.put("impact_wider_self_miss_career", data.getInt("impact_wider_self_miss_career"));
                        npbCollection.put("impact_wider_carer_miss_career", data.getInt("impact_wider_carer_miss_career"));
                        CreateNpbFollowup();
                        break;
                    case "NpbApplicantParticipant":
                        npbApplicantCollection.put("symptom_bone", data.getInt("symptom_bone"));
                        npbApplicantCollection.put("symptom_abdominal", data.getInt("symptom_abdominal"));
                        npbApplicantCollection.put("symptom_pain", data.getInt("symptom_pain"));
                        npbApplicantCollection.put("impact_wider_emergency", data.getInt("impact_wider_emergency"));
                        npbApplicantCollection.put("impact_wider_appointment", data.getInt("impact_wider_appointment"));
                        npbApplicantCollection.put("symptom_cognition", data.getInt("symptom_cognition"));
                        npbApplicantCollection.put("symptom_bleeding_rate", data.getInt("symptom_bleeding_rate"));
                        npbApplicantCollection.put("symptom_bleeding_impact", data.getInt("symptom_bleeding_impact"));
                        npbApplicantCollection.put("symptom_breathlessness_rate", data.getInt("symptom_breathlessness_rate"));
                        npbApplicantCollection.put("symptom_breathlessness_impact", data.getInt("symptom_breathlessness_impact"));
                        npbApplicantCollection.put("symptom_fatigue_rate", data.getInt("symptom_fatigue_rate"));
                        npbApplicantCollection.put("symptom_fatigue_impact", data.getInt("symptom_fatigue_impact"));
                        npbApplicantCollection.put("symptom_enlarge_organ", data.getInt("symptom_enlarge_organ"));
                        npbApplicantCollection.put("symptom_enlarge_organ_impact", data.getInt("symptom_enlarge_organ_impact"));
                        npbApplicantCollection.put("symptom_slow_growth", data.getInt("symptom_slow_growth"));
                        npbApplicantCollection.put("symptom_slow_growth_impact", data.getInt("symptom_slow_growth_impact"));
                        npbApplicantCollection.put("symptom_infection", data.getInt("symptom_infection"));
                        npbApplicantCollection.put("symptom_fracture", data.getInt("symptom_fracture"));
                        npbApplicantCollection.put("impact_family_give_up", data.getInt("impact_family_give_up"));
                        npbApplicantCollection.put("impact_family_disappointed", data.getInt("impact_family_disappointed"));
                        npbApplicantCollection.put("impact_family_worry_future", data.getInt("impact_family_worry_future"));
                        npbApplicantCollection.put("impact_family_closer", data.getInt("impact_family_closer"));
                        npbApplicantCollection.put("impact_wider_emergency", data.getInt("impact_wider_emergency"));
                        npbApplicantCollection.put("impact_wider_appointment", data.getInt("impact_wider_appointment"));
                        npbApplicantCollection.put("impact_wider_self_miss_career", data.getInt("impact_wider_self_miss_career"));
                        npbApplicantCollection.put("impact_wider_carer_miss_career", data.getInt("impact_wider_carer_miss_career"));
                        CreateApplicantNpbFollowup();
                        break;
                    case "NpcParticipant":
                        npcCollection.put("impact_ambulation", data.getInt("impact_ambulation"));
                        npcCollection.put("impact_manipulation", data.getInt("impact_manipulation"));
                        npcCollection.put("impact_speech", data.getInt("impact_speech"));
                        npcCollection.put("impact_swallowing", data.getInt("impact_swallowing"));
                        npcCollection.put("impact_eye_movement", data.getInt("impact_eye_movement"));
                        npcCollection.put("impact_seizure", data.getInt("impact_seizure"));
                        npcCollection.put("impact_cognitive_impaired", data.getInt("impact_cognitive_impaired"));
                        npcCollection.put("impact_family_disappointed", data.getInt("impact_family_disappointed"));
                        npcCollection.put("impact_family_give_up", data.getInt("impact_family_give_up"));
                        npcCollection.put("impact_family_worry_future", data.getInt("impact_family_worry_future"));
                        npcCollection.put("impact_family_closer_family", data.getInt("impact_family_closer_family"));
                        npcCollection.put("impact_wider_emergency", data.getInt("impact_wider_emergency"));
                        npcCollection.put("impact_wider_appointment", data.getInt("impact_wider_appointment"));
                        npcCollection.put("impact_wider_self_miss_career", data.getInt("impact_wider_self_miss_career"));
                        npcCollection.put("impact_wider_family_miss_career", data.getInt("impact_wider_family_miss_career"));
                        CreateNpcFollowup();
                        break;
                    case "NpcApplicantParticipant":
                        npcApplicantCollection.put("impact_ambulation", data.getInt("impact_ambulation"));
                        npcApplicantCollection.put("impact_manipulation", data.getInt("impact_manipulation"));
                        npcApplicantCollection.put("impact_speech", data.getInt("impact_speech"));
                        npcApplicantCollection.put("impact_swallowing", data.getInt("impact_swallowing"));
                        npcApplicantCollection.put("impact_eye_movement", data.getInt("impact_eye_movement"));
                        npcApplicantCollection.put("impact_seizure", data.getInt("impact_seizure"));
                        npcApplicantCollection.put("impact_cognitive_impaired", data.getInt("impact_cognitive_impaired"));
                        npcApplicantCollection.put("impact_family_disappointed", data.getInt("impact_family_disappointed"));
                        npcApplicantCollection.put("impact_family_give_up", data.getInt("impact_family_give_up"));
                        npcApplicantCollection.put("impact_family_worry_future", data.getInt("impact_family_worry_future"));
                        npcApplicantCollection.put("impact_family_closer_family", data.getInt("impact_family_closer_family"));
                        npcApplicantCollection.put("impact_wider_emergency", data.getInt("impact_wider_emergency"));
                        npcApplicantCollection.put("impact_wider_appointment", data.getInt("impact_wider_appointment"));
                        npcApplicantCollection.put("impact_wider_self_miss_career", data.getInt("impact_wider_self_miss_career"));
                        npcApplicantCollection.put("impact_wider_family_miss_career", data.getInt("impact_wider_family_miss_career"));
                        CreateApplicantNpcFollowup();
                        break;
                }
            }catch (Exception e){
                e.printStackTrace();
            }
        }


//        NiceSpinner niceSpinner = (NiceSpinner) view.findViewById(R.id.spinner);
//        List<String> dataset = new LinkedList<>(Arrays.asList("One", "Two", "Three", "Four", "Five"));
//        niceSpinner.attachDataSource(dataset);

        // ArrayAdapter<String> adapter = new ArrayAdapter<String>(getContext(), android.R.layout.simple_spinner_dropdown_item, countries);

//        NiceSpinnerAdapter adapter = new NiceSpinnerAdapter(getContext(), dataset, Color.DKGRAY, 2);
//        spinner.setAdapter(adapter);
        // Getting object reference to listview of main.xml
//        ListView listView = (ListView) view.findViewById(R.id.listView);
//
//        // Instantiating array adapter to populate the listView
//        // The layout android.R.layout.simple_list_item_single_choice creates radio button for each listview item
//
//        ArrayAdapter<String> adapter = new ArrayAdapter<String>(getContext(), android.R.layout.simple_list_item_single_choice, countries);
//
//        listView.setAdapter(adapter);

    }



    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

    }

    @Override
    public <U extends APIItem> void receiveData(List<U> items, String type) {
        switch (type){
            case "getANpcFollowup":
                for (U item : items) {
                    NpcFollowUp npcFollowUp = (NpcFollowUp) item;
                    Bundle data = new Bundle();
                    data = new Bundle();

                    data.putInt("followable_id", npcFollowUp.getFollowable_id());
                    data.putString("followable_type", npcFollowUp.getFollowable_type());
                    data.putInt("followup_id", npcFollowUp.getId());

                    Fragment followupDetailFragment = new FollowupDetailFragment();
                    Fragment followupFragment = new FollowupFragment();
                    followupDetailFragment.setArguments(data);

                    FragmentManager fm = getFragmentManager();
                    fm.getBackStackEntryAt(0);

                    FragmentTransaction transaction = getFragmentManager()
                            .beginTransaction();
                    transaction = fm.beginTransaction();

                    transaction.add(R.id.main_view, followupDetailFragment,
                            "FragmentFollowupDetail");
                    transaction.addToBackStack("FragmentFollowup");
                    transaction.show(followupDetailFragment);
                    transaction.setTransition(FragmentTransaction.TRANSIT_FRAGMENT_CLOSE);
                    transaction.commit();
                }
                break;
            case "getANpbFollowup":
                for (U item : items) {
                    NpbFollowUp npbFollowUp = (NpbFollowUp) item;
                    Bundle data = new Bundle();

                    data.putInt("followable_id", npbFollowUp.getFollowable_id());
                    data.putString("followable_type", npbFollowUp.getFollowable_type());
                    data.putInt("followup_id", npbFollowUp.getId());

                    Fragment followupDetailFragment = new FollowupDetailFragment();
                    Fragment followupFragment = new FollowupFragment();
                    followupDetailFragment.setArguments(data);

                    FragmentTransaction transaction = getFragmentManager()
                            .beginTransaction();

                    transaction.add(R.id.main_view, followupFragment,
                            "FragmentFollowup");
                    transaction.add(R.id.main_view, followupDetailFragment,
                            "FragmentFollowupDetail");
                    transaction.addToBackStack("FragmentFollowup");
                    transaction.show(followupDetailFragment);
                    transaction.setTransition(FragmentTransaction.TRANSIT_FRAGMENT_CLOSE);
                    transaction.commit();
                }
                break;
        }

    }

    @Override
    public <U extends APIItem> void receiveData(U item, String type) {
        final INPDRAPI api = new INPDRAPI(getContext());
        switch (type) {
            case "updateANpbFollowup":
                final AlertDialog alertDialog_npb = new AlertDialog.Builder(getContext()).create();
                alertDialog_npb.setTitle("Congratulations");
                alertDialog_npb.setMessage("The new questionnaire is successfully updated.");
                alertDialog_npb.setIcon(R.drawable.icon_congras);

                alertDialog_npb.setButton(AlertDialog.BUTTON_NEUTRAL, "Ok",
                        new DialogInterface.OnClickListener() {
                            public void onClick(DialogInterface dialog, int which) {
                                alertDialog_npb.dismiss();
                            }
                        });

                alertDialog_npb.show();

                ModelFacade.getInstance().getANpbFollowup(this, api, patientType, patientId, id);
                break;
            case "updateANpcFollowup":
                final AlertDialog alertDialog_npc = new AlertDialog.Builder(getContext()).create();
                alertDialog_npc.setTitle("Congratulations");
                alertDialog_npc.setMessage("The new questionnaire is successfully updated.");
                alertDialog_npc.setIcon(R.drawable.icon_congras);

                alertDialog_npc.setButton(AlertDialog.BUTTON_NEUTRAL, "Ok",
                        new DialogInterface.OnClickListener() {
                            public void onClick(DialogInterface dialog, int which) {
                                alertDialog_npc.dismiss();
                            }
                        });

                alertDialog_npc.show();

                ModelFacade.getInstance().getANpcFollowup(this, api, patientType, patientId, id);

                break;
            case "error":
                ErrorMessage error = (ErrorMessage) item;
                final AlertDialog alertDialog = new AlertDialog.Builder(getContext()).create();
                alertDialog.setTitle("Error");

                alertDialog.setMessage("Error" + error.getErrorCode() + ": " + error.getErrorMes());
                alertDialog.setIcon(R.drawable.icon_alert);

                alertDialog.setButton(AlertDialog.BUTTON_NEUTRAL, "Try Again",
                        new DialogInterface.OnClickListener() {
                            public void onClick(DialogInterface dialog, int which) {
                                alertDialog.dismiss();
                            }
                        });

                alertDialog.show();
                break;
        }
    }



    private void CreateNpcFollowup() {
        final APIReceivable receivable = this;
        final INPDRAPI api = new INPDRAPI(getContext());

        TextView textDiscription = (TextView)getView().findViewById(R.id.text_edit_explaination_wider_impact_followup);
        textDiscription.setText("The following section looks at the wide impact of Niemann-Pick disease. " +
                "For each question, please select the option that you feel is most accurate.");

        TextView textNpcCreate = (TextView)getView().findViewById(R.id.text_edit_wider_impact_followup_title);
        textNpcCreate.setText("Edit NP-C Followup Questionnaire");

        int index = 0;
        ViewGroup insertPoint = (ViewGroup) getView().findViewById(R.id.lty_edit_wider_impact_followup);
        LayoutInflater vi = (LayoutInflater) getActivity().getSystemService(Context.LAYOUT_INFLATER_SERVICE);

        List<String> answers_impact_wider_self_miss_career = new ArrayList<>();
        answers_impact_wider_self_miss_career.add(" ");
        for(int i = 0; i < NpcFollowupCollection.getImpact_wider_self_miss_career().size(); i++){
            answers_impact_wider_self_miss_career.add(NpcFollowupCollection.getImpact_wider_self_miss_career().get(i));
        }
        NpcInflater(vi, answers_impact_wider_self_miss_career, "impact_wider_self_miss_career", insertPoint, index, npcCollection, BasicInfo.getNpc_participant_followups_questions());
        index++;

        List<String> answers_impact_wider_family_miss_career = new ArrayList<>();
        answers_impact_wider_family_miss_career.add(" ");
        for(int i = 0; i < NpcFollowupCollection.getImpact_wider_family_miss_career().size(); i++){
            answers_impact_wider_family_miss_career.add(NpcFollowupCollection.getImpact_wider_family_miss_career().get(i));
        }
        NpcInflater(vi, answers_impact_wider_family_miss_career, "impact_wider_family_miss_career", insertPoint, index, npcCollection, BasicInfo.getNpc_participant_followups_questions());
        index++;

        List<String> answers_impact_wider_emergency = new ArrayList<>();
        answers_impact_wider_emergency.add(" ");
        for(int i = 0; i < NpcFollowupCollection.getImpact_wider_emergency().size(); i++){
            answers_impact_wider_emergency.add(NpcFollowupCollection.getImpact_wider_emergency().get(i));
        }
        NpcInflater(vi, answers_impact_wider_emergency, "impact_wider_emergency", insertPoint, index, npcCollection, BasicInfo.getNpc_participant_followups_questions());
        index++;

        List<String> answers_impact_wider_appointment = new ArrayList<>();
        answers_impact_wider_appointment.add(" ");
        for(int i = 0; i < NpcFollowupCollection.getImpact_wider_appointment().size(); i++){
            answers_impact_wider_appointment.add(NpcFollowupCollection.getImpact_wider_appointment().get(i));
        }
        NpcInflater(vi, answers_impact_wider_appointment, "impact_wider_appointment", insertPoint, index, npcCollection, BasicInfo.getNpc_participant_followups_questions());


        Button buttonDone = (Button) getView().findViewById(R.id.button_done);

        buttonDone.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                String path = String.format("/api/v1/%s/%d/npc_followups/%d",
                        patientType, patientId, id);

                URIBuilder builder = new URIBuilder();
                try {
                    builder.setScheme("http")
                            .setHost("10.0.3.2")
                            .setPort(3000)
                            .setPath(path)
                            .addParameter("impact_ambulation", npcCollection.get("impact_ambulation") + "")
                            .addParameter("impact_manipulation", npcCollection.get("impact_manipulation") + "")
                            .addParameter("impact_speech", npcCollection.get("impact_speech") + "")
                            .addParameter("impact_swallowing", npcCollection.get("impact_swallowing") + "")
                            .addParameter("impact_eye_movement", npcCollection.get("impact_eye_movement") + "")
                            .addParameter("impact_seizure", npcCollection.get("impact_seizure") + "")
                            .addParameter("impact_cognitive_impaired", npcCollection.get("impact_cognitive_impaired") + "")
                            .addParameter("impact_family_disappointed", npcCollection.get("impact_family_disappointed") + "")
                            .addParameter("impact_family_give_up", npcCollection.get("impact_family_give_up") + "")
                            .addParameter("impact_family_worry_future", npcCollection.get("impact_family_worry_future") + "")
                            .addParameter("impact_family_closer_family", npcCollection.get("impact_family_closer_family") + "")
                            .addParameter("impact_wider_self_miss_career", npcCollection.get("impact_wider_self_miss_career") + "")
                            .addParameter("impact_wider_family_miss_career", npcCollection.get("impact_wider_family_miss_career") + "")
                            .addParameter("impact_wider_emergency", npcCollection.get("impact_wider_emergency") + "")
                            .addParameter("impact_wider_appointment", npcCollection.get("impact_wider_appointment") + "")
                            .build();
                    // .fragment("section-name");
                    final String url = builder.toString();
                    Log.d("url=>", url);
                    AlertDialog alertDialog = new AlertDialog.Builder(getContext()).create();
                    alertDialog.setTitle("Alert");
                    alertDialog.setMessage("Ready to submit?");
                    alertDialog.setIcon(R.drawable.icon_alert);

                    alertDialog.setButton(AlertDialog.BUTTON_POSITIVE, "Yes",
                            new DialogInterface.OnClickListener() {
                                public void onClick(DialogInterface dialog, int which) {
                                    ModelFacade.getInstance().updateANpcFollowup(receivable, api, url);
                                }
                            });

                    alertDialog.setButton(AlertDialog.BUTTON_NEGATIVE, "No",
                            new DialogInterface.OnClickListener() {
                                public void onClick(DialogInterface dialog, int which) {
                                    dialog.dismiss();
                                }
                            });

                    alertDialog.show();

                } catch (URISyntaxException e) {
                    e.printStackTrace();
                }
            }
        });

    }

    private void NpcInflater(LayoutInflater vi, final List<String> items, final String param, ViewGroup insertPoint, int index, final Map<String, Integer> answers, Map<String, String> questions) {
        View v = vi.inflate(R.layout.item_create_followup, null);
        TextView textQuestion = (TextView)v.findViewById(R.id.text_create_followup_question);

        textQuestion.setText(questions.get(param));
        MaterialSpinner spinner = (MaterialSpinner) v.findViewById(R.id.spinner);
        spinner.setItems(items);

        spinner.setSelectedIndex(answers.get(param) + 1);
        spinner.setTextColor(getResources().getColor(R.color.colorPrimaryText));

        if(!isFirstTime){
            SharedPreferences myPrefs = getActivity().getSharedPreferences("myPrefs", 0);
            Log.d("not first time", "");
            if(myPrefs.contains(param)){
                spinner.setSelectedIndex(myPrefs.getInt(param, 0));
                Log.d("get prefs", myPrefs.getInt(param, 0)+ " " + param);
            }
        }

        spinner.setOnItemSelectedListener(new MaterialSpinner.OnItemSelectedListener<String>() {

            @Override
            public void onItemSelected(MaterialSpinner view, int position, long id, String item) {
                if(position != 0){
                    answers.put(param, position - 1);
                }
                int mSpnValue = position;
                SharedPreferences myPrefs = getActivity().getSharedPreferences("myPrefs", getContext().MODE_MULTI_PROCESS);
                SharedPreferences.Editor prefsEditor = myPrefs.edit();
                prefsEditor.putInt(param, mSpnValue);
                prefsEditor.commit();
                Log.d("valuee", " " + param + ", " + mSpnValue);
//
//                Context context = view.getContext();
//                CharSequence text = answers.get(param) + " " + param;
//                int duration = Toast.LENGTH_SHORT;
//
//                Toast toast = Toast.makeText(context, text, duration);
//                toast.show();
            }
        });
        spinner.onSaveInstanceState();
        answers.put(param, spinner.getSelectedIndex() - 1);

        insertPoint.addView(
                v,
                index,
                new ViewGroup.LayoutParams(ViewGroup.LayoutParams.MATCH_PARENT, ViewGroup.LayoutParams.WRAP_CONTENT));
    }

    private void CreateApplicantNpcFollowup() {
        final APIReceivable receivable = this;
        final INPDRAPI api = new INPDRAPI(getContext());

        TextView textDiscription = (TextView)getView().findViewById(R.id.text_edit_explaination_wider_impact_followup);
        textDiscription.setText("The following section looks at the wide impact of Niemann-Pick disease. " +
                "For each question, please select the option that you feel is most accurate.");

        TextView textNpcCreate = (TextView)getView().findViewById(R.id.text_edit_wider_impact_followup_title);
        textNpcCreate.setText("Edit NP-C Followup Questionnaire");

        int index = 0;
        ViewGroup insertPoint = (ViewGroup) getView().findViewById(R.id.lty_edit_wider_impact_followup);
        LayoutInflater vi = (LayoutInflater) getActivity().getSystemService(Context.LAYOUT_INFLATER_SERVICE);

        List<String> answers_impact_wider_self_miss_career = new ArrayList<>();
        answers_impact_wider_self_miss_career.add(" ");
        for(int i = 0; i < NpcApplicantFollowupCollection.getImpact_wider_self_miss_career().size(); i++){
            answers_impact_wider_self_miss_career.add(NpcApplicantFollowupCollection.getImpact_wider_self_miss_career().get(i));
        }
        NpcInflater(vi, answers_impact_wider_self_miss_career, "impact_wider_self_miss_career", insertPoint, index, npcApplicantCollection, BasicInfo.getNpc_applicant_participant_followups_questions());
        index++;

        List<String> answers_impact_wider_family_miss_career = new ArrayList<>();
        answers_impact_wider_family_miss_career.add(" ");
        for(int i = 0; i < NpcApplicantFollowupCollection.getImpact_wider_family_miss_career().size(); i++){
            answers_impact_wider_family_miss_career.add(NpcApplicantFollowupCollection.getImpact_wider_family_miss_career().get(i));
        }
        NpcInflater(vi, answers_impact_wider_family_miss_career, "impact_wider_family_miss_career", insertPoint, index, npcApplicantCollection, BasicInfo.getNpc_applicant_participant_followups_questions());
        index++;

        List<String> answers_impact_wider_emergency = new ArrayList<>();
        answers_impact_wider_emergency.add(" ");
        for(int i = 0; i < NpcApplicantFollowupCollection.getImpact_wider_emergency().size(); i++){
            answers_impact_wider_emergency.add(NpcApplicantFollowupCollection.getImpact_wider_emergency().get(i));
        }
        NpcInflater(vi, answers_impact_wider_emergency, "impact_wider_emergency", insertPoint, index, npcApplicantCollection, BasicInfo.getNpc_applicant_participant_followups_questions());
        index++;

        List<String> answers_impact_wider_appointment = new ArrayList<>();
        answers_impact_wider_appointment.add(" ");
        for(int i = 0; i < NpcApplicantFollowupCollection.getImpact_wider_appointment().size(); i++){
            answers_impact_wider_appointment.add(NpcApplicantFollowupCollection.getImpact_wider_appointment().get(i));
        }
        NpcInflater(vi, answers_impact_wider_appointment, "impact_wider_appointment", insertPoint, index, npcApplicantCollection, BasicInfo.getNpc_applicant_participant_followups_questions());


        Button buttonDone = (Button) getView().findViewById(R.id.button_done);

        buttonDone.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                String path = String.format("/api/v1/%s/%d/npc_followups/%d",
                        patientType, patientId, id);

                URIBuilder builder = new URIBuilder();
                try {
                    builder.setScheme("http")
                            .setHost("10.0.3.2")
                            .setPort(3000)
                            .setPath(path)
                            .addParameter("impact_ambulation", npcApplicantCollection.get("impact_ambulation") + "")
                            .addParameter("impact_manipulation", npcApplicantCollection.get("impact_manipulation") + "")
                            .addParameter("impact_speech", npcApplicantCollection.get("impact_speech") + "")
                            .addParameter("impact_swallowing", npcApplicantCollection.get("impact_swallowing") + "")
                            .addParameter("impact_eye_movement", npcApplicantCollection.get("impact_eye_movement") + "")
                            .addParameter("impact_seizure", npcApplicantCollection.get("impact_seizure") + "")
                            .addParameter("impact_cognitive_impaired", npcApplicantCollection.get("impact_cognitive_impaired") + "")
                            .addParameter("impact_family_disappointed", npcApplicantCollection.get("impact_family_disappointed") + "")
                            .addParameter("impact_family_give_up", npcApplicantCollection.get("impact_family_give_up") + "")
                            .addParameter("impact_family_worry_future", npcApplicantCollection.get("impact_family_worry_future") + "")
                            .addParameter("impact_family_closer_family", npcApplicantCollection.get("impact_family_closer_family") + "")
                            .addParameter("impact_wider_self_miss_career", npcApplicantCollection.get("impact_wider_self_miss_career") + "")
                            .addParameter("impact_wider_family_miss_career", npcApplicantCollection.get("impact_wider_family_miss_career") + "")
                            .addParameter("impact_wider_emergency", npcApplicantCollection.get("impact_wider_emergency") + "")
                            .addParameter("impact_wider_appointment", npcApplicantCollection.get("impact_wider_appointment") + "")
                            .build();
                    // .fragment("section-name");
                    final String url = builder.toString();
                    Log.d("url=>", url);
                    AlertDialog alertDialog = new AlertDialog.Builder(getContext()).create();
                    alertDialog.setTitle("Alert");
                    alertDialog.setMessage("Ready to submit?");
                    alertDialog.setIcon(R.drawable.icon_alert);

                    alertDialog.setButton(AlertDialog.BUTTON_POSITIVE, "Yes",
                            new DialogInterface.OnClickListener() {
                                public void onClick(DialogInterface dialog, int which) {
                                    ModelFacade.getInstance().updateANpcFollowup(receivable, api, url);
                                }
                            });

                    alertDialog.setButton(AlertDialog.BUTTON_NEGATIVE, "No",
                            new DialogInterface.OnClickListener() {
                                public void onClick(DialogInterface dialog, int which) {
                                    dialog.dismiss();
                                }
                            });

                    alertDialog.show();

                } catch (URISyntaxException e) {
                    e.printStackTrace();
                }
            }
        });
    }


    private void CreateNpbFollowup() {
        final APIReceivable receivable = this;
        final INPDRAPI api = new INPDRAPI(getContext());
        int index = 0;

        TextView textNpcCreate = (TextView)getView().findViewById(R.id.text_edit_wider_impact_followup_title);
        textNpcCreate.setText("Edit NP-B Followup Questionnaire");

        ViewGroup insertPoint = (ViewGroup) getView().findViewById(R.id.lty_edit_wider_impact_followup);
        LayoutInflater vi = (LayoutInflater) getActivity().getSystemService(Context.LAYOUT_INFLATER_SERVICE);


        List<String> answers_impact_wider_self_miss_career = new ArrayList<>();
        answers_impact_wider_self_miss_career.add(" ");
        for(int i = 0; i < NpbFollowupCollection.getImpact_wider_self_miss_career().size(); i++){
            answers_impact_wider_self_miss_career.add(NpbFollowupCollection.getImpact_wider_self_miss_career().get(i));
        }
        NpbInflater(vi, answers_impact_wider_self_miss_career, "impact_wider_self_miss_career", insertPoint, index, npbCollection, BasicInfo.getNpb_followups_questions());
        index++;

        List<String> answers_impact_wider_carer_miss_career = new ArrayList<>();
        answers_impact_wider_carer_miss_career.add(" ");
        for(int i = 0; i < NpbFollowupCollection.getImpact_wider_carer_miss_career().size(); i++){
            answers_impact_wider_carer_miss_career.add(NpbFollowupCollection.getImpact_wider_carer_miss_career().get(i));
        }
        NpbInflater(vi, answers_impact_wider_carer_miss_career, "impact_wider_carer_miss_career", insertPoint, index, npbCollection, BasicInfo.getNpb_followups_questions());
        index++;

        List<String> answers_impact_wider_emergency = new ArrayList<>();
        answers_impact_wider_emergency.add(" ");
        for(int i = 0; i < NpbFollowupCollection.getImpact_wider_emergency().size(); i++){
            answers_impact_wider_emergency.add(NpbFollowupCollection.getImpact_wider_emergency().get(i));
        }
        NpbInflater(vi, answers_impact_wider_emergency, "impact_wider_emergency", insertPoint, index, npbCollection, BasicInfo.getNpb_followups_questions());
        index++;

        List<String> answers_impact_wider_appointment = new ArrayList<>();
        answers_impact_wider_appointment.add(" ");
        for(int i = 0; i < NpbFollowupCollection.getImpact_wider_appointment().size(); i++){
            answers_impact_wider_appointment.add(NpbFollowupCollection.getImpact_wider_appointment().get(i));
        }
        NpbInflater(vi, answers_impact_wider_appointment, "impact_wider_appointment", insertPoint, index, npbCollection, BasicInfo.getNpb_followups_questions());



        Button buttonDone = (Button) getView().findViewById(R.id.button_done);

        buttonDone.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                String path = String.format("/api/v1/%s/%d/npb_followups/%d",
                        patientType, patientId, id);

                URIBuilder builder = new URIBuilder();
                try {
                    builder.setScheme("http")
                            .setHost("10.0.3.2")
                            .setPort(3000)
                            .setPath(path)
                            .addParameter("symptom_bone", npbCollection.get("symptom_bone") + "")
                            .addParameter("symptom_abdominal", npbCollection.get("symptom_abdominal") + "")
                            .addParameter("symptom_pain", npbCollection.get("symptom_pain") + "")
                            .addParameter("impact_wider_emergency", npbCollection.get("impact_wider_emergency") + "")
                            .addParameter("impact_wider_appointment", npbCollection.get("impact_wider_appointment") + "")
                            .addParameter("symptom_cognition", npbCollection.get("symptom_cognition") + "")
                            .addParameter("symptom_breathlessness_rate", npbCollection.get("symptom_breathlessness_rate") + "")
                            .addParameter("symptom_breathlessness_impact", npbCollection.get("symptom_breathlessness_impact") + "")
                            .addParameter("symptom_bleeding_rate", npbCollection.get("symptom_bleeding_rate") + "")
                            .addParameter("symptom_bleeding_impact", npbCollection.get("symptom_bleeding_impact") + "")
                            .addParameter("symptom_fatigue_rate", npbCollection.get("symptom_fatigue_rate") + "")
                            .addParameter("symptom_fatigue_impact", npbCollection.get("symptom_fatigue_impact") + "")
                            .addParameter("symptom_enlarge_organ", npbCollection.get("symptom_enlarge_organ") + "")
                            .addParameter("symptom_enlarge_organ_impact", npbCollection.get("symptom_enlarge_organ_impact") + "")
                            .addParameter("symptom_slow_growth", npbCollection.get("symptom_slow_growth") + "")
                            .addParameter("symptom_slow_growth_impact", npbCollection.get("symptom_slow_growth_impact") + "")
                            .addParameter("symptom_infection", npbCollection.get("symptom_infection") + "")
                            .addParameter("symptom_fracture", npbCollection.get("symptom_fracture") + "")
                            .addParameter("impact_family_disappointed", npbCollection.get("impact_family_disappointed") + "")
                            .addParameter("impact_family_give_up", npbCollection.get("impact_family_give_up") + "")
                            .addParameter("impact_family_worry_future", npbCollection.get("impact_family_worry_future") + "")
                            .addParameter("impact_family_closer", npbCollection.get("impact_family_closer") + "")
                            .addParameter("impact_wider_self_miss_career", npbCollection.get("impact_wider_self_miss_career") + "")
                            .addParameter("impact_wider_carer_miss_career", npbCollection.get("impact_wider_carer_miss_career") + "")
                            .build();
                    // .fragment("section-name");
                    final String url = builder.toString();
                    Log.d("url=>", url);

                    AlertDialog alertDialog = new AlertDialog.Builder(getContext()).create();
                    alertDialog.setTitle("Alert");
                    alertDialog.setMessage("Ready to submit?");
                    alertDialog.setIcon(R.drawable.icon_alert);

                    alertDialog.setButton(AlertDialog.BUTTON_POSITIVE, "Yes",
                            new DialogInterface.OnClickListener() {
                                public void onClick(DialogInterface dialog, int which) {
                                    ModelFacade.getInstance().updateANpbFollowup(receivable, api, url);
                                }
                            });

                    alertDialog.setButton(AlertDialog.BUTTON_NEGATIVE, "No",
                            new DialogInterface.OnClickListener() {
                                public void onClick(DialogInterface dialog, int which) {
                                    dialog.dismiss();
                                }
                            });

                    alertDialog.show();

                } catch (URISyntaxException e) {
                    e.printStackTrace();
                }
            }
        });

//        Spinner spinner = (Spinner) v.findViewById(R.id.spinner);
//        ArrayAdapter<String> adapter = new ArrayAdapter<String>(getContext(), android.R.layout.simple_spinner_dropdown_item, answers_bone_pain);
//
//        spinner.setAdapter(adapter);

    }

    private void NpbInflater(LayoutInflater vi, final List<String> items, final String param, ViewGroup insertPoint, int index, final Map<String, Integer> answers, Map<String, String> questions) {
        View v = vi.inflate(R.layout.item_create_followup, null);
        TextView textQuestion = (TextView)v.findViewById(R.id.text_create_followup_question);

        textQuestion.setText(questions.get(param));
        MaterialSpinner spinner = (MaterialSpinner) v.findViewById(R.id.spinner);
        spinner.setItems(items);

        spinner.setSelectedIndex(answers.get(param) + 1);
        spinner.setTextColor(getResources().getColor(R.color.colorPrimaryText));

        if(!isFirstTime){
            SharedPreferences myPrefs = getActivity().getSharedPreferences("myPrefs", 0);
            Log.d("not first time", "");
            if(myPrefs.contains(param)){
                spinner.setSelectedIndex(myPrefs.getInt(param, 0));
                Log.d("get prefs", myPrefs.getInt(param, 0)+ " " + param);
            }
        }

        spinner.setOnItemSelectedListener(new MaterialSpinner.OnItemSelectedListener<String>() {

            @Override
            public void onItemSelected(MaterialSpinner view, int position, long id, String item) {
                if(position != 0){
                    answers.put(param, position - 1);
                }
                int mSpnValue = position;
                SharedPreferences myPrefs = getActivity().getSharedPreferences("myPrefs", getContext().MODE_MULTI_PROCESS);
                SharedPreferences.Editor prefsEditor = myPrefs.edit();
                prefsEditor.putInt(param, mSpnValue);
                prefsEditor.commit();
                Log.d("valuee", " " + param + ", " + mSpnValue);

//                Context context = view.getContext();
//                CharSequence text = answers.get(param) + " " + param;
//                int duration = Toast.LENGTH_SHORT;
//
//                Toast toast = Toast.makeText(context, text, duration);
//                toast.show();
            }
        });
        spinner.onSaveInstanceState();
        answers.put(param, spinner.getSelectedIndex() - 1);

        insertPoint.addView(
                v,
                index,
                new ViewGroup.LayoutParams(ViewGroup.LayoutParams.MATCH_PARENT, ViewGroup.LayoutParams.WRAP_CONTENT));
    }


    private void CreateApplicantNpbFollowup() {
        final APIReceivable receivable = this;
        final INPDRAPI api = new INPDRAPI(getContext());
        int index = 0;

        TextView textNpcCreate = (TextView)getView().findViewById(R.id.text_edit_wider_impact_followup_title);
        textNpcCreate.setText("Edit NP-B Followup Questionnaire");

        ViewGroup insertPoint = (ViewGroup) getView().findViewById(R.id.lty_edit_wider_impact_followup);
        LayoutInflater vi = (LayoutInflater) getActivity().getSystemService(Context.LAYOUT_INFLATER_SERVICE);


        List<String> answers_impact_wider_self_miss_career = new ArrayList<>();
        answers_impact_wider_self_miss_career.add(" ");
        for(int i = 0; i < NpbApplicantFollowupCollection.getImpact_wider_self_miss_career().size(); i++){
            answers_impact_wider_self_miss_career.add(NpbApplicantFollowupCollection.getImpact_wider_self_miss_career().get(i));
        }
        NpbInflater(vi, answers_impact_wider_self_miss_career, "impact_wider_self_miss_career", insertPoint, index, npbApplicantCollection, BasicInfo.getNpb_applicant_participant_followups_questions());
        index++;

        List<String> answers_impact_wider_carer_miss_career = new ArrayList<>();
        answers_impact_wider_carer_miss_career.add(" ");
        for(int i = 0; i < NpbApplicantFollowupCollection.getImpact_wider_carer_miss_career().size(); i++){
            answers_impact_wider_carer_miss_career.add(NpbApplicantFollowupCollection.getImpact_wider_carer_miss_career().get(i));
        }
        NpbInflater(vi, answers_impact_wider_carer_miss_career, "impact_wider_carer_miss_career", insertPoint, index, npbApplicantCollection, BasicInfo.getNpb_applicant_participant_followups_questions());
        index++;

        List<String> answers_impact_wider_emergency = new ArrayList<>();
        answers_impact_wider_emergency.add(" ");
        for(int i = 0; i < NpbApplicantFollowupCollection.getImpact_wider_emergency().size(); i++){
            answers_impact_wider_emergency.add(NpbApplicantFollowupCollection.getImpact_wider_emergency().get(i));
        }
        NpbInflater(vi, answers_impact_wider_emergency, "impact_wider_emergency", insertPoint, index, npbApplicantCollection, BasicInfo.getNpb_applicant_participant_followups_questions());
        index++;

        List<String> answers_impact_wider_appointment = new ArrayList<>();
        answers_impact_wider_appointment.add(" ");
        for(int i = 0; i < NpbApplicantFollowupCollection.getImpact_wider_appointment().size(); i++){
            answers_impact_wider_appointment.add(NpbApplicantFollowupCollection.getImpact_wider_appointment().get(i));
        }
        NpbInflater(vi, answers_impact_wider_appointment, "impact_wider_appointment", insertPoint, index, npbApplicantCollection, BasicInfo.getNpb_applicant_participant_followups_questions());



        Button buttonDone = (Button) getView().findViewById(R.id.button_done);

        buttonDone.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                String path = String.format("/api/v1/%s/%d/npb_followups/%d",
                        patientType, patientId, id);

                URIBuilder builder = new URIBuilder();
                try {
                    builder.setScheme("http")
                            .setHost("10.0.3.2")
                            .setPort(3000)
                            .setPath(path)
                            .addParameter("symptom_bone", npbApplicantCollection.get("symptom_bone") + "")
                            .addParameter("symptom_abdominal", npbApplicantCollection.get("symptom_abdominal") + "")
                            .addParameter("symptom_pain", npbApplicantCollection.get("symptom_pain") + "")
                            .addParameter("impact_wider_emergency", npbApplicantCollection.get("impact_wider_emergency") + "")
                            .addParameter("impact_wider_appointment", npbApplicantCollection.get("impact_wider_appointment") + "")
                            .addParameter("symptom_cognition", npbApplicantCollection.get("symptom_cognition") + "")
                            .addParameter("symptom_breathlessness_rate", npbApplicantCollection.get("symptom_breathlessness_rate") + "")
                            .addParameter("symptom_breathlessness_impact", npbApplicantCollection.get("symptom_breathlessness_impact") + "")
                            .addParameter("symptom_bleeding_rate", npbApplicantCollection.get("symptom_bleeding_rate") + "")
                            .addParameter("symptom_bleeding_impact", npbApplicantCollection.get("symptom_bleeding_impact") + "")
                            .addParameter("symptom_fatigue_rate", npbApplicantCollection.get("symptom_fatigue_rate") + "")
                            .addParameter("symptom_fatigue_impact", npbApplicantCollection.get("symptom_fatigue_impact") + "")
                            .addParameter("symptom_enlarge_organ", npbApplicantCollection.get("symptom_enlarge_organ") + "")
                            .addParameter("symptom_enlarge_organ_impact", npbApplicantCollection.get("symptom_enlarge_organ_impact") + "")
                            .addParameter("symptom_slow_growth", npbApplicantCollection.get("symptom_slow_growth") + "")
                            .addParameter("symptom_slow_growth_impact", npbApplicantCollection.get("symptom_slow_growth_impact") + "")
                            .addParameter("symptom_infection", npbApplicantCollection.get("symptom_infection") + "")
                            .addParameter("symptom_fracture", npbApplicantCollection.get("symptom_fracture") + "")
                            .addParameter("impact_family_disappointed", npbApplicantCollection.get("impact_family_disappointed") + "")
                            .addParameter("impact_family_give_up", npbApplicantCollection.get("impact_family_give_up") + "")
                            .addParameter("impact_family_worry_future", npbApplicantCollection.get("impact_family_worry_future") + "")
                            .addParameter("impact_family_closer", npbApplicantCollection.get("impact_family_closer") + "")
                            .addParameter("impact_wider_self_miss_career", npbApplicantCollection.get("impact_wider_self_miss_career") + "")
                            .addParameter("impact_wider_carer_miss_career", npbApplicantCollection.get("impact_wider_carer_miss_career") + "")
                            .build();
                    // .fragment("section-name");
                    final String url = builder.toString();
                    Log.d("url=>", url);

                    AlertDialog alertDialog = new AlertDialog.Builder(getContext()).create();
                    alertDialog.setTitle("Alert");
                    alertDialog.setMessage("Ready to submit?");
                    alertDialog.setIcon(R.drawable.icon_alert);

                    alertDialog.setButton(AlertDialog.BUTTON_POSITIVE, "Yes",
                            new DialogInterface.OnClickListener() {
                                public void onClick(DialogInterface dialog, int which) {
                                    ModelFacade.getInstance().updateANpbFollowup(receivable, api, url);
                                }
                            });

                    alertDialog.setButton(AlertDialog.BUTTON_NEGATIVE, "No",
                            new DialogInterface.OnClickListener() {
                                public void onClick(DialogInterface dialog, int which) {
                                    dialog.dismiss();
                                }
                            });

                    alertDialog.show();

                } catch (URISyntaxException e) {
                    e.printStackTrace();
                }
            }
        });

    }
}
