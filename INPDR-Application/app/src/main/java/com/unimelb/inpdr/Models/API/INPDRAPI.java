package com.unimelb.inpdr.Models.API;

import android.content.Context;
import android.util.Log;

import com.android.volley.AuthFailureError;
import com.android.volley.Cache;
import com.android.volley.DefaultRetryPolicy;
import com.android.volley.NetworkError;
import com.android.volley.NetworkResponse;
import com.android.volley.NoConnectionError;
import com.android.volley.ParseError;
import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.RetryPolicy;
import com.android.volley.ServerError;
import com.android.volley.TimeoutError;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.BasicNetwork;
import com.android.volley.toolbox.DiskBasedCache;
import com.android.volley.toolbox.HurlStack;
import com.android.volley.toolbox.StringRequest;
import com.unimelb.inpdr.Models.*;
import com.unimelb.inpdr.Models.ErrorMessage;

import org.apache.http.client.utils.URIBuilder;
import org.apache.http.util.TextUtils;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;

/**
 * Created by Xintong on 20/04/2016.
 */
public class INPDRAPI {
    private RequestQueue mRequestQueue;

    public INPDRAPI(Context context) {
        Cache cache = new DiskBasedCache(context.getCacheDir(), 1024 * 1024); // 1MB cap
        BasicNetwork network = new BasicNetwork(new HurlStack());

        mRequestQueue = new RequestQueue(cache, network);
        mRequestQueue.start();
    }

    public void login(final APIReceivable receivable, String email, String password){

        String urlString = String.format(Params.LOGIN,
                email, password);
        Log.d("LOGIN", urlString);
        StringRequest req = new StringRequest(Request.Method.POST,
                urlString,
                new Response.Listener<String>() {
                    @Override
                    public void onResponse(String response) {
                        try {
//                            Log.d("responesssssss", response);
                            JSONObject object = new JSONObject(response.toString());
                            Authentication aus = new Authentication();
                            aus.setStatus(object.optString("status"));
                            aus.setToken(object.optString("token"));
                            Params.ACCESS_TOKEN = aus.getToken();
                            Log.d("Response login:", "token: " + Params.ACCESS_TOKEN);

                            receivable.receiveData(aus, "login");
                        } catch (Exception e) {
                            e.printStackTrace();
                        }
                    }
                }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                ErrorMessage errorObj = new ErrorMessage();
                if (error instanceof TimeoutError || error instanceof NoConnectionError) {
                    Log.e("TimeoutError", error.toString());
                    errorObj.setErrorCode(0);
                    errorObj.setErrorMes("TimeoutError" + ":" + " Please check your internet and try it again.");
                    receivable.receiveData(errorObj, "error");
                } else if (error instanceof AuthFailureError) {
                    errorObj.setErrorCode(0);
                    errorObj.setErrorMes("Authentication failure" + ":" + " Please try agian.");
                    Log.e("AuthFailureError", error.toString());
                } else if (error instanceof ServerError) {
                    errorObj.setErrorCode(0);
                    errorObj.setErrorMes("ServerError" + ":" + " Sorry, there are some issues on the server, please try again later.");
                    Log.e("ServerError", error.toString());
                } else if (error instanceof NetworkError) {
                    errorObj.setErrorCode(0);
                    errorObj.setErrorMes("NetworkError" + ":" + " Please check your internet connection and try again later.");
                    Log.e("NetworkError", error.toString());
                } else if (error instanceof ParseError) {
                    errorObj.setErrorCode(0);
                    errorObj.setErrorMes("ParseError" + ":" + "Parse error!");
                    Log.e("ParseError", error.toString());
                }else {
                    errorObj.setErrorCode(0);
                    errorObj.setErrorMes("error" + ": " + error.toString());
                    Log.e("error", error.toString());
                }
//                String json = null;
//                NetworkResponse response = error.networkResponse;
//                if(response != null && response.data != null) {
//                    json = new String(response.data);
//                    json = trimMessage(json, "error");
//                    if (json != null) Log.e("json", json);
//                    errorObj.setErrorCode(response.statusCode);
//                    errorObj.setErrorMes(json);
//                    receivable.receiveData(errorObj, "error");
//
//                }else{
//                    String errorMessage=error.getClass().getSimpleName();
//                    if(!TextUtils.isEmpty(errorMessage)){
//                        Log.e("other error", errorMessage);
//                        errorObj.setErrorMes(json);
//                        receivable.receiveData(errorObj, "error");
//                    }
//                }
            }
        });

        int socketTimeout = 30000;
        RetryPolicy policy = new DefaultRetryPolicy(socketTimeout, DefaultRetryPolicy.DEFAULT_MAX_RETRIES, DefaultRetryPolicy.DEFAULT_BACKOFF_MULT);
        req.setRetryPolicy(policy);
        mRequestQueue.add(req);
    }

    public void logout(final APIReceivable receivable){
        final String urlString = String.format(Params.LOGOUT);
        Log.d("LOGOUT", urlString);
        StringRequest req = new StringRequest(Request.Method.DELETE,
                urlString,
                new Response.Listener<String>() {
                    @Override
                    public void onResponse(String response) {
                        Log.d("response!!", urlString);
                        try {

//                            Log.d("responesssssss", response);
                            JSONObject object = new JSONObject(response.toString());
                            Authentication aus = new Authentication();
                            aus.setStatus(object.optString("status"));
                            aus.setToken(object.optString("token"));
                            Params.ACCESS_TOKEN = aus.getToken();
                            Log.d("Response logout:", "token: " + Params.ACCESS_TOKEN);
                            receivable.receiveData(aus, "logout");

                        } catch (Exception e) {
                            e.printStackTrace();
                        }
                    }
                }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                ErrorMessage errorObj = new ErrorMessage();
                if (error instanceof TimeoutError || error instanceof NoConnectionError) {
                    Log.e("TimeoutError", error.toString());
                    errorObj.setErrorCode(0);
                    errorObj.setErrorMes("TimeoutError" + ":" + " Please check your internet and try it again.");
                    receivable.receiveData(errorObj, "error");
                } else if (error instanceof AuthFailureError) {
                    errorObj.setErrorCode(0);
                    errorObj.setErrorMes("Authentication failure" + ":" + " Please try agian.");
                    Log.e("AuthFailureError", error.toString());
                } else if (error instanceof ServerError) {
                    errorObj.setErrorCode(0);
                    errorObj.setErrorMes("ServerError" + ":" + " Sorry, there are some issues on the server, please try again later.");
                    Log.e("ServerError", error.toString());
                } else if (error instanceof NetworkError) {
                    errorObj.setErrorCode(0);
                    errorObj.setErrorMes("NetworkError" + ":" + " Please check your internet connection and try again later.");
                    Log.e("NetworkError", error.toString());
                } else if (error instanceof ParseError) {
                    errorObj.setErrorCode(0);
                    errorObj.setErrorMes("ParseError" + ":" + "Parse error!");
                    Log.e("ParseError", error.toString());
                }else {
                    errorObj.setErrorCode(0);
                    errorObj.setErrorMes("error" + ": " + error.toString());
                    Log.e("error", error.toString());
                }
//                String json = null;
//                NetworkResponse response = error.networkResponse;
//                if(response != null && response.data != null) {
//                    json = new String(response.data);
//                    json = trimMessage(json, "error");
//                    if (json != null) Log.e("json", json);
//                    errorObj.setErrorCode(response.statusCode);
//                    errorObj.setErrorMes(json);
//                    receivable.receiveData(errorObj, "error");
//
//                }else{
//                    String errorMessage=error.getClass().getSimpleName();
//                    if(!TextUtils.isEmpty(errorMessage)){
//                        Log.e("other error", errorMessage);
//                        errorObj.setErrorMes(json);
//                        receivable.receiveData(errorObj, "error");
//                    }
//                }
            }
        }){

            @Override
            public Map<String, String> getHeaders()throws AuthFailureError{
                Map<String, String> headers = new HashMap<String, String>();
                try {
                    String auth = Params.ACCESS_TOKEN;
                    headers.put("x-auth-token", auth);

                }catch (Exception e){
                    Log.d("AuthFailureError", e.toString());
                }
                return headers;
            }};

        int socketTimeout = 30000;
        RetryPolicy policy = new DefaultRetryPolicy(socketTimeout, DefaultRetryPolicy.DEFAULT_MAX_RETRIES, DefaultRetryPolicy.DEFAULT_BACKOFF_MULT);
        req.setRetryPolicy(policy);
        mRequestQueue.add(req);

    }

    public void getAllPatients(final APIReceivable receivable){
        String urlString = String.format(Params.ALL_PATIENTS);

        Log.d("all patients", urlString);
        StringRequest req = new StringRequest(Request.Method.GET,
                urlString,
                new Response.Listener<String>() {
                    @Override
                    public void onResponse(String response) {
//                        Log.d("response!!", "");
                        try {

                            JSONObject object = new JSONObject(response);
                            JSONArray aData = new JSONArray(object.optString("patients"));

                            ArrayList<Patient> patients = new ArrayList<Patient>();
                            for(int i = 0; i < aData.length(); i++) {
                                JSONObject userObject = (JSONObject) aData.get(i);
                                Patient patient = new Patient(userObject);
//                                Log.d("Response ", i + " patient " + patient.getUser().getLast_name() +
//                                        " enrolled a " + patient.getEnrollable_type() + " patient named " +
//                                        patient.getPatient_first_name());
                                if(patient.getItem()!=null){
                                    Log.d("patient!! ", patient.getItem().getClass()+ "");
                                }
                                patients.add(patient);
//                                if(user.getParticipant_type().equals("NpcParticipant")){
//                                    getANpcParticipant(user.getParticipant_id(), receivable);
//                                }
                            }
                            receivable.receiveData(patients, "getAllPatients");

                        } catch (Exception e) {
                            e.printStackTrace();
                        }
                    }
                }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                ErrorMessage errorObj = new ErrorMessage();
                if (error instanceof TimeoutError || error instanceof NoConnectionError) {
                    Log.e("TimeoutError", error.toString());
                    errorObj.setErrorCode(0);
                    errorObj.setErrorMes("TimeoutError" + ":" + " Please check your internet and try it again.");
                    receivable.receiveData(errorObj, "error");
                } else if (error instanceof AuthFailureError) {
                    errorObj.setErrorCode(0);
                    errorObj.setErrorMes("Authentication failure" + ":" + " Please try agian.");
                    Log.e("AuthFailureError", error.toString());
                } else if (error instanceof ServerError) {
                    errorObj.setErrorCode(0);
                    errorObj.setErrorMes("ServerError" + ":" + " Sorry, there are some issues on the server, please try again later.");
                    Log.e("ServerError", error.toString());
                } else if (error instanceof NetworkError) {
                    errorObj.setErrorCode(0);
                    errorObj.setErrorMes("NetworkError" + ":" + " Please check your internet connection and try again later.");
                    Log.e("NetworkError", error.toString());
                } else if (error instanceof ParseError) {
                    errorObj.setErrorCode(0);
                    errorObj.setErrorMes("ParseError" + ":" + "Parse error!");
                    Log.e("ParseError", error.toString());
                }else {
                    errorObj.setErrorCode(0);
                    errorObj.setErrorMes("error" + ": " + error.toString());
                    Log.e("error", error.toString());
                }
//                String json = null;
//                NetworkResponse response = error.networkResponse;
//                if(response != null && response.data != null) {
//                    json = new String(response.data);
//                    json = trimMessage(json, "error");
//                    if (json != null) Log.e("json", json);
//                    errorObj.setErrorCode(response.statusCode);
//                    errorObj.setErrorMes(json);
//                    receivable.receiveData(errorObj, "error");
//
//                }else{
//                    String errorMessage=error.getClass().getSimpleName();
//                    if(!TextUtils.isEmpty(errorMessage)){
//                        Log.e("other error", errorMessage);
//                        errorObj.setErrorMes(json);
//                        receivable.receiveData(errorObj, "error");
//                    }
//                }
            }
        }){

            @Override
            public Map<String, String> getHeaders()throws AuthFailureError{
                Map<String, String> headers = new HashMap<String, String>();
                try {
                    String auth = Params.ACCESS_TOKEN;
                    headers.put("x-auth-token", auth);

                }catch (Exception e){
                    Log.d("AuthFailureError", e.toString());
                }
                return headers;
            }};

        int socketTimeout = 30000;
        RetryPolicy policy = new DefaultRetryPolicy(socketTimeout, DefaultRetryPolicy.DEFAULT_MAX_RETRIES, DefaultRetryPolicy.DEFAULT_BACKOFF_MULT);
        req.setRetryPolicy(policy);
        mRequestQueue.add(req);
    }

    public void getAPatient(int user_id, final APIReceivable receivable){
        String urlString = String.format(Params.A_PATIENT,
                user_id);
        StringRequest req = new StringRequest(Request.Method.GET,
                urlString,
                new Response.Listener<String>() {
                    @Override
                    public void onResponse(String response) {
//                        Log.d("response!!", "");
                        try {

                            JSONObject object = new JSONObject(response);
                            JSONArray aData = new JSONArray(object.optString("patients"));

                            ArrayList<Patient> patients = new ArrayList<Patient>();
                            for(int i = 0; i < aData.length(); i++) {
                                JSONObject userObject = (JSONObject) aData.get(i);
                                Patient patient = new Patient(userObject);

                                patients.add(patient);
                            }
                            receivable.receiveData(patients, "getAPatient");

                        } catch (Exception e) {
                            e.printStackTrace();
                        }
                    }
                }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                ErrorMessage errorObj = new ErrorMessage();
                if (error instanceof TimeoutError || error instanceof NoConnectionError) {
                    Log.e("TimeoutError", error.toString());
                    errorObj.setErrorCode(0);
                    errorObj.setErrorMes("TimeoutError" + ":" + " Please check your internet and try it again.");
                    receivable.receiveData(errorObj, "error");
                } else if (error instanceof AuthFailureError) {
                    errorObj.setErrorCode(0);
                    errorObj.setErrorMes("Authentication failure" + ":" + " Please try agian.");
                    Log.e("AuthFailureError", error.toString());
                } else if (error instanceof ServerError) {
                    errorObj.setErrorCode(0);
                    errorObj.setErrorMes("ServerError" + ":" + " Sorry, there are some issues on the server, please try again later.");
                    Log.e("ServerError", error.toString());
                } else if (error instanceof NetworkError) {
                    errorObj.setErrorCode(0);
                    errorObj.setErrorMes("NetworkError" + ":" + " Please check your internet connection and try again later.");
                    Log.e("NetworkError", error.toString());
                } else if (error instanceof ParseError) {
                    errorObj.setErrorCode(0);
                    errorObj.setErrorMes("ParseError" + ":" + "Parse error!");
                    Log.e("ParseError", error.toString());
                }else {
                    errorObj.setErrorCode(0);
                    errorObj.setErrorMes("error" + ": " + error.toString());
                    Log.e("error", error.toString());
                }
//                String json = null;
//                NetworkResponse response = error.networkResponse;
//                if(response != null && response.data != null) {
//                    json = new String(response.data);
//                    json = trimMessage(json, "error");
//                    if (json != null) Log.e("json", json);
//                    errorObj.setErrorCode(response.statusCode);
//                    errorObj.setErrorMes(json);
//                    receivable.receiveData(errorObj, "error");
//
//                }else{
//                    String errorMessage=error.getClass().getSimpleName();
//                    if(!TextUtils.isEmpty(errorMessage)){
//                        Log.e("other error", errorMessage);
//                        errorObj.setErrorMes(json);
//                        receivable.receiveData(errorObj, "error");
//                    }
//                }
            }
        }){

            @Override
            public Map<String, String> getHeaders()throws AuthFailureError{
                Map<String, String> headers = new HashMap<String, String>();
                try {
                    String auth = Params.ACCESS_TOKEN;
                    headers.put("x-auth-token", auth);

                }catch (Exception e){
                    Log.d("AuthFailureError", e.toString());
                }
                return headers;
            }};

        int socketTimeout = 30000;
        RetryPolicy policy = new DefaultRetryPolicy(socketTimeout, DefaultRetryPolicy.DEFAULT_MAX_RETRIES, DefaultRetryPolicy.DEFAULT_BACKOFF_MULT);
        req.setRetryPolicy(policy);
        mRequestQueue.add(req);
    }

    public void getAllNpcParticipants(final APIReceivable receivable){
        StringRequest req = new StringRequest(Request.Method.GET,
                Params.ALL_NPC_PARTICIPANTS,
                new Response.Listener<String>() {
                    @Override
                    public void onResponse(String response) {
//                        Log.d("response!!", "");
                        try {

                            JSONObject object = new JSONObject(response);
                            JSONArray aData = new JSONArray(object.optString("npc_participants"));

                            ArrayList<NpcParticipant> users = new ArrayList<NpcParticipant>();
                            for(int i = 0; i < aData.length(); i++) {
                                JSONObject userObject = (JSONObject) aData.get(i);
                                NpcParticipant user = new NpcParticipant(userObject);
//                                Log.d("Response ", i + " npc_participants " + user.getId() + " " );
                                users.add(user);
                            }
                            receivable.receiveData(users, "getAllNpcParticipants");

                        } catch (Exception e) {
                            e.printStackTrace();
                        }
                    }
                }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                Log.d("Volley", error.toString());
            }
        }){

            @Override
            public Map<String, String> getHeaders()throws AuthFailureError{
                Map<String, String> headers = new HashMap<String, String>();
                try {
                    String auth = Params.ACCESS_TOKEN;
                    headers.put("x-auth-token", auth);

                }catch (Exception e){
                    Log.d("AuthFailureError", e.toString());
                }
                return headers;
            }};

        int socketTimeout = 30000;
        RetryPolicy policy = new DefaultRetryPolicy(socketTimeout, DefaultRetryPolicy.DEFAULT_MAX_RETRIES, DefaultRetryPolicy.DEFAULT_BACKOFF_MULT);
        req.setRetryPolicy(policy);
        mRequestQueue.add(req);

    }

    public void getANpcParticipant(int id, final APIReceivable receivable){
        final String urlString = String.format(Params.A_NPC_PARTICIPANT,
                id);
        StringRequest req = new StringRequest(Request.Method.GET,
                urlString,
                new Response.Listener<String>() {
                    @Override
                    public void onResponse(String response) {
//                        Log.d("response!!", urlString);
                        try {
                            ArrayList<NpcParticipant> users = new ArrayList<NpcParticipant>();
                            JSONObject object = new JSONObject(response);
                            NpcParticipant user = new NpcParticipant(object);
                            users.add(user);
//                            Log.d("Response ", " npc_participants " + user.getId() + " ");
                            receivable.receiveData(users, "getANpcParticipants");
                        } catch (Exception e) {
                            e.printStackTrace();
                        }
                    }
                }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                ErrorMessage errorObj = new ErrorMessage();
                if (error instanceof TimeoutError || error instanceof NoConnectionError) {
                    Log.e("TimeoutError", error.toString());
                    errorObj.setErrorCode(0);
                    errorObj.setErrorMes("TimeoutError" + ":" + " Please check your internet and try it again.");
                    receivable.receiveData(errorObj, "error");
                } else if (error instanceof AuthFailureError) {
                    errorObj.setErrorCode(0);
                    errorObj.setErrorMes("Authentication failure" + ":" + " Please try agian.");
                    Log.e("AuthFailureError", error.toString());
                } else if (error instanceof ServerError) {
                    errorObj.setErrorCode(0);
                    errorObj.setErrorMes("ServerError" + ":" + " Sorry, there are some issues on the server, please try again later.");
                    Log.e("ServerError", error.toString());
                } else if (error instanceof NetworkError) {
                    errorObj.setErrorCode(0);
                    errorObj.setErrorMes("NetworkError" + ":" + " Please check your internet connection and try again later.");
                    Log.e("NetworkError", error.toString());
                } else if (error instanceof ParseError) {
                    errorObj.setErrorCode(0);
                    errorObj.setErrorMes("ParseError" + ":" + "Parse error!");
                    Log.e("ParseError", error.toString());
                }else {
                    errorObj.setErrorCode(0);
                    errorObj.setErrorMes("error" + ": " + error.toString());
                    Log.e("error", error.toString());
                }
//                String json = null;
//                NetworkResponse response = error.networkResponse;
//                if(response != null && response.data != null) {
//                    json = new String(response.data);
//                    json = trimMessage(json, "error");
//                    if (json != null) Log.e("json", json);
//                    errorObj.setErrorCode(response.statusCode);
//                    errorObj.setErrorMes(json);
//                    receivable.receiveData(errorObj, "error");
//
//                }else{
//                    String errorMessage=error.getClass().getSimpleName();
//                    if(!TextUtils.isEmpty(errorMessage)){
//                        Log.e("other error", errorMessage);
//                        errorObj.setErrorMes(json);
//                        receivable.receiveData(errorObj, "error");
//                    }
//                }
            }
        }){

            @Override
            public Map<String, String> getHeaders()throws AuthFailureError{
                Map<String, String> headers = new HashMap<String, String>();
                try {
                    String auth = Params.ACCESS_TOKEN;
                    headers.put("x-auth-token", auth);

                }catch (Exception e){
                    Log.d("AuthFailureError", e.toString());
                }
                return headers;
            }};

        int socketTimeout = 30000;
        RetryPolicy policy = new DefaultRetryPolicy(socketTimeout, DefaultRetryPolicy.DEFAULT_MAX_RETRIES, DefaultRetryPolicy.DEFAULT_BACKOFF_MULT);
        req.setRetryPolicy(policy);
        mRequestQueue.add(req);

    }

    public void getAApplicantNpbParticipant(){

    }

    public void getAllApplicantNpbParticipants(){

    }

    public void getANpbParticipant(int id, final APIReceivable receivable){
        final String urlString = String.format(Params.A_NPB_PARTICIPANT,
                id);
        StringRequest req = new StringRequest(Request.Method.GET,
                urlString,
                new Response.Listener<String>() {
                    @Override
                    public void onResponse(String response) {
//                        Log.d("response!!", urlString);
                        try {
                            ArrayList<NpbParticipant> users = new ArrayList<NpbParticipant>();
                            JSONObject object = new JSONObject(response);
                            NpbParticipant user = new NpbParticipant(object);
                            users.add(user);
//                            Log.d("Response ", " npb_participants " + user.getId() + " ");
                            receivable.receiveData(users, "getANpbParticipant");
                        } catch (Exception e) {
                            e.printStackTrace();
                        }
                    }
                }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                ErrorMessage errorObj = new ErrorMessage();
                if (error instanceof TimeoutError || error instanceof NoConnectionError) {
                    Log.e("TimeoutError", error.toString());
                    errorObj.setErrorCode(0);
                    errorObj.setErrorMes("TimeoutError" + ":" + " Please check your internet and try it again.");
                    receivable.receiveData(errorObj, "error");
                } else if (error instanceof AuthFailureError) {
                    errorObj.setErrorCode(0);
                    errorObj.setErrorMes("Authentication failure" + ":" + " Please try agian.");
                    Log.e("AuthFailureError", error.toString());
                } else if (error instanceof ServerError) {
                    errorObj.setErrorCode(0);
                    errorObj.setErrorMes("ServerError" + ":" + " Sorry, there are some issues on the server, please try again later.");
                    Log.e("ServerError", error.toString());
                } else if (error instanceof NetworkError) {
                    errorObj.setErrorCode(0);
                    errorObj.setErrorMes("NetworkError" + ":" + " Please check your internet connection and try again later.");
                    Log.e("NetworkError", error.toString());
                } else if (error instanceof ParseError) {
                    errorObj.setErrorCode(0);
                    errorObj.setErrorMes("ParseError" + ":" + "Parse error!");
                    Log.e("ParseError", error.toString());
                }else {
                    errorObj.setErrorCode(0);
                    errorObj.setErrorMes("error" + ": " + error.toString());
                    Log.e("error", error.toString());
                }
//                String json = null;
//                NetworkResponse response = error.networkResponse;
//                if(response != null && response.data != null) {
//                    json = new String(response.data);
//                    json = trimMessage(json, "error");
//                    if (json != null) Log.e("json", json);
//                    errorObj.setErrorCode(response.statusCode);
//                    errorObj.setErrorMes(json);
//                    receivable.receiveData(errorObj, "error");
//
//                }else{
//                    String errorMessage=error.getClass().getSimpleName();
//                    if(!TextUtils.isEmpty(errorMessage)){
//                        Log.e("other error", errorMessage);
//                        errorObj.setErrorMes(json);
//                        receivable.receiveData(errorObj, "error");
//                    }
//                }
            }
        }){

            @Override
            public Map<String, String> getHeaders()throws AuthFailureError{
                Map<String, String> headers = new HashMap<String, String>();
                try {
                    String auth = Params.ACCESS_TOKEN;
                    headers.put("x-auth-token", auth);

                }catch (Exception e){
                    Log.d("AuthFailureError", e.toString());
                }
                return headers;
            }};

        int socketTimeout = 30000;
        RetryPolicy policy = new DefaultRetryPolicy(socketTimeout, DefaultRetryPolicy.DEFAULT_MAX_RETRIES, DefaultRetryPolicy.DEFAULT_BACKOFF_MULT);
        req.setRetryPolicy(policy);
        mRequestQueue.add(req);
    }

    public void getALLNpbParticipants(final APIReceivable receivable){

        StringRequest req = new StringRequest(Request.Method.GET,
                Params.ALL_NPB_PARTICIPANTS,
                new Response.Listener<String>() {
                    @Override
                    public void onResponse(String response) {
//                        Log.d("response!!", "");
                        try {

                            JSONObject object = new JSONObject(response);
                            JSONArray aData = new JSONArray(object.optString("npb_participants"));

                            ArrayList<NpbParticipant> users = new ArrayList<NpbParticipant>();
                            for(int i = 0; i < aData.length(); i++) {
                                JSONObject userObject = (JSONObject) aData.get(i);
                                NpbParticipant user = new NpbParticipant(userObject);
//                                Log.d("Response ", i + " npb_participants " + user.getId() + " ");
                                users.add(user);
                            }
                            receivable.receiveData(users, "getALLNpbParticipants");

                        } catch (Exception e) {
                            e.printStackTrace();
                        }
                    }
                }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                ErrorMessage errorObj = new ErrorMessage();
                if (error instanceof TimeoutError || error instanceof NoConnectionError) {
                    Log.e("TimeoutError", error.toString());
                    errorObj.setErrorCode(0);
                    errorObj.setErrorMes("TimeoutError" + ":" + error.toString());
                    receivable.receiveData(errorObj, "error");
                } else if (error instanceof AuthFailureError) {
                    errorObj.setErrorCode(0);
                    errorObj.setErrorMes("AuthFailureError" + ":" + error.toString());
                    Log.e("AuthFailureError", error.toString());
                } else if (error instanceof ServerError) {
                    errorObj.setErrorCode(0);
                    errorObj.setErrorMes("ServerError" + ":" + error.toString());
                    Log.e("ServerError", error.toString());
                } else if (error instanceof NetworkError) {
                    errorObj.setErrorCode(0);
                    errorObj.setErrorMes("NetworkError" + ":" + error.toString());
                    Log.e("NetworkError", error.toString());
                } else if (error instanceof ParseError) {
                    errorObj.setErrorCode(0);
                    errorObj.setErrorMes("ParseError" + ":" + error.toString());
                    Log.e("ParseError", error.toString());
                }else {
                    errorObj.setErrorCode(0);
                    errorObj.setErrorMes("error" + ":" + error.toString());
                    Log.e("error", error.toString());
                }
//                String json = null;
//                NetworkResponse response = error.networkResponse;
//                if(response != null && response.data != null) {
//                    json = new String(response.data);
//                    json = trimMessage(json, "error");
//                    if (json != null) Log.e("json", json);
//                    errorObj.setErrorCode(response.statusCode);
//                    errorObj.setErrorMes(json);
//                    receivable.receiveData(errorObj, "error");
//
//                }else{
//                    String errorMessage=error.getClass().getSimpleName();
//                    if(!TextUtils.isEmpty(errorMessage)){
//                        Log.e("other error", errorMessage);
//                        errorObj.setErrorMes(json);
//                        receivable.receiveData(errorObj, "error");
//                    }
//                }
            }
        }){

            @Override
            public Map<String, String> getHeaders()throws AuthFailureError{
                Map<String, String> headers = new HashMap<String, String>();
                try {
                    String auth = Params.ACCESS_TOKEN;
                    headers.put("x-auth-token", auth);

                }catch (Exception e){
                    Log.d("AuthFailureError", e.toString());
                }
                return headers;
            }};

        int socketTimeout = 30000;
        RetryPolicy policy = new DefaultRetryPolicy(socketTimeout, DefaultRetryPolicy.DEFAULT_MAX_RETRIES, DefaultRetryPolicy.DEFAULT_BACKOFF_MULT);
        req.setRetryPolicy(policy);
        mRequestQueue.add(req);

    }

    public void getANpaApplicantParticipant(){

    }

    public void getALLNpaApplicantParticipants(){

    }

    public void getAllNpcApplicantParticipants(){

    }

    public void getANpcApplicantParticipant(){

    }

    public void getANpaFollowup(){

    }

    public void getAllNpaFollowups(){

    }

    public void getAllNpbFollowups(final APIReceivable receivable, String followable_type, int followable_id){
        final String urlString = String.format(Params.GET_ALL_NPB_FOLLOWUPS,
                followable_type, followable_id);
        Log.d("urrrrrllllll", urlString);
        StringRequest req = new StringRequest(Request.Method.GET,
                urlString,
                new Response.Listener<String>() {
                    @Override
                    public void onResponse(String response) {
//                        Log.d("response!!", urlString);
                        try {

                            JSONObject object = new JSONObject(response);
                            JSONArray aData = new JSONArray(object.optString("npb_followups"));

                            ArrayList<NpbFollowUp> followUps = new ArrayList<NpbFollowUp>();
                            for(int i = 0; i < aData.length(); i++) {
                                JSONObject followupObj = (JSONObject) aData.get(i);
                                NpbFollowUp followUp = new NpbFollowUp(followupObj);
//                                Log.d("Response ", i + " npb_participants " + followUp.getFollowable_id() + " " );
                                followUps.add(followUp);
                            }
                            receivable.receiveData(followUps, "getAllNpbFollowups");
                        } catch (Exception e) {
                            e.printStackTrace();
                        }
                    }
                }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                ErrorMessage errorObj = new ErrorMessage();
                if (error instanceof TimeoutError || error instanceof NoConnectionError) {
                    Log.e("TimeoutError", error.toString());
                    errorObj.setErrorCode(0);
                    errorObj.setErrorMes("TimeoutError" + ":" + " Please check your internet and try it again.");
                    receivable.receiveData(errorObj, "error");
                } else if (error instanceof AuthFailureError) {
                    errorObj.setErrorCode(0);
                    errorObj.setErrorMes("Authentication failure" + ":" + " Please try agian.");
                    Log.e("AuthFailureError", error.toString());
                } else if (error instanceof ServerError) {
                    errorObj.setErrorCode(0);
                    errorObj.setErrorMes("ServerError" + ":" + " Sorry, there are some issues on the server, please try again later.");
                    Log.e("ServerError", error.toString());
                } else if (error instanceof NetworkError) {
                    errorObj.setErrorCode(0);
                    errorObj.setErrorMes("NetworkError" + ":" + " Please check your internet connection and try again later.");
                    Log.e("NetworkError", error.toString());
                } else if (error instanceof ParseError) {
                    errorObj.setErrorCode(0);
                    errorObj.setErrorMes("ParseError" + ":" + "Parse error!");
                    Log.e("ParseError", error.toString());
                }else {
                    errorObj.setErrorCode(0);
                    errorObj.setErrorMes("error" + ": " + error.toString());
                    Log.e("error", error.toString());
                }
//                String json = null;
//                NetworkResponse response = error.networkResponse;
//                if(response != null && response.data != null) {
//                    json = new String(response.data);
//                    json = trimMessage(json, "error");
//                    if (json != null) Log.e("json", json);
//                    errorObj.setErrorCode(response.statusCode);
//                    errorObj.setErrorMes(json);
//                    receivable.receiveData(errorObj, "error");
//
//                }else{
//                    String errorMessage=error.getClass().getSimpleName();
//                    if(!TextUtils.isEmpty(errorMessage)){
//                        Log.e("other error", errorMessage);
//                        errorObj.setErrorMes(json);
//                        receivable.receiveData(errorObj, "error");
//                    }
//                }
            }
        }){

            @Override
            public Map<String, String> getHeaders()throws AuthFailureError{
                Map<String, String> headers = new HashMap<String, String>();
                try {
                    String auth = Params.ACCESS_TOKEN;
                    headers.put("x-auth-token", auth);

                }catch (Exception e){
                    Log.d("AuthFailureError", e.toString());
                }
                return headers;
            }};

        int socketTimeout = 30000;
        RetryPolicy policy = new DefaultRetryPolicy(socketTimeout, DefaultRetryPolicy.DEFAULT_MAX_RETRIES, DefaultRetryPolicy.DEFAULT_BACKOFF_MULT);
        req.setRetryPolicy(policy);
        mRequestQueue.add(req);
    }

    public void getANpbFollowup(final APIReceivable receivable, String followable_type, int followable_id, int id){
        final String urlString = String.format(Params.GET_A_NPB_FOLLOWUP,
                followable_type, followable_id, id);
        Log.d("GET_A_NPB_FOLLOWUP", urlString);
        StringRequest req = new StringRequest(Request.Method.GET,
                urlString,
                new Response.Listener<String>() {
                    @Override
                    public void onResponse(String response) {
//                        Log.d("response!!", urlString);
                        try {

                            JSONObject object = new JSONObject(response);
                            JSONArray aData = new JSONArray(object.optString("npb_followups"));

                            ArrayList<NpbFollowUp> followUps = new ArrayList<NpbFollowUp>();
                            for(int i = 0; i < aData.length(); i++) {
                                JSONObject followupObj = (JSONObject) aData.get(i);
                                NpbFollowUp followUp = new NpbFollowUp(followupObj);
//                                Log.d("Response ", i + " npb_participants " + followUp.getFollowable_id() + " " );
                                followUps.add(followUp);
                            }
                            receivable.receiveData(followUps, "getANpbFollowup");
                        } catch (Exception e) {
                            e.printStackTrace();
                        }
                    }
                }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                ErrorMessage errorObj = new ErrorMessage();
                if (error instanceof TimeoutError || error instanceof NoConnectionError) {
                    Log.e("TimeoutError", error.toString());
                    errorObj.setErrorCode(0);
                    errorObj.setErrorMes("TimeoutError" + ":" + " Please check your internet and try it again.");
                    receivable.receiveData(errorObj, "error");
                } else if (error instanceof AuthFailureError) {
                    errorObj.setErrorCode(0);
                    errorObj.setErrorMes("Authentication failure" + ":" + " Please try agian.");
                    Log.e("AuthFailureError", error.toString());
                } else if (error instanceof ServerError) {
                    errorObj.setErrorCode(0);
                    errorObj.setErrorMes("ServerError" + ":" + " Sorry, there are some issues on the server, please try again later.");
                    Log.e("ServerError", error.toString());
                } else if (error instanceof NetworkError) {
                    errorObj.setErrorCode(0);
                    errorObj.setErrorMes("NetworkError" + ":" + " Please check your internet connection and try again later.");
                    Log.e("NetworkError", error.toString());
                } else if (error instanceof ParseError) {
                    errorObj.setErrorCode(0);
                    errorObj.setErrorMes("ParseError" + ":" + "Parse error!");
                    Log.e("ParseError", error.toString());
                }else {
                    errorObj.setErrorCode(0);
                    errorObj.setErrorMes("error" + ": " + error.toString());
                    Log.e("error", error.toString());
                }
//                String json = null;
//                NetworkResponse response = error.networkResponse;
//                if(response != null && response.data != null) {
//                    json = new String(response.data);
//                    json = trimMessage(json, "error");
//                    if (json != null) Log.e("json", json);
//                    errorObj.setErrorCode(response.statusCode);
//                    errorObj.setErrorMes(json);
//                    receivable.receiveData(errorObj, "error");
//
//                }else{
//                    String errorMessage=error.getClass().getSimpleName();
//                    if(!TextUtils.isEmpty(errorMessage)){
//                        Log.e("other error", errorMessage);
//                        errorObj.setErrorMes(json);
//                        receivable.receiveData(errorObj, "error");
//                    }
//                }
            }
        }){

            @Override
            public Map<String, String> getHeaders()throws AuthFailureError{
                Map<String, String> headers = new HashMap<String, String>();
                try {
                    String auth = Params.ACCESS_TOKEN;
                    headers.put("x-auth-token", auth);

                }catch (Exception e){
                    Log.d("AuthFailureError", e.toString());
                }
                return headers;
            }};

        int socketTimeout = 30000;
        RetryPolicy policy = new DefaultRetryPolicy(socketTimeout, DefaultRetryPolicy.DEFAULT_MAX_RETRIES, DefaultRetryPolicy.DEFAULT_BACKOFF_MULT);
        req.setRetryPolicy(policy);
        mRequestQueue.add(req);

    }

    public void getAllNpcFollowups(final APIReceivable receivable, String followable_type, int followable_id){
        final String urlString = String.format(Params.GET_ALL_NPC_FOLLOWUPS,
                followable_type, followable_id);
        Log.d("urrrrrllllll", urlString);
        StringRequest req = new StringRequest(Request.Method.GET,
                urlString,
                new Response.Listener<String>() {
                    @Override
                    public void onResponse(String response) {
//                        Log.d("response!!", urlString);
                        try {

                            JSONObject object = new JSONObject(response);
                            JSONArray aData = new JSONArray(object.optString("npc_followups"));

                            ArrayList<NpcFollowUp> followUps = new ArrayList<NpcFollowUp>();
                            for(int i = 0; i < aData.length(); i++) {
                                JSONObject followupObj = (JSONObject) aData.get(i);
                                NpcFollowUp followUp = new NpcFollowUp(followupObj);
//                                Log.d("Response ", i + " npc_participants " + followUp.getFollowable_id() + " " );
                                followUps.add(followUp);
                            }
                            receivable.receiveData(followUps, "getAllNpcFollowups");
                        } catch (Exception e) {
                            e.printStackTrace();
                        }
                    }
                }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                ErrorMessage errorObj = new ErrorMessage();
                if (error instanceof TimeoutError || error instanceof NoConnectionError) {
                    Log.e("TimeoutError", error.toString());
                    errorObj.setErrorCode(0);
                    errorObj.setErrorMes("TimeoutError" + ":" + " Please check your internet and try it again.");
                } else if (error instanceof AuthFailureError) {
                    errorObj.setErrorCode(0);
                    errorObj.setErrorMes("Authentication failure" + ":" + " Please try agian.");
                    Log.e("AuthFailureError", error.toString());
                } else if (error instanceof ServerError) {
                    errorObj.setErrorCode(0);
                    errorObj.setErrorMes("ServerError" + ":" + " Sorry, there are some issues on the server, please try again later.");
                    Log.e("ServerError", error.toString());
                } else if (error instanceof NetworkError) {
                    errorObj.setErrorCode(0);
                    errorObj.setErrorMes("NetworkError" + ":" + " Please check your internet connection and try again later.");
                    Log.e("NetworkError", error.toString());
                } else if (error instanceof ParseError) {
                    errorObj.setErrorCode(0);
                    errorObj.setErrorMes("ParseError" + ":" + "Parse error!");
                    Log.e("ParseError", error.toString());
                }else {
                    errorObj.setErrorCode(0);
                    errorObj.setErrorMes("error" + ": " + error.toString());
                    Log.e("error", error.toString());
                }
                receivable.receiveData(errorObj, "error");
//                String json = null;
//                NetworkResponse response = error.networkResponse;
//                if(response != null && response.data != null) {
//                    json = new String(response.data);
//                    json = trimMessage(json, "error");
//                    if (json != null) Log.e("json", json);
//                    errorObj.setErrorCode(response.statusCode);
//                    errorObj.setErrorMes(json);
//                    receivable.receiveData(errorObj, "error");
//
//                }else{
//                    String errorMessage=error.getClass().getSimpleName();
//                    if(!TextUtils.isEmpty(errorMessage)){
//                        Log.e("other error", errorMessage);
//                        errorObj.setErrorMes(json);
//                        receivable.receiveData(errorObj, "error");
//                    }
//                }
            }
        }){

            @Override
            public Map<String, String> getHeaders()throws AuthFailureError{
                Map<String, String> headers = new HashMap<String, String>();
                try {
                    String auth = Params.ACCESS_TOKEN;
                    headers.put("x-auth-token", auth);

                }catch (Exception e){
                    Log.d("AuthFailureError", e.toString());
                }
                return headers;
            }};

        int socketTimeout = 30000;
        RetryPolicy policy = new DefaultRetryPolicy(socketTimeout, DefaultRetryPolicy.DEFAULT_MAX_RETRIES, DefaultRetryPolicy.DEFAULT_BACKOFF_MULT);
        req.setRetryPolicy(policy);
        mRequestQueue.add(req);

    }

    public void getANpcFollowup(final APIReceivable receivable, String followable_type, int followable_id, int id){
        final String urlString = String.format(Params.GET_A_NPC_FOLLOWUP,
                followable_type, followable_id, id);
        Log.d("fffffurl...........", urlString);
        StringRequest req = new StringRequest(Request.Method.GET,
                urlString,
                new Response.Listener<String>() {
                    @Override
                    public void onResponse(String response) {
//                        Log.d("response!!", urlString);
                        try {

                            JSONObject object = new JSONObject(response);
                            JSONArray aData = new JSONArray(object.optString("npc_followups"));

                            ArrayList<NpcFollowUp> followUps = new ArrayList<NpcFollowUp>();
                            for(int i = 0; i < aData.length(); i++) {
                                JSONObject followupObj = (JSONObject) aData.get(i);
                                NpcFollowUp followUp = new NpcFollowUp(followupObj);
                               // Log.d("Response ", i + " npc_participants " + followUp.getFollowable_id() + " " + followUp.getFollowable_type());
                                followUps.add(followUp);
                            }
                            receivable.receiveData(followUps, "getANpcFollowup");
                        } catch (Exception e) {
                            e.printStackTrace();
                        }
                    }
                }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                ErrorMessage errorObj = new ErrorMessage();
                if (error instanceof TimeoutError || error instanceof NoConnectionError) {
                    Log.e("TimeoutError", error.toString());
                    errorObj.setErrorCode(0);
                    errorObj.setErrorMes("TimeoutError" + ":" + " Please check your internet and try it again.");
                } else if (error instanceof AuthFailureError) {
                    errorObj.setErrorCode(0);
                    errorObj.setErrorMes("Authentication failure" + ":" + " Please try agian.");
                    Log.e("AuthFailureError", error.toString());
                } else if (error instanceof ServerError) {
                    errorObj.setErrorCode(0);
                    errorObj.setErrorMes("ServerError" + ":" + " Sorry, there are some issues on the server, please try again later.");
                    Log.e("ServerError", error.toString());
                } else if (error instanceof NetworkError) {
                    errorObj.setErrorCode(0);
                    errorObj.setErrorMes("NetworkError" + ":" + " Please check your internet connection and try again later.");
                    Log.e("NetworkError", error.toString());
                } else if (error instanceof ParseError) {
                    errorObj.setErrorCode(0);
                    errorObj.setErrorMes("ParseError" + ":" + "Parse error!");
                    Log.e("ParseError", error.toString());
                }else {
                    errorObj.setErrorCode(0);
                    errorObj.setErrorMes("error" + ": " + error.toString());
                    Log.e("error", error.toString());
                }
                receivable.receiveData(errorObj, "error");
//                String json = null;
//                NetworkResponse response = error.networkResponse;
//                if(response != null && response.data != null) {
//                    json = new String(response.data);
//                    json = trimMessage(json, "error");
//                    if (json != null) Log.e("json", json);
//                    errorObj.setErrorCode(response.statusCode);
//                    errorObj.setErrorMes(json);
//                    receivable.receiveData(errorObj, "error");
//
//                }else{
//                    String errorMessage=error.getClass().getSimpleName();
//                    if(!TextUtils.isEmpty(errorMessage)){
//                        Log.e("other error", errorMessage);
//                        errorObj.setErrorMes(json);
//                        receivable.receiveData(errorObj, "error");
//                    }
//                }
            }
        }){

            @Override
            public Map<String, String> getHeaders()throws AuthFailureError{
                Map<String, String> headers = new HashMap<String, String>();
                try {
                    String auth = Params.ACCESS_TOKEN;
                    headers.put("x-auth-token", auth);

                }catch (Exception e){
                    Log.d("AuthFailureError", e.toString());
                }
                return headers;
            }};

        int socketTimeout = 30000;
        RetryPolicy policy = new DefaultRetryPolicy(socketTimeout, DefaultRetryPolicy.DEFAULT_MAX_RETRIES, DefaultRetryPolicy.DEFAULT_BACKOFF_MULT);
        req.setRetryPolicy(policy);
        mRequestQueue.add(req);

    }

    public void deleteANpaFollowup(){

    }

    public void deleteANpbFollowup(final APIReceivable receivable, String followable_type, int followable_id, int id){
        final String urlString = String.format(Params.DELETE_A_NPB_FOLLOWUP,
                followable_type, followable_id, id);
        Log.d("DELETE_A_NPB_FOLLOWUP", urlString);
        StringRequest req = new StringRequest(Request.Method.DELETE,
                urlString,
                new Response.Listener<String>() {
                    @Override
                    public void onResponse(String response) {
//                        Log.d("response!!", urlString);
                        try {

                            JSONObject object = new JSONObject(response);
//                            Log.d("response delete ", object.toString());

                            NpbFollowUp followUp = new NpbFollowUp(object);
//                                Log.d("Response ", " npb_participants " + followUp.getFollowable_id() + " ");

                           receivable.receiveData(followUp, "deleteANpbFollowup");
                        } catch (Exception e) {
                            e.printStackTrace();
                        }
                    }
                }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                Log.e("here","errr");
                ErrorMessage errorObj = new ErrorMessage();
                if (error instanceof TimeoutError || error instanceof NoConnectionError) {
                    Log.e("TimeoutError", error.toString());
                    errorObj.setErrorCode(0);
                    errorObj.setErrorMes("TimeoutError" + ":" + " Please check your internet and try it again.");
                    receivable.receiveData(errorObj, "error");
                } else if (error instanceof AuthFailureError) {
                    errorObj.setErrorCode(0);
                    errorObj.setErrorMes("Authentication failure" + ":" + " Please try agian.");
                    Log.e("AuthFailureError", error.toString());
                } else if (error instanceof ServerError) {
                    errorObj.setErrorCode(0);
                    errorObj.setErrorMes("ServerError" + ":" + " Sorry, there are some issues on the server, please try again later.");
                    Log.e("ServerError", error.toString());
                } else if (error instanceof NetworkError) {
                    errorObj.setErrorCode(0);
                    errorObj.setErrorMes("NetworkError" + ":" + " Please check your internet connection and try again later.");
                    Log.e("NetworkError", error.toString());
                } else if (error instanceof ParseError) {
                    errorObj.setErrorCode(0);
                    errorObj.setErrorMes("ParseError" + ":" + "Parse error!");
                    Log.e("ParseError", error.toString());
                }else {
                    errorObj.setErrorCode(0);
                    errorObj.setErrorMes("error" + ": " + error.toString());
                    Log.e("error", error.toString());
                }
                receivable.receiveData(errorObj, "error");
//                String json = null;
//                NetworkResponse response = error.networkResponse;
//                if(response != null && response.data != null) {
//                    json = new String(response.data);
//                    json = trimMessage(json, "error");
//                    if (json != null) Log.e("json", json);
//                    errorObj.setErrorCode(response.statusCode);
//                    errorObj.setErrorMes(json);
//                    receivable.receiveData(errorObj, "error");
//
//                }else{
//                    String errorMessage=error.getClass().getSimpleName();
//                    if(!TextUtils.isEmpty(errorMessage)){
//                        Log.e("other error", errorMessage);
//                        errorObj.setErrorMes(json);
//                        receivable.receiveData(errorObj, "error");
//                    }
//                }
            }
        }){

            @Override
            public Map<String, String> getHeaders()throws AuthFailureError{
                Map<String, String> headers = new HashMap<String, String>();
                try {
                    String auth = Params.ACCESS_TOKEN;
                    headers.put("x-auth-token", auth);

                }catch (Exception e){
                    Log.d("AuthFailureError", e.toString());
                }
                return headers;
            }};

        int socketTimeout = 30000;
        RetryPolicy policy = new DefaultRetryPolicy(socketTimeout, DefaultRetryPolicy.DEFAULT_MAX_RETRIES, DefaultRetryPolicy.DEFAULT_BACKOFF_MULT);
        req.setRetryPolicy(policy);
        mRequestQueue.add(req);


    }

    public void deleteANpcFollowup(final APIReceivable receivable, String followable_type, int followable_id, int id){
        final String urlString = String.format(Params.DELETE_A_NPC_FOLLOWUP,
                followable_type, followable_id, id);
        Log.d("DELETE_A_NPC_FOLLOWUP", urlString);
        StringRequest req = new StringRequest(Request.Method.DELETE,
                urlString,
                new Response.Listener<String>() {
                    @Override
                    public void onResponse(String response) {
//                        Log.d("response!!", urlString);
                        try {

                            JSONObject object = new JSONObject(response);

                            NpcFollowUp followUp = new NpcFollowUp(object);
//                            Log.d("Response delete:", " npc_participants " + followUp.getFollowable_id() + " ");

                            receivable.receiveData(followUp, "deleteANpcFollowup");
                        } catch (Exception e) {
                            e.printStackTrace();
                        }
                    }
                }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                ErrorMessage errorObj = new ErrorMessage();
                if (error instanceof TimeoutError || error instanceof NoConnectionError) {
                    Log.e("TimeoutError", error.toString());
                    errorObj.setErrorCode(0);
                    errorObj.setErrorMes("TimeoutError" + ":" + " Please check your internet and try it again.");
                } else if (error instanceof AuthFailureError) {
                    errorObj.setErrorCode(0);
                    errorObj.setErrorMes("Authentication failure" + ":" + " Please try agian.");
                    Log.e("AuthFailureError", error.toString());
                } else if (error instanceof ServerError) {
                    errorObj.setErrorCode(0);
                    errorObj.setErrorMes("ServerError" + ":" + " Sorry, there are some issues on the server, please try again later.");
                    Log.e("ServerError", error.toString());
                } else if (error instanceof NetworkError) {
                    errorObj.setErrorCode(0);
                    errorObj.setErrorMes("NetworkError" + ":" + " Please check your internet connection and try again later.");
                    Log.e("NetworkError", error.toString());
                } else if (error instanceof ParseError) {
                    errorObj.setErrorCode(0);
                    errorObj.setErrorMes("ParseError" + ":" + "Parse error!");
                    Log.e("ParseError", error.toString());
                }else {
                    errorObj.setErrorCode(0);
                    errorObj.setErrorMes("error" + ": " + error.toString());
                    Log.e("error", error.toString());
                }
                receivable.receiveData(errorObj, "error");
            }
        }){

            @Override
            public Map<String, String> getHeaders()throws AuthFailureError{
                Map<String, String> headers = new HashMap<String, String>();
                try {
                    String auth = Params.ACCESS_TOKEN;
                    headers.put("x-auth-token", auth);

                }catch (Exception e){
                    Log.d("AuthFailureError", e.toString());
                }
                return headers;
            }};

        int socketTimeout = 30000;
        RetryPolicy policy = new DefaultRetryPolicy(socketTimeout, DefaultRetryPolicy.DEFAULT_MAX_RETRIES, DefaultRetryPolicy.DEFAULT_BACKOFF_MULT);
        req.setRetryPolicy(policy);
        mRequestQueue.add(req);

    }

    public void createANpaFollowup(){

    }

    public void createANpbFollowup(final APIReceivable receivable, String url){

        Log.d("POST_A_NPB_FOLLOWUP", url);
        StringRequest req = new StringRequest(Request.Method.POST,
                url,
                new Response.Listener<String>() {
                    @Override
                    public void onResponse(String response) {
                        try {
//                            Log.d("responesssssss", response);
                            JSONObject object = new JSONObject(response);
                            NpbFollowUp followUp = new NpbFollowUp(object);
//                            Log.d("Response create:", " npb_participants " + followUp.getFollowable_id() + " " + followUp.getId());

                            receivable.receiveData(followUp, "createANpbFollowup");
                        } catch (Exception e) {
                            e.printStackTrace();
                        }
                    }
                }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                ErrorMessage errorObj = new ErrorMessage();
                if (error instanceof TimeoutError || error instanceof NoConnectionError) {
                    Log.e("TimeoutError", error.toString());
                    errorObj.setErrorCode(0);
                    errorObj.setErrorMes("TimeoutError" + ":" + " Please check your internet and try it again.");
                    receivable.receiveData(errorObj, "error");
                } else if (error instanceof AuthFailureError) {
                    errorObj.setErrorCode(0);
                    errorObj.setErrorMes("Authentication failure" + ":" + " Please try agian.");
                    Log.e("AuthFailureError", error.toString());
                } else if (error instanceof ServerError) {
                    errorObj.setErrorCode(0);
                    errorObj.setErrorMes("ServerError" + ":" + " Sorry, there are some issues on the server, please try again later.");
                    Log.e("ServerError", error.toString());
                } else if (error instanceof NetworkError) {
                    errorObj.setErrorCode(0);
                    errorObj.setErrorMes("NetworkError" + ":" + " Please check your internet connection and try again later.");
                    Log.e("NetworkError", error.toString());
                } else if (error instanceof ParseError) {
                    errorObj.setErrorCode(0);
                    errorObj.setErrorMes("ParseError" + ":" + "Parse error!");
                    Log.e("ParseError", error.toString());
                }else {
                    errorObj.setErrorCode(0);
                    errorObj.setErrorMes("error" + ": " + error.toString());
                    Log.e("error", error.toString());
                }
            }
        }){

            @Override
            public Map<String, String> getHeaders()throws AuthFailureError{
                Map<String, String> headers = new HashMap<String, String>();
                try {
                    String auth = Params.ACCESS_TOKEN;
                    headers.put("x-auth-token", auth);

                }catch (Exception e){
                    Log.d("AuthFailureError", e.toString());
                }
                return headers;
            }};

        int socketTimeout = 30000;
        RetryPolicy policy = new DefaultRetryPolicy(socketTimeout, DefaultRetryPolicy.DEFAULT_MAX_RETRIES, DefaultRetryPolicy.DEFAULT_BACKOFF_MULT);
        req.setRetryPolicy(policy);
        mRequestQueue.add(req);

    }

    private String trimMessage(String json, String key) {
            String trimmedString = null;

            try{
                JSONObject obj = new JSONObject(json);
                trimmedString = obj.getString(key);
            } catch(JSONException e){
                e.printStackTrace();
                return null;
            }

            return trimmedString;
        }

    public void createANpcFollowup(final APIReceivable receivable, String url){
        Log.d("POST_A_NPC_FOLLOWUP", url);
        StringRequest req = new StringRequest(Request.Method.POST,
                url,
                new Response.Listener<String>() {
                    @Override
                    public void onResponse(String response) {
                        try {
//                            Log.d("responesssssss", response);
                            JSONObject object = new JSONObject(response);
                            NpcFollowUp followUp = new NpcFollowUp(object);
//                            Log.d("Response create:", " npc_participants " + followUp.getFollowable_id() + " " + followUp.getId());

                            receivable.receiveData(followUp, "createANpcFollowup");
                        } catch (Exception e) {
                            e.printStackTrace();
                        }
                    }
                }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                ErrorMessage errorObj = new ErrorMessage();
                if (error instanceof TimeoutError || error instanceof NoConnectionError) {
                    Log.e("TimeoutError", error.toString());
                    errorObj.setErrorCode(0);
                    errorObj.setErrorMes("TimeoutError" + ":" + " Please check your internet and try it again.");
                    receivable.receiveData(errorObj, "error");
                } else if (error instanceof AuthFailureError) {
                    errorObj.setErrorCode(0);
                    errorObj.setErrorMes("Authentication failure" + ":" + " Please try agian.");
                    Log.e("AuthFailureError", error.toString());
                } else if (error instanceof ServerError) {
                    errorObj.setErrorCode(0);
                    errorObj.setErrorMes("ServerError" + ":" + " Sorry, there are some issues on the server, please try again later.");
                    Log.e("ServerError", error.toString());
                } else if (error instanceof NetworkError) {
                    errorObj.setErrorCode(0);
                    errorObj.setErrorMes("NetworkError" + ":" + " Please check your internet connection and try again later.");
                    Log.e("NetworkError", error.toString());
                } else if (error instanceof ParseError) {
                    errorObj.setErrorCode(0);
                    errorObj.setErrorMes("ParseError" + ":" + "Parse error!");
                    Log.e("ParseError", error.toString());
                }else {
                    errorObj.setErrorCode(0);
                    errorObj.setErrorMes("error" + ": " + error.toString());
                    Log.e("error", error.toString());
                }
                String json = null;
                NetworkResponse response = error.networkResponse;
//                if(response != null && response.data != null) {
//                    json = new String(response.data);
//                    json = trimMessage(json, "error");
//                    if (json != null) Log.e("json", json);
//                    errorObj.setErrorCode(response.statusCode);
//                    errorObj.setErrorMes(json);
//                    receivable.receiveData(errorObj, "error");
//
//                }else{
//                    String errorMessage=error.getClass().getSimpleName();
//                    if(!TextUtils.isEmpty(errorMessage)){
//                        Log.e("other error", errorMessage);
//                        errorObj.setErrorMes(json);
//                        receivable.receiveData(errorObj, "error");
//                    }
//                }
            }
        }){

            @Override
            public Map<String, String> getHeaders()throws AuthFailureError{
                Map<String, String> headers = new HashMap<String, String>();
                try {
                    String auth = Params.ACCESS_TOKEN;
                    headers.put("x-auth-token", auth);

                }catch (Exception e){
                    Log.d("AuthFailureError", e.toString());
                }
                return headers;
            }};

        int socketTimeout = 30000;
        RetryPolicy policy = new DefaultRetryPolicy(socketTimeout, DefaultRetryPolicy.DEFAULT_MAX_RETRIES, DefaultRetryPolicy.DEFAULT_BACKOFF_MULT);
        req.setRetryPolicy(policy);
        mRequestQueue.add(req);
    }

    public void updateANpaFollowup(){

    }

    public void updateANpbFollowup(final APIReceivable receivable, String url){
        Log.d("PUT_A_NPB_FOLLOWUP", url);
        StringRequest req = new StringRequest(Request.Method.PUT,
                url,
                new Response.Listener<String>() {
                    @Override
                    public void onResponse(String response) {
                        try {
//                            Log.d("responesssssss", response);
//                            Log.d("Response update:", response.toString());
                            ErrorMessage responseObj = new ErrorMessage();
                            responseObj.setErrorMes(response.toString());

                            receivable.receiveData(responseObj, "updateANpbFollowup");
                        } catch (Exception e) {
                            e.printStackTrace();
                        }
                    }
                }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                ErrorMessage errorObj = new ErrorMessage();
                if (error instanceof TimeoutError || error instanceof NoConnectionError) {
                    Log.e("TimeoutError", error.toString());
                    errorObj.setErrorCode(0);
                    errorObj.setErrorMes("TimeoutError" + ":" + " Please check your internet and try it again.");
                    receivable.receiveData(errorObj, "error");
                } else if (error instanceof AuthFailureError) {
                    errorObj.setErrorCode(0);
                    errorObj.setErrorMes("Authentication failure" + ":" + " Please try agian.");
                    Log.e("AuthFailureError", error.toString());
                } else if (error instanceof ServerError) {
                    errorObj.setErrorCode(0);
                    errorObj.setErrorMes("ServerError" + ":" + " Sorry, there are some issues on the server, please try again later.");
                    Log.e("ServerError", error.toString());
                } else if (error instanceof NetworkError) {
                    errorObj.setErrorCode(0);
                    errorObj.setErrorMes("NetworkError" + ":" + " Please check your internet connection and try again later.");
                    Log.e("NetworkError", error.toString());
                } else if (error instanceof ParseError) {
                    errorObj.setErrorCode(0);
                    errorObj.setErrorMes("ParseError" + ":" + "Parse error!");
                    Log.e("ParseError", error.toString());
                }else {
                    errorObj.setErrorCode(0);
                    errorObj.setErrorMes("error" + ": " + error.toString());
                    Log.e("error", error.toString());
                }
//                if(response != null && response.data != null) {
//                    json = new String(response.data);
//                    json = trimMessage(json, "error");
//                    if (json != null) Log.e("json", json);
//                    errorObj.setErrorCode(response.statusCode);
//                    errorObj.setErrorMes(json);
//                    receivable.receiveData(errorObj, "error");
//
//                }else{
//                    String errorMessage=error.getClass().getSimpleName();
//                    if(!TextUtils.isEmpty(errorMessage)){
//                        Log.e("other error", errorMessage);
//                        errorObj.setErrorMes(json);
//                        receivable.receiveData(errorObj, "error");
//                    }
//                }
            }
        }){

            @Override
            public Map<String, String> getHeaders()throws AuthFailureError{
                Map<String, String> headers = new HashMap<String, String>();
                try {
                    String auth = Params.ACCESS_TOKEN;
                    headers.put("x-auth-token", auth);

                }catch (Exception e){
                    Log.d("AuthFailureError", e.toString());
                }
                return headers;
            }};

        int socketTimeout = 30000;
        RetryPolicy policy = new DefaultRetryPolicy(socketTimeout, DefaultRetryPolicy.DEFAULT_MAX_RETRIES, DefaultRetryPolicy.DEFAULT_BACKOFF_MULT);
        req.setRetryPolicy(policy);
        mRequestQueue.add(req);


    }

    public void updateANpcFollowup(final APIReceivable receivable, String url){
        Log.d("PUT_A_NPc_FOLLOWUP", url);
        StringRequest req = new StringRequest(Request.Method.PUT,
                url,
                new Response.Listener<String>() {
                    @Override
                    public void onResponse(String response) {
                        try {
//                            Log.d("responesssssss", response);
//                            Log.d("Response update:", response.toString());
                            ErrorMessage responseObj = new ErrorMessage();
                            responseObj.setErrorMes(response.toString());

                            receivable.receiveData(responseObj, "updateANpcFollowup");
                        } catch (Exception e) {
                            e.printStackTrace();
                        }
                    }
                }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                ErrorMessage errorObj = new ErrorMessage();
                if (error instanceof TimeoutError || error instanceof NoConnectionError) {
                    Log.e("TimeoutError", error.toString());
                    errorObj.setErrorCode(0);
                    errorObj.setErrorMes("TimeoutError" + ":" + " Please check your internet and try it again.");
                    receivable.receiveData(errorObj, "error");
                } else if (error instanceof AuthFailureError) {
                    errorObj.setErrorCode(0);
                    errorObj.setErrorMes("Authentication failure" + ":" + " Please try agian.");
                    Log.e("AuthFailureError", error.toString());
                } else if (error instanceof ServerError) {
                    errorObj.setErrorCode(0);
                    errorObj.setErrorMes("ServerError" + ":" + " Sorry, there are some issues on the server, please try again later.");
                    Log.e("ServerError", error.toString());
                } else if (error instanceof NetworkError) {
                    errorObj.setErrorCode(0);
                    errorObj.setErrorMes("NetworkError" + ":" + " Please check your internet connection and try again later.");
                    Log.e("NetworkError", error.toString());
                } else if (error instanceof ParseError) {
                    errorObj.setErrorCode(0);
                    errorObj.setErrorMes("ParseError" + ":" + "Parse error!");
                    Log.e("ParseError", error.toString());
                }else {
                    errorObj.setErrorCode(0);
                    errorObj.setErrorMes("error" + ": " + error.toString());
                    Log.e("error", error.toString());
                }
//                String json = null;
//                NetworkResponse response = error.networkResponse;
//                if(response != null && response.data != null) {
//                    json = new String(response.data);
//                    json = trimMessage(json, "error");
//                    if (json != null) Log.e("json", json);
//                    errorObj.setErrorCode(response.statusCode);
//                    errorObj.setErrorMes(json);
//                    receivable.receiveData(errorObj, "error");
//
//                }else{
//                    String errorMessage=error.getClass().getSimpleName();
//                    if(!TextUtils.isEmpty(errorMessage)){
//                        Log.e("other error", errorMessage);
//                        errorObj.setErrorMes(json);
//                        receivable.receiveData(errorObj, "error");
//                    }
//                }
            }
        }){

            @Override
            public Map<String, String> getHeaders()throws AuthFailureError{
                Map<String, String> headers = new HashMap<String, String>();
                try {
                    String auth = Params.ACCESS_TOKEN;
                    headers.put("x-auth-token", auth);

                }catch (Exception e){
                    Log.d("AuthFailureError", e.toString());
                }
                return headers;
            }};

        int socketTimeout = 30000;
        RetryPolicy policy = new DefaultRetryPolicy(socketTimeout, DefaultRetryPolicy.DEFAULT_MAX_RETRIES, DefaultRetryPolicy.DEFAULT_BACKOFF_MULT);
        req.setRetryPolicy(policy);
        mRequestQueue.add(req);

    }


    public void clearCache(){
        this.mRequestQueue.getCache().clear();
    }
}
