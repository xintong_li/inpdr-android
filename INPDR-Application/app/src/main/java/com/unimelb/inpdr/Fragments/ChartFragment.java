package com.unimelb.inpdr.Fragments;


import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.widget.SwipeRefreshLayout;
import android.support.v7.app.AlertDialog;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.TextView;
import android.widget.Toast;


import com.unimelb.inpdr.ChartActivity;
import com.unimelb.inpdr.LineChartActivity;
import com.unimelb.inpdr.LineColumnDependencyActivity;
import com.unimelb.inpdr.MainActivity;
import com.unimelb.inpdr.Models.API.APIItem;
import com.unimelb.inpdr.Models.API.APIReceivable;
import com.unimelb.inpdr.Models.API.INPDRAPI;
import com.unimelb.inpdr.Models.API.ModelFacade;
import com.unimelb.inpdr.Models.ErrorMessage;
import com.unimelb.inpdr.Models.NpbFollowUp;
import com.unimelb.inpdr.Models.NpcFollowUp;
import com.unimelb.inpdr.Models.Patient;
import com.unimelb.inpdr.R;

import java.util.ArrayList;
import java.util.List;

import me.drakeet.materialdialog.MaterialDialog;

/**
 * A simple {@link Fragment} subclass.
 */
public class ChartFragment extends Fragment implements APIReceivable{

    private SwipeRefreshLayout swipeRefreshLayout;
    private ArrayList<Patient> patients = new ArrayList<>();
    int patientId;
    String patientType;
    String uniqueId;
    private ArrayList<NpbFollowUp> npbFollowUps = new ArrayList<>();
    private ArrayList<NpcFollowUp> npcFollowUps = new ArrayList<>();
    boolean isRefreshed = false;

    final APIReceivable receivable = this;
    Fragment currentFragment;

    public ChartFragment() {
        // Required empty public constructor
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        return inflater.inflate(R.layout.fragment_chart, container, false);
    }

    @Override
    public void onStart(){
        super.onStart();
        ((MainActivity)getActivity()).setActionBarTitle("Statistical Chart");
    }

    @Override
    public void onViewCreated(View view, Bundle savedInstanceState) {
        final INPDRAPI api = new INPDRAPI(getContext());
        currentFragment = this;
        Bundle data = getArguments();

        ((MainActivity)getActivity()).setActionBarTitle("Statistical Chart");

        ModelFacade.getInstance().getAllPatients(this, api);

        swipeRefreshLayout = (SwipeRefreshLayout) getView().findViewById(R.id.swipe_container);
        // the refresh listner. this would be called when the layout is pulled down
        swipeRefreshLayout.setOnRefreshListener(new SwipeRefreshLayout.OnRefreshListener() {

            @Override
            public void onRefresh() {
                swipeRefreshLayout.setRefreshing(true);
                ViewGroup insertPoint = (ViewGroup) getView().findViewById(R.id.lty_all_followup);
                if(insertPoint.getChildCount() > 2){
                    insertPoint.removeViews(2, insertPoint.getChildCount() - 2);
                }
                ModelFacade.getInstance().getAllPatients(receivable, api);
                Log.e("refreshing", swipeRefreshLayout.isRefreshing() + "");
                //displayPatients();
                isRefreshed = true;
                swipeRefreshLayout.setRefreshing(false);
                Toast.makeText(getView().getContext(), "refreshed!", Toast.LENGTH_SHORT).show();
            }
        });

        Button buttonViewChart= (Button) view.findViewById(R.id.button_view_chart);
        buttonViewChart.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                SharedPreferences preferences = getActivity().getSharedPreferences("myPrefs", 0);
                preferences.edit().clear().commit();

                Intent intent = new Intent(getContext(), ChartActivity.class);
                startActivity(intent);
            }
        });

        Button buttonLineChart = (Button) view.findViewById(R.id.button_line_chart);

        buttonLineChart.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(getContext(), LineChartActivity.class);
                intent.putExtra("followable_id", patientId);
                intent.putExtra("followable_type", patientType);
                startActivity(intent);
            }
        });

    }

    @Override
    public <U extends APIItem> void receiveData(List<U> items, String type) {
        final INPDRAPI api = new INPDRAPI(getContext());
        switch (type){
            case "getAllPatients":
                patients.clear();
                for (U item : items) {
                    Patient feed = (Patient) item;
                    patientId = feed.getEnrollable_id();
                    patientType = feed.getEnrollable_type();
                    switch (patientType){
                        case "NpbParticipant":case "NpbApplicantParticipant":
                            ModelFacade.getInstance().getAllNpbFollowups(this, api, patientType, patientId);
                            break;
                        case "NpcParticipant":case "NpcApplicantParticipant":
                            ModelFacade.getInstance().getAllNpcFollowups(this, api, patientType, patientId);
                            break;
                    }
                    patients.add(feed);
                    //Log.d("!!!!!data received!!!", feed.getUser().getEmail() + "");
                }
                break;
            case "getAllNpbFollowups":
                npbFollowUps.clear();
                for (U item : items) {
                    NpbFollowUp feed = (NpbFollowUp) item;
                    npbFollowUps.add(feed);
                    //Log.d("!!!!!data received!!!", feed.getUser().getEmail() + "");
                }
                displayNpbFollowups(npbFollowUps);
                break;
            case "getAllNpcFollowups":
                npcFollowUps.clear();
                for (U item : items) {
                    NpcFollowUp feed = (NpcFollowUp) item;
                    npcFollowUps.add(feed);
                    //Log.d("!!!!!data received!!!", feed.getUser().getEmail() + "");
                }
                displayNpcFollowups(npcFollowUps);
                break;
        }
    }

    @Override
    public <U extends APIItem> void receiveData(U item, String type) {
        INPDRAPI api = new INPDRAPI(getContext());
        switch (type) {
            case "deleteANpbFollowup":
                NpbFollowUp npbFollowUp = (NpbFollowUp) item;
                for(NpbFollowUp followUp : npbFollowUps){
                    if(followUp.getFollowable_id() == npbFollowUp.getFollowable_id() && followUp.getId() == npbFollowUp.getId()){
                        Log.d("Delete npbfollowup ", "followable_id:  " + npbFollowUp.getFollowable_id() + " id:" + npbFollowUp.getId());
                        npbFollowUps.remove(followUp);
                        Toast.makeText(getView().getContext(), "FollowUp " + npbFollowUp.getId() + " Deleted!", Toast.LENGTH_SHORT).show();
                    }
                }
                final MaterialDialog mMaterialDialog = new MaterialDialog(getContext());
                mMaterialDialog.setTitle("Succeed");
                mMaterialDialog.setMessage("You have successfully deleted " + npbFollowUp.getFollowable_type() + " " + npbFollowUp.getId());

                mMaterialDialog.setPositiveButton("Yes", new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        mMaterialDialog.dismiss();
                    }});

                mMaterialDialog.show();
                ModelFacade.getInstance().getAllNpbFollowups(receivable, api, patientType, patientId);
                break;

            case "deleteANpcFollowup":
                NpcFollowUp npcFollowUp = (NpcFollowUp) item;
                for(NpcFollowUp followUp : npcFollowUps){
                    if(followUp.getFollowable_id() == npcFollowUp.getFollowable_id() && followUp.getId() == npcFollowUp.getId()){
                        Log.d("Delete npbfollowup ", "followable_id:  " + npcFollowUp.getFollowable_id() + " id:" + npcFollowUp.getId());
                        Toast.makeText(getView().getContext(), "FollowUp " + npcFollowUp.getId() + " Deleted!", Toast.LENGTH_SHORT).show();
                    }
                }

                final MaterialDialog mMaterialDialog_c = new MaterialDialog(getContext());
                mMaterialDialog_c.setTitle("Succeed");
                mMaterialDialog_c.setMessage("You have successfully deleted " + npcFollowUp.getFollowable_type() + " " + npcFollowUp.getId());

                mMaterialDialog_c.setPositiveButton("Yes", new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        mMaterialDialog_c.dismiss();
                    }
                });
                mMaterialDialog_c.show();

                ModelFacade.getInstance().getAllNpcFollowups(receivable, api, patientType, patientId);
                break;
            case "error":
                ErrorMessage error = (ErrorMessage) item;
                final AlertDialog alertDialog = new AlertDialog.Builder(getContext()).create();
                alertDialog.setTitle("Error");
                alertDialog.setMessage("Error" + error.getErrorCode() + ": " + error.getErrorMes());
                alertDialog.setIcon(R.drawable.icon_alert);

                alertDialog.setButton(AlertDialog.BUTTON_NEUTRAL, "Try Again",
                        new DialogInterface.OnClickListener() {
                            public void onClick(DialogInterface dialog, int which) {
                                alertDialog.dismiss();
                            }
                        });

                alertDialog.show();
        }
    }

    public void displayNpcFollowups(ArrayList<NpcFollowUp> followUps){
        ViewGroup insertPoint = (ViewGroup) getView().findViewById(R.id.lty_all_followup);

        LayoutInflater vi = (LayoutInflater) getActivity().getSystemService(Context.LAYOUT_INFLATER_SERVICE);

        for (final NpcFollowUp npcFollowUp : followUps) {
            View v = vi.inflate(R.layout.item_all_followup, null);

            if(npcFollowUp.getCreated_at()!=null) {
                TextView textCreateDate = (TextView) v.findViewById(R.id.text_followup_create_date_detail);
                textCreateDate.setText(npcFollowUp.getCreated_at().substring(0, 10));
            }

            TextView textPatientId = (TextView) v. findViewById(R.id.text_unique_id);
            textPatientId.setText("Patient ID: "+npcFollowUp.getFollowable_id());

            TextView textPatientType = (TextView) v. findViewById(R.id.text_patient_type);
            textPatientType.setText(npcFollowUp.getFollowable_type());

            TextView textFollowupId = (TextView) v. findViewById(R.id.text_followup_id);
            textFollowupId.setText("ID: " + npcFollowUp.getId());

            Button buttonChart = (Button) v.findViewById(R.id.button_chart);

            buttonChart.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    Intent intent = new Intent(getContext(), LineColumnDependencyActivity.class);
                    intent.putExtra("followable_id", npcFollowUp.getFollowable_id());
                    intent.putExtra("followable_type", npcFollowUp.getFollowable_type());
                    intent.putExtra("followup_id", npcFollowUp.getId());
                    startActivity(intent);
                }
            });

            insertPoint.addView(
                    v,
                    2,
                    new ViewGroup.LayoutParams(ViewGroup.LayoutParams.MATCH_PARENT, ViewGroup.LayoutParams.WRAP_CONTENT));

        }

    }
    public void displayNpbFollowups(ArrayList<NpbFollowUp> followUps){
        ViewGroup insertPoint = (ViewGroup) getView().findViewById(R.id.lty_all_followup);

        LayoutInflater vi = (LayoutInflater) getActivity().getSystemService(Context.LAYOUT_INFLATER_SERVICE);


        for (final NpbFollowUp npbFollowUp : followUps) {
            View v = vi.inflate(R.layout.item_all_followup, null);

            if(npbFollowUp.getCreated_at()!=null) {
                TextView textCreateDate = (TextView) v.findViewById(R.id.text_followup_create_date_detail);
                textCreateDate.setText(npbFollowUp.getCreated_at().substring(0, 10));
            }

            TextView textPatientId = (TextView) v. findViewById(R.id.text_unique_id);
            textPatientId.setText("Patient ID: " + npbFollowUp.getFollowable_id());

            TextView textPatientType = (TextView) v. findViewById(R.id.text_patient_type);
            textPatientType.setText(npbFollowUp.getFollowable_type());

            TextView textFollowupId = (TextView) v. findViewById(R.id.text_followup_id);
            textFollowupId.setText("ID: " + npbFollowUp.getId());

            Button buttonChart = (Button) v.findViewById(R.id.button_chart);

            buttonChart.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    Intent intent = new Intent(getContext(), LineColumnDependencyActivity.class);
                    intent.putExtra("followable_id", npbFollowUp.getFollowable_id());
                    intent.putExtra("followable_type", npbFollowUp.getFollowable_type());
                    intent.putExtra("followup_id", npbFollowUp.getId());
                    startActivity(intent);
                }
            });
            insertPoint.addView(
                    v,
                    2,
                    new ViewGroup.LayoutParams(ViewGroup.LayoutParams.MATCH_PARENT, ViewGroup.LayoutParams.WRAP_CONTENT));
        }

    }

}
