package com.unimelb.inpdr.Models.API;

/**
 * Created by Xintong on 20/04/2016.
 */
public interface APIMessageReceivable {
    void receiveMes(String mes, String type);
}
