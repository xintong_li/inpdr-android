package com.unimelb.inpdr.Models;

import java.util.HashMap;
import java.util.Map;

/**
 * Created by Xintong on 7/05/2016.
 */
public class NpcApplicantFollowupCollection {
    private static Map<Integer, String> impact_ambulation =
            new HashMap<>();

    private static Map<Integer, String> impact_manipulation =
            new HashMap<>();

    private static Map<Integer, String> impact_speech =
            new HashMap<>();

    private static Map<Integer, String> impact_swallowing =
            new HashMap<>();

    private static Map<Integer, String> impact_eye_movement =
            new HashMap<>();

    private static Map<Integer, String> impact_seizure =
            new HashMap<>();

    private static Map<Integer, String> impact_cognitive_impaired =
            new HashMap<>();

    private static Map<Integer, String> impact_wider_self_miss_career =
            new HashMap<>();

    private static Map<Integer, String> impact_wider_family_miss_career =
            new HashMap<>();

    private static Map<Integer, String> impact_wider_emergency =
            new HashMap<>();

    private static Map<Integer, String> impact_wider_appointment =
            new HashMap<>();

    private static Map<Integer, String> impact_family_collection =
            new HashMap<>();

    public static NpcApplicantFollowupCollection getInstance(){
        NpcApplicantFollowupCollection followupCollection = new NpcApplicantFollowupCollection();
        return followupCollection;
    }
    private NpcApplicantFollowupCollection(){
        getImpact_family_collection().put(0, "Strongly Agree");
        getImpact_family_collection().put(1, "Agree");
        getImpact_family_collection().put(2, "Disagree");
        getImpact_family_collection().put(3, "Strongly Disagree");

        getImpact_ambulation().put(0, "He/she has no problems walking about");
        getImpact_ambulation().put(1, "He/she is clumsy");
        getImpact_ambulation().put(2, "He/she is unsteady on his/her feet but can walk unaided indoors and outdoors");
        getImpact_ambulation().put(3, "When he/she is outdoors, he/she needs assistance to move around");
        getImpact_ambulation().put(4, "He/she needs assistance both indoors and outdoors to move around");
        getImpact_ambulation().put(5, "He/she cannot walk at all and to get around we always use a wheelchair");
        getImpact_ambulation().put(6, "Not applicable - he/she is a baby so hasn't yet reached walking age");

        getImpact_manipulation().put(0, "He/she has no problems coordinating movement");
        getImpact_manipulation().put(1, "His/her movement can be shaky or trembling");
        getImpact_manipulation().put(2, "He/she has some problems with coordinating movement");
        getImpact_manipulation().put(3, "He/she has problems with coordinating movement, but can still feed him/herself");
        getImpact_manipulation().put(4, "He/she cannot perform any activities independently and needs help to do everything");

        getImpact_speech().put(0, "He/she has no problems with speech");
        getImpact_speech().put(1, "He/she is picking up words later than expected");
        getImpact_speech().put(2, "His/her speech can be difficult to understand");
        getImpact_speech().put(3, "His/her speech is very difficult to understand and really only people who know him/her very well can understand what he/she is saying");
        getImpact_speech().put(4, "He/she cannot speak, but communicates to us in other ways");
        getImpact_speech().put(5, "He/she cannot communicate at all");
        getImpact_speech().put(6, "Not applicable - he/she is too young to be expected to speak");

        getImpact_swallowing().put(0, "He/she has no difficulty in swallowing");
        getImpact_swallowing().put(1, "He/she chews abnormally, taking longer when chewing solid food");
        getImpact_swallowing().put(2, "He/she sometimes has difficulty in swallowing, and may occasionally cough while eating or drinking");
        getImpact_swallowing().put(3, "He/she has difficulty in swallowing every day, frequently coughing or choking on food or drinks");
        getImpact_swallowing().put(4, "His/her difficulties in swallowing are so severe that he/she now is fed by nasogastric tube or button fed.");

        getImpact_eye_movement().put(0, "His/her eyes move normally");
        getImpact_eye_movement().put(1, "His/her eyes move slowly");
        getImpact_eye_movement().put(2, "He/she has difficulty moving his/her eyes to look up or down");
        getImpact_eye_movement().put(3, "He/she cannot move his/her eyes (up and down or left and right)");

        getImpact_seizure().put(0, "He/she does not have any seizures");
        getImpact_seizure().put(1, "He/she has had occasional seizures but does not take any medicine for these");
        getImpact_seizure().put(2, "He/she has seizures but they are controlled by anti-epileptic drugs");
        getImpact_seizure().put(3, "He/she has seizures despite taking more than two anti-epileptic drugs");

        getImpact_cognitive_impaired().put(0, "Not at all");
        getImpact_cognitive_impaired().put(1, "A little bit");
        getImpact_cognitive_impaired().put(2, "Quite a bit");
        getImpact_cognitive_impaired().put(3, "Very much");

        getImpact_wider_self_miss_career().put(0, "No");
        getImpact_wider_self_miss_career().put(1, "Yes, less than 5 days");
        getImpact_wider_self_miss_career().put(2, "Yes, between 1 week and 1 month");
        getImpact_wider_self_miss_career().put(3, "Yes, more than 1 month");
        getImpact_wider_self_miss_career().put(4, "They have been unable to work/study due to the illness");
        getImpact_wider_self_miss_career().put(5, "N/A they are too young for school");

        getImpact_wider_family_miss_career().put(0, "No");
        getImpact_wider_family_miss_career().put(1, "Yes, less than 5 days");
        getImpact_wider_family_miss_career().put(2, "Yes, between 1 week and 1 month");
        getImpact_wider_family_miss_career().put(3, "Yes, more than 1 month");
        getImpact_wider_family_miss_career().put(4, "I have been unable to work/study due to the illness");
        getImpact_wider_family_miss_career().put(5, "I have changed my working pattern to be able to care for my loved one, i.e. working part-time");

        getImpact_wider_emergency().put(0, "Never");
        getImpact_wider_emergency().put(1, "1-3 times");
        getImpact_wider_emergency().put(2, "4-7 times");
        getImpact_wider_emergency().put(3, "8 times or more");

        getImpact_wider_appointment().put(0, "Never");
        getImpact_wider_appointment().put(1, "1-3 times");
        getImpact_wider_appointment().put(2, "4-7 times");
        getImpact_wider_appointment().put(3, "8 times or more");
    }

    public static Map<Integer, String> getImpact_ambulation() {
        return impact_ambulation;
    }

    public static void setImpact_ambulation(Map<Integer, String> impact_ambulation) {
        NpcApplicantFollowupCollection.impact_ambulation = impact_ambulation;
    }

    public static Map<Integer, String> getImpact_manipulation() {
        return impact_manipulation;
    }

    public static void setImpact_manipulation(Map<Integer, String> impact_manipulation) {
        NpcApplicantFollowupCollection.impact_manipulation = impact_manipulation;
    }

    public static Map<Integer, String> getImpact_speech() {
        return impact_speech;
    }

    public static void setImpact_speech(Map<Integer, String> impact_speech) {
        NpcApplicantFollowupCollection.impact_speech = impact_speech;
    }

    public static Map<Integer, String> getImpact_swallowing() {
        return impact_swallowing;
    }

    public static void setImpact_swallowing(Map<Integer, String> impact_swallowing) {
        NpcApplicantFollowupCollection.impact_swallowing = impact_swallowing;
    }

    public static Map<Integer, String> getImpact_eye_movement() {
        return impact_eye_movement;
    }

    public static void setImpact_eye_movement(Map<Integer, String> impact_eye_movement) {
        NpcApplicantFollowupCollection.impact_eye_movement = impact_eye_movement;
    }

    public static Map<Integer, String> getImpact_seizure() {
        return impact_seizure;
    }

    public static void setImpact_seizure(Map<Integer, String> impact_seizure) {
        NpcApplicantFollowupCollection.impact_seizure = impact_seizure;
    }

    public static Map<Integer, String> getImpact_cognitive_impaired() {
        return impact_cognitive_impaired;
    }

    public static void setImpact_cognitive_impaired(Map<Integer, String> impact_cognitive_impaired) {
        NpcApplicantFollowupCollection.impact_cognitive_impaired = impact_cognitive_impaired;
    }

    public static Map<Integer, String> getImpact_wider_self_miss_career() {
        return impact_wider_self_miss_career;
    }

    public static void setImpact_wider_self_miss_career(Map<Integer, String> impact_wider_self_miss_career) {
        NpcApplicantFollowupCollection.impact_wider_self_miss_career = impact_wider_self_miss_career;
    }

    public static Map<Integer, String> getImpact_wider_family_miss_career() {
        return impact_wider_family_miss_career;
    }

    public static void setImpact_wider_family_miss_career(Map<Integer, String> impact_wider_family_miss_career) {
        NpcApplicantFollowupCollection.impact_wider_family_miss_career = impact_wider_family_miss_career;
    }

    public static Map<Integer, String> getImpact_wider_emergency() {
        return impact_wider_emergency;
    }

    public static void setImpact_wider_emergency(Map<Integer, String> impact_wider_emergency) {
        NpcApplicantFollowupCollection.impact_wider_emergency = impact_wider_emergency;
    }

    public static Map<Integer, String> getImpact_wider_appointment() {
        return impact_wider_appointment;
    }

    public static void setImpact_wider_appointment(Map<Integer, String> impact_wider_appointment) {
        NpcApplicantFollowupCollection.impact_wider_appointment = impact_wider_appointment;
    }

    public static Map<Integer, String> getImpact_family_collection() {
        return impact_family_collection;
    }

    public static void setImpact_family_collection(Map<Integer, String> impact_family_collection) {
        NpcApplicantFollowupCollection.impact_family_collection = impact_family_collection;
    }
}
