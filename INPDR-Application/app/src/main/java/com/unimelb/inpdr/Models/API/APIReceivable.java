package com.unimelb.inpdr.Models.API;

import java.util.List;

/**
 * Created by Xintong on 20/04/2016.
 */
public interface APIReceivable {
    <U extends APIItem> void receiveData(List<U> items, String type);
    <U extends APIItem> void receiveData(U item, String type);
}
