package com.unimelb.inpdr.Models;

import java.util.HashMap;
import java.util.Map;
import java.util.Objects;

/**
 * Created by Xintong on 9/05/2016.
 */
public final class Selection {
    static Map isSelected = new HashMap<Boolean, String>();
    static Map isYes = new HashMap<Integer, String>();

    private Selection(){
        isSelected.put(false, "Unselected");
        isSelected.put(true, "Selected");

        isYes.put(0, "Yes");
        isYes.put(1, "No");
        isYes.put(2, "Unknown");
    }

    public static Selection getInstance(){
        Selection s = new Selection();
        return s;
    }

    public static Object getSelection(Boolean s){
        return isSelected.get(s);
    }
    public static Object getYes(Integer y){ return isYes.get(y);}
}
