package com.unimelb.inpdr.Models;

import com.unimelb.inpdr.Models.API.APIItem;

import org.json.JSONObject;

/**
 * Created by Xintong on 22/04/2016.
 */
public class NpbFollowUp extends APIItem{
    private int id;
    private int symptom_bone;
    private int symptom_abdominal;
    private int symptom_pain;
    private int symptom_breathlessness_rate;
    private int symptom_breathlessness_impact;
    private int symptom_bleeding_rate;
    private int symptom_bleeding_impact;
    private int symptom_fatigue_rate;
    private int symptom_fatigue_impact;
    private int symptom_enlarge_organ;
    private int symptom_enlarge_organ_impact;
    private int symptom_slow_growth;
    private int symptom_slow_growth_impact;
    private int symptom_infection;
    private int symptom_fracture;
    private int symptom_cognition;
    private int impact_family_disappointed;
    private int impact_family_give_up;
    private int impact_family_worry_future;
    private int impact_family_closer;
    private int impact_wider_self_miss_career;
    private int impact_wider_carer_miss_career;
    private int impact_wider_emergency;
    private int impact_wider_appointment;
    private String created_at;
    private String updated_at;
    private int followable_id;
    private String followable_type;

    public NpbFollowUp(JSONObject obj){
        setId(obj.optInt("id"));
        setSymptom_bone(obj.optInt("symptom_bone"));
        setSymptom_abdominal(obj.optInt("symptom_abdominal"));
        setSymptom_pain(obj.optInt("symptom_pain"));
        setSymptom_breathlessness_rate(obj.optInt("symptom_breathlessness_rate"));
        setSymptom_breathlessness_impact(obj.optInt("symptom_breathlessness_impact"));
        setSymptom_bleeding_rate(obj.optInt("symptom_bleeding_rate"));
        setSymptom_bleeding_impact(obj.optInt("symptom_bleeding_impact"));
        setSymptom_fatigue_rate(obj.optInt("symptom_fatigue_rate"));
        setSymptom_fatigue_impact(obj.optInt("symptom_fatigue_impact"));
        setSymptom_enlarge_organ(obj.optInt("symptom_enlarge_organ"));
        setSymptom_enlarge_organ_impact(obj.optInt("symptom_enlarge_organ_impact"));
        setSymptom_slow_growth(obj.optInt("symptom_slow_growth"));
        setSymptom_slow_growth_impact(obj.optInt("symptom_slow_growth_impact"));
        setSymptom_infection(obj.optInt("symptom_infection"));
        setSymptom_fracture(obj.optInt("symptom_fracture"));
        setSymptom_cognition(obj.optInt("symptom_cognition"));
        setImpact_family_disappointed(obj.optInt("impact_family_disappointed"));
        setImpact_family_give_up(obj.optInt("impact_family_give_up"));
        setImpact_family_worry_future(obj.optInt("impact_family_worry_future"));
        setImpact_family_closer(obj.optInt("impact_family_closer"));
        setImpact_wider_self_miss_career(obj.optInt("impact_wider_self_miss_career"));
        setImpact_wider_carer_miss_career(obj.optInt("impact_wider_carer_miss_career"));
        setImpact_wider_emergency(obj.optInt("impact_wider_emergency"));
        setImpact_wider_appointment(obj.optInt("impact_wider_appointment"));
        setCreated_at(obj.optString("created_at"));
        setUpdated_at(obj.optString("updated_at"));
        setFollowable_id(obj.optInt("followable_id"));
        setFollowable_type(obj.optString("followable_type"));
    }

    public int getSymptom_bone() {
        return symptom_bone;
    }

    public void setSymptom_bone(int symptom_bone) {
        this.symptom_bone = symptom_bone;
    }

    public int getSymptom_abdominal() {
        return symptom_abdominal;
    }

    public void setSymptom_abdominal(int symptom_abdominal) {
        this.symptom_abdominal = symptom_abdominal;
    }

    public int getSymptom_pain() {
        return symptom_pain;
    }

    public void setSymptom_pain(int symptom_pain) {
        this.symptom_pain = symptom_pain;
    }

    public int getSymptom_breathlessness_rate() {
        return symptom_breathlessness_rate;
    }

    public void setSymptom_breathlessness_rate(int symptom_breathlessness_rate) {
        this.symptom_breathlessness_rate = symptom_breathlessness_rate;
    }

    public int getSymptom_breathlessness_impact() {
        return symptom_breathlessness_impact;
    }

    public void setSymptom_breathlessness_impact(int symptom_breathlessness_impact) {
        this.symptom_breathlessness_impact = symptom_breathlessness_impact;
    }

    public int getSymptom_bleeding_rate() {
        return symptom_bleeding_rate;
    }

    public void setSymptom_bleeding_rate(int symptom_bleeding_rate) {
        this.symptom_bleeding_rate = symptom_bleeding_rate;
    }

    public int getSymptom_bleeding_impact() {
        return symptom_bleeding_impact;
    }

    public void setSymptom_bleeding_impact(int symptom_bleeding_impact) {
        this.symptom_bleeding_impact = symptom_bleeding_impact;
    }

    public int getSymptom_fatigue_rate() {
        return symptom_fatigue_rate;
    }

    public void setSymptom_fatigue_rate(int symptom_fatigue_rate) {
        this.symptom_fatigue_rate = symptom_fatigue_rate;
    }

    public int getSymptom_fatigue_impact() {
        return symptom_fatigue_impact;
    }

    public void setSymptom_fatigue_impact(int symptom_fatigue_impact) {
        this.symptom_fatigue_impact = symptom_fatigue_impact;
    }

    public int getSymptom_enlarge_organ() {
        return symptom_enlarge_organ;
    }

    public void setSymptom_enlarge_organ(int symptom_enlarge_organ) {
        this.symptom_enlarge_organ = symptom_enlarge_organ;
    }

    public int getSymptom_enlarge_organ_impact() {
        return symptom_enlarge_organ_impact;
    }

    public void setSymptom_enlarge_organ_impact(int symptom_enlarge_organ_impact) {
        this.symptom_enlarge_organ_impact = symptom_enlarge_organ_impact;
    }

    public int getSymptom_slow_growth() {
        return symptom_slow_growth;
    }

    public void setSymptom_slow_growth(int symptom_slow_growth) {
        this.symptom_slow_growth = symptom_slow_growth;
    }

    public int getSymptom_slow_growth_impact() {
        return symptom_slow_growth_impact;
    }

    public void setSymptom_slow_growth_impact(int symptom_slow_growth_impact) {
        this.symptom_slow_growth_impact = symptom_slow_growth_impact;
    }

    public int getSymptom_infection() {
        return symptom_infection;
    }

    public void setSymptom_infection(int symptom_infection) {
        this.symptom_infection = symptom_infection;
    }

    public int getSymptom_fracture() {
        return symptom_fracture;
    }

    public void setSymptom_fracture(int symptom_fracture) {
        this.symptom_fracture = symptom_fracture;
    }

    public int getSymptom_cognition() {
        return symptom_cognition;
    }

    public void setSymptom_cognition(int symptom_cognition) {
        this.symptom_cognition = symptom_cognition;
    }

    public int getImpact_family_disappointed() {
        return impact_family_disappointed;
    }

    public void setImpact_family_disappointed(int impact_family_disappointed) {
        this.impact_family_disappointed = impact_family_disappointed;
    }

    public int getImpact_family_give_up() {
        return impact_family_give_up;
    }

    public void setImpact_family_give_up(int impact_family_give_up) {
        this.impact_family_give_up = impact_family_give_up;
    }

    public int getImpact_family_worry_future() {
        return impact_family_worry_future;
    }

    public void setImpact_family_worry_future(int impact_family_worry_future) {
        this.impact_family_worry_future = impact_family_worry_future;
    }

    public int getImpact_family_closer() {
        return impact_family_closer;
    }

    public void setImpact_family_closer(int impact_family_closer) {
        this.impact_family_closer = impact_family_closer;
    }

    public int getImpact_wider_self_miss_career() {
        return impact_wider_self_miss_career;
    }

    public void setImpact_wider_self_miss_career(int impact_wider_self_miss_career) {
        this.impact_wider_self_miss_career = impact_wider_self_miss_career;
    }

    public int getImpact_wider_carer_miss_career() {
        return impact_wider_carer_miss_career;
    }

    public void setImpact_wider_carer_miss_career(int impact_wider_carer_miss_career) {
        this.impact_wider_carer_miss_career = impact_wider_carer_miss_career;
    }

    public int getImpact_wider_emergency() {
        return impact_wider_emergency;
    }

    public void setImpact_wider_emergency(int impact_wider_emergency) {
        this.impact_wider_emergency = impact_wider_emergency;
    }

    public int getImpact_wider_appointment() {
        return impact_wider_appointment;
    }

    public void setImpact_wider_appointment(int impact_wider_appointment) {
        this.impact_wider_appointment = impact_wider_appointment;
    }

    public String getCreated_at() {
        return created_at;
    }

    public void setCreated_at(String created_at) {
        this.created_at = created_at;
    }

    public String getUpdated_at() {
        return updated_at;
    }

    public void setUpdated_at(String updated_at) {
        this.updated_at = updated_at;
    }

    public int getFollowable_id() {
        return followable_id;
    }

    public void setFollowable_id(int followable_id) {
        this.followable_id = followable_id;
    }

    public String getFollowable_type() {
        return followable_type;
    }

    public void setFollowable_type(String followable_type) {
        this.followable_type = followable_type;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }
}
