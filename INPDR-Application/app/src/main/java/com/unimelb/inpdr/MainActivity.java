package com.unimelb.inpdr;

import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.graphics.drawable.Drawable;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentPagerAdapter;
import android.support.v4.view.ViewPager;
import android.support.v7.app.ActionBar;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.text.SpannableString;
import android.text.Spanned;
import android.text.style.ImageSpan;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;

import com.unimelb.inpdr.Fragments.ChartFragment;
import com.unimelb.inpdr.Fragments.HomeFragment;
import com.unimelb.inpdr.Models.API.APIItem;
import com.unimelb.inpdr.Models.API.APIReceivable;
import com.unimelb.inpdr.Models.API.INPDRAPI;
import com.unimelb.inpdr.Models.API.ModelFacade;
import com.unimelb.inpdr.SlidingTabs.SlidingTabLayout;
import com.unimelb.inpdr.Sources.FontsOverride;

import java.util.List;

import me.drakeet.materialdialog.MaterialDialog;

public class MainActivity extends AppCompatActivity implements APIReceivable{
    SectionsPagerAdapter sectionsPagerAdapter;
    ViewPager viewPager;
    private SlidingTabLayout slidingTabLayout;
    int user_id;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        FontsOverride.setDefaultFont(this, "DEFAULT", "OpenSans-Regular.ttf");
        FontsOverride.setDefaultFont(this, "MONOSPACE", "Hack-Regular.ttf");
        FontsOverride.setDefaultFont(this, "SANS_SERIF", "OpenSans-Regular.ttf");

        setContentView(R.layout.activity_main);
        SharedPreferences preferences = getSharedPreferences("myPrefs", 0);
        preferences.edit().clear().commit();

        sectionsPagerAdapter = new SectionsPagerAdapter(getSupportFragmentManager(), this);

        viewPager = (ViewPager) findViewById(R.id.pager);
        viewPager.setAdapter(sectionsPagerAdapter);

        slidingTabLayout = (SlidingTabLayout) findViewById(R.id.slidingTabs);
        slidingTabLayout.setCustomTabView(R.layout.custom_tab_view, R.id.tabText);
        slidingTabLayout.setDistributeEvenly(true);
        slidingTabLayout.setViewPager(viewPager);

        ActionBar bar = getSupportActionBar();
        bar.setTitle("INPDR");
    }
    public void setActionBarTitle(String title){
        ActionBar bar = getSupportActionBar();
        bar.setTitle(title);
        bar.show();
    }
    @Override
    public void onBackPressed() {
        FragmentManager fm = getSupportFragmentManager();
        Log.d("Onbackpressed", fm.getBackStackEntryCount()+"");
        if (fm.getBackStackEntryCount() > 0) {
            for(int i = 0; i < fm.getBackStackEntryCount() -1; i++){
                Log.d("back fragments ", fm.getFragments().get(i).getId() + " " + fm.getFragments().get(i).getTag());
            }

            fm.popBackStack();
        } else {
            super.onBackPressed();
        }
    }


    @Override
    protected void onStart() {
        super.onStart();

        ModelFacade.getInstance().bindService(this);
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
//        getMenuInflater().inflate(R.menu.menu_menu, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        final APIReceivable receivable = this;
        final INPDRAPI api = new INPDRAPI(getApplicationContext());
        //noinspection SimplifiableIfStatement

        switch (id){
            case R.id.action_settings:
                Log.d("item selected activity", "");
                // as a favorite...
                Log.d("logout pressed", "llll");

                final MaterialDialog mMaterialDialog = new MaterialDialog(this);
                mMaterialDialog.setTitle("Confirmation");
                mMaterialDialog.setMessage("Are you sure you want to logout?");
                mMaterialDialog.setPositiveButton("OK", new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        ModelFacade.getInstance().logout(receivable, api);
                    }
                });
                mMaterialDialog.setNegativeButton("CANCEL", new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        mMaterialDialog.dismiss();
                    }
                });

                mMaterialDialog.show();
                return true;
            default:
                // If we got here, the user's action was not recognized.
                // Invoke the superclass to handle it.
                return super.onOptionsItemSelected(item);
        }

    }

    @Override
    public <U extends APIItem> void receiveData(List<U> items, String type) {

    }

    @Override
    public <U extends APIItem> void receiveData(U item, String type) {
        switch (type){
            case "logout":
                Intent intent = new Intent(this, LoginActivity.class);
                startActivity(intent);
                break;
        }
    }

    /****
     * Inner Class
     *
     */
    public class SectionsPagerAdapter extends FragmentPagerAdapter {
        Context c;
        int icons[] = {R.drawable.ic_home,
                R.drawable.ic_barchart};

        public SectionsPagerAdapter(android.support.v4.app.FragmentManager fm, Context c) {
            super(fm);
            this.c = c;
        }

        @Override
        public Fragment getItem(int position) {
            Fragment fragment = null;
            switch (position){
                case 0:
                    fragment = new HomeFragment();
                    break;
                case 1:
                    fragment = new ChartFragment();
                    break;
            }
            return fragment;
        }

        @Override
        public int getCount() {
            return 2;
        }

        @Override
        public CharSequence getPageTitle(int position) {
            Drawable drawable = getResources().getDrawable(icons[position]);
            drawable.setBounds(0,0,80,70);
            ImageSpan imageSpan = new ImageSpan(drawable);
            SpannableString spannableString = new SpannableString(" ");
            spannableString.setSpan(imageSpan,0, spannableString.length(), Spanned.SPAN_EXCLUSIVE_EXCLUSIVE);
            return spannableString;

        }
    }
}
