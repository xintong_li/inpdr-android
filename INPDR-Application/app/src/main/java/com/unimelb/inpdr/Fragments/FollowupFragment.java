package com.unimelb.inpdr.Fragments;

import android.content.Context;
import android.content.DialogInterface;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentTransaction;
import android.support.v4.widget.SwipeRefreshLayout;
import android.support.v7.app.AlertDialog;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.TextView;
import android.widget.Toast;

import com.unimelb.inpdr.MainActivity;
import com.unimelb.inpdr.Models.API.APIItem;
import com.unimelb.inpdr.Models.API.APIReceivable;
import com.unimelb.inpdr.Models.API.INPDRAPI;
import com.unimelb.inpdr.Models.API.ModelFacade;
import com.unimelb.inpdr.Models.ErrorMessage;
import com.unimelb.inpdr.Models.NpbFollowUp;
import com.unimelb.inpdr.Models.NpcFollowUp;
import com.unimelb.inpdr.R;

import java.util.ArrayList;
import java.util.List;

import me.drakeet.materialdialog.MaterialDialog;

/**
 * Created by Xintong on 14/05/2016.
 */
public class FollowupFragment extends Fragment implements APIReceivable{
    private SwipeRefreshLayout swipeRefreshLayout;
    int patientId;
    String patientType;
    String uniqueId;
    private ArrayList<NpbFollowUp> npbFollowUps = new ArrayList<>();
    private ArrayList<NpcFollowUp> npcFollowUps = new ArrayList<>();

    final APIReceivable receivable = this;
    Fragment currentFragment;

    public FollowupFragment() {
        // Required empty public constructor
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        return inflater.inflate(R.layout.fragment_followup, container, false);
    }

    @Override
    public void onStart(){
        super.onStart();
        ((MainActivity)getActivity()).setActionBarTitle("Followup");
    }

    @Override
    public void onViewCreated(View view, Bundle savedInstanceState) {
        final INPDRAPI api = new INPDRAPI(getContext());
        currentFragment = this;
        Bundle data = getArguments();

        ((MainActivity)getActivity()).setActionBarTitle("Followup");

        if(data != null){
            try {
                patientId = data.getInt("followable_id");
                patientType = data.getString("followable_type");
                uniqueId = data.getString("unique_id");
                switch (patientType){
                    case "NpbParticipant":case "NpbApplicantParticipant":
                        ModelFacade.getInstance().getAllNpbFollowups(this, api, patientType, patientId);
                        break;
                    case "NpcParticipant":case "NpcApplicantParticipant":
                        ModelFacade.getInstance().getAllNpcFollowups(this, api, patientType, patientId);
                        break;
                }

                swipeRefreshLayout = (SwipeRefreshLayout) getView().findViewById(R.id.swipe_container);
                // the refresh listner. this would be called when the layout is pulled down
                swipeRefreshLayout.setOnRefreshListener(new SwipeRefreshLayout.OnRefreshListener() {

                    @Override
                    public void onRefresh() {
                        swipeRefreshLayout.setRefreshing(true);
                        switch (patientType){
                            case "NpbParticipant":
                                ModelFacade.getInstance().getAllNpbFollowups(receivable, api, patientType, patientId);
                                break;
                            case "NpcParticipant":
                                ModelFacade.getInstance().getAllNpcFollowups(receivable, api, patientType, patientId);
                                break;
                        }
                        Log.e("refreshing", swipeRefreshLayout.isRefreshing() + "");
                        //displayPatients();
                        swipeRefreshLayout.setRefreshing(false);
                        Toast.makeText(getView().getContext(), "refreshed!", Toast.LENGTH_SHORT).show();
                    }
                });
            }catch (Exception e){
                e.printStackTrace();
            }
        }
        Button buttonCreate = (Button) view.findViewById(R.id.button_new_questionnaire);
        buttonCreate.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                SharedPreferences preferences = getActivity().getSharedPreferences("myPrefs", 0);
                preferences.edit().clear().commit();
                Toast.makeText(getView().getContext(), "refreshed!", Toast.LENGTH_SHORT).show();
                Log.d("backkkk!", getFragmentManager().getBackStackEntryCount()+"");
                Bundle data = new Bundle();
                data.putInt("followable_id", patientId);
                data.putString("followable_type", patientType);
                Fragment followupCreateFragment = new FollowupCreateFragment();
                followupCreateFragment.setArguments(data);

                FragmentTransaction transaction = getFragmentManager()
                        .beginTransaction();
                transaction.add(R.id.main_view, followupCreateFragment,
                        "FragmentCreate"); //now you have an instance of newFragment1//now you have an instance of newFragment2
                transaction.addToBackStack("FragmentFollowup");
                transaction.show(followupCreateFragment);
                transaction.setTransition(FragmentTransaction.TRANSIT_FRAGMENT_OPEN);
                transaction.commit();

//                FragmentManager fm = getFragmentManager();
//                FragmentTransaction ft = fm.beginTransaction();
//
//                ft.replace(R.id.main_view, followupCreateFragment);
//                ft.addToBackStack(null);
//                ft.commit();
            }
        });
    }

    @Override
    public <U extends APIItem> void receiveData(List<U> items, String type) {
        switch (type){
            case "getAllNpbFollowups":
                npbFollowUps.clear();
                for (U item : items) {
                    NpbFollowUp feed = (NpbFollowUp) item;
                    npbFollowUps.add(feed);
                    //Log.d("!!!!!data received!!!", feed.getUser().getEmail() + "");
                }
                displayNpbFollowups(npbFollowUps);
                break;
            case "getAllNpcFollowups":
                npcFollowUps.clear();
                for (U item : items) {
                    NpcFollowUp feed = (NpcFollowUp) item;
                    npcFollowUps.add(feed);
                    //Log.d("!!!!!data received!!!", feed.getUser().getEmail() + "");
                }
                displayNpcFollowups(npcFollowUps);
                break;
        }
    }

    @Override
    public <U extends APIItem> void receiveData(U item, String type) {
        INPDRAPI api = new INPDRAPI(getContext());
        switch (type) {
            case "deleteANpbFollowup":
                NpbFollowUp npbFollowUp = (NpbFollowUp) item;
                for(NpbFollowUp followUp : npbFollowUps){
                    if(followUp.getFollowable_id() == npbFollowUp.getFollowable_id() && followUp.getId() == npbFollowUp.getId()){
                        Log.d("Delete npbfollowup ", "followable_id:  " + npbFollowUp.getFollowable_id() + " id:" + npbFollowUp.getId());
                        npbFollowUps.remove(followUp);
                        Toast.makeText(getView().getContext(), "FollowUp " + npbFollowUp.getId() + " Deleted!", Toast.LENGTH_SHORT).show();
                    }
                }
                final MaterialDialog mMaterialDialog = new MaterialDialog(getContext());
                mMaterialDialog.setTitle("Succeed");
                mMaterialDialog.setMessage("You have successfully deleted " + npbFollowUp.getFollowable_type() + " " + npbFollowUp.getId());

                mMaterialDialog.setPositiveButton("Yes", new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        mMaterialDialog.dismiss();
                    }});

                mMaterialDialog.show();
                ModelFacade.getInstance().getAllNpbFollowups(receivable, api, patientType, patientId);
                break;

            case "deleteANpcFollowup":
                NpcFollowUp npcFollowUp = (NpcFollowUp) item;
                for(NpcFollowUp followUp : npcFollowUps){
                    if(followUp.getFollowable_id() == npcFollowUp.getFollowable_id() && followUp.getId() == npcFollowUp.getId()){
                        Log.d("Delete npbfollowup ", "followable_id:  " + npcFollowUp.getFollowable_id() + " id:" + npcFollowUp.getId());
                        Toast.makeText(getView().getContext(), "FollowUp " + npcFollowUp.getId() + " Deleted!", Toast.LENGTH_SHORT).show();
                    }
                }

                final MaterialDialog mMaterialDialog_c = new MaterialDialog(getContext());
                mMaterialDialog_c.setTitle("Succeed");
                mMaterialDialog_c.setMessage("You have successfully deleted " + npcFollowUp.getFollowable_type() + " " + npcFollowUp.getId());

                mMaterialDialog_c.setPositiveButton("Yes", new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        mMaterialDialog_c.dismiss();
                    }
                });
                mMaterialDialog_c.show();

                ModelFacade.getInstance().getAllNpcFollowups(receivable, api, patientType, patientId);
                break;
            case "error":
                ErrorMessage error = (ErrorMessage) item;
                final MaterialDialog mMaterialDialog_e = new MaterialDialog(getContext());
                mMaterialDialog_e.setTitle("Error");
                ErrorMessage errorMessage = (ErrorMessage) item;
                mMaterialDialog_e.setMessage(errorMessage.getErrorMes());
                mMaterialDialog_e.setPositiveButton("Ok", new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        mMaterialDialog_e.dismiss();
                    }
                });
                mMaterialDialog_e.show();
        }
    }

    public void displayNpcFollowups(ArrayList<NpcFollowUp> followUps){
        ViewGroup insertPoint = (ViewGroup) getView().findViewById(R.id.lty_followup);
        if(insertPoint.getChildCount() > 2){
            insertPoint.removeViews(2, insertPoint.getChildCount() - 2);
        }

        TextView textPatientId = (TextView)getView().findViewById(R.id.text_followup_patient_id);
        textPatientId.setText("NPC" + uniqueId);
        LayoutInflater vi = (LayoutInflater) getActivity().getSystemService(Context.LAYOUT_INFLATER_SERVICE);

        for (final NpcFollowUp npcFollowUp : followUps) {
            View v = vi.inflate(R.layout.item_followup, null);

            if(npcFollowUp.getCreated_at()!=null) {
                TextView textCreateDate = (TextView) v.findViewById(R.id.text_followup_create_date_detail);
                textCreateDate.setText(npcFollowUp.getCreated_at().substring(0, 10));
            }

            Button buttonShow = (Button) v.findViewById(R.id.button_show_followup);

            buttonShow.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    Bundle data = new Bundle();
                    data.putInt("followable_id", npcFollowUp.getFollowable_id());
                    data.putString("followable_type", npcFollowUp.getFollowable_type());
                    data.putInt("followup_id", npcFollowUp.getId());
                    Fragment followupDetailFragment = new FollowupDetailFragment();
                    followupDetailFragment.setArguments(data);

                    FragmentManager fm = getFragmentManager();
                    FragmentTransaction ft = fm.beginTransaction();

                    ft.replace(R.id.main_view, followupDetailFragment);
                    ft.addToBackStack(null);
                    ft.commit();
                }
            });

            Button buttonEdit = (Button) v.findViewById(R.id.button_edit_followup);

            buttonEdit.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    Bundle data = new Bundle();
                    data.putInt("followable_id", npcFollowUp.getFollowable_id());
                    data.putString("followable_type", npcFollowUp.getFollowable_type());
                    data.putInt("followup_id", npcFollowUp.getId());
                    Fragment followupEditFragment = new FollowupEditFragment();
                    followupEditFragment.setArguments(data);

                    FragmentManager fm = getFragmentManager();
                    FragmentTransaction ft = fm.beginTransaction();

                    ft.replace(R.id.main_view, followupEditFragment);
                    ft.addToBackStack("FollowupFragment");
                    ft.commit();
                }
            });

            final APIReceivable receivable = this;
            final int followup_id = npcFollowUp.getId();
            final int followable_id =  npcFollowUp.getFollowable_id();
            final String followable_type = npcFollowUp.getFollowable_type();
            Button buttonDestroy = (Button) v.findViewById(R.id.button_destroy_followup);
            final INPDRAPI api = new INPDRAPI(getContext());


            buttonDestroy.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    final MaterialDialog mMaterialDialog = new MaterialDialog(getContext());
                    mMaterialDialog.setTitle("Confirmation");
                    mMaterialDialog.setMessage("Are you sure you want to delete this item?");

                    mMaterialDialog.setPositiveButton("YES", new View.OnClickListener() {
                        @Override
                        public void onClick(View v) {
                            ModelFacade.getInstance().deleteANpcFollowup(receivable, api, followable_type, followable_id, followup_id);
                            mMaterialDialog.dismiss();
                        }
                    });
                    mMaterialDialog.setNegativeButton("CANCEL", new View.OnClickListener() {
                        @Override
                        public void onClick(View v) {
                            mMaterialDialog.dismiss();
                        }
                    });

                    mMaterialDialog.show();
                }
            });

            insertPoint.addView(
                    v,
                    2,
                    new ViewGroup.LayoutParams(ViewGroup.LayoutParams.MATCH_PARENT, ViewGroup.LayoutParams.WRAP_CONTENT));

        }

    }
    public void displayNpbFollowups(ArrayList<NpbFollowUp> followUps){
        ViewGroup insertPoint = (ViewGroup) getView().findViewById(R.id.lty_followup);
        if(insertPoint.getChildCount() > 2){
            insertPoint.removeViews(2, insertPoint.getChildCount()-2);
        }
        TextView textPatientId = (TextView)getView().findViewById(R.id.text_followup_patient_id);
        textPatientId.setText("NPB" + uniqueId);
        LayoutInflater vi = (LayoutInflater) getActivity().getSystemService(Context.LAYOUT_INFLATER_SERVICE);

        for (final NpbFollowUp npbFollowUp : followUps) {
            View v = vi.inflate(R.layout.item_followup, null);

            if(npbFollowUp.getCreated_at()!=null) {
                TextView textCreateDate = (TextView) v.findViewById(R.id.text_followup_create_date_detail);
                textCreateDate.setText(npbFollowUp.getCreated_at().substring(0, 10));
            }


            Button buttonShow = (Button) v.findViewById(R.id.button_show_followup);

            buttonShow.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    Bundle data = new Bundle();
                    data.putInt("followable_id", npbFollowUp.getFollowable_id());
                    data.putString("followable_type", npbFollowUp.getFollowable_type());
                    data.putInt("followup_id", npbFollowUp.getId());
                    Fragment followupDetailFragment = new FollowupDetailFragment();
                    followupDetailFragment.setArguments(data);

                    FragmentManager fm = getFragmentManager();
                    FragmentTransaction ft = fm.beginTransaction();

                    ft.replace(R.id.main_view, followupDetailFragment);
                    ft.addToBackStack("FollowupFragment");
                    ft.commit();
                }
            });

            Button buttonEdit = (Button) v.findViewById(R.id.button_edit_followup);

            buttonEdit.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    Bundle data = new Bundle();
                    data.putInt("followable_id", npbFollowUp.getFollowable_id());
                    data.putString("followable_type", npbFollowUp.getFollowable_type());
                    data.putInt("followup_id", npbFollowUp.getId());
                    Fragment followupEditFragment = new FollowupEditFragment();
                    followupEditFragment.setArguments(data);

                    FragmentManager fm = getFragmentManager();
                    FragmentTransaction ft = fm.beginTransaction();

                    ft.replace(R.id.main_view, followupEditFragment);
                    ft.addToBackStack("FollowupFragment");
                    ft.commit();
                }
            });

            final APIReceivable receivable = this;
            final int followup_id = npbFollowUp.getId();
            final int followable_id = npbFollowUp.getFollowable_id();
            final String followable_type = npbFollowUp.getFollowable_type();
            final INPDRAPI api = new INPDRAPI(getContext());

            Button buttonDestroy = (Button) v.findViewById(R.id.button_destroy_followup);

            buttonDestroy.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    final MaterialDialog mMaterialDialog = new MaterialDialog(getContext());
                    mMaterialDialog.setTitle("Confirmation");
                    mMaterialDialog.setMessage("Are you sure you want to delete this item?");

                    mMaterialDialog.setPositiveButton("YES", new View.OnClickListener() {
                        @Override
                        public void onClick(View v) {
                            ModelFacade.getInstance().deleteANpbFollowup(receivable, api, followable_type, followable_id, followup_id);
                            mMaterialDialog.dismiss();
                        }
                    });
                    mMaterialDialog.setNegativeButton("CANCEL", new View.OnClickListener() {
                        @Override
                        public void onClick(View v) {
                            mMaterialDialog.dismiss();
                        }
                    });

                    mMaterialDialog.show();
                }
            });

            insertPoint.addView(
                    v,
                    2,
                    new ViewGroup.LayoutParams(ViewGroup.LayoutParams.MATCH_PARENT, ViewGroup.LayoutParams.WRAP_CONTENT));
        }

    }
}
