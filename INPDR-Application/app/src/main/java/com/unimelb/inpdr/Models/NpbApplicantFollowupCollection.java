package com.unimelb.inpdr.Models;

import java.util.HashMap;
import java.util.Map;

/**
 * Created by Xintong on 7/05/2016.
 */
public final class NpbApplicantFollowupCollection {

    private static Map<Integer, String> symptom_enlarge_organ_collection = new HashMap<>();

    private static Map<Integer, String> impact_family_collection =
            new HashMap<>();

    private static Map<Integer, String> symptom_bone_collection =
            new HashMap<>();

    private static Map<Integer, String> symptom_abdominal_collection =
            new HashMap<>();

    private static Map<Integer, String> symptom_pain_collection =
            new HashMap<>();

    private static Map<Integer, String> symptom_bleeding_rate_collection =
            new HashMap<>();

    private static Map<Integer, String> symptom_bleeding_impact_collection =
            new HashMap<>();

    private static Map<Integer, String> symptom_enlarge_organ_impact_collection =
            new HashMap<>();

    private static Map<Integer, String> symptom_fatigue_impact_collection =
            new HashMap<>();

    private static Map<Integer, String> symptom_slow_growth_collection =
            new HashMap<>();

    private static Map<Integer, String> symptom_slow_growth_impact_collection =
            new HashMap<>();

    private static Map<Integer, String> symptom_infection_collection =
            new HashMap<>();

    private static Map<Integer, String> symptom_fracture_collection =
            new HashMap<>();

    private static Map<Integer, String> symptom_cognition_collection =
            new HashMap<>();

    private static Map<Integer, String> symptom_breathlessness_rate_collection =
            new HashMap<>();

    private static Map<Integer, String> symptom_breathlessness_impact_collection =
            new HashMap<>();


    private static Map<Integer, String> symptom_fatigue_rate_collection =
            new HashMap<>();

    private static Map<Integer, String> impact_wider_carer_miss_career =
            new HashMap<>();

    private static Map<Integer, String> impact_wider_self_miss_career =
            new HashMap<>();

    private static Map<Integer, String> impact_wider_emergency =
            new HashMap<>();

    private static Map<Integer, String> impact_wider_appointment =
            new HashMap<>();


    public static NpbApplicantFollowupCollection getInstance(){
        NpbApplicantFollowupCollection followupCollection = new NpbApplicantFollowupCollection();
        return  followupCollection;
    }


    private NpbApplicantFollowupCollection(){
        getSymptom_fatigue_rate_collection().put(0, "Never");
        getSymptom_fatigue_rate_collection().put(1, "Rarely");
        getSymptom_fatigue_rate_collection().put(2, "Sometimes");
        getSymptom_fatigue_rate_collection().put(3, "Often");
        getSymptom_fatigue_rate_collection().put(4, "Always");

        getSymptom_breathlessness_rate_collection().put(0, "Never");
        getSymptom_breathlessness_rate_collection().put(1, "On running");
        getSymptom_breathlessness_rate_collection().put(2, "On climbing stairs");
        getSymptom_breathlessness_rate_collection().put(3, "On walking");
        getSymptom_breathlessness_rate_collection().put(4, "At rest");

        getSymptom_enlarge_organ_collection().put(0, "Not at all");
        getSymptom_enlarge_organ_collection().put(1, "Somewhat");
        getSymptom_enlarge_organ_collection().put(2, "Very much");

        getImpact_family_collection().put(0, "Strongly Agree");
        getImpact_family_collection().put(1, "Agree");
        getImpact_family_collection().put(2, "Disagree");
        getImpact_family_collection().put(3, "Strongly Disagree");

        getSymptom_bone_collection().put(0,"Never");
        getSymptom_bone_collection().put(1,"Monthly");
        getSymptom_bone_collection().put(2,"Weekly");
        getSymptom_bone_collection().put(3,"Every day");
        getSymptom_bone_collection().put(4, "All the time");

        getSymptom_abdominal_collection().put(0,"Never");
        getSymptom_abdominal_collection().put(1,"Monthly");
        getSymptom_abdominal_collection().put(2,"Weekly");
        getSymptom_abdominal_collection().put(3,"Every day");
        getSymptom_abdominal_collection().put(4, "All the time");

        getSymptom_pain_collection().put(0, "None at all");
        getSymptom_pain_collection().put(1, "Very little");
        getSymptom_pain_collection().put(2, "Some");
        getSymptom_pain_collection().put(3, "Quite a lot");
        getSymptom_pain_collection().put(4, "Very much");

        getSymptom_bleeding_rate_collection().put(0,"Never");
        getSymptom_bleeding_rate_collection().put(1,"Monthly");
        getSymptom_bleeding_rate_collection().put(2,"Weekly");
        getSymptom_bleeding_rate_collection().put(3,"Every day");
        getSymptom_bleeding_rate_collection().put(4, "All the time");

        getSymptom_bleeding_impact_collection().put(0, "None at all");
        getSymptom_bleeding_impact_collection().put(1, "Very little");
        getSymptom_bleeding_impact_collection().put(2, "Some");
        getSymptom_bleeding_impact_collection().put(3, "Quite a lot");
        getSymptom_bleeding_impact_collection().put(4, "Very much");

        getSymptom_fatigue_impact_collection().put(0, "None at all");
        getSymptom_fatigue_impact_collection().put(1, "Very little");
        getSymptom_fatigue_impact_collection().put(2, "Some");
        getSymptom_fatigue_impact_collection().put(3, "Quite a lot");
        getSymptom_fatigue_impact_collection().put(4, "Very much");

        getSymptom_enlarge_organ_impact_collection().put(0, "None at all");
        getSymptom_enlarge_organ_impact_collection().put(1, "Very little");
        getSymptom_enlarge_organ_impact_collection().put(2, "Some");
        getSymptom_enlarge_organ_impact_collection().put(3, "Quite a lot");
        getSymptom_enlarge_organ_impact_collection().put(4, "Very much");

        getSymptom_slow_growth_collection().put(0, "Not at all");
        getSymptom_slow_growth_collection().put(1, "Somewhat");
        getSymptom_slow_growth_collection().put(2, "Very much");

        getSymptom_slow_growth_impact_collection().put(0, "None at all");
        getSymptom_slow_growth_impact_collection().put(1, "Very little");
        getSymptom_slow_growth_impact_collection().put(2, "Some");
        getSymptom_slow_growth_impact_collection().put(3, "Quite a lot");
        getSymptom_slow_growth_impact_collection().put(4, "Very much");

        getSymptom_breathlessness_impact_collection().put(0, "None at all");
        getSymptom_breathlessness_impact_collection().put(1, "Very little");
        getSymptom_breathlessness_impact_collection().put(2, "Some");
        getSymptom_breathlessness_impact_collection().put(3, "Quite a lot");
        getSymptom_breathlessness_impact_collection().put(4, "Very much");

        getSymptom_infection_collection().put(0, "0");
        getSymptom_infection_collection().put(1, "1-2");
        getSymptom_infection_collection().put(2, "3-4");
        getSymptom_infection_collection().put(3, "Over 4");

        getSymptom_fracture_collection().put(0, "0");
        getSymptom_fracture_collection().put(1, "1-3");
        getSymptom_fracture_collection().put(2, "4-6");
        getSymptom_fracture_collection().put(3, "Over 6");

        getSymptom_cognition_collection().put(0, "None at all");
        getSymptom_cognition_collection().put(1, "Very little");
        getSymptom_cognition_collection().put(2, "Some");
        getSymptom_cognition_collection().put(3, "Quite a lot");
        getSymptom_cognition_collection().put(4, "Very much");

        getImpact_wider_carer_miss_career().put(0, "No");
        getImpact_wider_carer_miss_career().put(1, "Yes, less than 5 days");
        getImpact_wider_carer_miss_career().put(2, "Yes, between 1 week and 1 month");
        getImpact_wider_carer_miss_career().put(3, "Yes, more than 1 month");
        getImpact_wider_carer_miss_career().put(4, "I have been unable to work/study due to the illness");
        getImpact_wider_carer_miss_career().put(5, "I have changed my working pattern to be able to care for my loved one, i.e. working part-time");

        getImpact_wider_self_miss_career().put(0, "No");
        getImpact_wider_self_miss_career().put(1, "Yes, less than 5 days");
        getImpact_wider_self_miss_career().put(2, "Yes, between 1 week and 1 month");
        getImpact_wider_self_miss_career().put(3, "Yes, more than 1 month");
        getImpact_wider_self_miss_career().put(4, "They have been unable to work/study due to the illness");
        getImpact_wider_self_miss_career().put(5, "N/A they are too young for school");

        getImpact_wider_emergency().put(0, "Never");
        getImpact_wider_emergency().put(1, "1-3 times");
        getImpact_wider_emergency().put(2, "4-7 times");
        getImpact_wider_emergency().put(3, "8 times or more");

        getImpact_wider_appointment().put(0, "Never");
        getImpact_wider_appointment().put(1, "1-3 times");
        getImpact_wider_appointment().put(2, "4-7 times");
        getImpact_wider_appointment().put(3, "8 times or more");
    }


    public static Map<Integer, String> getSymptom_enlarge_organ_collection() {
        return symptom_enlarge_organ_collection;
    }

    public static void setSymptom_enlarge_organ_collection(Map<Integer, String> symptom_enlarge_organ_collection) {
        NpbApplicantFollowupCollection.symptom_enlarge_organ_collection = symptom_enlarge_organ_collection;
    }

    public static Map<Integer, String> getImpact_family_collection() {
        return impact_family_collection;
    }

    public static void setImpact_family_collection(Map<Integer, String> impact_family_collection) {
        NpbApplicantFollowupCollection.impact_family_collection = impact_family_collection;
    }

    public static Map<Integer, String> getSymptom_bone_collection() {
        return symptom_bone_collection;
    }

    public static void setSymptom_bone_collection(Map<Integer, String> symptom_bone_collection) {
        NpbApplicantFollowupCollection.symptom_bone_collection = symptom_bone_collection;
    }

    public static Map<Integer, String> getSymptom_abdominal_collection() {
        return symptom_abdominal_collection;
    }

    public static void setSymptom_abdominal_collection(Map<Integer, String> symptom_abdominal_collection) {
        NpbApplicantFollowupCollection.symptom_abdominal_collection = symptom_abdominal_collection;
    }

    public static Map<Integer, String> getSymptom_pain_collection() {
        return symptom_pain_collection;
    }

    public static void setSymptom_pain_collection(Map<Integer, String> symptom_pain_collection) {
        NpbApplicantFollowupCollection.symptom_pain_collection = symptom_pain_collection;
    }

    public static Map<Integer, String> getSymptom_bleeding_rate_collection() {
        return symptom_bleeding_rate_collection;
    }

    public static void setSymptom_bleeding_rate_collection(Map<Integer, String> symptom_bleeding_rate_collection) {
        NpbApplicantFollowupCollection.symptom_bleeding_rate_collection = symptom_bleeding_rate_collection;
    }

    public static Map<Integer, String> getSymptom_bleeding_impact_collection() {
        return symptom_bleeding_impact_collection;
    }

    public static void setSymptom_bleeding_impact_collection(Map<Integer, String> symptom_bleeding_impact_collection) {
        NpbApplicantFollowupCollection.symptom_bleeding_impact_collection = symptom_bleeding_impact_collection;
    }

    public static Map<Integer, String> getSymptom_enlarge_organ_impact_collection() {
        return symptom_enlarge_organ_impact_collection;
    }

    public static void setSymptom_enlarge_organ_impact_collection(Map<Integer, String> symptom_enlarge_organ_impact_collection) {
        NpbApplicantFollowupCollection.symptom_enlarge_organ_impact_collection = symptom_enlarge_organ_impact_collection;
    }

    public static Map<Integer, String> getSymptom_fatigue_impact_collection() {
        return symptom_fatigue_impact_collection;
    }

    public static void setSymptom_fatigue_impact_collection(Map<Integer, String> symptom_fatigue_impact_collection) {
        NpbApplicantFollowupCollection.symptom_fatigue_impact_collection = symptom_fatigue_impact_collection;
    }

    public static Map<Integer, String> getSymptom_slow_growth_collection() {
        return symptom_slow_growth_collection;
    }

    public static void setSymptom_slow_growth_collection(Map<Integer, String> symptom_slow_growth_collection) {
        NpbApplicantFollowupCollection.symptom_slow_growth_collection = symptom_slow_growth_collection;
    }

    public static Map<Integer, String> getSymptom_slow_growth_impact_collection() {
        return symptom_slow_growth_impact_collection;
    }

    public static void setSymptom_slow_growth_impact_collection(Map<Integer, String> symptom_slow_growth_impact_collection) {
        NpbApplicantFollowupCollection.symptom_slow_growth_impact_collection = symptom_slow_growth_impact_collection;
    }

    public static Map<Integer, String> getSymptom_infection_collection() {
        return symptom_infection_collection;
    }

    public static void setSymptom_infection_collection(Map<Integer, String> symptom_infection_collection) {
        NpbApplicantFollowupCollection.symptom_infection_collection = symptom_infection_collection;
    }

    public static Map<Integer, String> getSymptom_fracture_collection() {
        return symptom_fracture_collection;
    }

    public static void setSymptom_fracture_collection(Map<Integer, String> symptom_fracture_collection) {
        NpbApplicantFollowupCollection.symptom_fracture_collection = symptom_fracture_collection;
    }

    public static Map<Integer, String> getSymptom_cognition_collection() {
        return symptom_cognition_collection;
    }

    public static void setSymptom_cognition_collection(Map<Integer, String> symptom_cognition_collection) {
        NpbApplicantFollowupCollection.symptom_cognition_collection = symptom_cognition_collection;
    }

    public static Map<Integer, String> getSymptom_breathlessness_rate_collection() {
        return symptom_breathlessness_rate_collection;
    }

    public static void setSymptom_breathlessness_rate_collection(Map<Integer, String> symptom_breathlessness_rate_collection) {
        NpbApplicantFollowupCollection.symptom_breathlessness_rate_collection = symptom_breathlessness_rate_collection;
    }

    public static Map<Integer, String> getSymptom_fatigue_rate_collection() {
        return symptom_fatigue_rate_collection;
    }

    public static void setSymptom_fatigue_rate_collection(Map<Integer, String> symptom_fatigue_rate_collection) {
        NpbApplicantFollowupCollection.symptom_fatigue_rate_collection = symptom_fatigue_rate_collection;
    }

    public static Map<Integer, String> getImpact_wider_carer_miss_career() {
        return impact_wider_carer_miss_career;
    }

    public static void setImpact_wider_carer_miss_career(Map<Integer, String> impact_wider_carer_miss_career) {
        NpbApplicantFollowupCollection.impact_wider_carer_miss_career = impact_wider_carer_miss_career;
    }

    public static Map<Integer, String> getImpact_wider_self_miss_career() {
        return impact_wider_self_miss_career;
    }

    public static void setImpact_wider_self_miss_career(Map<Integer, String> impact_wider_self_miss_career) {
        NpbApplicantFollowupCollection.impact_wider_self_miss_career = impact_wider_self_miss_career;
    }

    public static Map<Integer, String> getImpact_wider_emergency() {
        return impact_wider_emergency;
    }

    public static void setImpact_wider_emergency(Map<Integer, String> impact_wider_emergency) {
        NpbApplicantFollowupCollection.impact_wider_emergency = impact_wider_emergency;
    }

    public static Map<Integer, String> getImpact_wider_appointment() {
        return impact_wider_appointment;
    }

    public static void setImpact_wider_appointment(Map<Integer, String> impact_wider_appointment) {
        NpbApplicantFollowupCollection.impact_wider_appointment = impact_wider_appointment;
    }

    public static Map<Integer, String> getSymptom_breathlessness_impact_collection() {
        return symptom_breathlessness_impact_collection;
    }

    public static void setSymptom_breathlessness_impact_collection(Map<Integer, String> symptom_breathlessness_impact_collection) {
        NpbApplicantFollowupCollection.symptom_breathlessness_impact_collection = symptom_breathlessness_impact_collection;
    }
}
