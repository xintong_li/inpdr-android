package com.unimelb.inpdr.Fragments;

import android.content.Context;
import android.content.DialogInterface;
import android.graphics.Color;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentTransaction;
import android.support.v7.app.AlertDialog;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.TextView;
import android.widget.Toast;

import com.jaredrummler.materialspinner.MaterialSpinner;
import com.unimelb.inpdr.MainActivity;
import com.unimelb.inpdr.Models.API.APIItem;
import com.unimelb.inpdr.Models.API.APIReceivable;
import com.unimelb.inpdr.Models.API.INPDRAPI;
import com.unimelb.inpdr.Models.BasicInfo;
import com.unimelb.inpdr.Models.ErrorMessage;
import com.unimelb.inpdr.Models.NpbApplicantFollowupCollection;
import com.unimelb.inpdr.Models.NpbFollowUp;
import com.unimelb.inpdr.Models.NpbFollowupCollection;
import com.unimelb.inpdr.Models.NpcApplicantFollowupCollection;
import com.unimelb.inpdr.Models.NpcFollowupCollection;
import com.unimelb.inpdr.Models.Selection;
import com.unimelb.inpdr.R;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Set;

import me.drakeet.materialdialog.MaterialDialog;

/**
 * Created by Xintong on 15/05/2016.
 */
public class FollowupCreateFragment extends Fragment implements APIReceivable {

    int patientId;
    String patientType;
    Map<String, String> npbCollection = new HashMap<>();
    Map<String, String> npbApplicantCollection = new HashMap<>();
    Map<String, String> npcCollection = new HashMap<>();
    Map<String, String> npcApplicantCollection = new HashMap<>();

    Map<String, Integer> npbIndexes = new HashMap<>();
    Map<String, Integer> npcIndexes = new HashMap<>();
    List<String> npbParams = new ArrayList<>();
    List<String> npcParams = new ArrayList<>();

    boolean isFirstTime = true;

    public FollowupCreateFragment() {
        // Required empty public constructor
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        return inflater.inflate(R.layout.fragment_followup_create, container, false);
    }

    @Override
    public void onStart(){
        super.onStart();
        ((MainActivity)getActivity()).setActionBarTitle("Create Followup");
    }

    @Override
    public void onViewCreated(View view, Bundle savedInstanceState) {
        BasicInfo.getInstance();
        NpbFollowupCollection.getInstance();
        NpbApplicantFollowupCollection.getInstance();
        NpcFollowupCollection.getInstance();
        NpcApplicantFollowupCollection.getInstance();
        Selection.getInstance();

        npbIndexes.put("symptom_bone", 0);
        npbIndexes.put("symptom_abdominal", 1);
        npbIndexes.put("symptom_pain", 2);
        npbIndexes.put("symptom_breathlessness_rate", 3);
        npbIndexes.put("symptom_breathlessness_impact", 4);
        npbIndexes.put("symptom_bleeding_rate", 5);
        npbIndexes.put("symptom_bleeding_impact", 6);
        npbIndexes.put("symptom_fatigue_rate", 7);
        npbIndexes.put("symptom_fatigue_impact", 8);
        npbIndexes.put("symptom_enlarge_organ", 9);
        npbIndexes.put("symptom_enlarge_organ_impact", 10);
        npbIndexes.put("symptom_slow_growth", 11);
        npbIndexes.put("symptom_slow_growth_impact", 12);
        npbIndexes.put("symptom_infection", 13);
        npbIndexes.put("symptom_fracture", 14);
        npbIndexes.put("symptom_cognition", 15);

        npbParams.add("symptom_bone");
        npbParams.add("symptom_abdominal");
        npbParams.add("symptom_pain");
        npbParams.add("symptom_breathlessness_rate");
        npbParams.add("symptom_breathlessness_impact");
        npbParams.add("symptom_bleeding_rate");
        npbParams.add("symptom_bleeding_impact");
        npbParams.add("symptom_fatigue_rate");
        npbParams.add("symptom_fatigue_impact");
        npbParams.add("symptom_enlarge_organ");
        npbParams.add("symptom_enlarge_organ_impact");
        npbParams.add("symptom_slow_growth");
        npbParams.add("symptom_slow_growth_impact");
        npbParams.add("symptom_infection");
        npbParams.add("symptom_fracture");
        npbParams.add("symptom_cognition");
//        npbIndexes.put("impact_family_disappointed", 16);
//        npbIndexes.put("impact_family_give_up", 17);
//        npbIndexes.put("impact_family_worry_future", 18);
//        npbIndexes.put("impact_family_closer", 19);
//        npbIndexes.put("impact_wider_self_miss_career", 20);
//        npbIndexes.put("impact_wider_carer_miss_career", 21);
//        npbIndexes.put("impact_wider_emergency", 22);
//        npbIndexes.put("impact_wider_appointment", 23);

        npcIndexes.put("impact_ambulation", 0);
        npcIndexes.put("impact_manipulation", 1);
        npcIndexes.put("impact_speech", 2);
        npcIndexes.put("impact_swallowing", 3);
        npcIndexes.put("impact_eye_movement", 4);
        npcIndexes.put("impact_seizure", 5);
        npcIndexes.put("impact_cognitive_impaired", 6);

        npcParams.add("impact_ambulation");
        npcParams.add("impact_manipulation");
        npcParams.add("impact_speech");
        npcParams.add("impact_swallowing");
        npcParams.add("impact_eye_movement");
        npcParams.add("impact_seizure");
        npcParams.add("impact_cognitive_impaired");


        Bundle data = getArguments();

        if (data != null) {
            try {
                patientId = data.getInt("followable_id");
                patientType = data.getString("followable_type");
                switch (patientType) {
                    case "NpbParticipant":
                        CreateNpbFollowup();
                        break;
                    case "NpbApplicantParticipant":
                        CreateApplicantNpbFollowup();
                        break;
                    case "NpcParticipant":
                        CreateNpcFollowup();
                        break;
                    case "NpcApplicantParticipant":
                        CreateApplicantNpcFollowup();
                        break;
                }
            }catch (Exception e){
                e.printStackTrace();
            }
        }

        ((MainActivity)getActivity()).setActionBarTitle("Create Followup");

//        NiceSpinner niceSpinner = (NiceSpinner) view.findViewById(R.id.spinner);
//        List<String> dataset = new LinkedList<>(Arrays.asList("One", "Two", "Three", "Four", "Five"));
//        niceSpinner.attachDataSource(dataset);

            // ArrayAdapter<String> adapter = new ArrayAdapter<String>(getContext(), android.R.layout.simple_spinner_dropdown_item, countries);

//        NiceSpinnerAdapter adapter = new NiceSpinnerAdapter(getContext(), dataset, Color.DKGRAY, 2);
//        spinner.setAdapter(adapter);
            // Getting object reference to listview of main.xml
//        ListView listView = (ListView) view.findViewById(R.id.listView);
//
//        // Instantiating array adapter to populate the listView
//        // The layout android.R.layout.simple_list_item_single_choice creates radio button for each listview item
//
//        ArrayAdapter<String> adapter = new ArrayAdapter<String>(getContext(), android.R.layout.simple_list_item_single_choice, countries);
//
//        listView.setAdapter(adapter);

    }



    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

    }

    @Override
    public <U extends APIItem> void receiveData(List<U> items, String type) {

    }

    @Override
    public <U extends APIItem> void receiveData(U item, String type) {
        switch (type) {
            case "createANpbFollowup":
                NpbFollowUp npbFollowUp = (NpbFollowUp)item;
                Bundle data = new Bundle();

                data.putInt("followable_id", npbFollowUp.getFollowable_id());
                data.putString("followable_type", npbFollowUp.getFollowable_type());
                data.putInt("followup_id", npbFollowUp.getId());

                Fragment followupDetailFragment = new FollowupDetailFragment();
                FragmentTransaction transaction = getFragmentManager()
                        .beginTransaction();
                transaction.add(R.id.main_view, followupDetailFragment,
                        "FragmentFollowupDetail");
                transaction.addToBackStack("FragmentFollowup");
                transaction.show(followupDetailFragment);
                transaction.setTransition(FragmentTransaction.TRANSIT_FRAGMENT_CLOSE);
                transaction.commit();
                break;
            case "createANpcFollowup":
                break;
            case "error":
                ErrorMessage error = (ErrorMessage) item;
                final MaterialDialog mMaterialDialog_e = new MaterialDialog(getContext());
                mMaterialDialog_e.setTitle("Error");
                ErrorMessage errorMessage = (ErrorMessage) item;
                mMaterialDialog_e.setMessage(errorMessage.getErrorMes());
                mMaterialDialog_e.setPositiveButton("Ok", new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        mMaterialDialog_e.dismiss();
                    }
                });
                mMaterialDialog_e.show();
                break;
        }
    }

    private void CreateNpcFollowup() {
        TextView textDescription = (TextView)getView().findViewById(R.id.text_explanation_create_followup);
        textDescription.setText("This section contains questions regarding how NP-C has affected a person's health. " +
                "It relates to a wide range of symptoms associated with NP-C. Please note that NP-C is very " +
                "variable and not all patients experience the same symptoms. For example, some people with NP-C never have seizures. " +
                "You can only choose one of the given options. If none of them fit exactly, simply choose the one that is most appropriate.");

        TextView textNpcCreate = (TextView)getView().findViewById(R.id.text_create_followup_title);
        textNpcCreate.setText("Create NP-C Followup Questionnaire");

        TextView textNpcCategory = (TextView)getView().findViewById(R.id.text_q_category_create_followup);
        textNpcCategory.setText("NP-C Impact Questions");

        int index = 0;
        final ViewGroup insertPoint = (ViewGroup) getView().findViewById(R.id.lty_create_followup);
        LayoutInflater vi = (LayoutInflater) getActivity().getSystemService(Context.LAYOUT_INFLATER_SERVICE);


        List<String> answers_impact_ambulation = new ArrayList<>();
        answers_impact_ambulation.add(" ");
        for(int i = 0; i < NpcFollowupCollection.getImpact_ambulation().size(); i++){
            answers_impact_ambulation.add(NpcFollowupCollection.getImpact_ambulation().get(i));
        }
        index = npcIndexes.get("impact_ambulation");

        NpcInflater(vi, answers_impact_ambulation, "impact_ambulation", insertPoint, npcIndexes.get("impact_ambulation"), npcCollection, BasicInfo.getNpc_participant_followups_questions());

        List<String> answers_impact_manipulation = new ArrayList<>();
        answers_impact_manipulation.add(" ");
        for(int i = 0; i < NpcFollowupCollection.getImpact_manipulation().size(); i++){
            answers_impact_manipulation.add(NpcFollowupCollection.getImpact_manipulation().get(i));
        }

        NpcInflater(vi, answers_impact_manipulation, "impact_manipulation", insertPoint, npcIndexes.get("impact_manipulation"), npcCollection, BasicInfo.getNpc_participant_followups_questions());
        index ++;

        List<String> answers_impact_speech = new ArrayList<>();
        answers_impact_speech.add(" ");
        for(int i = 0; i < NpcFollowupCollection.getImpact_speech().size(); i++){
            answers_impact_speech.add(NpcFollowupCollection.getImpact_speech().get(i));
        }

        NpcInflater(vi, answers_impact_speech, "impact_speech", insertPoint, npcIndexes.get("impact_speech"), npcCollection, BasicInfo.getNpc_participant_followups_questions());
        index ++;

        List<String> answers_impact_swallowing = new ArrayList<>();
        answers_impact_swallowing.add(" ");
        for(int i = 0; i < NpcFollowupCollection.getImpact_swallowing().size(); i++){
            answers_impact_swallowing.add(NpcFollowupCollection.getImpact_swallowing().get(i));
        }

        NpcInflater(vi, answers_impact_swallowing, "impact_swallowing", insertPoint, npcIndexes.get("impact_swallowing"), npcCollection, BasicInfo.getNpc_participant_followups_questions());
        index ++;

        List<String> answers_impact_eye_movement = new ArrayList<>();
        answers_impact_eye_movement.add(" ");
        for(int i = 0; i < NpcFollowupCollection.getImpact_eye_movement().size(); i++){
            answers_impact_eye_movement.add(NpcFollowupCollection.getImpact_eye_movement().get(i));
        }

        NpcInflater(vi, answers_impact_eye_movement, "impact_eye_movement", insertPoint, npcIndexes.get("impact_eye_movement"), npcCollection, BasicInfo.getNpc_participant_followups_questions());
        index ++;

        List<String> answers_impact_seizure = new ArrayList<>();
        answers_impact_seizure.add(" ");
        for(int i = 0; i < NpcFollowupCollection.getImpact_seizure().size(); i++){
            answers_impact_seizure.add(NpcFollowupCollection.getImpact_seizure().get(i));
        }

        NpcInflater(vi, answers_impact_seizure, "impact_seizure", insertPoint, npcIndexes.get("impact_seizure"), npcCollection, BasicInfo.getNpc_participant_followups_questions());
        index ++;

        List<String> answers_impact_cognitive_impaired = new ArrayList<>();
        answers_impact_cognitive_impaired.add(" ");
        for(int i = 0; i < NpcFollowupCollection.getImpact_cognitive_impaired().size(); i++){
            answers_impact_cognitive_impaired.add(NpcFollowupCollection.getImpact_cognitive_impaired().get(i));
        }

        NpcInflater(vi, answers_impact_cognitive_impaired, "impact_cognitive_impaired", insertPoint, npcIndexes.get("impact_cognitive_impaired"), npcCollection, BasicInfo.getNpc_participant_followups_questions());
        index ++;


        Button buttonNext = (Button) getView().findViewById(R.id.button_followup_next);
        buttonNext.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                boolean isFullAnswer = true;
                for (int i = 0; i < npcIndexes.size(); i++) {
                    if(!npcCollection.containsKey(npcParams.get(i))){
                        isFullAnswer = false;
                        TextView textQuestion = (TextView)insertPoint.getChildAt(i).findViewById(R.id.text_create_followup_question);
                        textQuestion.setHint(BasicInfo.getNpc_participant_followups_questions().get(npcParams.get(i)) + " cannot be blank");
                        textQuestion.setTextColor(Color.RED);
                    }else {
                        TextView textQuestion = (TextView)insertPoint.getChildAt(i).findViewById(R.id.text_create_followup_question);
                        textQuestion.setHint(BasicInfo.getNpc_participant_followups_questions().get(npcParams.get(i))+ " cannot be blank");
                        textQuestion.setTextColor(Color.BLACK);
                    }
                }

                if(isFullAnswer) {
                    Toast.makeText(getView().getContext(), "next!", Toast.LENGTH_SHORT).show();
                    Bundle data = new Bundle();
                    data.putInt("followable_id", patientId);
                    data.putString("followable_type", patientType);
                    for (int i = 0; i < npcIndexes.size(); i++){
                        data.putString(npcParams.get(i), npcCollection.get(npcParams.get(i)));
                    }
//                    data.putString("impact_ambulation", npcCollection.get("impact_ambulation"));
//                    data.putString("impact_manipulation", npcCollection.get("impact_manipulation"));
//                    data.putString("impact_speech", npcCollection.get("impact_speech"));
//                    data.putString("impact_swallowing", npcCollection.get("impact_swallowing"));
//                    data.putString("impact_eye_movement", npcCollection.get("impact_eye_movement"));
//                    data.putString("impact_seizure", npcCollection.get("impact_seizure"));
//                    data.putString("impact_cognitive_impaired", npcCollection.get("impact_cognitive_impaired"));
                    data.putBoolean("first_time", isFirstTime);

                    Fragment followupFamilyFragment;

                    FragmentManager fm = getActivity().getSupportFragmentManager();
                    FragmentTransaction transaction = fm.beginTransaction();
//

                    followupFamilyFragment = new FollowupFamilyFragment();


                    followupFamilyFragment.setArguments(data);
//                ft.replace(R.id.main_view, followupFamilyFragment);
//                ft.addToBackStack(null);
//                ft.commit();
                    Log.d("back", fm.getBackStackEntryCount() + "");

                    transaction.add(R.id.main_view, followupFamilyFragment,
                            "FragmentFamily"); //now you have an instance of newFragment1//now you have an instance of newFragment2
                    transaction.addToBackStack("FragmentFamily");
                    transaction.addToBackStack("FragmentCreate");
                    transaction.show(followupFamilyFragment);
                    transaction.setTransition(FragmentTransaction.TRANSIT_FRAGMENT_OPEN);
                    transaction.commit();
                    isFirstTime = false;
                }else {
                    Toast.makeText(getView().getContext(), "issue!", Toast.LENGTH_SHORT).show();
                }

            }
        });
    }

    private void CreateApplicantNpcFollowup() {
        TextView textDescription = (TextView)getView().findViewById(R.id.text_explanation_create_followup);
        textDescription.setText("This section contains questions regarding how NP-C has affected a person's health. " +
                "It relates to a wide range of symptoms associated with NP-C. Please note that NP-C is very " +
                "variable and not all patients experience the same symptoms. For example, some people with NP-C never have seizures. " +
                "You can only choose one of the given options. If none of them fit exactly, simply choose the one that is most appropriate.");

        TextView textNpcCreate = (TextView)getView().findViewById(R.id.text_create_followup_title);
        textNpcCreate.setText("Create NP-C Followup Questionnaire");

        TextView textNpcCategory = (TextView)getView().findViewById(R.id.text_q_category_create_followup);
        textNpcCategory.setText("NP-C Impact Questions");

        int index = 0;
        final ViewGroup insertPoint = (ViewGroup) getView().findViewById(R.id.lty_create_followup);
        LayoutInflater vi = (LayoutInflater) getActivity().getSystemService(Context.LAYOUT_INFLATER_SERVICE);


        List<String> answers_impact_ambulation = new ArrayList<>();
        answers_impact_ambulation.add(" ");
        for(int i = 0; i < NpcApplicantFollowupCollection.getImpact_ambulation().size(); i++){
            answers_impact_ambulation.add(NpcApplicantFollowupCollection.getImpact_ambulation().get(i));
        }

        NpcInflater(vi, answers_impact_ambulation, "impact_ambulation", insertPoint, npcIndexes.get("impact_ambulation"), npcApplicantCollection, BasicInfo.getNpc_applicant_participant_followups_questions());
        index ++;

        List<String> answers_impact_manipulation = new ArrayList<>();
        answers_impact_manipulation.add(" ");
        for(int i = 0; i < NpcApplicantFollowupCollection.getImpact_manipulation().size(); i++){
            answers_impact_manipulation.add(NpcApplicantFollowupCollection.getImpact_manipulation().get(i));
        }

        NpcInflater(vi, answers_impact_manipulation, "impact_manipulation", insertPoint, npcIndexes.get("impact_manipulation"), npcApplicantCollection, BasicInfo.getNpc_applicant_participant_followups_questions());
        index ++;

        List<String> answers_impact_speech = new ArrayList<>();
        answers_impact_speech.add(" ");
        for(int i = 0; i < NpcApplicantFollowupCollection.getImpact_speech().size(); i++){
            answers_impact_speech.add(NpcApplicantFollowupCollection.getImpact_speech().get(i));
        }

        NpcInflater(vi, answers_impact_speech, "impact_speech", insertPoint, npcIndexes.get("impact_speech"), npcApplicantCollection, BasicInfo.getNpc_applicant_participant_followups_questions());
        index ++;

        List<String> answers_impact_swallowing = new ArrayList<>();
        answers_impact_swallowing.add(" ");
        for(int i = 0; i < NpcApplicantFollowupCollection.getImpact_swallowing().size(); i++){
            answers_impact_swallowing.add(NpcApplicantFollowupCollection.getImpact_swallowing().get(i));
        }

        NpcInflater(vi, answers_impact_swallowing, "impact_swallowing", insertPoint, npcIndexes.get("impact_swallowing"), npcApplicantCollection, BasicInfo.getNpc_applicant_participant_followups_questions());
        index ++;

        List<String> answers_impact_eye_movement = new ArrayList<>();
        answers_impact_eye_movement.add(" ");
        for(int i = 0; i < NpcApplicantFollowupCollection.getImpact_eye_movement().size(); i++){
            answers_impact_eye_movement.add(NpcApplicantFollowupCollection.getImpact_eye_movement().get(i));
        }

        NpcInflater(vi, answers_impact_eye_movement, "impact_eye_movement", insertPoint, npcIndexes.get("impact_eye_movement"), npcApplicantCollection, BasicInfo.getNpc_applicant_participant_followups_questions());
        index ++;

        List<String> answers_impact_seizure = new ArrayList<>();
        answers_impact_seizure.add(" ");
        for(int i = 0; i < NpcApplicantFollowupCollection.getImpact_seizure().size(); i++){
            answers_impact_seizure.add(NpcApplicantFollowupCollection.getImpact_seizure().get(i));
        }

        NpcInflater(vi, answers_impact_seizure, "impact_seizure", insertPoint, npcIndexes.get("impact_seizure"), npcApplicantCollection, BasicInfo.getNpc_applicant_participant_followups_questions());
        index ++;

        List<String> answers_impact_cognitive_impaired = new ArrayList<>();
        answers_impact_cognitive_impaired.add(" ");
        for(int i = 0; i < NpcApplicantFollowupCollection.getImpact_cognitive_impaired().size(); i++){
            answers_impact_cognitive_impaired.add(NpcApplicantFollowupCollection.getImpact_cognitive_impaired().get(i));
        }

        NpcInflater(vi, answers_impact_cognitive_impaired, "impact_cognitive_impaired", insertPoint, npcIndexes.get("impact_cognitive_impaired"), npcApplicantCollection, BasicInfo.getNpc_applicant_participant_followups_questions());
        index ++;


        Button buttonNext = (Button) getView().findViewById(R.id.button_followup_next);
        buttonNext.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                boolean isFullAnswer = true;
                for (int i = 0; i < npcIndexes.size(); i++) {
                    if(!npcApplicantCollection.containsKey(npcParams.get(i))){
                        isFullAnswer = false;
                        TextView textQuestion = (TextView)insertPoint.getChildAt(i).findViewById(R.id.text_create_followup_question);
                        textQuestion.setHint(BasicInfo.getNpc_applicant_participant_followups_questions().get(npcParams.get(i)) + " cannot be blank");
                        textQuestion.setTextColor(Color.RED);
                    }else {
                        TextView textQuestion = (TextView)insertPoint.getChildAt(i).findViewById(R.id.text_create_followup_question);
                        textQuestion.setHint(BasicInfo.getNpc_applicant_participant_followups_questions().get(npcParams.get(i)) + " cannot be blank");
                        textQuestion.setTextColor(Color.BLACK);
                    }
                }

                if(isFullAnswer) {
                    Toast.makeText(getView().getContext(), "next!", Toast.LENGTH_SHORT).show();

                    Bundle data = new Bundle();
                    data.putInt("followable_id", patientId);
                    data.putString("followable_type", patientType);
                    for (int i = 0; i < npcIndexes.size(); i++){
                        data.putString(npcParams.get(i), npcApplicantCollection.get(npcParams.get(i)));
                    }
//                    data.putString("impact_ambulation", npcApplicantCollection.get("impact_ambulation"));
//                    data.putString("impact_manipulation", npcApplicantCollection.get("impact_manipulation"));
//                    data.putString("impact_speech", npcApplicantCollection.get("impact_speech"));
//                    data.putString("impact_swallowing", npcApplicantCollection.get("impact_swallowing"));
//                    data.putString("impact_eye_movement", npcApplicantCollection.get("impact_eye_movement"));
//                    data.putString("impact_seizure", npcApplicantCollection.get("impact_seizure"));
//                    data.putString("impact_cognitive_impaired", npcApplicantCollection.get("impact_cognitive_impaired"));
                    data.putBoolean("first_time", isFirstTime);

                    Fragment followupFamilyFragment;

                    FragmentManager fm = getActivity().getSupportFragmentManager();
                    FragmentTransaction transaction = fm.beginTransaction();
//

                    followupFamilyFragment = new FollowupFamilyFragment();


                    followupFamilyFragment.setArguments(data);
//                ft.replace(R.id.main_view, followupFamilyFragment);
//                ft.addToBackStack(null);
//                ft.commit();
                    Log.d("back", fm.getBackStackEntryCount() + "");

                    transaction.add(R.id.main_view, followupFamilyFragment,
                            "FragmentFamily"); //now you have an instance of newFragment1//now you have an instance of newFragment2
                    transaction.addToBackStack("FragmentFamily");
                    transaction.addToBackStack("FragmentCreate");
                    transaction.show(followupFamilyFragment);
                    transaction.setTransition(FragmentTransaction.TRANSIT_FRAGMENT_OPEN);
                    transaction.commit();
                    isFirstTime = false;
                }else {
                    Toast.makeText(getView().getContext(), "issue!", Toast.LENGTH_SHORT).show();
                }

            }
        });
    }
    private void NpcInflater(LayoutInflater vi, final List<String> items, final String param, ViewGroup insertPoint, int index, final Map<String, String> answers, Map<String, String> questions) {
        View v = vi.inflate(R.layout.item_create_followup, null);
        final TextView textQuestion = (TextView)v.findViewById(R.id.text_create_followup_question);

        textQuestion.setText(questions.get(param));
        MaterialSpinner spinner = (MaterialSpinner) v.findViewById(R.id.spinner);
        spinner.setItems(items);
        spinner.setTextColor(getResources().getColor(R.color.colorPrimaryText));

        spinner.setOnItemSelectedListener(new MaterialSpinner.OnItemSelectedListener<String>() {
            @Override
            public void onItemSelected(MaterialSpinner view, int position, long id, String item) {
                if (position != 0) {
                    textQuestion.setTextColor(Color.BLACK);
                    answers.put(param, position - 1 + "");
                } else {
                    if (answers.containsKey(param)) {
                        answers.remove(param);
                    }
                }
//                Context context = view.getContext();
//                CharSequence text = answers.get(param) + " " + param;
//                int duration = Toast.LENGTH_SHORT;
//
//                Toast toast = Toast.makeText(context, text, duration);
//                toast.show();
            }
        });
        spinner.onSaveInstanceState();

        insertPoint.addView(
                v,
                index,
                new ViewGroup.LayoutParams(ViewGroup.LayoutParams.MATCH_PARENT, ViewGroup.LayoutParams.WRAP_CONTENT));
    }


    private void CreateNpbFollowup() {
        final APIReceivable receivable = this;
        final INPDRAPI api = new INPDRAPI(getContext());
        int index = 0;
        final ViewGroup insertPoint = (ViewGroup) getView().findViewById(R.id.lty_create_followup);
        LayoutInflater vi = (LayoutInflater) getActivity().getSystemService(Context.LAYOUT_INFLATER_SERVICE);


        List<String> answers_bone_pain = new ArrayList<>();
        answers_bone_pain.add(" ");
        for(int i = 0; i < NpbFollowupCollection.getSymptom_bone_collection().size(); i++){
            answers_bone_pain.add(NpbFollowupCollection.getSymptom_bone_collection().get(i));
        }

        NpbInflater(vi, answers_bone_pain, "symptom_bone", insertPoint, npbIndexes.get("symptom_bone"), npbCollection, BasicInfo.getNpb_followups_questions());
        index ++;

        List<String> answers_symptom_abdominal = new ArrayList<>();
        answers_symptom_abdominal.add(" ");
        for(int i = 0; i < NpbFollowupCollection.getSymptom_abdominal_collection().size(); i++){
            answers_symptom_abdominal.add(NpbFollowupCollection.getSymptom_abdominal_collection().get(i));
        }
        NpbInflater(vi, answers_symptom_abdominal, "symptom_abdominal", insertPoint, npbIndexes.get("symptom_abdominal"), npbCollection, BasicInfo.getNpb_followups_questions());
        index++;

        List<String> answers_symptom_pain = new ArrayList<>();
        answers_symptom_pain.add(" ");
        for(int i = 0; i < NpbFollowupCollection.getSymptom_pain_collection().size(); i++){
            answers_symptom_pain.add(NpbFollowupCollection.getSymptom_pain_collection().get(i));
        }
        NpbInflater(vi, answers_symptom_pain, "symptom_pain", insertPoint, npbIndexes.get("symptom_pain"), npbCollection, BasicInfo.getNpb_followups_questions());
        index++;

        List<String> answers_symptom_breathlessness_rate = new ArrayList<>();
        answers_symptom_breathlessness_rate.add(" ");
        for(int i = 0; i < NpbFollowupCollection.getSymptom_breathlessness_rate_collection().size(); i++){
            answers_symptom_breathlessness_rate.add(NpbFollowupCollection.getSymptom_breathlessness_rate_collection().get(i));
        }
        NpbInflater(vi, answers_symptom_breathlessness_rate, "symptom_breathlessness_rate", insertPoint, npbIndexes.get("symptom_breathlessness_rate"), npbCollection, BasicInfo.getNpb_followups_questions());
        index++;

        List<String> answers_symptom_breathlessness_impact = new ArrayList<>();
        answers_symptom_breathlessness_impact.add(" ");
        for(int i = 0; i < NpbFollowupCollection.getSymptom_breathlessness_impact_collection().size(); i++){
            answers_symptom_breathlessness_impact.add(NpbFollowupCollection.getSymptom_breathlessness_impact_collection().get(i));
        }
        NpbInflater(vi, answers_symptom_breathlessness_impact, "symptom_breathlessness_impact", insertPoint, npbIndexes.get("symptom_breathlessness_impact"), npbCollection, BasicInfo.getNpb_followups_questions());
        index++;

        List<String> answers_symptom_bleeding_rate = new ArrayList<>();
        answers_symptom_bleeding_rate.add(" ");
        for(int i = 0; i < NpbFollowupCollection.getSymptom_bleeding_rate_collection().size(); i++){
            answers_symptom_bleeding_rate.add(NpbFollowupCollection.getSymptom_bleeding_rate_collection().get(i));
        }
        NpbInflater(vi, answers_symptom_bleeding_rate, "symptom_bleeding_rate", insertPoint, npbIndexes.get("symptom_bleeding_rate"), npbCollection, BasicInfo.getNpb_followups_questions());
        index++;

        List<String> answers_symptom_bleeding_impact = new ArrayList<>();
        answers_symptom_bleeding_impact.add(" ");
        for(int i = 0; i < NpbFollowupCollection.getSymptom_bleeding_impact_collection().size(); i++){
            answers_symptom_bleeding_impact.add(NpbFollowupCollection.getSymptom_bleeding_impact_collection().get(i));
        }
        NpbInflater(vi, answers_symptom_bleeding_impact, "symptom_bleeding_impact", insertPoint, npbIndexes.get("symptom_bleeding_impact"), npbCollection, BasicInfo.getNpb_followups_questions());
        index++;

        List<String> answers_symptom_fatigue_rate = new ArrayList<>();
        answers_symptom_fatigue_rate.add(" ");
        for(int i = 0; i < NpbFollowupCollection.getSymptom_fatigue_rate_collection().size(); i++){
            answers_symptom_fatigue_rate.add(NpbFollowupCollection.getSymptom_fatigue_rate_collection().get(i));
        }
        NpbInflater(vi, answers_symptom_fatigue_rate, "symptom_fatigue_rate", insertPoint, npbIndexes.get("symptom_fatigue_rate"), npbCollection, BasicInfo.getNpb_followups_questions());
        index++;

        List<String> answers_symptom_fatigue_impact = new ArrayList<>();
        answers_symptom_fatigue_impact.add(" ");
        for(int i = 0; i < NpbFollowupCollection.getSymptom_fatigue_impact_collection().size(); i++){
            answers_symptom_fatigue_impact.add(NpbFollowupCollection.getSymptom_fatigue_impact_collection().get(i));
        }
        NpbInflater(vi, answers_symptom_fatigue_impact, "symptom_fatigue_impact", insertPoint, npbIndexes.get("symptom_fatigue_impact"), npbCollection, BasicInfo.getNpb_followups_questions());
        index++;

        List<String> answers_symptom_enlarge_organ = new ArrayList<>();
        answers_symptom_enlarge_organ.add(" ");
        for(int i = 0; i < NpbFollowupCollection.getSymptom_enlarge_organ_collection().size(); i++){
            answers_symptom_enlarge_organ.add(NpbFollowupCollection.getSymptom_enlarge_organ_collection().get(i));
        }
        NpbInflater(vi, answers_symptom_enlarge_organ, "symptom_enlarge_organ", insertPoint, npbIndexes.get("symptom_enlarge_organ"), npbCollection, BasicInfo.getNpb_followups_questions());
        index++;

        List<String> answers_symptom_enlarge_organ_impact = new ArrayList<>();
        answers_symptom_enlarge_organ_impact.add(" ");
        for(int i = 0; i < NpbFollowupCollection.getSymptom_enlarge_organ_impact_collection().size(); i++){
            answers_symptom_enlarge_organ_impact.add(NpbFollowupCollection.getSymptom_enlarge_organ_impact_collection().get(i));
        }
        NpbInflater(vi, answers_symptom_enlarge_organ_impact, "symptom_enlarge_organ_impact", insertPoint, npbIndexes.get("symptom_enlarge_organ_impact"), npbCollection, BasicInfo.getNpb_followups_questions());
        index++;

        List<String> answers_symptom_slow_growth = new ArrayList<>();
        answers_symptom_slow_growth.add(" ");
        for(int i = 0; i < NpbFollowupCollection.getSymptom_slow_growth_collection().size(); i++){
            answers_symptom_slow_growth.add(NpbFollowupCollection.getSymptom_slow_growth_collection().get(i));
        }
        NpbInflater(vi, answers_symptom_slow_growth, "symptom_slow_growth", insertPoint, npbIndexes.get("symptom_slow_growth"), npbCollection, BasicInfo.getNpb_followups_questions());
        index++;

        List<String> answers_symptom_slow_growth_impact = new ArrayList<>();
        answers_symptom_slow_growth_impact.add(" ");
        for(int i = 0; i < NpbFollowupCollection.getSymptom_slow_growth_impact_collection().size(); i++){
            answers_symptom_slow_growth_impact.add(NpbFollowupCollection.getSymptom_slow_growth_impact_collection().get(i));
        }
        NpbInflater(vi, answers_symptom_slow_growth_impact, "symptom_slow_growth_impact", insertPoint, npbIndexes.get("symptom_slow_growth_impact"), npbCollection, BasicInfo.getNpb_followups_questions());
        index++;

        List<String> answers_symptom_infection = new ArrayList<>();
        answers_symptom_infection.add(" ");
        for(int i = 0; i < NpbFollowupCollection.getSymptom_infection_collection().size(); i++){
            answers_symptom_infection.add(NpbFollowupCollection.getSymptom_infection_collection().get(i));
        }
        NpbInflater(vi, answers_symptom_infection, "symptom_infection", insertPoint, npbIndexes.get("symptom_infection"), npbCollection, BasicInfo.getNpb_followups_questions());
        index++;

        List<String> answers_symptom_fracture = new ArrayList<>();
        answers_symptom_fracture.add(" ");
        for(int i = 0; i < NpbFollowupCollection.getSymptom_fracture_collection().size(); i++){
            answers_symptom_fracture.add(NpbFollowupCollection.getSymptom_fracture_collection().get(i));
        }
        NpbInflater(vi, answers_symptom_fracture, "symptom_fracture", insertPoint, npbIndexes.get("symptom_fracture"), npbCollection, BasicInfo.getNpb_followups_questions());
        index++;

        List<String> answers_symptom_cognition = new ArrayList<>();
        answers_symptom_cognition.add(" ");
        for(int i = 0; i < NpbFollowupCollection.getSymptom_cognition_collection().size(); i++){
            answers_symptom_cognition.add(NpbFollowupCollection.getSymptom_cognition_collection().get(i));
        }
        NpbInflater(vi, answers_symptom_cognition, "symptom_cognition", insertPoint, npbIndexes.get("symptom_cognition"), npbCollection, BasicInfo.getNpb_followups_questions());
        index++;

        Button buttonNext = (Button) getView().findViewById(R.id.button_followup_next);
        buttonNext.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                boolean isFullAnswer = true;
                for (int i = 0; i < npbIndexes.size(); i++) {
                    if(!npbCollection.containsKey(npbParams.get(i))){
                        isFullAnswer = false;
                        Log.d("lack of param", npbParams.get(i) + " " + i);
                        TextView textQuestion = (TextView)insertPoint.getChildAt(i).findViewById(R.id.text_create_followup_question);
                        textQuestion.setHint(BasicInfo.getNpb_followups_questions().get(npbParams.get(i)) + " cannot be blank");
                        textQuestion.setTextColor(Color.RED);
                    }else {
                        Log.d("got param", npbParams.get(i) + " " + i);
                        TextView textQuestion = (TextView)insertPoint.getChildAt(i).findViewById(R.id.text_create_followup_question);
                        textQuestion.setHint(BasicInfo.getNpb_followups_questions().get(npbParams.get(i)) + " cannot be blank");
                        textQuestion.setTextColor(Color.BLACK);
                    }
                }

                if(isFullAnswer) {
                    Toast.makeText(getView().getContext(), "next!", Toast.LENGTH_SHORT).show();

                    Bundle data = new Bundle();
                    data.putInt("followable_id", patientId);
                    data.putString("followable_type", patientType);
                    for (int i = 0; i < npbIndexes.size(); i++){
                        data.putString(npbParams.get(i), npbCollection.get(npbParams.get(i)));
                    }
//                    data.putString("symptom_bone", npbCollection.get("symptom_bone"));
//                    data.putString("symptom_abdominal", npbCollection.get("symptom_abdominal"));
//                    data.putString("symptom_pain", npbCollection.get("symptom_pain"));
//                    data.putString("symptom_cognition", npbCollection.get("symptom_cognition"));
//                    data.putString("symptom_breathlessness_rate", npbCollection.get("symptom_breathlessness_rate"));
//                    data.putString("symptom_breathlessness_impact", npbCollection.get("symptom_breathlessness_impact"));
//                    data.putString("symptom_bleeding_rate", npbCollection.get("symptom_bleeding_rate"));
//                    data.putString("symptom_bleeding_impact", npbCollection.get("symptom_bleeding_impact"));
//                    data.putString("symptom_fatigue_rate", npbCollection.get("symptom_fatigue_rate"));
//                    data.putString("symptom_fatigue_impact", npbCollection.get("symptom_fatigue_impact"));
//                    data.putString("symptom_enlarge_organ", npbCollection.get("symptom_enlarge_organ"));
//                    data.putString("symptom_enlarge_organ_impact", npbCollection.get("symptom_enlarge_organ_impact"));
//                    data.putString("symptom_slow_growth", npbCollection.get("symptom_slow_growth"));
//                    data.putString("symptom_slow_growth_impact", npbCollection.get("symptom_slow_growth_impact"));
//                    data.putString("symptom_infection", npbCollection.get("symptom_infection"));
//                    data.putString("symptom_fracture", npbCollection.get("symptom_fracture"));
//                    data.putString("impact_family_disappointed", npbCollection.get("impact_family_disappointed"));
//                    data.putString("impact_family_give_up", npbCollection.get("impact_family_give_up"));
//                    data.putString("impact_family_worry_future", npbCollection.get("impact_family_worry_future"));
//                    data.putString("impact_family_closer", npbCollection.get("impact_family_closer"));
//                    data.putString("impact_wider_self_miss_career", npbCollection.get("impact_wider_self_miss_career"));
//                    data.putString("impact_wider_carer_miss_career", npbCollection.get("impact_wider_carer_miss_career"));
//                    data.putString("impact_wider_emergency", npbCollection.get("impact_wider_emergency"));
//                    data.putString("impact_wider_appointment", npbCollection.get("impact_wider_appointment"));
                    data.putBoolean("first_time", isFirstTime);

                    Fragment followupFamilyFragment;

                    FragmentManager fm = getActivity().getSupportFragmentManager();
                    FragmentTransaction transaction = fm.beginTransaction();
//

                    followupFamilyFragment = new FollowupFamilyFragment();


                    followupFamilyFragment.setArguments(data);
//                ft.replace(R.id.main_view, followupFamilyFragment);
//                ft.addToBackStack(null);
//                ft.commit();
                    Log.d("back", fm.getBackStackEntryCount() + "");

                    transaction.add(R.id.main_view, followupFamilyFragment,
                            "FragmentFamily"); //now you have an instance of newFragment1//now you have an instance of newFragment2
                    transaction.addToBackStack("FragmentFamily");
                    transaction.addToBackStack("FragmentCreate");
                    transaction.show(followupFamilyFragment);
                    transaction.setTransition(FragmentTransaction.TRANSIT_FRAGMENT_OPEN);
                    transaction.commit();
                    isFirstTime = false;
                }else {
                    Toast.makeText(getView().getContext(), "issue!", Toast.LENGTH_SHORT).show();
                }
            }
        });

//        Spinner spinner = (Spinner) v.findViewById(R.id.spinner);
//        ArrayAdapter<String> adapter = new ArrayAdapter<String>(getContext(), android.R.layout.simple_spinner_dropdown_item, answers_bone_pain);
//
//        spinner.setAdapter(adapter);

    }

    private void NpbInflater(LayoutInflater vi, final List<String> items, final String param, ViewGroup insertPoint, int index, final Map<String, String> answers, Map<String, String> questions) {
        View v = vi.inflate(R.layout.item_create_followup, null);
        final TextView textQuestion = (TextView)v.findViewById(R.id.text_create_followup_question);

        textQuestion.setText(questions.get(param));
        MaterialSpinner spinner = (MaterialSpinner) v.findViewById(R.id.spinner);
        spinner.setItems(items);
        spinner.setTextColor(getResources().getColor(R.color.colorPrimaryText));

        spinner.setOnItemSelectedListener(new MaterialSpinner.OnItemSelectedListener<String>() {

            @Override
            public void onItemSelected(MaterialSpinner view, int position, long id, String item) {
                if (position != 0) {
                    textQuestion.setTextColor(Color.BLACK);
                    answers.put(param, position - 1 + "");
                }else {
                    if(answers.containsKey(param)){
                        answers.remove(param);
                    }
                }
//                Context context = view.getContext();
//                CharSequence text = answers.get(param) + " " + param;
//                int duration = Toast.LENGTH_SHORT;
//
//                Toast toast = Toast.makeText(context, text, duration);
//                toast.show();
            }
        });
        spinner.onSaveInstanceState();

        insertPoint.addView(
                v,
                index,
                new ViewGroup.LayoutParams(ViewGroup.LayoutParams.MATCH_PARENT, ViewGroup.LayoutParams.WRAP_CONTENT));
    }


    private void CreateApplicantNpbFollowup() {
        final APIReceivable receivable = this;
        final INPDRAPI api = new INPDRAPI(getContext());
        int index = 0;
        final ViewGroup insertPoint = (ViewGroup) getView().findViewById(R.id.lty_create_followup);
        LayoutInflater vi = (LayoutInflater) getActivity().getSystemService(Context.LAYOUT_INFLATER_SERVICE);


        List<String> answers_bone_pain = new ArrayList<>();
        answers_bone_pain.add(" ");
        for(int i = 0; i < NpbApplicantFollowupCollection.getSymptom_bone_collection().size(); i++){
            answers_bone_pain.add(NpbApplicantFollowupCollection.getSymptom_bone_collection().get(i));
        }

        NpbInflater(vi, answers_bone_pain, "symptom_bone", insertPoint, npbIndexes.get("symptom_bone"), npbApplicantCollection, BasicInfo.getNpb_applicant_participant_followups_questions());
        index ++;

        List<String> answers_symptom_abdominal = new ArrayList<>();
        answers_symptom_abdominal.add(" ");
        for(int i = 0; i < NpbApplicantFollowupCollection.getSymptom_abdominal_collection().size(); i++){
            answers_symptom_abdominal.add(NpbApplicantFollowupCollection.getSymptom_abdominal_collection().get(i));
        }
        NpbInflater(vi, answers_symptom_abdominal, "symptom_abdominal", insertPoint, npbIndexes.get("symptom_abdominal"), npbApplicantCollection, BasicInfo.getNpb_applicant_participant_followups_questions());
        index++;

        List<String> answers_symptom_pain = new ArrayList<>();
        answers_symptom_pain.add(" ");
        for(int i = 0; i < NpbApplicantFollowupCollection.getSymptom_pain_collection().size(); i++){
            answers_symptom_pain.add(NpbApplicantFollowupCollection.getSymptom_pain_collection().get(i));
        }
        NpbInflater(vi, answers_symptom_pain, "symptom_pain", insertPoint, npbIndexes.get("symptom_pain"), npbApplicantCollection, BasicInfo.getNpb_applicant_participant_followups_questions());
        index++;

        List<String> answers_symptom_breathlessness_rate = new ArrayList<>();
        answers_symptom_breathlessness_rate.add(" ");
        for(int i = 0; i < NpbApplicantFollowupCollection.getSymptom_breathlessness_rate_collection().size(); i++){
            answers_symptom_breathlessness_rate.add(NpbApplicantFollowupCollection.getSymptom_breathlessness_rate_collection().get(i));
        }
        NpbInflater(vi, answers_symptom_breathlessness_rate, "symptom_breathlessness_rate", insertPoint, npbIndexes.get("symptom_breathlessness_rate"), npbApplicantCollection, BasicInfo.getNpb_applicant_participant_followups_questions());
        index++;

        List<String> answers_symptom_breathlessness_impact = new ArrayList<>();
        answers_symptom_breathlessness_impact.add(" ");
        for(int i = 0; i < NpbApplicantFollowupCollection.getSymptom_breathlessness_impact_collection().size(); i++){
            answers_symptom_breathlessness_impact.add(NpbApplicantFollowupCollection.getSymptom_breathlessness_impact_collection().get(i));
        }
        NpbInflater(vi, answers_symptom_breathlessness_impact, "symptom_breathlessness_impact", insertPoint, npbIndexes.get("symptom_breathlessness_impact"), npbApplicantCollection, BasicInfo.getNpb_applicant_participant_followups_questions());
        index++;

        List<String> answers_symptom_bleeding_rate = new ArrayList<>();
        answers_symptom_bleeding_rate.add(" ");
        for(int i = 0; i < NpbApplicantFollowupCollection.getSymptom_bleeding_rate_collection().size(); i++){
            answers_symptom_bleeding_rate.add(NpbApplicantFollowupCollection.getSymptom_bleeding_rate_collection().get(i));
        }
        NpbInflater(vi, answers_symptom_bleeding_rate, "symptom_bleeding_rate", insertPoint, npbIndexes.get("symptom_bleeding_rate"), npbApplicantCollection, BasicInfo.getNpb_applicant_participant_followups_questions());
        index++;

        List<String> answers_symptom_bleeding_impact = new ArrayList<>();
        answers_symptom_bleeding_impact.add(" ");
        for(int i = 0; i < NpbApplicantFollowupCollection.getSymptom_bleeding_impact_collection().size(); i++){
            answers_symptom_bleeding_impact.add(NpbApplicantFollowupCollection.getSymptom_bleeding_impact_collection().get(i));
        }
        NpbInflater(vi, answers_symptom_bleeding_impact, "symptom_bleeding_impact", insertPoint, npbIndexes.get("symptom_bleeding_impact"), npbApplicantCollection, BasicInfo.getNpb_applicant_participant_followups_questions());
        index++;

        List<String> answers_symptom_fatigue_rate = new ArrayList<>();
        answers_symptom_fatigue_rate.add(" ");
        for(int i = 0; i < NpbApplicantFollowupCollection.getSymptom_fatigue_rate_collection().size(); i++){
            answers_symptom_fatigue_rate.add(NpbApplicantFollowupCollection.getSymptom_fatigue_rate_collection().get(i));
        }
        NpbInflater(vi, answers_symptom_fatigue_rate, "symptom_fatigue_rate", insertPoint, npbIndexes.get("symptom_fatigue_rate"), npbApplicantCollection, BasicInfo.getNpb_applicant_participant_followups_questions());
        index++;

        List<String> answers_symptom_fatigue_impact = new ArrayList<>();
        answers_symptom_fatigue_impact.add(" ");
        for(int i = 0; i < NpbApplicantFollowupCollection.getSymptom_fatigue_impact_collection().size(); i++){
            answers_symptom_fatigue_impact.add(NpbApplicantFollowupCollection.getSymptom_fatigue_impact_collection().get(i));
        }
        NpbInflater(vi, answers_symptom_fatigue_impact, "symptom_fatigue_impact", insertPoint, npbIndexes.get("symptom_fatigue_impact"), npbApplicantCollection, BasicInfo.getNpb_applicant_participant_followups_questions());
        index++;

        List<String> answers_symptom_enlarge_organ = new ArrayList<>();
        answers_symptom_enlarge_organ.add(" ");
        for(int i = 0; i < NpbApplicantFollowupCollection.getSymptom_enlarge_organ_collection().size(); i++){
            answers_symptom_enlarge_organ.add(NpbApplicantFollowupCollection.getSymptom_enlarge_organ_collection().get(i));
        }
        NpbInflater(vi, answers_symptom_enlarge_organ, "symptom_enlarge_organ", insertPoint, npbIndexes.get("symptom_enlarge_organ"), npbApplicantCollection, BasicInfo.getNpb_applicant_participant_followups_questions());
        index++;

        List<String> answers_symptom_enlarge_organ_impact = new ArrayList<>();
        answers_symptom_enlarge_organ_impact.add(" ");
        for(int i = 0; i < NpbApplicantFollowupCollection.getSymptom_enlarge_organ_impact_collection().size(); i++){
            answers_symptom_enlarge_organ_impact.add(NpbApplicantFollowupCollection.getSymptom_enlarge_organ_impact_collection().get(i));
        }
        NpbInflater(vi, answers_symptom_enlarge_organ_impact, "symptom_enlarge_organ_impact", insertPoint, npbIndexes.get("symptom_enlarge_organ_impact"), npbApplicantCollection, BasicInfo.getNpb_applicant_participant_followups_questions());
        index++;

        List<String> answers_symptom_slow_growth = new ArrayList<>();
        answers_symptom_slow_growth.add(" ");
        for(int i = 0; i < NpbApplicantFollowupCollection.getSymptom_slow_growth_collection().size(); i++){
            answers_symptom_slow_growth.add(NpbApplicantFollowupCollection.getSymptom_slow_growth_collection().get(i));
        }
        NpbInflater(vi, answers_symptom_slow_growth, "symptom_slow_growth", insertPoint, npbIndexes.get("symptom_slow_growth"), npbApplicantCollection, BasicInfo.getNpb_applicant_participant_followups_questions());
        index++;

        List<String> answers_symptom_slow_growth_impact = new ArrayList<>();
        answers_symptom_slow_growth_impact.add(" ");
        for(int i = 0; i < NpbApplicantFollowupCollection.getSymptom_slow_growth_impact_collection().size(); i++){
            answers_symptom_slow_growth_impact.add(NpbApplicantFollowupCollection.getSymptom_slow_growth_impact_collection().get(i));
        }
        NpbInflater(vi, answers_symptom_slow_growth_impact, "symptom_slow_growth_impact", insertPoint, npbIndexes.get("symptom_slow_growth_impact"), npbApplicantCollection, BasicInfo.getNpb_applicant_participant_followups_questions());
        index++;

        List<String> answers_symptom_infection = new ArrayList<>();
        answers_symptom_infection.add(" ");
        for(int i = 0; i < NpbApplicantFollowupCollection.getSymptom_infection_collection().size(); i++){
            answers_symptom_infection.add(NpbApplicantFollowupCollection.getSymptom_infection_collection().get(i));
        }
        NpbInflater(vi, answers_symptom_infection, "symptom_infection", insertPoint, npbIndexes.get("symptom_infection"), npbApplicantCollection, BasicInfo.getNpb_applicant_participant_followups_questions());
        index++;

        List<String> answers_symptom_fracture = new ArrayList<>();
        answers_symptom_fracture.add(" ");
        for(int i = 0; i < NpbApplicantFollowupCollection.getSymptom_fracture_collection().size(); i++){
            answers_symptom_fracture.add(NpbApplicantFollowupCollection.getSymptom_fracture_collection().get(i));
        }
        NpbInflater(vi, answers_symptom_fracture, "symptom_fracture", insertPoint, npbIndexes.get("symptom_fracture"), npbApplicantCollection, BasicInfo.getNpb_applicant_participant_followups_questions());
        index++;

        List<String> answers_symptom_cognition = new ArrayList<>();
        answers_symptom_cognition.add(" ");
        for(int i = 0; i < NpbApplicantFollowupCollection.getSymptom_cognition_collection().size(); i++){
            answers_symptom_cognition.add(NpbFollowupCollection.getSymptom_cognition_collection().get(i));
        }
        NpbInflater(vi, answers_symptom_cognition, "symptom_cognition", insertPoint, npbIndexes.get("symptom_cognition"), npbApplicantCollection, BasicInfo.getNpb_applicant_participant_followups_questions());
        index++;

        Button buttonNext = (Button) getView().findViewById(R.id.button_followup_next);
        buttonNext.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                boolean isFullAnswer = true;
                for (int i = 0; i < npbIndexes.size(); i++) {
                    if(!npbApplicantCollection.containsKey(npbParams.get(i))){
                        isFullAnswer = false;
                        TextView textQuestion = (TextView)insertPoint.getChildAt(i).findViewById(R.id.text_create_followup_question);
                        textQuestion.setHint(BasicInfo.getNpb_applicant_participant_followups_questions().get(npbParams.get(i)) + " cannot be blank");
                        textQuestion.setTextColor(Color.RED);
                    } else {
                        TextView textQuestion = (TextView) insertPoint.getChildAt(i).findViewById(R.id.text_create_followup_question);
                        textQuestion.setHint(BasicInfo.getNpb_applicant_participant_followups_questions().get(npbParams.get(i)) + " cannot be blank");
                        textQuestion.setTextColor(Color.BLACK);
                    }
                }

                if(isFullAnswer) {
                    Toast.makeText(getView().getContext(), "next!", Toast.LENGTH_SHORT).show();

                    Bundle data = new Bundle();
                    data.putInt("followable_id", patientId);
                    data.putString("followable_type", patientType);
                    for (int i = 0; i < npbIndexes.size(); i++){
                        data.putString(npbParams.get(i), npbApplicantCollection.get(npbParams.get(i)));
                    }
//                    data.putString("symptom_bone", npbApplicantCollection.get("symptom_bone"));
//                    data.putString("symptom_abdominal", npbApplicantCollection.get("symptom_abdominal"));
//                    data.putString("symptom_pain", npbApplicantCollection.get("symptom_pain"));
//                    data.putString("symptom_cognition", npbApplicantCollection.get("symptom_cognition"));
//                    data.putString("symptom_breathlessness_rate", npbApplicantCollection.get("symptom_breathlessness_rate"));
//                    data.putString("symptom_breathlessness_impact", npbApplicantCollection.get("symptom_breathlessness_impact"));
//                    data.putString("symptom_bleeding_rate", npbApplicantCollection.get("symptom_bleeding_rate"));
//                    data.putString("symptom_bleeding_impact", npbApplicantCollection.get("symptom_bleeding_impact"));
//                    data.putString("symptom_fatigue_rate", npbApplicantCollection.get("symptom_fatigue_rate"));
//                    data.putString("symptom_fatigue_impact", npbApplicantCollection.get("symptom_fatigue_impact"));
//                    data.putString("symptom_enlarge_organ", npbApplicantCollection.get("symptom_enlarge_organ"));
//                    data.putString("symptom_enlarge_organ_impact", npbApplicantCollection.get("symptom_enlarge_organ_impact"));
//                    data.putString("symptom_slow_growth", npbApplicantCollection.get("symptom_slow_growth"));
//                    data.putString("symptom_slow_growth_impact", npbApplicantCollection.get("symptom_slow_growth_impact"));
//                    data.putString("symptom_infection", npbApplicantCollection.get("symptom_infection"));
//                    data.putString("symptom_fracture", npbApplicantCollection.get("symptom_fracture"));
//                    data.putString("impact_family_disappointed", npbApplicantCollection.get("impact_family_disappointed"));
//                    data.putString("impact_family_give_up", npbApplicantCollection.get("impact_family_give_up"));
//                    data.putString("impact_family_worry_future", npbApplicantCollection.get("impact_family_worry_future"));
//                    data.putString("impact_family_closer", npbApplicantCollection.get("impact_family_closer"));
//                    data.putString("impact_wider_emergency", npbApplicantCollection.get("impact_wider_emergency"));
//                    data.putString("impact_wider_appointment", npbApplicantCollection.get("impact_wider_appointment"));
//                    data.putString("impact_wider_self_miss_career", npbApplicantCollection.get("impact_wider_self_miss_career"));
//                    data.putString("impact_wider_carer_miss_career", npbApplicantCollection.get("impact_wider_carer_miss_career"));
                    data.putBoolean("first_time", isFirstTime);

                    Fragment followupFamilyFragment;

                    FragmentManager fm = getActivity().getSupportFragmentManager();
                    FragmentTransaction transaction = fm.beginTransaction();
//

                    followupFamilyFragment = new FollowupFamilyFragment();


                    followupFamilyFragment.setArguments(data);
//                ft.replace(R.id.main_view, followupFamilyFragment);
//                ft.addToBackStack(null);
//                ft.commit();
                    Log.d("back", fm.getBackStackEntryCount() + "");

                    transaction.add(R.id.main_view, followupFamilyFragment,
                            "FragmentFamily"); //now you have an instance of newFragment1//now you have an instance of newFragment2
                    transaction.addToBackStack("FragmentFamily");
                    transaction.addToBackStack("FragmentCreate");
                    transaction.show(followupFamilyFragment);
                    transaction.setTransition(FragmentTransaction.TRANSIT_FRAGMENT_OPEN);
                    transaction.commit();
                    isFirstTime = false;
                }else {
                    Toast.makeText(getView().getContext(), "issue!", Toast.LENGTH_SHORT).show();
                }
            }
        });
    }



}