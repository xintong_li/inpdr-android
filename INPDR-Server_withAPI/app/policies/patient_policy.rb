class PatientPolicy
  attr_reader :user, :record

  def initialize(user, record)
    @user = user
    @record = record
  end

  def index?
    defined? @user
  end

  def show?
    @user.admin? || @user.study_team? || @record.user == @user
  end

  def create?
    defined? @user
  end

  def new?
    create?
  end

  def update?
    user.study_team? || @record.user == @user
  end

  def edit?
    update?
  end

  def destroy?
    false
  end

  def verify?
    user.study_team?
  end

  def consent?
    consent?
  end

  def consent_action?
    create?
  end

  def scope
    Pundit.policy_scope!(user, record.class)
  end

  class Scope
    attr_reader :user, :scope

    def initialize(user, scope)
      @user = user
      @scope = scope
    end

    def resolve
      if user.study_team?
        scope.all
      else
        scope.where(user: user)
      end
    end
  end
end
