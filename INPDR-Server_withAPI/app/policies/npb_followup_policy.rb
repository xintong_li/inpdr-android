class NpbFollowupPolicy
  attr_reader :user, :record

  def initialize(user, record)
    @user = user
    @record = record
  end

  def index?
    true
  end

  def show?
    @user.admin? || @user.study_team? || (@record.followable && @record.followable.patient && @record.followable.patient.user == @user)
  end

  def create?
    true
  end

  def new?
    create?
  end

  def update?
    @user.admin? || @user.study_team? || (@record.followable && @record.followable.patient && @record.followable.patient.user == @user)
  end

  def edit?
    update?
  end

  def destroy?
    @user.admin? || @user.study_team?
  end

  def scope
    Pundit.policy_scope!(user, record.class)
  end

  class Scope
    attr_reader :user, :scope

    def initialize(user, scope)
      @user = user
      @scope = scope
    end

    def resolve
      if user.study_team?
        scope.all
      else
        scope.joins(:followable).joins(:patient).where('patients.user_id' => user)
      end
    end
  end
end
