class ParticipantMailer < ApplicationMailer
  #Email sent when a participant had been verified on a existing user account
  def confirmation_email_existing(user)
    @user = user

    mail(to: @user.email, subject: t('devise.mailer.confirmation_instructions.subject'))
  end

  #Email sent when a new user account had been set after verify
  def confirmation_email(user, temporary_password)
    @user = user
    @temp_password = temporary_password

    mail(to: @user.email, subject: t('devise.mailer.confirmation_instructions.subject'))
  end

  #Email sent when someone without an account registered for INPDR
  def enrolment_email(email)
    mail(to: email, subject: t('mailer.registration_mailer.subject'))
  end

  #Email send when a participant had been verified
  def verify_email(participant)
    @participant = participant
    mail(to: participant.user.email, subject: t('mailer.verify_email.subject'))
  end
end
