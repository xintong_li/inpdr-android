class ApplicationMailer < ActionMailer::Base
  default from: "admin@inpdr.org"
  layout 'mailer'
end
