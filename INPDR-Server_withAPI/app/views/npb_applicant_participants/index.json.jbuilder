json.array!(@npb_applicant_participants) do |npb_applicant_participant|
  json.extract! npb_applicant_participant, :id
  json.url npb_applicant_participant_url(npb_applicant_participant, format: :json)
end
