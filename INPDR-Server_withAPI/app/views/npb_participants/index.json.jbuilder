json.array!(@npb_participants) do |npb_participant|
  json.extract! npb_participant, :id
  json.url npb_participant_url(npb_participant, format: :json)
end
