json.array!(@npc_followups) do |npc_followup|
  json.extract! npc_followup, :id
  json.url npc_followup_url(npc_followup, format: :json)
end
