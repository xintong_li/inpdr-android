json.array!(@npc_applicant_participants) do |npc_applicant_participant|
  json.extract! npc_applicant_participant, :id
  json.url npc_applicant_participant_url(npc_applicant_participant, format: :json)
end
