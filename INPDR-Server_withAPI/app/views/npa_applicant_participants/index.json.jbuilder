json.array!(@npa_applicant_participants) do |npa_applicant_participant|
  json.extract! npa_applicant_participant, :id
  json.url npa_applicant_participant_url(npa_applicant_participant, format: :json)
end
