json.array!(@npc_participants) do |npc_participant|
  json.extract! npc_participant, :id
  json.url npc_participant_url(npc_participant, format: :json)
end
