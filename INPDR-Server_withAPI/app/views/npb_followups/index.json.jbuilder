json.array!(@npb_followups) do |npb_followup|
  json.extract! npb_followup, :id
  json.url npb_followup_url(npb_followup, format: :json)
end
