# Place all the behaviors and hooks related to the matching controller here.
# All this logic will automatically be available in application.js.
# You can use CoffeeScript in this file: http://coffeescript.org/

ready = ->
    checkRelationship = ->
        selected = $('#patient_relation_to_patient option').filter(':selected').text()
        if selected is 'Self'
            $('.patient_patient_first_name').hide()
            $('.patient_patient_last_name').hide()
            $('.patient_gender').hide()
            $('.patient_date_of_birth').hide()
        else
            $('.patient_patient_first_name').show()
            $('.patient_patient_last_name').show()
            $('.patient_gender').show()
            $('.patient_date_of_birth').show()

    root = exports ? this
    root.setPatientNotFinish = (status) ->
        $("#patient_not_finish").val(status)

    jQuery ($) ->
        checkRelationship()

        $(document).on "change", '#patient_relation_to_patient', ->
            checkRelationship()

$(document).ready(ready)
$(document).on('page:load', ready)

