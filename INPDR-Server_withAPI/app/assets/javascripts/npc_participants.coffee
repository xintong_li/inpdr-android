# Place all the behaviors and hooks related to the matching controller here.
# All this logic will automatically be available in application.js.
# You can use CoffeeScript in this file: http://coffeescript.org/

ready = ->
    checkSymptoms = ->
        year = $('#npc_participant_symptom_age_year option').filter(':selected').text()
        month = $('#npc_participant_symptom_age_month option').filter(':selected').text()
        if (year || month)
            $('#npc_participant_symptom_age_na').prop('disabled', true);
            $('#npc_participant_symptom_age_na')[0].checked = null;
            $('#npc_participant_symptom_age_unknown').prop('disabled', true);
            $('#npc_participant_symptom_age_unknown')[0].checked = null;
        else
            $('#npc_participant_symptom_age_na').prop('disabled', false);
            $('#npc_participant_symptom_age_unknown').prop('disabled', false);

    checkConfirmByDoctor = ->
        year = $('#npc_participant_age_diagnosed_year option').filter(':selected').text()
        month = $('#npc_participant_age_diagnosed_month option').filter(':selected').text()
        if (year || month)
            $('#npc_participant_age_diagnosed_known').prop('disabled', true);
            $('#npc_participant_age_diagnosed_known')[0].checked = null;
        else
            $('#npc_participant_age_diagnosed_known').prop('disabled', false);

    root = exports ? this
    root.setNpcParticipantNotFinish = (status) ->
        $("#npc_participant_not_finish").val(status)

    jQuery ($) ->
        checkSymptoms()
        checkConfirmByDoctor()

        $(document).on "change", '#npc_participant_symptom_age_year', ->
            checkSymptoms()
        $(document).on "change", '#npc_participant_symptom_age_month', ->
            checkSymptoms()
        $(document).on "change", '#npc_participant_age_diagnosed_year', ->
            checkConfirmByDoctor()
        $(document).on "change", '#npc_participant_age_diagnosed_month', ->
            checkConfirmByDoctor()
        

$(document).ready(ready)
$(document).on('page:load', ready)