# Place all the behaviors and hooks related to the matching controller here.
# All this logic will automatically be available in application.js.
# You can use CoffeeScript in this file: http://coffeescript.org/
ready = ->
    checkRelationship = ->
        selected = $('#npa_applicant_participant_applicant_relation option').filter(':selected').text()
        if selected is 'Other'
            $('.npa_applicant_participant_applicant_relation_specify').show()
        else
            $('.npa_applicant_participant_applicant_relation_specify').hide()
    checkSymptoms = ->
        year = $('#npa_applicant_participant_symptom_age_year option').filter(':selected').text()
        month = $('#npa_applicant_participant_symptom_age_month option').filter(':selected').text()
        if (year || month)
            $('#npa_applicant_participant_symptom_age_na').prop('disabled', true);
            $('#npa_applicant_participant_symptom_age_na')[0].checked = null;
            $('#npa_applicant_participant_symptom_age_unknown').prop('disabled', true);
            $('#npa_applicant_participant_symptom_age_unknown')[0].checked = null;
        else
            $('#npa_applicant_participant_symptom_age_na').prop('disabled', false);
            $('#npa_applicant_participant_symptom_age_unknown').prop('disabled', false);

    checkConfirmByDoctor = ->
        year = $('#npa_applicant_participant_age_diagnosed_year option').filter(':selected').text()
        month = $('#npa_applicant_participant_age_diagnosed_month option').filter(':selected').text()
        if (year || month)
            $('#npa_applicant_participant_age_diagnosed_known').prop('disabled', true);
            $('#npa_applicant_participant_age_diagnosed_known')[0].checked = null;
        else
            $('#npa_applicant_participant_age_diagnosed_known').prop('disabled', false);

    root = exports ? this
    root.setNpaApplicantParticipantNotFinish = (status) ->
        $("#npa_applicant_participant_not_finish").val(status)

    jQuery ($) ->
        checkSymptoms()
        checkConfirmByDoctor()
        checkRelationship()
        $(document).on "change", '#npa_applicant_participant_symptom_age_year', ->
            checkSymptoms()
        $(document).on "change", '#npa_applicant_participant_symptom_age_month', ->
            checkSymptoms()
        $(document).on "change", '#npa_applicant_participant_age_diagnosed_year', ->
            checkConfirmByDoctor()
        $(document).on "change", '#npa_applicant_participant_age_diagnosed_month', ->
            checkConfirmByDoctor()
        $(document).on "change", '#npa_applicant_participant_applicant_relation', ->
            checkRelationship()

$(document).ready(ready)
$(document).on('page:load', ready)
