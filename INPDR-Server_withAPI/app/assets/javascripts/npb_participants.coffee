# Place all the behaviors and hooks related to the matching controller here.
# All this logic will automatically be available in application.js.
# You can use CoffeeScript in this file: http://coffeescript.org/


ready = ->
    checkSymptoms = ->
        year = $('#npb_participant_symptom_age_year option').filter(':selected').text()
        month = $('#npb_participant_symptom_age_month option').filter(':selected').text()
        if (year || month)
            $('#npb_participant_symptom_age_na').prop('disabled', true);
            $('#npb_participant_symptom_age_na')[0].checked = null;
            $('#npb_participant_symptom_age_unknown').prop('disabled', true);
            $('#npb_participant_symptom_age_unknown')[0].checked = null;
        else
            $('#npb_participant_symptom_age_na').prop('disabled', false);
            $('#npb_participant_symptom_age_unknown').prop('disabled', false);

    checkConfirmByDoctor = ->
        year = $('#npb_participant_age_diagnosed_year option').filter(':selected').text()
        month = $('#npb_participant_age_diagnosed_month option').filter(':selected').text()
        if (year || month)
            $('#npb_participant_age_diagnosed_known').prop('disabled', true);
            $('#npb_participant_age_diagnosed_known')[0].checked = null;
        else
            $('#npb_participant_age_diagnosed_known').prop('disabled', false);


    root = exports ? this
    root.setNpbParticipantNotFinish = (status) ->
        $("#npb_participant_not_finish").val(status)

    jQuery ($) ->
        checkSymptoms()
        checkConfirmByDoctor()

        $(document).on "change", '#npb_participant_symptom_age_year', ->
            checkSymptoms()
        $(document).on "change", '#npb_participant_symptom_age_month', ->
            checkSymptoms()
        $(document).on "change", '#npb_participant_age_diagnosed_year', ->
            checkConfirmByDoctor()
        $(document).on "change", '#npb_participant_age_diagnosed_month', ->
            checkConfirmByDoctor()


$(document).ready(ready)
$(document).on('page:load', ready)