
$(document).on('page:load ready', function(){
    $('.form_datetime').datepicker({
        format: "yyyy/mm/dd",
        autoclose: true,
        todayBtn: "linked",
        clearBtn: true,
        todayHighlight: true
    })
});
