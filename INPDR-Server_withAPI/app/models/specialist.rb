class Specialist < ActiveRecord::Base
  belongs_to :patient, polymorphic: true

  has_one :address, as: :person, dependent: :destroy, validate: false

  accepts_nested_attributes_for :address, :allow_destroy => true

  VALID_EMAIL_REGEX = /\A[\w+\-.]+@[a-z\d\-.]+\.[a-z]+\z/i
  validates :email, length: {maximum: 255},
            :allow_blank => true,
            format: {with: VALID_EMAIL_REGEX}
end
