class NpcParticipant < Participant
  has_many :followups, :class_name => "NpcFollowup", as: :followable, dependent: :destroy

  def followup_count
    followups.size
  end

  def generate_unique_id
    self.unique_id = loop do
      #TODO: Improve the random number algorithm
      random_number = SecureRandom.random_number(100000)
      break random_number unless (NpcParticipant.exists?(:unique_id => ('pNPC-' + random_number.to_s)) ||
          (NpcApplicantParticipant.exists?(:unique_id => ('pNPC-' + random_number.to_s))))
    end
    self.unique_id.insert(0, 'pNPC-')
  end
end
