class NpbFollowup < ActiveRecord::Base

  belongs_to :followable, polymorphic: true

  validates :symptom_bone,
            :symptom_abdominal,
            :symptom_pain,
            :symptom_breathlessness_rate,
            :symptom_breathlessness_impact,
            :symptom_bleeding_rate,
            :symptom_bleeding_impact,
            :symptom_fatigue_rate,
            :symptom_fatigue_impact,
            :symptom_enlarge_organ,
            :symptom_enlarge_organ_impact,
            :symptom_slow_growth,
            :symptom_slow_growth_impact,
            :symptom_infection,
            :symptom_fracture,
            :symptom_cognition,

            :impact_family_disappointed,
            :impact_family_give_up,
            :impact_family_worry_future,
            :impact_family_closer,

            :impact_wider_self_miss_career,
            :impact_wider_carer_miss_career,
            :impact_wider_emergency,
            :impact_wider_appointment,
            presence: true

  def impact_family_collection
    [['Strongly Agree', 0],
     ['Agree', 1],
     ['Disagree', 2],
     ['Strongly Disagree', 3]]
  end

  def symptom_bone_collection
    [['Never', 0],
     ['Monthly', 1],
     ['Weekly', 2],
     ['Every day', 3],
     ['All the time', 4]]
  end

  def symptom_abdominal_collection
    [['Never', 0],
     ['Monthly', 1],
     ['Weekly', 2],
     ['Every day', 3],
     ['All the time', 4]]
  end

  def symptom_pain_collection
    [['None at all', 0],
     ['Very little', 1],
     ['Some', 2],
     ['Quite a lot', 3],
     ['Very much', 4]]
  end

  def symptom_breathlessness_rate_collection
    [['Never', 0],
     ['On running', 1],
     ['On climbing stairs', 2],
     ['On walking', 3],
     ['At rest', 4]]
  end

  def symptom_breathlessness_impact_collection
    [['None at all', 0],
     ['Very little', 1],
     ['Some', 2],
     ['Quite a lot', 3],
     ['Very much', 4]]
  end

  def symptom_bleeding_rate_collection
    [['Never', 0],
     ['Monthly', 1],
     ['Weekly', 2],
     ['Every day', 3],
     ['All the time', 4]]
  end

  def symptom_bleeding_impact_collection
    [['None at all', 0],
     ['Very little', 1],
     ['Some', 2],
     ['Quite a lot', 3],
     ['Very much', 4]]
  end

  def symptom_fatigue_rate_collection
    [['Never', 0],
     ['Rarely', 1],
     ['Sometimes', 2],
     ['Often', 3],
     ['Always', 4]]
  end

  def symptom_fatigue_impact_collection
    [['None at all', 0],
     ['Very little', 1],
     ['Some', 2],
     ['Quite a lot', 3],
     ['Very much', 4]]
  end

  def symptom_enlarge_organ_collection
    [['Not at all', 0],
     ['Somewhat', 1],
     ['Very much', 2]]
  end

  def symptom_enlarge_organ_impact_collection
    [['None at all', 0],
     ['Very little', 1],
     ['Some', 2],
     ['Quite a lot', 3],
     ['Very much', 4]]
  end

  def symptom_slow_growth_collection
    [['Not at all', 0],
     ['Somewhat', 1],
     ['Very much', 2]]
  end

  def symptom_slow_growth_impact_collection
    [['None at all', 0],
     ['Very little', 1],
     ['Some', 2],
     ['Quite a lot', 3],
     ['Very much', 4]]
  end

  def symptom_infection_collection
    [['0', 0],
     ['1-2', 1],
     ['3-4', 2],
     ['Over 4', 3]]
  end

  def symptom_fracture_collection
    [['0', 0],
     ['1-3', 1],
     ['4-6', 2],
     ['Over 6', 3]]
  end

  def symptom_cognition_collection
    [['None at all', 0],
     ['Very little', 1],
     ['Some', 2],
     ['Quite a lot', 3],
     ['Very much', 4]]
  end

  def impact_wider_self_miss_career_collection
    if self.followable.class == NpbParticipant
      [[I18n.t('npb_followups.npb_participants.selects.impact_wider_self_miss_career.select_no'), 0],
       [I18n.t('npb_followups.npb_participants.selects.impact_wider_self_miss_career.less_than_5'), 1],
       [I18n.t('npb_followups.npb_participants.selects.impact_wider_self_miss_career.between_1_week_1_month'), 2],
       [I18n.t('npb_followups.npb_participants.selects.impact_wider_self_miss_career.more_than_1_month'), 3],
       [I18n.t('npb_followups.npb_participants.selects.impact_wider_self_miss_career.unable_to_work'), 4]]
    else
      [[I18n.t('npb_followups.npb_applicant_participants.selects.impact_wider_self_miss_career.select_no'), 0],
       [I18n.t('npb_followups.npb_applicant_participants.selects.impact_wider_self_miss_career.less_than_5'), 1],
       [I18n.t('npb_followups.npb_applicant_participants.selects.impact_wider_self_miss_career.between_1_week_1_month'), 2],
       [I18n.t('npb_followups.npb_applicant_participants.selects.impact_wider_self_miss_career.more_than_1_month'), 3],
       [I18n.t('npb_followups.npb_applicant_participants.selects.impact_wider_self_miss_career.unable_to_work'), 4],
       [I18n.t('npb_followups.npb_applicant_participants.selects.impact_wider_self_miss_career.select_na'), 5]]
    end
  end

  def impact_wider_carer_miss_career_collection
    if self.followable.class == NpbParticipant
      [[I18n.t('npb_followups.npb_participants.selects.impact_wider_carer_miss_career.select_no'), 0],
       [I18n.t('npb_followups.npb_participants.selects.impact_wider_carer_miss_career.less_than_5'), 1],
       [I18n.t('npb_followups.npb_participants.selects.impact_wider_carer_miss_career.between_1_week_1_month'), 2],
       [I18n.t('npb_followups.npb_participants.selects.impact_wider_carer_miss_career.more_than_1_month'), 3],
       [I18n.t('npb_followups.npb_participants.selects.impact_wider_carer_miss_career.unable_tp_work'), 4],
       [I18n.t('npb_followups.npb_participants.selects.impact_wider_carer_miss_career.change_pattern'), 5],
       [I18n.t('npb_followups.npb_participants.selects.impact_wider_carer_miss_career.select_na'), 6]]
    else
      [[I18n.t('npb_followups.npb_applicant_participants.selects.impact_wider_carer_miss_career.select_no'), 0],
       [I18n.t('npb_followups.npb_applicant_participants.selects.impact_wider_carer_miss_career.less_than_5'), 1],
       [I18n.t('npb_followups.npb_applicant_participants.selects.impact_wider_carer_miss_career.between_1_week_1_month'), 2],
       [I18n.t('npb_followups.npb_applicant_participants.selects.impact_wider_carer_miss_career.more_than_1_month'), 3],
       [I18n.t('npb_followups.npb_applicant_participants.selects.impact_wider_carer_miss_career.unable_tp_work'), 4],
       [I18n.t('npb_followups.npb_applicant_participants.selects.impact_wider_carer_miss_career.change_pattern'), 5]]
    end
  end

  def impact_wider_emergency_collection
    if self.followable.class == NpbParticipant
      [[I18n.t('npb_followups.npb_participants.selects.impact_wider_emergency.never'), 0],
       [I18n.t('npb_followups.npb_participants.selects.impact_wider_emergency.1_to_3'), 1],
       [I18n.t('npb_followups.npb_participants.selects.impact_wider_emergency.4_to_7'), 2],
       [I18n.t('npb_followups.npb_participants.selects.impact_wider_emergency.more_than_8'), 3]]
    else
      [[I18n.t('npb_followups.npb_applicant_participants.selects.impact_wider_emergency.never'), 0],
       [I18n.t('npb_followups.npb_applicant_participants.selects.impact_wider_emergency.1_to_3'), 1],
       [I18n.t('npb_followups.npb_applicant_participants.selects.impact_wider_emergency.4_to_7'), 2],
       [I18n.t('npb_followups.npb_applicant_participants.selects.impact_wider_emergency.more_than_8'), 3]]
    end
  end

  def impact_wider_appointment_collection
    if self.followable.class == NpbParticipant
      [[I18n.t('npb_followups.npb_participants.selects.impact_wider_appointment.never'), 0],
       [I18n.t('npb_followups.npb_participants.selects.impact_wider_appointment.1_to_3'), 1],
       [I18n.t('npb_followups.npb_participants.selects.impact_wider_appointment.4_to_7'), 2],
       [I18n.t('npb_followups.npb_participants.selects.impact_wider_appointment.more_than_8'), 3]]
    else
      [[I18n.t('npb_followups.npb_applicant_participants.selects.impact_wider_appointment.never'), 0],
       [I18n.t('npb_followups.npb_applicant_participants.selects.impact_wider_appointment.1_to_3'), 1],
       [I18n.t('npb_followups.npb_applicant_participants.selects.impact_wider_appointment.4_to_7'), 2],
       [I18n.t('npb_followups.npb_applicant_participants.selects.impact_wider_appointment.more_than_8'), 3]]
    end
  end
end
