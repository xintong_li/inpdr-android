class Patient < ActiveRecord::Base
  belongs_to :user

  belongs_to :enrollable, polymorphic: true

  validates_date :date_of_birth, :allow_nil => true, :on_or_before => lambda { Date.current }

  validates :registrant_first_name, :registrant_last_name, :presence => true

  has_one :address, as: :person, dependent: :destroy, validate: true

  accepts_nested_attributes_for :address

  has_one :specialist, as: :patient, :dependent => :destroy, :validate => true
  accepts_nested_attributes_for :specialist

  VALID_EMAIL_REGEX = /\A[\w+\-.]+@[a-z\d\-.]+\.[a-z]+\z/i

  def self.gender_collection
    [['Male', 0],
     ['Female', 1]]
  end

  def self.applicant_relation_collection
    [['Self', 0],
     ['Mother', 1],
     ['Father', 2],
     ['Guardian', 3],
     ['Husband', 4],
     ['Wife', 5],
     ['Partner', 6],
     ['Brother', 7],
     ['Sister', 8],
     ['Son', 9],
     ['Daughter', 10],
     ['Carer', 11],
     ['Other', 12]]
  end

  def validate_consent?
    !self.consent.nil? && !self.consent_name.blank?
  end

  def generate_unique_id
    self.unique_id = loop do
      random_number = SecureRandom.random_number(1000000)
      break random_number unless (Patient.exists?(:unique_id => random_number))
    end
  end

  def get_unique_id
    if defined? self.enrollable
      if ((self.enrollable.is_a? NpcParticipant) || (self.enrollable.is_a? NpcApplicantParticipant))
        "NPC-#{self.unique_id}"
      elsif ((self.enrollable.is_a? NpbParticipant) || (self.enrollable.is_a? NpbApplicantParticipant))
        "NPB-#{self.unique_id}"
      else
        "NPA-#{self.unique_id}"
      end
    else
      "#{self.registrant_first_name} #{self.registrant_last_name}"
    end
  end
end
