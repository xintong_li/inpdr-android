class NpcFollowup < ActiveRecord::Base
  belongs_to :followable, polymorphic: true

  validates :impact_ambulation,
            :impact_manipulation,
            :impact_speech,
            :impact_swallowing,
            :impact_eye_movement,
            :impact_seizure,
            :impact_cognitive_impaired,
            :impact_family_disappointed,
            :impact_family_give_up,
            :impact_family_worry_future,
            :impact_family_closer_family,
            :impact_wider_self_miss_career,
            :impact_wider_family_miss_career,
            :impact_wider_emergency,
            :impact_wider_appointment,
            presence: true

  def impact_ambulation_collection
    if self.followable.class == NpcParticipant
      [[I18n.t('npc_followups.npc_participants.selects.impact_ambulation.normal'), 0],
       [I18n.t('npc_followups.npc_participants.selects.impact_ambulation.clumsiness'), 1],
       [I18n.t('npc_followups.npc_participants.selects.impact_ambulation.autonomous'), 2],
       [I18n.t('npc_followups.npc_participants.selects.impact_ambulation.outdoor_assisted_ambulation'), 3],
       [I18n.t('npc_followups.npc_participants.selects.impact_ambulation.indoor_assisted_ambulation'), 4],
       [I18n.t('npc_followups.npc_participants.selects.impact_ambulation.wheelchair_bound'), 5]]
    else
      [[I18n.t('npc_followups.npc_applicant_participants.selects.impact_ambulation.normal'), 0],
       [I18n.t('npc_followups.npc_applicant_participants.selects.impact_ambulation.clumsiness'), 1],
       [I18n.t('npc_followups.npc_applicant_participants.selects.impact_ambulation.autonomous'), 2],
       [I18n.t('npc_followups.npc_applicant_participants.selects.impact_ambulation.outdoor_assisted_ambulation'), 3],
       [I18n.t('npc_followups.npc_applicant_participants.selects.impact_ambulation.indoor_assisted_ambulation'), 4],
       [I18n.t('npc_followups.npc_applicant_participants.selects.impact_ambulation.wheelchair_bound'), 5],
       [I18n.t('npc_followups.npc_applicant_participants.selects.impact_ambulation.not_applicable'), 6]]
    end
  end
  
  def impact_manipulation_collection
    if self.followable.class == NpcParticipant
      [[I18n.t('npc_followups.npc_participants.selects.impact_manipulation.normal'), 0],
       [I18n.t('npc_followups.npc_participants.selects.impact_manipulation.tremor'), 1],
       [I18n.t('npc_followups.npc_participants.selects.impact_manipulation.slight_dysmetria'), 2],
       [I18n.t('npc_followups.npc_participants.selects.impact_manipulation.mild_dysmetria'), 3],
       [I18n.t('npc_followups.npc_participants.selects.impact_manipulation.severe_dysmetria'), 4]]
    else
      [[I18n.t('npc_followups.npc_applicant_participants.selects.impact_manipulation.normal'), 0],
       [I18n.t('npc_followups.npc_applicant_participants.selects.impact_manipulation.tremor'), 1],
       [I18n.t('npc_followups.npc_applicant_participants.selects.impact_manipulation.slight_dysmetria'), 2],
       [I18n.t('npc_followups.npc_applicant_participants.selects.impact_manipulation.mild_dysmetria'), 3],
       [I18n.t('npc_followups.npc_applicant_participants.selects.impact_manipulation.severe_dysmetria'), 4]]
    end
  end

  def impact_speech_collection
    if self.followable.class == NpcParticipant
      [[I18n.t('npc_followups.npc_participants.selects.impact_speech.normal'), 0],
       [I18n.t('npc_followups.npc_participants.selects.impact_speech.mild_dysmetria'), 1],
       [I18n.t('npc_followups.npc_participants.selects.impact_speech.severe_dysmetria'), 2],
       [I18n.t('npc_followups.npc_participants.selects.impact_speech.nonverbal_communication'), 3]]
    else
      [[I18n.t('npc_followups.npc_applicant_participants.selects.impact_speech.normal'), 0],
       [I18n.t('npc_followups.npc_applicant_participants.selects.impact_speech.delayed_acquisitions'), 1],
       [I18n.t('npc_followups.npc_applicant_participants.selects.impact_speech.mild_dysmetria'), 2],
       [I18n.t('npc_followups.npc_applicant_participants.selects.impact_speech.severe_dysmetria'), 3],
       [I18n.t('npc_followups.npc_applicant_participants.selects.impact_speech.nonverbal_communication'), 4],
       [I18n.t('npc_followups.npc_applicant_participants.selects.impact_speech.absence_of_communications'), 5],
       [I18n.t('npc_followups.npc_applicant_participants.selects.impact_speech.not_applicable'), 6]]
    end
  end

  def impact_swallowing_collection
    if self.followable.class == NpcParticipant
      [[I18n.t('npc_followups.npc_participants.selects.impact_swallowing.normal'), 0],
       [I18n.t('npc_followups.npc_participants.selects.impact_swallowing.abnormal_chewing'), 1],
       [I18n.t('npc_followups.npc_participants.selects.impact_swallowing.occasional_dysphagia'), 2],
       [I18n.t('npc_followups.npc_participants.selects.impact_swallowing.faily_dysphagia'), 3],
       [I18n.t('npc_followups.npc_participants.selects.impact_swallowing.gastric_button_feeding'), 4]]
    else
      [[I18n.t('npc_followups.npc_applicant_participants.selects.impact_swallowing.normal'), 0],
       [I18n.t('npc_followups.npc_applicant_participants.selects.impact_swallowing.abnormal_chewing'), 1],
       [I18n.t('npc_followups.npc_applicant_participants.selects.impact_swallowing.occasional_dysphagia'), 2],
       [I18n.t('npc_followups.npc_applicant_participants.selects.impact_swallowing.faily_dysphagia'), 3],
       [I18n.t('npc_followups.npc_applicant_participants.selects.impact_swallowing.gastric_button_feeding'), 4]]
    end
  end

  def impact_eye_movement_collection
    if self.followable.class == NpcParticipant
      [[I18n.t('npc_followups.npc_participants.selects.impact_eye_movement.normal'), 0],
       [I18n.t('npc_followups.npc_participants.selects.impact_eye_movement.slow_ocular_pursuit'), 1],
       [I18n.t('npc_followups.npc_participants.selects.impact_eye_movement.vertical_opthalmoplegia'), 2],
       [I18n.t('npc_followups.npc_participants.selects.impact_eye_movement.complete_opthalmoplegia'), 3]]
    else
      [[I18n.t('npc_followups.npc_applicant_participants.selects.impact_eye_movement.normal'), 0],
       [I18n.t('npc_followups.npc_applicant_participants.selects.impact_eye_movement.slow_ocular_pursuit'), 1],
       [I18n.t('npc_followups.npc_applicant_participants.selects.impact_eye_movement.vertical_opthalmoplegia'), 2],
       [I18n.t('npc_followups.npc_applicant_participants.selects.impact_eye_movement.complete_opthalmoplegia'), 3]]
    end
  end

  def impact_seizure_collection
    if self.followable.class == NpcParticipant
      [[I18n.t('npc_followups.npc_participants.selects.impact_seizure.select_no'), 0],
       [I18n.t('npc_followups.npc_participants.selects.impact_seizure.occasional'), 1],
       [I18n.t('npc_followups.npc_participants.selects.impact_seizure.seizures_with_aed'), 2],
       [I18n.t('npc_followups.npc_participants.selects.impact_seizure.seizures_resistant_to_aed'), 3]]
    else
      [[I18n.t('npc_followups.npc_applicant_participants.selects.impact_seizure.select_no'), 0],
       [I18n.t('npc_followups.npc_applicant_participants.selects.impact_seizure.occasional'), 1],
       [I18n.t('npc_followups.npc_applicant_participants.selects.impact_seizure.seizures_with_aed'), 2],
       [I18n.t('npc_followups.npc_applicant_participants.selects.impact_seizure.seizures_resistant_to_aed'), 3]]
    end
  end

  def impact_cognitive_impaired_collection
    if self.followable.class == NpcParticipant
      [[I18n.t('npc_followups.npc_participants.selects.impact_cognitive_impaired.not_at_all'), 0],
       [I18n.t('npc_followups.npc_participants.selects.impact_cognitive_impaired.a_little_bit'), 1],
       [I18n.t('npc_followups.npc_participants.selects.impact_cognitive_impaired.quite_a_bit'), 2],
       [I18n.t('npc_followups.npc_participants.selects.impact_cognitive_impaired.very_much'), 3]]
    else
      [[I18n.t('npc_followups.npc_applicant_participants.selects.impact_cognitive_impaired.not_at_all'), 0],
       [I18n.t('npc_followups.npc_applicant_participants.selects.impact_cognitive_impaired.a_little_bit'), 1],
       [I18n.t('npc_followups.npc_applicant_participants.selects.impact_cognitive_impaired.quite_a_bit'), 2],
       [I18n.t('npc_followups.npc_applicant_participants.selects.impact_cognitive_impaired.very_much'), 3]]
    end
  end

  def self.impact_family_collection
    [['Strongly Agree', 0],
    ['Agree', 1],
    ['Disagree', 2],
    ['Strongly Disagree', 3]]
  end

  def impact_wider_self_miss_career_collection
    if self.followable.class == NpcParticipant
      [[I18n.t('npc_followups.npc_participants.selects.impact_wider_self_miss_career.select_no'), 0],
       [I18n.t('npc_followups.npc_participants.selects.impact_wider_self_miss_career.less_than_5'), 1],
       [I18n.t('npc_followups.npc_participants.selects.impact_wider_self_miss_career.between_1_week_1_month'), 2],
       [I18n.t('npc_followups.npc_participants.selects.impact_wider_self_miss_career.more_than_1_month'), 3],
       [I18n.t('npc_followups.npc_participants.selects.impact_wider_self_miss_career.unable_to_work'), 4]]
    else
      [[I18n.t('npc_followups.npc_applicant_participants.selects.impact_wider_self_miss_career.select_no'), 0],
       [I18n.t('npc_followups.npc_applicant_participants.selects.impact_wider_self_miss_career.less_than_5'), 1],
       [I18n.t('npc_followups.npc_applicant_participants.selects.impact_wider_self_miss_career.between_1_week_1_month'), 2],
       [I18n.t('npc_followups.npc_applicant_participants.selects.impact_wider_self_miss_career.more_than_1_month'), 3],
       [I18n.t('npc_followups.npc_applicant_participants.selects.impact_wider_self_miss_career.unable_to_work'), 4],
       [I18n.t('npc_followups.npc_applicant_participants.selects.impact_wider_self_miss_career.select_na'), 5]]
    end
  end

  def impact_wider_family_miss_career_collection
    if self.followable.class == NpcParticipant
      [[I18n.t('npc_followups.npc_participants.selects.impact_wider_family_miss_career.select_no'), 0],
       [I18n.t('npc_followups.npc_participants.selects.impact_wider_family_miss_career.less_than_5'), 1],
       [I18n.t('npc_followups.npc_participants.selects.impact_wider_family_miss_career.between_1_week_1_month'), 2],
       [I18n.t('npc_followups.npc_participants.selects.impact_wider_family_miss_career.more_than_1_month'), 3],
       [I18n.t('npc_followups.npc_participants.selects.impact_wider_family_miss_career.unable_tp_work'), 4],
      [I18n.t('npc_followups.npc_participants.selects.impact_wider_family_miss_career.change_pattern'), 5],
      [I18n.t('npc_followups.npc_participants.selects.impact_wider_family_miss_career.select_na'), 6]]
    else
      [[I18n.t('npc_followups.npc_applicant_participants.selects.impact_wider_family_miss_career.select_no'), 0],
       [I18n.t('npc_followups.npc_applicant_participants.selects.impact_wider_family_miss_career.less_than_5'), 1],
       [I18n.t('npc_followups.npc_applicant_participants.selects.impact_wider_family_miss_career.between_1_week_1_month'), 2],
       [I18n.t('npc_followups.npc_applicant_participants.selects.impact_wider_family_miss_career.more_than_1_month'), 3],
       [I18n.t('npc_followups.npc_applicant_participants.selects.impact_wider_family_miss_career.unable_tp_work'), 4],
      [I18n.t('npc_followups.npc_applicant_participants.selects.impact_wider_family_miss_career.change_pattern'), 5]]
    end
  end

  def impact_wider_emergency_collection
    if self.followable.class == NpcParticipant
      [[I18n.t('npc_followups.npc_participants.selects.impact_wider_emergency.never'), 0],
       [I18n.t('npc_followups.npc_participants.selects.impact_wider_emergency.1_to_3'), 1],
       [I18n.t('npc_followups.npc_participants.selects.impact_wider_emergency.4_to_7'), 2],
       [I18n.t('npc_followups.npc_participants.selects.impact_wider_emergency.more_than_8'), 3]]
    else
      [[I18n.t('npc_followups.npc_applicant_participants.selects.impact_wider_emergency.never'), 0],
       [I18n.t('npc_followups.npc_applicant_participants.selects.impact_wider_emergency.1_to_3'), 1],
       [I18n.t('npc_followups.npc_applicant_participants.selects.impact_wider_emergency.4_to_7'), 2],
       [I18n.t('npc_followups.npc_applicant_participants.selects.impact_wider_emergency.more_than_8'), 3]]
    end
  end

  def impact_wider_appointment_collection
    if self.followable.class == NpcParticipant
      [[I18n.t('npc_followups.npc_participants.selects.impact_wider_appointment.never'), 0],
       [I18n.t('npc_followups.npc_participants.selects.impact_wider_appointment.1_to_3'), 1],
       [I18n.t('npc_followups.npc_participants.selects.impact_wider_appointment.4_to_7'), 2],
       [I18n.t('npc_followups.npc_participants.selects.impact_wider_appointment.more_than_8'), 3]]
    else
      [[I18n.t('npc_followups.npc_applicant_participants.selects.impact_wider_appointment.never'), 0],
       [I18n.t('npc_followups.npc_applicant_participants.selects.impact_wider_appointment.1_to_3'), 1],
       [I18n.t('npc_followups.npc_applicant_participants.selects.impact_wider_appointment.4_to_7'), 2],
       [I18n.t('npc_followups.npc_applicant_participants.selects.impact_wider_appointment.more_than_8'), 3]]
    end
  end
end
