class ParticipantWithApplicant < Participant

  self.abstract_class = true

  #validates :first_name, :surname, presence: true

  def self.applicant_relation_collection
    [['Mother', 0],
     ['Father', 1],
     ['Guardian', 2],
     ['Husband', 3],
     ['Wife', 4],
     ['Partner', 5],
     ['Brother', 6],
     ['Sister', 7],
     ['Son', 8],
     ['Daughter', 9],
     ['Carer', 10],
     ['Other', 11]]
  end
end
