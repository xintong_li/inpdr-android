class Address < ActiveRecord::Base

  belongs_to :person, polymorphic: true

  validates :line_1, :county, :post_code, :country, presence: true

  def country_name
    country = ISO3166::Country[self.country]
    country.translations[I18n.locale.to_s] || country.name
  end

end
