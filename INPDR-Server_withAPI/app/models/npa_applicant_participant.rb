class NpaApplicantParticipant < ParticipantWithApplicant
  has_many :followups, :class_name => "NpbFollowup", as: :followable, dependent: :destroy

  def followup_count
    followups.size
  end

  def self.applicant_relation_collection
    [['Mother', 0],
     ['Father', 1],
     ['Guardian', 2],
     ['Carer', 3],
     ['Other', 4]]
  end

  def generate_unique_id
    self.unique_id = loop do
      #TODO: Improve the random number algorithm
      random_number = SecureRandom.random_number(100000)
      break random_number unless (NpaApplicantParticipant.exists?(:unique_id => ('pNPA-' + random_number.to_s)))
    end
    self.unique_id.insert(0, 'pNPA-')
  end

end
