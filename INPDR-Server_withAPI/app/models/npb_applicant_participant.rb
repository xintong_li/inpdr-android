class NpbApplicantParticipant < ParticipantWithApplicant
  has_many :followups, :class_name => "NpbFollowup", as: :followable, dependent: :destroy

  def followup_count
    followups.size
  end

  def generate_unique_id
    self.unique_id = loop do
      #TODO: Improve the random number algorithm
      random_number = SecureRandom.random_number(100000)
      break random_number unless (NpbApplicantParticipant.exists?(:unique_id => ('pNPB-' + random_number.to_s)) ||
          (NpbParticipant.exists?(:unique_id => ('pNPB-' + random_number.to_s))))
    end
    self.unique_id.insert(0, 'pNPB-')
  end
end
