class Participant < ActiveRecord::Base

  self.abstract_class = true

  # Gene disgnosis uploader uysing Carrierwave
  mount_uploader :gene_diagnosis, GeneDiagnosisUploader

  has_one :patient, :as => :enrollable

  def self.family_diagnosed_collection
    [['Yes', 0],
     ['No', 1],
     ['Unknown', 2]]
  end

  def self.baby_symptoms_collection
    [['Yes', 0],
     ['No', 1],
     ['Unknown', 2]]
  end

  def followup_class
    if ((self.is_a? NpcParticipant) || (self.is_a? NpcApplicantParticipant))
      NpcFollowup
    elsif ((self.is_a? NpbParticipant) || (self.is_a? NpbApplicantParticipant))
      NpbFollowup
    else
      nil
    end
  end

  def lock_new_questionnaire
    return false if self.followup_count == 0
    sorted_questionnaires = self.followups.sort_by &:created_at
    latest_questionnaire = sorted_questionnaires.last
    return false if (latest_questionnaire.created_at < (Time.now - 3.months))
    return true
  end
end
