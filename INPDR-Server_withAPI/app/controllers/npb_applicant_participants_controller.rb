class NpbApplicantParticipantsController < ApplicationController
  before_action :authenticate_user!
  before_action :set_npb_applicant_participant, only: [:show, :edit, :update, :destroy]
  before_filter :get_patient

  # GET /npb_applicant_participants
  # GET /npb_applicant_participants.json
  def index
    if current_user.study_team
      @participants_verified = NpbApplicantParticipant.where(:verified => true)
    else
      @participants_verified = current_user.npb_applicant_participants.where(:verified => true)
    end

    @participants_unverified = NpbApplicantParticipant.where(:verified => false)
  end

  # GET /npb_applicant_participants/1
  # GET /npb_applicant_participants/1.json
  def show
    @participant = NpbApplicantParticipant.find(params[:id])
    respond_to do |format|
      format.html # show.html.erb
      format.json { render json: @participant }
    end
  end

  # GET /npb_applicant_participants/new
  def new
    @npb_applicant_participant = NpbApplicantParticipant.new
    authorize @npb_applicant_participant
  end

  # GET /npb_applicant_participants/1/edit
  def edit
    authorize @npb_applicant_participant
  end

  # POST /npb_applicant_participants
  # POST /npb_applicant_participants.json
  def create
    @npb_applicant_participant = NpbApplicantParticipant.new(npb_applicant_participant_params)
    authorize @npb_applicant_participant
    @npb_applicant_participant.patient = @patient
    respond_to do |format|
      if @npb_applicant_participant.not_finish?
        if @npb_applicant_participant.save
          format.html { redirect_to patients_url, notice: t("#{controller_name}.notice.save_for_later") }
          format.json { render :show, :status => :created, :location => @npb_applicant_participant }
        else
          format.html { render :new }
          format.json { render :json => @npb_applicant_participant.errors, :status => :unprocessable_entity }
        end
      else
        if @npb_applicant_participant.save
          #format.html { redirect_to patients_path }
          format.html { redirect_to pages_thank_enrol_path }
          format.json { render :show, :status => :created, :location => @npb_applicant_participant }
        else
          format.html { render :new }
          format.json { render :json => @npb_applicant_participant.errors, :status => :unprocessable_entity }
        end
      end
    end
  end

  # PATCH/PUT /npb_applicant_participants/1
  # PATCH/PUT /npb_applicant_participants/1.json
  def update
    respond_to do |format|
      if @npb_applicant_participant.update(npb_applicant_participant_params)
        format.html { redirect_to @npb_applicant_participant, notice: 'NP-B applicant participant was successfully updated.' }
        format.json { render :show, status: :ok, location: @npb_applicant_participant }
      else
        format.html { render :edit }
        format.json { render json: @npb_applicant_participant.errors, status: :unprocessable_entity }
      end
    end
  end

  # DELETE /npb_applicant_participants/1
  # DELETE /npb_applicant_participants/1.json
  def destroy
    @npb_applicant_participant.destroy
    respond_to do |format|
      format.html { redirect_to npb_applicant_participants_url, notice: 'NP-Bapplicant participant was successfully destroyed.' }
      format.json { head :no_content }
    end
  end

  # GET /npb_applicant_participants/1/verify
  # GET /npb_applicant_participants/1/verify.json
  def verify
    @npb_applicant_participant = NpbApplicantParticipant.find(params[:id])
    unless @npb_applicant_participant.verified
      @npb_applicant_participant.verified = true
    end

    respond_to do |format|
      if @npb_applicant_participant.save
        ParticipantMailer.verify_email(@npb_applicant_participant).deliver_now
        format.html { redirect_to npb_applicant_participants_path, notice: 'Successfully verified participant.' }
        format.json { render :show, status: :ok, location: @npb_applicant_participant }
      else
        format.html { render :show }
        format.json { render json: @npb_applicant_participant.errors, status: :unprocessable_entity }
      end
    end
  end

  private
  # Use callbacks to share common setup or constraints between actions.
  def set_npb_applicant_participant
    @npb_applicant_participant = NpbApplicantParticipant.find(params[:id])
  end

  # Never trust parameters from the scary internet, only allow the white list through.
  def npb_applicant_participant_params
    params.require(:npb_applicant_participant).permit(:age_diagnosed_known,
                                                      :age_diagnosed_year,
                                                      :age_diagnosed_month,
                                                      :family_diagnosed,
                                                      :family_diagnosed_relation,
                                                      :applicant_relation,
                                                      :applicant_relation_specify,
                                                      :hospital_name,
                                                      { address_attributes: [
                                                          :_destroy,
                                                          :line_1,
                                                          :line_2,
                                                          :town,
                                                          :county,
                                                          :post_code,
                                                          :country
                                                      ] },
                                                      :hospital_email,
                                                      :gene_diagnosis,
                                                      :gene_diagnosis_cache,
                                                      :gene_test_performed,
                                                      :symptom_age_year,
                                                      :symptom_age_month,
                                                      :symptom_age_unknown,
                                                      :symptom_age_na,
                                                      :symptom_enlarged_liver,
                                                      :symptom_breathing,
                                                      :symptom_anaemia,
                                                      :symptom_bleeding,
                                                      :symptom_growth_delay,
                                                      :symptom_developmental_delay,
                                                      :symptom_bone_pain,
                                                      :symptom_abdominal_pain,
                                                      :symptom_other,
                                                      :symptom_other_specify,
                                                      :hospital_clinician_name,
                                                      :hospital_phone,
                                                      :unable_to_contact_specialist,
                                                      :consent,
                                                      :consent_name,
                                                      :not_finish,
                                                      { specialists_attributes: [:_destroy,
                                                                                 :id,
                                                                                 :name,
                                                                                 :hospital_name,
                                                                                 { address_attributes: [
                                                                                     :_destroy,
                                                                                     :line_1,
                                                                                     :line_2,
                                                                                     :town,
                                                                                     :county,
                                                                                     :post_code,
                                                                                     :country
                                                                                 ] },
                                                                                 :number,
                                                                                 :email] })
  end
end
