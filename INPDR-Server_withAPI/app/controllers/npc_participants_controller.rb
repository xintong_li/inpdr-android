class NpcParticipantsController < ApplicationController
  before_action :authenticate_user!
  before_action :set_npc_participant, only: [:show, :edit, :update, :destroy]
  before_filter :get_patient

  # GET /npc_participants
  # GET /npc_participants.json
  def index
    if current_user.study_team
      @participants_verified = NpcParticipant.where(:verified => true)
    else
      @participants_verified = current_user.npc_participants.where(:verified => true)
    end
    @participants_unverified = NpcParticipant.where(:verified => false)
  end

  # GET /npc_participants/1
  # GET /npc_participants/1.json
  def show
    @participant = NpcParticipant.find(params[:id])
    respond_to do |format|
      format.html # show.html.erb
      format.json { render json: @participant}
    end
  end

  # GET /npc_participants/new
  def new
    @npc_participant = NpcParticipant.new
    authorize @npc_participant
  end

  # GET /npc_participants/1/edit
  def edit
    authorize @npc_participant
  end

  # POST /npc_participants
  # POST /npc_participants.json
  def create
    @npc_participant = NpcParticipant.new(npc_participant_params)
    @npc_participant.patient = @patient
    authorize @npc_participant

    respond_to do |format|
      if @npc_participant.not_finish?
        if @npc_participant.save
          format.html { redirect_to patients_url, notice: t("#{controller_name}.notice.save_for_later") }
          format.json { render :show, :status => :created, :location => @npc_participant }
        else
          format.html { render :new }
          format.json { render :json => @npc_participant.errors, :status => :unprocessable_entity }
        end
      else
        if @npc_participant.save
          format.html { redirect_to pages_thank_enrol_path }
          format.json { render :show, :status => :created, :location => @npc_participant }
        else
          format.html { render :new }
          format.json { render :json => @npc_participant.errors, :status => :unprocessable_entity }
        end
      end
    end
  end

  # PATCH/PUT /npc_participants/1
  # PATCH/PUT /npc_participants/1.json
  def update
    respond_to do |format|
      if @npc_participant.update(npc_participant_params)
        format.html { redirect_to @npc_participant, notice: 'NP-C participant was successfully updated.' }
        format.json { render :show, status: :ok, location: @npc_participant }
      else
        format.html { render :edit }
        format.json { render json: @npc_participant.errors, status: :unprocessable_entity }
      end
    end
  end

  # DELETE /npc_participants/1
  # DELETE /npc_participants/1.json
  def destroy
    @npc_participant.destroy
    respond_to do |format|
      format.html { redirect_to npc_participants_url, notice: 'NP-Cparticipant was successfully destroyed.' }
      format.json { head :no_content }
    end
  end



  private
  # Use callbacks to share common setup or constraints between actions.
  def set_npc_participant
    @npc_participant = NpcParticipant.find(params[:id])
  end

# Never trust parameters from the scary internet, only allow the white list through.
  def npc_participant_params
    params.require(:npc_participant).permit(:age_diagnosed_known,
                                            :age_diagnosed_year,
                                            :age_diagnosed_month,
                                            :family_diagnosed,
                                            :family_diagnosed_relation,
                                            :hospital_name,
                                            {address_attributes: [
                                                :_destroy,
                                                :line_1,
                                                :line_2,
                                                :town,
                                                :county,
                                                :post_code,
                                                :country
                                            ]},
                                            :hospital_email,
                                            :gene_diagnosis,
                                            :gene_diagnosis_cache,
                                            :gene_test_performed,
                                            :unable_to_contact_specialist,
                                            :baby_symptoms,
                                            :symptom_age_year,
                                            :symptom_age_month,
                                            :symptom_age_unknown,
                                            :symptom_age_na,
                                            :symptom_delayed_milestone,
                                            :symptom_ppers,
                                            :symptom_coordication,
                                            :symptom_eye,
                                            :symptom_behavioural,
                                            :symptom_seizure,
                                            :symptom_psychiatric,
                                            :symptom_other,
                                            :symptom_other_specify,
                                            :hospital_clinician_name,
                                            :hospital_phone,
                                            :not_finish,
                                            {specialists_attributes: [:_destroy,
                                                                      :id,
                                                                      :name,
                                                                      :hospital_name,
                                                                      {address_attributes: [
                                                                          :_destroy,
                                                                          :line_1,
                                                                          :line_2,
                                                                          :town,
                                                                          :county,
                                                                          :post_code,
                                                                          :country
                                                                      ]},
                                                                      :number,
                                                                      :email]})
  end
end
