class NpcFollowupsController < ApplicationController
  before_action :authenticate_user!
  before_action :set_npc_followup, only: [:show, :edit, :update, :destroy]
  before_filter :get_followable

  # GET /npc_followups
  # GET /npc_followups.json
  def index
    @npc_followups = NpcFollowup.where(:followable => @followable)
  end

  # GET /npc_followups/1
  # GET /npc_followups/1.json
  def show
    @npc_followup = NpcFollowup.find(params[:id])
    @npc_followup.followable = @followable
  end

  # GET /npc_followups/new
  def new
    @npc_followup =  NpcFollowup.new
    @npc_followup.followable = @followable
  end

  # GET /npc_followups/1/edit
  def edit
    @npc_followup = NpcFollowup.find(params[:id])
    @npc_followup.followable = @followable
  end

  # POST /npc_followups
  # POST /npc_followups.json
  def create
    @npc_followup = NpcFollowup.new(npc_followup_params)
    @npc_followup.followable = @followable

    respond_to do |format|
      if @npc_followup.save
        format.html { redirect_to polymorphic_url([@followable.patient, @followable, @npc_followup]), notice: t('npc_followups.defaults.submit.success').html_safe }
        format.json { render :show, status: :created, location: @npc_followup }
      else
        format.html { render :new }
        format.json { render json: @npc_followup.errors, status: :unprocessable_entity }
      end
    end
  end

  # PATCH/PUT /npc_followups/1
  # PATCH/PUT /npc_followups/1.json
  def update
    respond_to do |format|
      if @npc_followup.update(npc_followup_params)
        format.html { redirect_to polymorphic_url([@followable.patient, @followable, @npc_followup]), notice: 'NP-C followup questionnaire was successfully updated.' }
        format.json { render :show, status: :ok, location: @npc_followup }
      else
        format.html { render :edit }
        format.json { render json: @npc_followup.errors, status: :unprocessable_entity }
      end
    end
  end

  # DELETE /npc_followups/1
  # DELETE /npc_followups/1.json
  def destroy
    @npc_followup.destroy
    respond_to do |format|
      format.html { redirect_to polymorphic_url([@followable.patient, @followable, NpcFollowup]), notice: 'NP-C followup questionnaire was successfully destroyed.' }
      format.json { head :no_content }
    end
  end

  private
  # Use callbacks to share common setup or constraints between actions.
  def set_npc_followup
    @npc_followup = NpcFollowup.find(params[:id])
  end

  # Never trust parameters from the scary internet, only allow the white list through.
  def npc_followup_params
    params.require(:npc_followup).permit(
        :impact_ambulation,
        :impact_manipulation,
        :impact_speech,
        :impact_swallowing,
        :impact_eye_movement,
        :impact_seizure,
        :impact_cognitive_impaired,
        :impact_family_disappointed,
        :impact_family_give_up,
        :impact_family_worry_future,
        :impact_family_closer_family,
        :impact_wider_self_miss_career,
        :impact_wider_family_miss_career,
        :impact_wider_emergency,
        :impact_wider_appointment)
  end

  def get_followable
    @followable = params[:followable].classify.constantize.find(followable_id)
  end

  def followable_id
    params[(params[:followable].singularize + '_id').to_sym]
  end

end
