class RegistrationsController < Devise::RegistrationsController
  def new
    build_resource({})
    set_minimum_password_length
    yield resource if block_given?
    respond_with self.resource
  end

  # GET /resource/edit
  def edit
    render :edit
  end

  protected

  def after_sign_up_path_for(resource)
    patients_path
  end



  def after_inactive_sign_up_path_for(resource)
    pages_registration_path
  end
end