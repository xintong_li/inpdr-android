class NpbFollowupsController < ApplicationController
  before_action :authenticate_user!
  before_action :set_npb_followup, only: [:show, :edit, :update, :destroy]
  before_filter :get_followable

  # GET /npb_followups
  # GET /npb_followups.json
  def index
    @npb_followups = NpbFollowup.where(:followable => @followable)
  end

  # GET /npb_followups/1
  # GET /npb_followups/1.json
  def show
    @npb_followup = NpbFollowup.find(params[:id])
    @npb_followup.followable = @followable
  end

  # GET /npb_followups/new
  def new
    @npb_followup =  NpbFollowup.new
    @npb_followup.followable = @followable
  end

  # GET /npb_followups/1/edit
  def edit
    @npb_followup = NpbFollowup.find(params[:id])
    @npb_followup.followable = @followable
  end

  # POST /npb_followups
  # POST /npb_followups.json
  def create
    @npb_followup = NpbFollowup.new(npb_followup_params)
    @npb_followup.followable = @followable

    respond_to do |format|
      if @npb_followup.save
        format.html { redirect_to polymorphic_url([@followable.patient, @followable, @npb_followup]), notice: t('npb_followups.defaults.submit.success') }
        format.json { render :show, status: :created, location: @npb_followup }
      else
        format.html { render :new }
        format.json { render json: @npb_followup.errors, status: :unprocessable_entity }
      end
    end
  end

  # PATCH/PUT /npb_followups/1
  # PATCH/PUT /npb_followups/1.json
  def update
    respond_to do |format|
      if @npb_followup.update(npb_followup_params)
        format.html { redirect_to polymorphic_url([@followable.patient, @followable, @npb_followup]), notice: 'NP-B followup questionnaire was successfully updated.' }
        format.json { render :show, status: :ok, location: @npb_followup }
      else
        format.html { render :edit }
        format.json { render json: @npb_followup.errors, status: :unprocessable_entity }
      end
    end
  end

  # DELETE /npb_followups/1
  # DELETE /npb_followups/1.json
  def destroy
    @npb_followup.destroy
    respond_to do |format|
      format.html { redirect_to polymorphic_url([@followable.patient, @followable, NpbFollowup]), notice: 'NP-B followup questionnaire was successfully destroyed.' }
      format.json { head :no_content }
    end
  end

  private
  # Use callbacks to share common setup or constraints between actions.
  def set_npb_followup
    @npb_followup = NpbFollowup.find(params[:id])
  end

  # Never trust parameters from the scary internet, only allow the white list through.
  def npb_followup_params
    params.require(:npb_followup).permit(
        :symptom_bone,
        :symptom_abdominal,
        :symptom_pain,
        :symptom_breathlessness_rate,
        :symptom_breathlessness_impact,
        :symptom_bleeding_rate,
        :symptom_bleeding_impact,
        :symptom_fatigue_rate,
        :symptom_fatigue_impact,
        :symptom_enlarge_organ,
        :symptom_enlarge_organ_impact,
        :symptom_slow_growth,
        :symptom_slow_growth_impact,
        :symptom_infection,
        :symptom_fracture,
        :symptom_cognition,

        :impact_family_disappointed,
        :impact_family_give_up,
        :impact_family_worry_future,
        :impact_family_closer,

        :impact_wider_self_miss_career,
        :impact_wider_carer_miss_career,
        :impact_wider_emergency,
        :impact_wider_appointment
        )

  end

  def get_followable
    @followable = params[:followable].classify.constantize.find(followable_id)
  end

  def followable_id
    params[(params[:followable].singularize + '_id').to_sym]
  end

end
