class NpaApplicantParticipantsController < ApplicationController
  before_action :authenticate_user!
  before_action :set_npa_applicant_participant, :only => [:show, :edit, :update, :destroy]
  before_filter :get_patient

  # GET /npa_applicant_participants
  # GET /npa_applicant_participants.json
  def index
    if current_user.study_team
      @participants_verified = NpaApplicantParticipant.where(:verified => true)
    else
      @participants_verified = current_user.npa_applicant_participants.where(:verified => true)
    end

    @participants_unverified = NpaApplicantParticipant.where(:verified => false)
  end

  # GET /npa_applicant_participants/1
  # GET /npa_applicant_participants/1.json
  def show
    @participant = NpaApplicantParticipant.find(params[:id])
    respond_to do |format|
      format.html # show.html.erb
      format.json { render json: @participant }
    end
  end

  # GET /npa_applicant_participants/new
  def new
    @npa_applicant_participant = NpaApplicantParticipant.new
    authorize @npa_applicant_participant
  end

  # GET /npa_applicant_participants/1/edit
  def edit
    authorize @npa_applicant_participant
  end


  # POST /npa_applicant_participants
  # POST /npa_applicant_participants.json
  def create
    @npa_applicant_participant = NpaApplicantParticipant.new(npa_applicant_participant_params)
    authorize @npa_applicant_participant
    @npa_applicant_participant.patient = @patient
    respond_to do |format|
      if @npa_applicant_participant.not_finish?
        if @npa_applicant_participant.save
          format.html { redirect_to patients_url, notice: t("#{controller_name}.notice.save_for_later") }
          format.json { render :show, :status => :created, :location => @npa_applicant_participant }
        else
          format.html { render :new }
          format.json { render :json => @npa_applicant_participant.errors, :status => :unprocessable_entity }
        end
      else
        if @npa_applicant_participant.save
          #format.html { redirect_to patients_path }
          format.html { redirect_to pages_thank_enrol_path }
          format.json { render :show, :status => :created, :location => @npa_applicant_participant }
        else
          format.html { render :new }
          format.json { render :json => @npa_applicant_participant.errors, :status => :unprocessable_entity }
        end
      end
    end
  end


  # PATCH/PUT /npa_applicant_participants/1
  # PATCH/PUT /npa_applicant_participants/1.json
  def update
    respond_to do |format|
      if @npa_applicant_participant.update(npa_applicant_participant_params)
        format.html { redirect_to @npa_applicant_participant, notice: 'NP-A applicant participant was successfully updated.' }
        format.json { render :show, status: :ok, location: @npa_applicant_participant }
      else
        format.html { render :edit }
        format.json { render json: @npa_applicant_participant.errors, status: :unprocessable_entity }
      end
    end
  end

  # DELETE /npa_applicant_participants/1
  # DELETE /npa_applicant_participants/1.json
  def destroy
    @npa_applicant_participant.destroy
    respond_to do |format|
      format.html { redirect_to npa_applicant_participants_url, notice: 'NP-A applicant participant was successfully destroyed.' }
      format.json { head :no_content }
    end
  end

  # GET /npa_applicant_participants/1/verify
  # GET /npa_applicant_participants/1/verify.json
  def verify
    @npa_applicant_participant = NpaApplicantParticipant.find(params[:id])
    unless @npa_applicant_participant.verified
      @npa_applicant_participant.verified = true
    end

    respond_to do |format|
      if @npa_applicant_participant.save
        ParticipantMailer.verify_email(@npa_applicant_participant).deliver_now
        format.html { redirect_to npa_applicant_participants_path, notice: 'Successfully verified participant.' }
        format.json { render :show, status: :ok, location: @npa_applicant_participant }
      else
        format.html { render :show }
        format.json { render json: @npa_applicant_participant.errors, status: :unprocessable_entity }
      end
    end
  end

  private
  # Use callbacks to share common setup or constraints between actions.
  def set_npa_applicant_participant
    @npa_applicant_participant = NpaApplicantParticipant.find(params[:id])
  end

  # Never trust parameters from the scary internet, only allow the white list through.
  def npa_applicant_participant_params
    params.require(:npa_applicant_participant).permit(:verified,
                                                      :age_diagnosed_known,
                                                      :age_diagnosed_year,
                                                      :age_diagnosed_month,
                                                      :family_diagnosed,
                                                      :family_diagnosed_relation,
                                                      :applicant_relation,
                                                      :applicant_relation_specify,
                                                      :hospital_name,
                                                      { address_attributes: [
                                                          :_destroy,
                                                          :line_1,
                                                          :line_2,
                                                          :town,
                                                          :county,
                                                          :post_code,
                                                          :country
                                                      ] },
                                                      :symptom_age_year,
                                                      :symptom_age_month,
                                                      :symptom_age_unknown,
                                                      :symptom_age_na,
                                                      :hospital_clinician_name,
                                                      :hospital_phone,
                                                      :hospital_email,
                                                      :gene_diagnosis,
                                                      :gene_diagnosis_cache,
                                                      :gene_test_performed,
                                                      :unable_to_contact_specialist,
                                                      :consent,
                                                      :consent_name,
                                                      :not_finish,
                                                      { specialists_attributes: [:_destroy,
                                                                                 :id,
                                                                                 :name,
                                                                                 :hospital_name,
                                                                                 { address_attributes: [
                                                                                     :_destroy,
                                                                                     :line_1,
                                                                                     :line_2,
                                                                                     :town,
                                                                                     :county,
                                                                                     :post_code,
                                                                                     :country
                                                                                 ] },
                                                                                 :number,
                                                                                 :email] })
  end

end
