class PagesController < ApplicationController

  before_action :authenticate_user!, :only => [:enrol, :followups, :home, :thank_enrol, :verifications]
  def index
    render :file => 'pages/index.html.erb'
  end

  # def sign_in
  #   render :file => 'pages/SignIn.html.erb', :layout => false
  # end
  #
  # def forgot_password
  #   render :file => 'pages/ForgotPassword.html.erb', :layout => false
  # end

  def registration
  end

  def followups
    @user = current_user
  end

  def verifications
  end

  def enrol
  end

  def thank_enrol
  end

end
