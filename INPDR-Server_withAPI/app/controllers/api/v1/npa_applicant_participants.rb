module API
  module V1
    class NpaApplicantParticipants < Grape::API
      include API::V1::Defaults

      resource :npa_applicant_participants do
        desc "Return all npa_applicant_participants"
        get "", root: :npa_applicant_participants do
          policy_scope(NpaApplicantParticipant)
        end

        desc "Return a npa_applicant_participants"
        params do
          requires :id, type: String, desc: "ID of the
            npa_applicant_participants"
        end
        get ":id", root: "npa_applicant_participants" do
          participant = NpaApplicantParticipant.where(id: permitted_params[:id]).first!
          authorize participant, :show? unless participant.nil?
          participant
        end

        # desc "create a new npa_applicant_participants"
        # params do
        #   requires :consent, type: Boolean
        #   requires :consent_name, type: String
        # end
        # post do
        #   NpaApplicantParticipant.creat!({
        #     gender: params[:gender],
        #     date_of_birth: params[:date_of_birth],
        #     first_name: params[:first_name],
        #     middle_name: params[:middle_name],
        #     surname: params[:surname],
        #     age_diagnosed_known: params[:age_diagnosed_known],
        #     age_diagnosed_year: params[:age_diagnosed_year],
        #     age_diagnosed_month: params[:age_diagnosed_month],
        #     family_diagnosed: params[:family_diagnosed],
        #     family_diagnosed_relation: params[:family_diagnosed_relation],
        #
        #     })
      end
    end
  end
end
