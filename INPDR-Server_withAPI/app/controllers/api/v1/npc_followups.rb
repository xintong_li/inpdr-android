module API
  module V1
    class NpcFollowups < Grape::API
      include API::V1::Defaults

      # http://localhost:3000/api/v1/npc_followups/6?id=3
      # desc "Return a npc_followup"
      # params do
      #   requires :followable_id, type: String, desc: "id of the
      #     npc_participant"
      #   optional :id, type: Integer, desc: "id of the npc_followup"
      # end
      # get ":followable_id", root: :npc_followups do
      #     NpcFollowup.where(followable_id: params[:followable_id], id: params[:id])
      # end

      # http://localhost:3000/api/v1/npc_followups/6

      desc "Return the amount of followups"
      params do
        requires :followable_id, type: Integer, desc: "id of the npc_participant"
        requires :followable_type, type: String, desc: "npc_participant or npc_applicant_participant"
      end
      get ":followable_type/:followable_id/npc_followups/count", root: :count do
        followable_type = params[:followable_type].camelize
        followable_id = params[:followable_id]
        followable =  followable_type.constantize.find(followable_id)
        authorize followable, :show? unless followable.nil?
        NpcFollowup.where(followable_id: followable_id, followable_type: followable_type).count
      end


      desc "Return a npc_followup belongs to a particular npc_participant"
      params do
        requires :followable_type, type: String, desc: "npc_participant or npc_applicant_participant"
        requires :followable_id, type: Integer, desc: "id of the npc_participant"
        requires :id, type: Integer, desc: "id of the followup"
      end
      get ":followable_type/:followable_id/npc_followups/:id", root: :npc_followups do
        followable_type = params[:followable_type].camelize
        followable_id = params[:followable_id]
        id = params[:id]
        followable =  followable_type.constantize.find(followable_id)
        authorize followable, :show? unless followable.nil?
        followup = NpcFollowup.where(id: id,
                          followable_id: followable_id,
                          followable_type: followable_type)

      end

      desc "Return all npc_followups that belongs a particular this npc_participant"
      params do
        requires :followable_id, type: Integer, desc: "id of the npc_participant"
        requires :followable_type, type: String, desc: "npc_participant or npc_applicant_participant"
      end
      get ":followable_type/:followable_id/npc_followups", root: :npc_followups do
        followable_type = params[:followable_type].camelize
        followable_id = params[:followable_id]
        followable =  followable_type.constantize.find(followable_id)
        authorize followable, :show? unless followable.nil?
        NpcFollowup.where(followable_id: followable_id, followable_type: followable_type)
      end

      desc "Delete a npc_followup"
      params do
        requires :followable_id, type: Integer, desc: "id of the npc_participant"
        requires :id, type: Integer, desc: "id of the NpcFollowup"
        requires :followable_type, type: String, desc: "npc_participant or npc_applicant_participant"
      end
      delete ":followable_type/:followable_id/npc_followups/:id", root: :npc_followups do
        followable_type = params[:followable_type].camelize
        followable_id = params[:followable_id]
        id = params[:id]
        authorize NpcFollowup, :destroy?
        NpcFollowup.where(id: id,
                          followable_id: followable_id,
                          followable_type: followable_type).first.destroy!
      end

      desc "Create a NpcFollowup"
      params do
        requires :followable_id, type: Integer, desc: "id of the npc_participant or npc_applicant_participants"
        requires :followable_type, type: String, desc: "npc_participants or npc_applicant_participants"
      end
      post ":followable_type/:followable_id/npc_followups" do
        followable_type = params[:followable_type].camelize
        followable_id = params[:followable_id]
        followable =  followable_type.constantize.find(followable_id)
        authorize followable, :create? unless followable.nil?
        NpcFollowup.create!({
                                impact_ambulation: params[:impact_ambulation],
                                impact_manipulation: params[:impact_manipulation],
                                impact_speech: params[:impact_speech],
                                impact_swallowing: params[:impact_swallowing],
                                impact_eye_movement: params[:impact_eye_movement],
                                impact_seizure: params[:impact_seizure],
                                impact_cognitive_impaired: params[:impact_cognitive_impaired],
                                impact_family_disappointed: params[:impact_family_disappointed],
                                impact_family_give_up: params[:impact_family_give_up],
                                impact_family_worry_future: params[:impact_family_worry_future],
                                impact_family_closer_family: params[:impact_family_closer_family],
                                impact_wider_self_miss_career: params[:impact_wider_self_miss_career],
                                impact_wider_family_miss_career: params[:impact_wider_family_miss_career],
                                impact_wider_emergency: params[:impact_wider_emergency],
                                impact_wider_appointment: params[:impact_wider_appointment],
                                followable_id: followable_id,
                                followable_type: followable_type
                            })
      end

      desc "Update a NpcFollowup"
      params do
        requires :followable_type, type: String, desc: "npc_participants or npc_applicant_participants"
        requires :followable_id, type: Integer, desc: "id of the npc_participant or npc_applicant_participants"
        requires :id, type: Integer, desc: "id of the NpcFollowup"
      end
      put ":followable_type/:followable_id/npc_followups/:id" do
        followable_type = params[:followable_type].camelize
        followable_id = params[:followable_id]
        id = params[:id]
        followable =  followable_type.constantize.find(followable_id)
        authorize followable, :update? unless followable.nil?
        NpcFollowup.where(followable_type: followable_type,
                          followable_id: followable_id,
                          id: id).first.update!({
                                                    impact_ambulation: params[:impact_ambulation],
                                                    impact_manipulation: params[:impact_manipulation],
                                                    impact_speech: params[:impact_speech],
                                                    impact_swallowing: params[:impact_swallowing],
                                                    impact_eye_movement: params[:impact_eye_movement],
                                                    impact_seizure: params[:impact_seizure],
                                                    impact_cognitive_impaired: params[:impact_cognitive_impaired],
                                                    impact_family_disappointed: params[:impact_family_disappointed],
                                                    impact_family_give_up: params[:impact_family_give_up],
                                                    impact_family_worry_future: params[:impact_family_worry_future],
                                                    impact_family_closer_family: params[:impact_family_closer_family],
                                                    impact_wider_self_miss_career: params[:impact_wider_self_miss_career],
                                                    impact_wider_family_miss_career: params[:impact_wider_family_miss_career],
                                                    impact_wider_emergency: params[:impact_wider_emergency],
                                                    impact_wider_appointment: params[:impact_wider_appointment],
                                                })
      end
    end


  end
end
