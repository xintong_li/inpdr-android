module API
  module V1
    class NpbApplicantParticipants < Grape::API
      include API::V1::Defaults

      resource :npb_applicant_participants do
        desc "Return all npb_applicant_participants"
        get "", root: :npb_applicant_participants do
          policy_scope(NpbApplicantParticipant)
        end

        desc "Return a npb_applicant_participants"
        params do
          requires :id, type: String, desc: "id of the
            npb_applicant_participants"
        end
        get ":id", root: "npb_applicant_participants" do
          participant = NpbApplicantParticipant.where(id: permitted_params[:id]).first!
          authorize participant, :show? unless participant.nil?
          participant
        end
      end
    end
  end
end