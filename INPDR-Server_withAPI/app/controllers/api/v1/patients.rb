module API
  module V1
    class Patients < Grape::API
      include API::V1::Defaults

      resource :patients do
        desc "Return all patients"
        get "", root: "patients" do
          @patients = policy_scope(Patient)
          present @patients, :with => API::Entities::Patients
        end

        desc "Return a Patient"
        params do
          requires :id, type: String, desc: "ID of the
            Patient"
        end
        get ":id", root: "patients" do
          @patients = Patient.where(id: permitted_params[:id])
          authorize @patients[0], :show? unless @patients.blank?
          present @patients, :with => API::Entities::Patients
        end

        desc "Return a Patient belongs to user"
        params do
          requires :user_id, type: String, desc: "user_id of the
            Patient"
        end

        # Use /patients
        # get "/user_id/:user_id", root: "patients" do
        #   @patient = Patient.where(user_id: permitted_params[:user_id])
        #   present @patient, :with => API::Entities::Patients
        # end
      end
    end
  end
end
