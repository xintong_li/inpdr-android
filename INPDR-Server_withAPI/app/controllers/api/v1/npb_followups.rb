module API
  module V1
    class NpbFollowups < Grape::API
      include API::V1::Defaults

      desc "Return the amount of followups"
      params do
        requires :followable_id, type: Integer, desc: "id of the npb_participant"
        requires :followable_type, type: String, desc: "npb_participant or npb_applicant_participant"
      end
      get ":followable_type/:followable_id/npb_followups/count", root: :count do
        followable_type = params[:followable_type].camelize
        followable_id = params[:followable_id]
        followable =  followable_type.constantize.find(followable_id)
        authorize followable, :show? unless followable.nil?
        NpbFollowup.where(followable_id: followable_id, followable_type: followable_type).count
      end

      desc "Return a npb_followup belongs to a particular npb_participant"
      params do
        requires :followable_type, type: String, desc: "npb_participant or npb_applicant_participant"
        requires :id, type: Integer, desc: "id of the followup"
        requires :followable_id, type: Integer, desc: "id of the partient"
      end
      get ":followable_type/:followable_id/npb_followups/:id", root: :npb_followups do
        followable_type = params[:followable_type].camelize
        followable_id = params[:followable_id]
        id = params[:id]
        followable =  followable_type.constantize.find(followable_id)
        authorize followable, :show? unless followable.nil?
        NpbFollowup.where(id: id,
                          followable_id: followable_id,
                          followable_type: followable_type)
      end

      desc "Return all npb_followups that belongs to a particular npb_participant"
      params do
        requires :followable_id, type: Integer, desc: "id of the npb_participant"
        requires :followable_type, type: String, desc: "npb_participant or npb_applicant_participant"
      end
      get ":followable_type/:followable_id/npb_followups", root: :npb_followups do
        followable_type = params[:followable_type].camelize
        followable_id = params[:followable_id]
        followable =  followable_type.constantize.find(followable_id)
        authorize followable, :show? unless followable.nil?
        NpbFollowup.where(followable_id: followable_id, followable_type: followable_type)
      end

      desc "Delete a npb_followup"
      params do
        requires :followable_type, type: String, desc: "npb_participant or npb_applicant_participant"
        requires :id, type: Integer, desc: "id of the followup"
        requires :followable_id, type: Integer, desc: "id of the partient"
      end
      delete ":followable_type/:followable_id/npb_followups/:id", root: :npb_followups do
        followable_type = params[:followable_type].camelize
        followable_id = params[:followable_id]
        id = params[:id]
        authorize NpbFollowup, :destroy?
        NpbFollowup.where(id: id,
                          followable_id: followable_id,
                          followable_type: followable_type).first.destroy!
      end

      desc "Create a NpbFollowup"
      params do
        requires :followable_id, type: Integer, desc: "id of the npb_participant"
        requires :followable_type, type: String, desc: "npb_applicant_participants or npb_participant"
      end
      post ":followable_type/:followable_id/npb_followups" do
        followable_type = params[:followable_type].camelize
        followable_id = params[:followable_id]
        followable =  followable_type.constantize.find(followable_id)
        authorize followable, :create? unless followable.nil?
        NpbFollowup.create!({
                                symptom_bone: params[:symptom_bone],
                                symptom_abdominal: params[:symptom_abdominal],
                                symptom_pain: params[:symptom_pain],
                                symptom_breathlessness_rate: params[:symptom_breathlessness_rate],
                                symptom_breathlessness_impact: params[:symptom_breathlessness_impact],
                                symptom_bleeding_rate: params[:symptom_bleeding_rate],
                                symptom_bleeding_impact: params[:symptom_bleeding_impact],
                                symptom_fatigue_rate: params[:symptom_fatigue_rate],
                                symptom_fatigue_impact: params[:symptom_fatigue_impact],
                                symptom_enlarge_organ: params[:symptom_enlarge_organ],
                                symptom_enlarge_organ_impact: params[:symptom_enlarge_organ_impact],
                                symptom_slow_growth: params[:symptom_slow_growth],
                                symptom_slow_growth_impact: params[:symptom_slow_growth_impact],
                                symptom_infection: params[:symptom_infection],
                                symptom_fracture: params[:symptom_fracture],
                                symptom_cognition: params[:symptom_cognition],
                                impact_family_disappointed: params[:impact_family_disappointed],
                                impact_family_give_up: params[:impact_family_give_up],
                                impact_family_worry_future: params[:impact_family_worry_future],
                                impact_family_closer: params[:impact_family_closer],
                                impact_wider_self_miss_career: params[:impact_wider_self_miss_career],
                                impact_wider_carer_miss_career: params[:impact_wider_carer_miss_career],
                                impact_wider_emergency: params[:impact_wider_emergency],
                                impact_wider_appointment: params[:impact_wider_appointment],
                                followable_id: followable_id,
                                followable_type: followable_type
                            })
      end

      desc "Update a NpbFollowup"
      params do
        requires :followable_type, type: String, desc: "npb_participants or npb_applicant_participants"
        requires :followable_id, type: Integer, desc: "id of the npb_followups"
        requires :id, type: Integer, desc: "id of the NpbFollowup"
      end
      put ":followable_type/:followable_id/npb_followups/:id" do
        followable_type = params[:followable_type].camelize
        followable_id = params[:followable_id]
        id = params[:id]
        followable =  followable_type.constantize.find(followable_id)
        authorize followable, :update? unless followable.nil?
        NpbFollowup.where(followable_type: followable_type,
                          followable_id: followable_id,
                          id: id).first.update!({
                                                    symptom_bone: params[:symptom_bone],
                                                    symptom_abdominal: params[:symptom_abdominal],
                                                    symptom_pain: params[:symptom_pain],
                                                    symptom_breathlessness_rate: params[:symptom_breathlessness_rate],
                                                    symptom_breathlessness_impact: params[:symptom_breathlessness_impact],
                                                    symptom_bleeding_rate: params[:symptom_bleeding_rate],
                                                    symptom_bleeding_impact: params[:symptom_bleeding_impact],
                                                    symptom_fatigue_rate: params[:symptom_fatigue_rate],
                                                    symptom_fatigue_impact: params[:symptom_fatigue_impact],
                                                    symptom_enlarge_organ: params[:symptom_enlarge_organ],
                                                    symptom_enlarge_organ_impact: params[:symptom_enlarge_organ_impact],
                                                    symptom_slow_growth: params[:symptom_slow_growth],
                                                    symptom_slow_growth_impact: params[:symptom_slow_growth_impact],
                                                    symptom_infection: params[:symptom_infection],
                                                    symptom_fracture: params[:symptom_fracture],
                                                    symptom_cognition: params[:symptom_cognition],
                                                    impact_family_disappointed: params[:impact_family_disappointed],
                                                    impact_family_give_up: params[:impact_family_give_up],
                                                    impact_family_worry_future: params[:impact_family_worry_future],
                                                    impact_family_closer: params[:impact_family_closer],
                                                    impact_wider_self_miss_career: params[:impact_wider_self_miss_career],
                                                    impact_wider_carer_miss_career: params[:impact_wider_carer_miss_career],
                                                    impact_wider_emergency: params[:impact_wider_emergency],
                                                    impact_wider_appointment: params[:impact_wider_appointment],
                                                })
      end
    end

  end
end
