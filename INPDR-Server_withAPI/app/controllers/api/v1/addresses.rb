module API
  module V1
    class Addresses < Grape::API
      include API::V1::Defaults

      resource :addresses do
        desc "Return all addresses"
        get "", root: "addresses" do
          Address.all
         
          # User.all
        end

        desc "Return an address"
        params do
          requires :id, type: String, desc: "ID of the
            user"
        end
        get ":id", root: "Addresses" do
          Address.where(id: permitted_params[:id])
        end
      end
    end
  end
end