module API
  module V1
    class NpcApplicantParticipants < Grape::API
      include API::V1::Defaults

      resource :npc_applicant_participants do
        desc "Return all npc_applicant_participants"
        get "", root: :npc_applicant_participants do
          policy_scope(NpcApplicantParticipant)
        end

        desc "Return a npc_applicant_participants"
        params do
          requires :id, type: String, desc: "id of the
            npc_applicant_participants"
        end
        get ":id", root: "npc_applicant_participants" do
          participant = NpcApplicantParticipant.where(id: permitted_params[:id]).first!
          authorize participant, :show? unless participant.nil?
          participant
        end
      end
    end
  end
end
