module API
  module V1
    class Base < Grape::API
      mount API::V1::NpaApplicantParticipants
      mount API::V1::NpcParticipants
      mount API::V1::NpcFollowups
      mount API::V1::NpcApplicantParticipants
      mount API::V1::NpbFollowups
      mount API::V1::NpbParticipants
      mount API::V1::NpbApplicantParticipants
      mount API::V1::Patients
    end
  end
end
