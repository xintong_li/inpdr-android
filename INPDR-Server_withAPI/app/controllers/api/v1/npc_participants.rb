module API
  module V1
    class NpcParticipants < Grape::API
      include API::V1::Defaults

      resource :npc_participants do
        desc "Return all npc_participants"
        get "", root: :npc_participants do
          policy_scope(NpcParticipant)
        end

        desc "Return a npc_participants"
        params do
          requires :id, type: String, desc: "id of the
            npc_participants"
        end
        get ":id", root: "npc_participants" do
          participant = NpcParticipant.where(id: permitted_params[:id]).first!
          authorize participant, :show? unless participant.nil?
          participant
        end

        # desc "Return a npc_followups"
        # params do
        #   requires :id, type: String, desc: "id of the
        #     npc_participants"
        # end
        # get ":id", root: "npc_followups" do
        #   NpcFollowups.where(id: permitted_params[:id]).first!
        # end

        # desc "create a new npc_participant"
        # params do
        #   requires :consent, type: Boolean
        #   requires :consent_name, type: String
        # end
        # post do
        #   NpcParticipant.create!({
        #     date_of_birth:params[:date_of_birth],
        #     gender:params[:gender],
        #     age_diagnosed_known:params[:age_diagnosed_known],
        #     age_diagnosed_year:params[:age_diagnosed_year],
        #     age_diagnosed_month:params[:age_diagnosed_month],
        #     family_diagnosed:params[:family_diagnosed],
        #     family_diagnosed_relation:params[:family_diagnosed_relation],
        #     hospital_name:params[:hospital_name],
        #     hospital_address:params[:hospital_address],
        #     hospital_email:params[:hospital_email],
        #     gene_diagnosis:params[:gene_diagnosis],
        #     gene_diagnosis_cache:params[:gene_diagnosis_cache],
        #     gene_test_performed:params[:gene_test_performed],
        #     baby_symptoms:params[:baby_symptoms],
        #     symptom_age_year:params[:symptom_age_year],
        #     symptom_age_month:params[:symptom_age_month],
        #     symptom_age_unknown:params[:symptom_age_unknown],
        #     symptom_age_na:params[:symptom_age_na],
        #     symptom_delayed_milestone:params[:symptom_delayed_milestone],
        #     symptom_ppers:params[:symptom_ppers],
        #     symptom_coordication:params[:symptom_coordication],
        #     symptom_eye:params[:symptom_eye],
        #     symptom_behavioural:params[:symptom_behavioural],
        #     symptom_seizure:params[:symptom_seizure],
        #     symptom_psychiatric:params[:symptom_psychiatric],
        #     hospital_clinician_name:params[:hospital_clinician_name],
        #     hospital_phone:params[:hospital_phone],
        #     consent:params[:consent],
        #     consent_name:params[:consent_name],
        #     symptom_other:params[:symptom_other],
        #     symptom_other_specify:params[:symptom_other_specify]
        #   })
        # end
        #
        # desc "delete an npc_participants"
        # params do
        #   requires :id, type: String, desc: "id of the
        #     npc_participants"
        # end
        # delete ':id' do
        #   NpcParticipant.find(params[:id]).destroy!
        # end
        #
        # desc "update an npc_participants"
        # params do
        #   requires :id, type: String, desc: "id of the
        #     npc_participants"
        # end
        # put ':id' do
        #   # NpcParticipant.find(params[:id]).update_attribute(:gender, params[:gender])
        #   NpcParticipant.find(params[:id]).update!({
        #     date_of_birth:params[:date_of_birth],
        #     gender:params[:gender],
        #     age_diagnosed_known:params[:age_diagnosed_known],
        #     age_diagnosed_year:params[:age_diagnosed_year],
        #     age_diagnosed_month:params[:age_diagnosed_month],
        #     family_diagnosed:params[:family_diagnosed],
        #     family_diagnosed_relation:params[:family_diagnosed_relation],
        #     hospital_name:params[:hospital_name],
        #     hospital_address:params[:hospital_address],
        #     hospital_email:params[:hospital_email],
        #     gene_diagnosis:params[:gene_diagnosis],
        #     gene_diagnosis_cache:params[:gene_diagnosis_cache],
        #     gene_test_performed:params[:gene_test_performed],
        #     baby_symptoms:params[:baby_symptoms],
        #     symptom_age_year:params[:symptom_age_year],
        #     symptom_age_month:params[:symptom_age_month],
        #     symptom_age_unknown:params[:symptom_age_unknown],
        #     symptom_age_na:params[:symptom_age_na],
        #     symptom_delayed_milestone:params[:symptom_delayed_milestone],
        #     symptom_ppers:params[:symptom_ppers],
        #     symptom_coordication:params[:symptom_coordication],
        #     symptom_eye:params[:symptom_eye],
        #     symptom_behavioural:params[:symptom_behavioural],
        #     symptom_seizure:params[:symptom_seizure],
        #     symptom_psychiatric:params[:symptom_psychiatric],
        #     symptom_other:params[:symptom_other],
        #     symptom_other_specify:params[:symptom_other_specify],
        #     hospital_clinician_name:params[:hospital_clinician_name],
        #     hospital_phone:params[:hospital_phone]
        #     })
        #     NpcParticipant.find(params[:id]).save!
        # end


      end
    end
  end
end
