module API
  module V1
    class NpbParticipants < Grape::API
      include API::V1::Defaults

      resource :npb_participants do
        desc "Return all npb_participants"
        get "", root: :npb_participants do
          policy_scope(NpbParticipant)
        end

        desc "Return a npb_participants"
        params do
          requires :id, type: String, desc: "id of the
            npb_participants"
        end
        get ":id", root: "npb_participants" do
          participant = NpbParticipant.where(id: permitted_params[:id]).first!
          authorize participant, :show? unless participant.nil?
          participant
        end

        desc "Return a npb_participants"
        params do
          requires :id, type: String, desc: "id of the
            npc_participants"
        end
        get ":id", root: "npb_participants" do
          NpbFollowups.where(id: permitted_params[:id]).first!
        end

        # Removed for coding consistency
        # desc "create a new npb_participants"
        # params do
        #     requires :consent, type: Boolean, desc: "consent"
        #     requires :consent_name, type: String, desc:"consent_name"
        # end
        ## This takes care of parameter validation

        # Removed for Security
        ## This takes care of creating NpbParticipant
        # post do
        #   {date_of_birth:params[:date_of_birth],
        #     gender:params[:gender],}
        # end
        # post do
        #   NpbParticipant.create!({
        #     date_of_birth:params[:date_of_birth],
        #     gender:params[:gender],
        #     first_name: params[:first_name],
        #     middle_name: params[:middle_name],
        #     surname: params[:surname],
        #     age_diagnosed_known:params[:age_diagnosed_known],
        #     age_diagnosed_year:params[:age_diagnosed_year],
        #     age_diagnosed_month:params[:age_diagnosed_month],
        #     family_diagnosed:params[:family_diagnosed],
        #     family_diagnosed_relation:params[:family_diagnosed_relation],
        #     applicant_relation:params[:applicant_relation],
        #     applicant_relation_specify:params[:applicant_relation_specify],
        #     hospital_name:params[:hospital_name],
        #     hospital_address:params[:hospital_address],
        #     hospital_email:params[:hospital_email],
        #     gene_diagnosis:params[:gene_diagnosis],
        #     gene_diagnosis_cache:params[:gene_diagnosis_cache],
        #     gene_test_performed:params[:gene_test_performed],
        #     symptom_age_year:params[:symptom_age_year],
        #     symptom_age_month:params[:symptom_age_month],
        #     symptom_age_unknown:params[:symptom_age_unknown],
        #     symptom_age_na:params[:symptom_age_na],
        #     symptom_enlarged_liver:params[:symptom_enlarged_liver],
        #     symptom_breathing:params[:symptom_breathing],
        #     symptom_anaemia:params[:symptom_anaemia],
        #     symptom_bleeding:params[:symptom_bleeding],
        #     symptom_growth_delay:params[:symptom_growth_delay],
        #     symptom_developmental_delay:params[:symptom_developmental_delay],
        #     symptom_bone_pain:params[:symptom_bone_pain],
        #     symptom_abdominal_pain:params[:symptom_abdominal_pain],
        #     symptom_other:params[:symptom_other],
        #     symptom_other_specify:params[:symptom_other_specify],
        #     hospital_clinician_name:params[:hospital_clinician_name],
        #     hospital_phone:params[:hospital_phone],
        #     consent:params[:consent],
        #     consent_name:params[:consent_name],
        #   })
        # end
        #
        # desc "delete an npb_participants"
        # params do
        #   requires :id, type: String, desc: "id of the
        #     npb_participants"
        # end
        # delete ':id' do
        #   NpbParticipant.find(params[:id]).destroy!
        # end

        # desc "update an npb_participants"
        # params do
        #   requires :id, type: String, desc: "id of the
        #     npb_participants"
        # end
        # put ':id' do
        #   # NpcParticipant.find(params[:id]).update_attribute(:gender, params[:gender])
        #   NpbParticipant.find(params[:id]).update!({
        #     date_of_birth:params[:date_of_birth],
        #     gender:params[:gender],
        #     age_diagnosed_known:params[:age_diagnosed_known],
        #     age_diagnosed_year:params[:age_diagnosed_year],
        #     age_diagnosed_month:params[:age_diagnosed_month],
        #     family_diagnosed:params[:family_diagnosed],
        #     family_diagnosed_relation:params[:family_diagnosed_relation],
        #     hospital_name:params[:hospital_name],
        #     hospital_address:params[:hospital_address],
        #     hospital_email:params[:hospital_email],
        #     gene_diagnosis:params[:gene_diagnosis],
        #     gene_diagnosis_cache:params[:gene_diagnosis_cache],
        #     gene_test_performed:params[:gene_test_performed],
        #     baby_symptoms:params[:baby_symptoms],
        #     symptom_age_year:params[:symptom_age_year],
        #     symptom_age_month:params[:symptom_age_month],
        #     symptom_age_unknown:params[:symptom_age_unknown],
        #     symptom_age_na:params[:symptom_age_na],
        #     symptom_delayed_milestone:params[:symptom_delayed_milestone],
        #     symptom_ppers:params[:symptom_ppers],
        #     symptom_coordication:params[:symptom_coordication],
        #     symptom_eye:params[:symptom_eye],
        #     symptom_behavioural:params[:symptom_behavioural],
        #     symptom_seizure:params[:symptom_seizure],
        #     symptom_psychiatric:params[:symptom_psychiatric],
        #     symptom_other:params[:symptom_other],
        #     symptom_other_specify:params[:symptom_other_specify],
        #     hospital_clinician_name:params[:hospital_clinician_name],
        #     hospital_phone:params[:hospital_phone],
        #     symptom_psychiatric:params[:symptom_psychiatric],
        #     symptom_other:params[:symptom_other],
        #     symptom_other_specify:params[:symptom_other_specify]
        #     })
        #     NpbParticipant.find(params[:id]).save!
        # end


      end
    end
  end
end
