module API
  class Session < Grape::API
    format :json
    desc 'End-points for the Session'

    params do
      requires :email, type: String, desc: 'email'
      requires :password, type: String, desc: 'password'
    end

    desc 'Session Login via email and password'
    post 'login' do
      email = params[:email]
      password = params[:password]

      if email.nil? or password.nil?
        error!({error_code: 404, error_message: "Invalid Email or Password."}, 401)
        return
      end

      user = User.find_by_email email
      if user.present? && user.valid_password?(password)
        user.ensure_authentication_token
        user.save
        {status: 'ok', token: user.authentication_token}
      else
        error_msg = 'Bad Authentication Parameters'
        error!({'error_msg' => error_msg}, 401)
      end
    end

    desc 'Session Logout via email and password'
    delete 'logout' do
      authentication_token = headers['X-Auth-Token']
      if authentication_token.nil?
        error!({error_code: 401, error_message: 'Bad Authentication Parameters'}, 401)
        return
      end

      user = User.find_by_authentication_token authentication_token
      if user.present?
        user.update_columns(authentication_token: nil)
        {status: 'ok', token: user.authentication_token}
      else
        error_msg = 'Bad Authentication Parameters'
        error!({'error_msg' => error_msg}, 401)
      end
    end

  end
end
