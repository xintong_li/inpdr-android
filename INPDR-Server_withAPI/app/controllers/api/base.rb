module API
  class Base < Grape::API
    prefix 'api'

    helpers Pundit
    helpers do
      def current_user
        resource_owner
      end
    end

    rescue_from Pundit::NotAuthorizedError do |e|
      error_response(message: 'Unauthorized action', status: 401)
    end

    mount API::V1::Base
    mount API::Session
  end
end 