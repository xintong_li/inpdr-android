module API
  module Entities
    class User < Grape::Entity
      expose :id
      expose :email
    end

    class Enrollable < Grape::Entity
      # npa
      expose :id
      expose :age_diagnosed_known
      expose :age_diagnosed_year
      expose :age_diagnosed_month
      expose :family_diagnosed
      expose :family_diagnosed_relation
      expose :applicant_relation, :if => Proc.new {|g| defined?(g.applicant_relation)}
      expose :applicant_relation_specify, :if => Proc.new {|g| defined?(g.applicant_relation_specify)}
      expose :gene_test_performed
      expose :symptom_age_year
      expose :symptom_age_month
      expose :symptom_age_unknown
      expose :symptom_age_na
      expose :hospital_clinician_name
      expose :gene_diagnosis
      expose :unable_to_contact_specialist
      expose :not_finish

      # npb_applicant
      expose :symptom_enlarged_liver, :if => Proc.new {|g| defined?(g.symptom_enlarged_liver)}
      expose :symptom_breathing, :if => Proc.new {|g| defined?(g.symptom_breathing)}
      expose :symptom_anaemia, :if => Proc.new {|g| defined?(g.symptom_anaemia)}
      expose :symptom_bleeding, :if => Proc.new {|g| defined?(g.symptom_bleesding)}
      expose :symptom_growth_delay, :if => Proc.new {|g| defined?(g.symptom_growth_delay)}
      expose :symptom_developmental_delay, :if => Proc.new {|g| defined?(g.symptom_developmental_delay)}
      expose :symptom_bone_pain, :if => Proc.new {|g| defined?(g.symptom_bone_pain)}
      expose :symptom_abdominal_pain, :if => Proc.new {|g| defined?(g.symptom_abdominal_pain)}
      expose :symptom_other, :if => Proc.new {|g| defined?(g.symptom_other)}
      expose :symptom_other_specify, :if => Proc.new {|g| defined?(g.symptom_other_specify)}


      # npc
      expose :baby_symptoms, :if => Proc.new {|g| defined?(g.baby_symptoms)}
      expose :symptom_delayed_milestone, :if => Proc.new {|g| defined?(g.symptom_delayed_milestone)}
      expose :symptom_ppers, :if => Proc.new {|g| defined?(g.symptom_ppers)}
      expose :symptom_coordication, :if => Proc.new {|g| defined?(g.symptom_coordication)}
      expose :symptom_eye, :if => Proc.new {|g| defined?(g.symptom_eye)}
      expose :symptom_behavioural, :if => Proc.new {|g| defined?(g.symptom_behavioural)}
      expose :symptom_seizure, :if => Proc.new {|g| defined?(g.symptom_seizure)}
      expose :symptom_psychiatric, :if => Proc.new {|g| defined?(g.symptom_psychiatric)}
    end

    class Patients < Grape::Entity
      expose :id
      expose :registrant_first_name
      expose :registrant_last_name
      expose :relation_to_patient
      expose :addresses_id
      expose :patient_first_name
      expose :patient_last_name
      expose :gender
      expose :date_of_birth
      expose :unique_id
      expose :consent
      expose :consent_name
      expose :verified
      expose :not_finish
      expose :user, using: API::Entities::User
      expose :enrollable_type
      expose :enrollable_id
      expose :enrollable, using: API::Entities::Enrollable

      def version
        options[:version]
      end
    end
  end
end
