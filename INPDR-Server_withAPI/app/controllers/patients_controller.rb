class PatientsController < ApplicationController
  before_action :authenticate_user!
  before_action :set_patient, :only => [:show, :edit, :update, :destroy]

  def enrol
  end

  # GET /patients/consent
  def consent
    @patient = Patient.new(patient_params)
    authorize @patient
  end

  # POST /patients/consent_action
  def consent_action
    @patient = Patient.new(patient_params)
    authorize @patient
    @patient.user = current_user
    @patient.generate_unique_id
    respond_to do |format|
      if @patient.valid?
        if @patient.validate_consent? && @patient.save
          ParticipantMailer.enrolment_email(current_user.email).deliver_now
          format.html { redirect_to patient_enrol_path(@patient) }
          format.json { render :show, status: :created, location: @patient }
        else
          flash.now[:alert] = t('flash.alert.consent')
          format.html { render :consent }
          format.json { render json: @patient.errors, status: :unprocessable_entity }
        end
      else
        @patient.populate
        format.html { render :new }
        format.json { render json: @patient.errors, status: :unprocessable_entity }
      end
    end
  end

  # GET /patients
  # GET /patients.json
  def index
    @user = current_user
    authorize Patient
    if @user.study_team
      @all_patients = Patient.all
    end
  end

  # GET /patients/1
  # GET /patients/1.json
  def show
    authorize @patient
  end

  # GET /patients/new
  def new
    @patient = Patient.new
    authorize @patient
    @patient.build_address
    specialist = Specialist.new
    specialist.build_address
    @patient.specialist = specialist
  end

  # GET /patients/1/edit
  def edit
  end

  # POST /patients
  # POST /patients.json
  def create
    @patient = Patient.new(patient_params)
    authorize @patient
    @patient.user = current_user
    respond_to do |format|
      # if @patient.not_finish?
      #   if @patient.save(validate: false)
      #     format.html { redirect_to patients_path }
      #     format.json { render :show, status: :created, location: @patient }
      #   else
      #     format.html { render :new }
      #     format.json { render json: @patient.errors, status: :unprocessable_entity }
      #   end
      # else
        if @patient.valid?
          format.html { render :consent }
          format.json { render :consent }
        else
          format.html { render :new }
          format.json { render json: @patient.errors, status: :unprocessable_entity }
        end
      # end
    end
  end

  # PATCH/PUT /patients/1
  # PATCH/PUT /patients/1.json
  def update
    authorize @patient
    @patient.user = current_user
    respond_to do |format|
      if @patient.update(patient_params)
        format.html { redirect_to @patient, :notice => 'Patient was successfully updated.' }
        format.json { render :show, :status => :ok, :location => @patient }
      else
        format.html { render :edit }
        format.json { render :json => @patient.errors, :status => :unprocessable_entity }
      end
    end
  end

  # DELETE /patients/1
  # DELETE /patients/1.json
  def destroy
    @patient.destroy
    authorize @patient
    respond_to do |format|
      format.html { redirect_to patients_url, :notice => 'Patient was successfully destroyed.' }
      format.json { head :no_content }
    end
  end

  # GET /patients/1/verify
  # GET /patients/1/verify.json
  def verify
    @patient = Patient.find(params[:id])
    authorize @patient
    if !@patient.verified? && (defined? @patient.enrollable) && !@patient.enrollable.not_finish?
      @patient.verified = true
    end

    respond_to do |format|
      if @patient.save
        ParticipantMailer.verify_email(@patient).deliver_now
        format.html { redirect_to patients_path, notice: 'Successfully verified participant.' }
        format.json { render :show, status: :ok, location: @patient }
      else
        format.html { render :show }
        format.json { render json: @patient.errors, status: :unprocessable_entity }
      end
    end
  end

  private
  # Use callbacks to share common setup or constraints between actions.
  def set_patient
    @patient = Patient.find(params[:id])
  end

  # Never trust parameters from the scary internet, only allow the white list through.
  def patient_params
    params.require(:patient).permit(:registrant_first_name,
                                    :registrant_last_name,
                                    :relation_to_patient,
                                    {:address_attributes => [
                                        :_destroy,
                                        :line_1,
                                        :line_2,
                                        :town,
                                        :county,
                                        :post_code,
                                        :country
                                    ]},
                                    :patient_first_name,
                                    :patient_last_name,
                                    :gender,
                                    :date_of_birth,
                                    :consent,
                                    :consent_name,
                                    :not_finish,
                                    {:specialist_attributes => [:id,
                                                                 :name,
                                                                 :hospital_name,
                                                                 {:address_attributes => [
                                                                     :_destroy,
                                                                     :line_1,
                                                                     :line_2,
                                                                     :town,
                                                                     :county,
                                                                     :post_code,
                                                                     :country
                                                                 ]},
                                                                 :number,
                                                                 :email]})
  end
end
