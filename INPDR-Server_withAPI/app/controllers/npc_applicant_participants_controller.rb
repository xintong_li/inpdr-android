class NpcApplicantParticipantsController < ApplicationController
  before_action :authenticate_user!
  before_action :set_npc_applicant_participant, only: [:show, :edit, :update, :destroy]
  before_filter :get_patient

  # GET /npc_applicant_participants
  # GET /npc_applicant_participants.json
  def index
    authorize NpcApplicantParticipant
    if current_user.study_team
      @participants_verified = NpcApplicantParticipant.where(:verified => true)
    else
      @participants_verified = current_user.npc_applicant_participants.where(:verified => true)
    end

    @participants_unverified = NpcApplicantParticipant.where(:verified => false)
  end

  # GET /npc_applicant_participants/1
  # GET /npc_applicant_participants/1.json
  def show
    @participant = NpcApplicantParticipant.find(params[:id])
    authorize @participant
    respond_to do |format|
      format.html # show.html.erb
      format.json { render json: @participant }
    end
  end

  # GET /npc_applicant_participants/new
  def new
    @npc_applicant_participant = NpcApplicantParticipant.new
    authorize @npc_applicant_participant
  end

  # GET /npc_applicant_participants/1/edit
  def edit
    authorize @npc_applicant_participant
  end

  # POST /npc_applicant_participants
  # POST /npc_applicant_participants.json
  def create
    @npc_applicant_participant = NpcApplicantParticipant.new(npc_applicant_participant_params)
    authorize @npc_applicant_participant
    @npc_applicant_participant.patient = @patient
    respond_to do |format|
      if @npc_applicant_participant.not_finish?
        if @npc_applicant_participant.save
          format.html { redirect_to patients_url, notice: t("#{controller_name}.notice.save_for_later") }
          format.json { render :show, :status => :created, :location => @npc_applicant_participant }
        else
          format.html { render :new }
          format.json { render :json => @npc_applicant_participant.errors, :status => :unprocessable_entity }
        end
      else
        if @npc_applicant_participant.save
          #format.html { redirect_to patients_path }
          format.html { redirect_to pages_thank_enrol_path }
          format.json { render :show, :status => :created, :location => @npc_applicant_participant }
        else
          format.html { render :new }
          format.json { render :json => @npc_applicant_participant.errors, :status => :unprocessable_entity }
        end
      end
    end
  end

  # PATCH/PUT /npc_applicant_participants/1
  # PATCH/PUT /npc_applicant_participants/1.json
  def update
    authorize @npc_applicant_participant
    respond_to do |format|
      if @npc_applicant_participant.update(npc_applicant_participant_params)
        format.html { redirect_to @npc_applicant_participant, notice: 'NP-C applicant participant was successfully updated.' }
        format.json { render :show, status: :ok, location: @npc_applicant_participant }
      else
        format.html { render :edit }
        format.json { render json: @npc_applicant_participant.errors, status: :unprocessable_entity }
      end
    end
  end

  # DELETE /npc_applicant_participants/1
  # DELETE /npc_applicant_participants/1.json
  def destroy
    authorize @npc_applicant_participant
    @npc_applicant_participant.destroy
    respond_to do |format|
      format.html { redirect_to npc_applicant_participants_url, notice: 'NP-Capplicant participant was successfully destroyed.' }
      format.json { head :no_content }
    end
  end

  # GET /npc_applicant_participants/1/verify
  # GET /npc_applicant_participants/1/verify.json
  def verify
    @npc_applicant_participant = NpcApplicantParticipant.find(params[:id])
    authorize @npc_applicant_participant
    unless @npc_applicant_participant.verified
      @npc_applicant_participant.verified = true
    end

    respond_to do |format|
      if @npc_applicant_participant.save
        ParticipantMailer.verify_email(@npc_applicant_participant).deliver_now
        format.html { redirect_to npc_applicant_participants_path, notice: 'Successfully verified participant.' }
        format.json { render :show, status: :ok, location: @npc_applicant_participant }
      else
        format.html { render :show }
        format.json { render json: @npc_applicant_participant.errors, status: :unprocessable_entity }
      end
    end
  end

  private
  # Use callbacks to share common setup or constraints between actions.
  def set_npc_applicant_participant
    @npc_applicant_participant = NpcApplicantParticipant.find(params[:id])
  end

  # Never trust parameters from the scary internet, only allow the white list through.
  def npc_applicant_participant_params
    params.require(:npc_applicant_participant).permit(:age_diagnosed_known,
                                                      :age_diagnosed_year,
                                                      :age_diagnosed_month,
                                                      :family_diagnosed,
                                                      :family_diagnosed_relation,
                                                      :applicant_relation,
                                                      :applicant_relation_specify,
                                                      :hospital_name,
                                                      { address_attributes: [
                                                          :_destroy,
                                                          :line_1,
                                                          :line_2,
                                                          :town,
                                                          :county,
                                                          :post_code,
                                                          :country
                                                      ] },
                                                      :hospital_email,
                                                      :gene_diagnosis,
                                                      :gene_diagnosis_cache,
                                                      :gene_test_performed,
                                                      :unable_to_contact_specialist,
                                                      :baby_symptoms,
                                                      :symptom_age_year,
                                                      :symptom_age_month,
                                                      :symptom_age_unknown,
                                                      :symptom_age_na,
                                                      :symptom_delayed_milestone,
                                                      :symptom_ppers,
                                                      :symptom_coordication,
                                                      :symptom_eye,
                                                      :symptom_behavioural,
                                                      :symptom_seizure,
                                                      :symptom_psychiatric,
                                                      :symptom_other,
                                                      :symptom_other_specify,
                                                      :hospital_clinician_name,
                                                      :hospital_phone,
                                                      :consent,
                                                      :consent_name,
                                                      :not_finish,
                                                      { specialists_attributes: [:_destroy,
                                                                                 :id,
                                                                                 :name,
                                                                                 :hospital_name,
                                                                                 { address_attributes: [
                                                                                     :_destroy,
                                                                                     :line_1,
                                                                                     :line_2,
                                                                                     :town,
                                                                                     :county,
                                                                                     :post_code,
                                                                                     :country
                                                                                 ] },
                                                                                 :number,
                                                                                 :email] })
  end
end
