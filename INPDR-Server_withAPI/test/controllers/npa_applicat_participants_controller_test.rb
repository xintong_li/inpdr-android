require 'test_helper'

class NpaApplicantParticipantsControllerTest < ActionController::TestCase
  setup do
    @npa_applicant_participant = npa_applicant_participants(:one)
  end

  test "should get index" do
    get :index
    assert_response :success
    assert_not_nil assigns(:npa_applicant_participants)
  end

  test "should get new" do
    get :new
    assert_response :success
  end

  test "should create npa_applicant_participant" do
    assert_difference('NpaApplicantParticipant.count') do
      post :create, npa_applicant_participant: {  }
    end

    assert_redirected_to npa_applicant_participant_path(assigns(:npa_applicant_participant))
  end

  test "should show npa_applicant_participant" do
    get :show, id: @npa_applicant_participant
    assert_response :success
  end

  test "should get edit" do
    get :edit, id: @npa_applicant_participant
    assert_response :success
  end

  test "should update npa_applicant_participant" do
    patch :update, id: @npa_applicant_participant, npa_applicant_participant: {  }
    assert_redirected_to npa_applicant_participant_path(assigns(:npa_applicant_participant))
  end

  test "should destroy npa_applicant_participant" do
    assert_difference('NpaApplicantParticipant.count', -1) do
      delete :destroy, id: @npa_applicant_participant
    end

    assert_redirected_to npa_applicant_participants_path
  end
end
