require 'test_helper'

class NpbFollowupsControllerTest < ActionController::TestCase
  setup do
    @npb_followup = npb_followups(:one)
  end

  test "should get index" do
    get :index
    assert_response :success
    assert_not_nil assigns(:npb_followups)
  end

  test "should get new" do
    get :new
    assert_response :success
  end

  test "should create npb_followup" do
    assert_difference('NpbFollowup.count') do
      post :create, npb_followup: {  }
    end

    assert_redirected_to npb_followup_path(assigns(:npb_followup))
  end

  test "should show npb_followup" do
    get :show, id: @npb_followup
    assert_response :success
  end

  test "should get edit" do
    get :edit, id: @npb_followup
    assert_response :success
  end

  test "should update npb_followup" do
    patch :update, id: @npb_followup, npb_followup: {  }
    assert_redirected_to npb_followup_path(assigns(:npb_followup))
  end

  test "should destroy npb_followup" do
    assert_difference('NpbFollowup.count', -1) do
      delete :destroy, id: @npb_followup
    end

    assert_redirected_to npb_followups_path
  end
end
