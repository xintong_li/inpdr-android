require 'test_helper'

class NpbParticipantsControllerTest < ActionController::TestCase
  setup do
    @npb_participant = npb_participants(:one)
  end

  test "should get index" do
    get :index
    assert_response :success
    assert_not_nil assigns(:npb_participants)
  end

  test "should get new" do
    get :new
    assert_response :success
  end

  test "should create npb_participant" do
    assert_difference('NpbParticipant.count') do
      post :create, npb_participant: {  }
    end

    assert_redirected_to npb_participant_path(assigns(:npb_participant))
  end

  test "should show npb_participant" do
    get :show, id: @npb_participant
    assert_response :success
  end

  test "should get edit" do
    get :edit, id: @npb_participant
    assert_response :success
  end

  test "should update npb_participant" do
    patch :update, id: @npb_participant, npb_participant: {  }
    assert_redirected_to npb_participant_path(assigns(:npb_participant))
  end

  test "should destroy npb_participant" do
    assert_difference('NpbParticipant.count', -1) do
      delete :destroy, id: @npb_participant
    end

    assert_redirected_to npb_participants_path
  end
end
