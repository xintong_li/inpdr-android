require 'test_helper'

class NpcFollowupsControllerTest < ActionController::TestCase
  setup do
    @npc_followup = npc_followups(:one)
  end

  test "should get index" do
    get :index
    assert_response :success
    assert_not_nil assigns(:npc_followups)
  end

  test "should get new" do
    get :new
    assert_response :success
  end

  test "should create npc_followup" do
    assert_difference('NpcFollowup.count') do
      post :create, npc_followup: {  }
    end

    assert_redirected_to npc_followup_path(assigns(:npc_followup))
  end

  test "should show npc_followup" do
    get :show, id: @npc_followup
    assert_response :success
  end

  test "should get edit" do
    get :edit, id: @npc_followup
    assert_response :success
  end

  test "should update npc_followup" do
    patch :update, id: @npc_followup, npc_followup: {  }
    assert_redirected_to npc_followup_path(assigns(:npc_followup))
  end

  test "should destroy npc_followup" do
    assert_difference('NpcFollowup.count', -1) do
      delete :destroy, id: @npc_followup
    end

    assert_redirected_to npc_followups_path
  end
end
