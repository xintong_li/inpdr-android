require 'test_helper'

class NpcApplicantParticipantsControllerTest < ActionController::TestCase
  setup do
    @npc_applicant_participant = npc_applicant_participants(:one)
  end

  test "should get index" do
    get :index
    assert_response :success
    assert_not_nil assigns(:npc_applicant_participants)
  end

  test "should get new" do
    get :new
    assert_response :success
  end

  test "should create npc_applicant_participant" do
    assert_difference('NpcApplicantParticipant.count') do
      post :create, npc_applicant_participant: {  }
    end

    assert_redirected_to npc_applicant_participant_path(assigns(:npc_applicant_participant))
  end

  test "should show npc_applicant_participant" do
    get :show, id: @npc_applicant_participant
    assert_response :success
  end

  test "should get edit" do
    get :edit, id: @npc_applicant_participant
    assert_response :success
  end

  test "should update npc_applicant_participant" do
    patch :update, id: @npc_applicant_participant, npc_applicant_participant: {  }
    assert_redirected_to npc_applicant_participant_path(assigns(:npc_applicant_participant))
  end

  test "should destroy npc_applicant_participant" do
    assert_difference('NpcApplicantParticipant.count', -1) do
      delete :destroy, id: @npc_applicant_participant
    end

    assert_redirected_to npc_applicant_participants_path
  end
end
