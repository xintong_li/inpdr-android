require 'test_helper'

class NpbApplicantParticipantsControllerTest < ActionController::TestCase
  setup do
    @npb_applicant_participant = npb_applicant_participants(:one)
  end

  test "should get index" do
    get :index
    assert_response :success
    assert_not_nil assigns(:npb_applicant_participants)
  end

  test "should get new" do
    get :new
    assert_response :success
  end

  test "should create npb_applicant_participant" do
    assert_difference('NpbApplicantParticipant.count') do
      post :create, npb_applicant_participant: {  }
    end

    assert_redirected_to npb_applicant_participant_path(assigns(:npb_applicant_participant))
  end

  test "should show npb_applicant_participant" do
    get :show, id: @npb_applicant_participant
    assert_response :success
  end

  test "should get edit" do
    get :edit, id: @npb_applicant_participant
    assert_response :success
  end

  test "should update npb_applicant_participant" do
    patch :update, id: @npb_applicant_participant, npb_applicant_participant: {  }
    assert_redirected_to npb_applicant_participant_path(assigns(:npb_applicant_participant))
  end

  test "should destroy npb_applicant_participant" do
    assert_difference('NpbApplicantParticipant.count', -1) do
      delete :destroy, id: @npb_applicant_participant
    end

    assert_redirected_to npb_applicant_participants_path
  end
end
