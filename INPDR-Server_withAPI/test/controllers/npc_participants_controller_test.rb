require 'test_helper'

class NpcParticipantsControllerTest < ActionController::TestCase
  setup do
    @npc_participant = npc_participants(:one)
  end

  test "should get index" do
    get :index
    assert_response :success
    assert_not_nil assigns(:npc_participants)
  end

  test "should get new" do
    get :new
    assert_response :success
  end

  test "should create npc_participant" do
    assert_difference('NpcParticipant.count') do
      post :create, npc_participant: {  }
    end

    assert_redirected_to npc_participant_path(assigns(:npc_participant))
  end

  test "should show npc_participant" do
    get :show, id: @npc_participant
    assert_response :success
  end

  test "should get edit" do
    get :edit, id: @npc_participant
    assert_response :success
  end

  test "should update npc_participant" do
    patch :update, id: @npc_participant, npc_participant: {  }
    assert_redirected_to npc_participant_path(assigns(:npc_participant))
  end

  test "should destroy npc_participant" do
    assert_difference('NpcParticipant.count', -1) do
      delete :destroy, id: @npc_participant
    end

    assert_redirected_to npc_participants_path
  end
end
