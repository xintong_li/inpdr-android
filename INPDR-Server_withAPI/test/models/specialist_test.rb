require 'test_helper'

class SpecialistTest < ActiveSupport::TestCase
  def setup
    @specialist = Specialist.new(name: 'Loren Bruns',
                           address: '45 Evansdale road',
                           email: 'hook_38@hotmail.com')
  end

  test "should be valid" do
    assert @specialist.valid?
  end


  test "Name should not be compulsory" do
    @specialist.name = ' '
    assert @specialist.valid?
  end

  test "Address should not be compulsory" do
    @specialist.address = ' '
    assert @specialist.valid?
  end

  test "Email should be in the right format" do
    @specialist.email = 'www.william.com'
    assert_not @specialist.valid?
  end

  test "Email should not be compulsory" do
    @specialist.email = ''
    assert @specialist.valid?
  end
end
