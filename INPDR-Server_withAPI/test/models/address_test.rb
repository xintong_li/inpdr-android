require 'test_helper'

class AddressTest < ActiveSupport::TestCase
  def setup
    @address = Address.new(line_1: 'Unit 9',
                           line_2: '45 Evansdale road',
                           town: 'Hawthorn',
                           county: 'Victoria',
                           post_code: '3122',
                           country: 'Australia',
                           country_specify: '')
  end

  test "should be valid" do
    assert @address.valid?
  end

  test "Line 1 should be compulsory" do
    @address.line_1 = ' '
    assert_not @address.valid?
  end

  test "County should be compulsory" do
    @address.county = ' '
    assert_not @address.valid?
  end

  test "Post code should be compulsory" do
    @address.post_code = ' '
    assert_not @address.valid?
  end

  test "Country should be compulsory" do
    @address.country = ' '
    assert_not @address.valid?
  end

  test "Line 2 should not be compulsory" do
    @address.line_2 = ' '
    assert @address.valid?
  end

  test "Town should not be compulsory" do
    @address.town = ' '
    assert @address.valid?
  end

  test "Country specify should not be compulsory" do
    @address.country_specify = ' '
    assert @address.valid?
  end
end
