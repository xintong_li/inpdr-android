require 'test_helper'

class NpcParticipantTest < ActiveSupport::TestCase

  def setup
    @participant = NpcParticipant.new(first_name: 'William',
                                      middle_name: '',
                                      surname: 'Hu',
                                      previous_surname: '',
                                      email: 'hook_38@hotmail.com',
                                      date_of_birth: '12-08-1981',
                                      gender: 0,
                                      age_diagnosed_known: true,
                                      age_diagnosed_year: 2,
                                      age_diagnosed_month: 3,
                                      family_diagnosed: 0,
                                      family_diagnosed_relation: 'poo',
                                      hospital_name: 'WIHI',
                                      hospital_address: 'Hospitl address',
                                      hospital_email: 'something@gmail.com',
                                      gene_test_performed: true)

    @address = Address.new(line_1: 'Unit 9',
                           line_2: '45 Evansdale road',
                           town: 'Hawthorn',
                           county: 'Victoria',
                           post_code: '3122',
                           country: 'Australia',
                           country_specify: '')

    @specialist = Specialist.new(name: 'Guido',
                                 address: 'Something street',
                                 email: 'Guido@gmail.com')

    @specialist2 = Specialist.new(name: 'Glenn',
                                 address: 'Something else street',
                                 email: 'Glenn@gmail.com')

    @participant.address = @address
    @participant.specialists.push(@specialist)
    @participant.specialists.push(@specialist2)

  end

  test "should be valid" do
    assert @participant.valid?
  end

  test "Participant can add specialist" do
    assert_equal @specialist, @participant.specialists.first
  end

  test "Participant can add more then one specialist" do
    assert_equal @specialist2, @participant.specialists[1]
  end

  test "Participant can call specialist name" do
    assert_equal 'Guido', @participant.specialists.first.name
  end

  test "Participant can call specialist name from more then one specialist" do
    assert_equal 'Something else street', @participant.specialists[1].address
  end

  test "Test address set for participant" do
    assert_equal @participant.address, @address
  end

  test "Test validation on address from participant" do
    @participant.address.line_1 = ' '
    assert_not @participant.valid?
  end

  test "First name should be compulsory" do
    @participant.first_name = ' '
    assert_not @participant.valid?
  end

  test "Surname name should be compulsory" do
    @participant.surname = ' '
    assert_not @participant.valid?
  end

  test "Mid name should not be compulsory" do
    @participant.middle_name = nil
    assert @participant.valid?
  end

  test "Previous surname should not be compulsory" do
    @participant.previous_surname = nil
    assert @participant.valid?
  end

  test "Email should be compulsory" do
    @participant.email = ' '
    assert_not @participant.valid?
  end

  test "Email should be in the right format" do
    @participant.email = 'www.william.com'
    assert_not @participant.valid?
  end

  test "Address should be compulsory" do
    @participant.address = nil
    assert_not @participant.valid?
  end

  test "Date of birth should not be compulsory" do
    @participant.date_of_birth = nil
    assert @participant.valid?
  end

  test "Gender should not be compulsory" do
    @participant.gender = nil
    assert @participant.valid?
  end


  test "Age diagnosed known should not be compulsory" do
    @participant.age_diagnosed_known = nil
    assert @participant.valid?
  end

  test "Age diagnosed year should not be compulsory" do
    @participant.age_diagnosed_year = nil
    assert @participant.valid?
  end

  test "Age diagnosed month should not be compulsory" do
    @participant.age_diagnosed_month = nil
    assert @participant.valid?
  end

  test "Family diagnosed with disease should not be compulsory" do
    @participant.family_diagnosed = nil
    assert @participant.valid?
  end

  test "Relationship of family diagnosed with disease should not be compulsory" do
    @participant.family_diagnosed_relation = nil
    assert @participant.valid?
  end

  test "Hospital name should not be compulsory" do
    @participant.hospital_name = nil
    assert @participant.valid?
  end

  test "Hospital address should not be compulsory" do
    @participant.hospital_address = nil
    assert @participant.valid?
  end

  test "Hospital email should not be compulsory" do
    @participant.hospital_email = nil
    assert @participant.valid?
  end

  test "Hospital email should be in the right format" do
    @participant.hospital_email = 'www.hotmail.com'
    assert_not @participant.valid?
  end

  test "Gene test performed should not be compulsory" do
    @participant.gene_test_performed = nil
    assert @participant.valid?
  end
end


