require 'test_helper'

class NpbParticipantTest < ActiveSupport::TestCase
  def setup
    @participant = NpbParticipant.new(first_name: 'William',
                                      middle_name: '',
                                      surname: 'Hu',
                                      previous_surname: '',
                                      email: 'hook_38@hotmail.com',
                                      date_of_birth: '12-08-1981',
                                      gender: 0)

    @address = Address.new(line_1: 'Unit 9',
                           line_2: '45 Evansdale road',
                           town: 'Hawthorn',
                           county: 'Victoria',
                           post_code: '3122',
                           country: 'Australia',
                           country_specify: '')

    @participant.address = @address

  end

  test "should be valid" do
    assert @participant.valid?
  end

  test "Test address set for participant" do
    assert_equal @participant.address, @address
  end

  test "Test validation on address from participant" do
    @participant.address.line_1 = ' '
    assert_not @participant.valid?
  end

  test "First name should be compulsory" do
    @participant.first_name = ' '
    assert_not @participant.valid?
  end

  test "Surname name should be compulsory" do
    @participant.surname = ' '
    assert_not @participant.valid?
  end

  test "Mid name should not be compulsory" do
    @participant.middle_name = nil
    assert @participant.valid?
  end

  test "Previous surname should not be compulsory" do
    @participant.previous_surname = nil
    assert @participant.valid?
  end

  test "Email should be compulsory" do
    @participant.email = ' '
    assert_not @participant.valid?
  end

  test "Email should be in the right format" do
    @participant.email = 'www.william.com'
    assert_not @participant.valid?
  end

  test "Address should be compulsory" do
    @participant.address = nil
    assert_not @participant.valid?
  end

  test "Date of birth should not be compulsory" do
    @participant.date_of_birth = nil
    assert @participant.valid?
  end

  test "Gender should not be compulsory" do
    @participant.gender = nil
    assert @participant.valid?
  end
end
