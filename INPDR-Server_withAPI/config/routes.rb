Rails.application.routes.draw do

  mount API::Base => "/"

  match '/contacts', :to => 'contacts#new', :via => 'get'
  resources :contacts, :only => [:new, :create]

  devise_for :users, :controllers => {:registrations => :registrations}


  root :to => 'pages#index'


  #get 'SignIn.html' => 'pages#sign_in'
  #get 'ForgotPassword.html' => 'pages#forgot_password'


  get 'pages/index'
  #get 'pages/home'
  get 'pages/registration'
  get 'pages/terms_and_conditions'
  get 'pages/privacy_statement'
  get 'pages/followups'
  get 'pages/verifications'
  get 'pages/thank_enrol'


  post 'patients/consent_action', to: 'patients#consent_action'
  resources :patients do
    get 'enrol'
    get 'verify', :on => :member
    resources :npa_applicant_participants do
      #resources :npb_followups, :defaults => { :followable => 'npa_applicant_participants'}
    end

    resources :npb_participants do
      resources :npb_followups, :defaults => {:followable => 'npb_participants'}
    end

    resources :npb_applicant_participants do
      resources :npb_followups, :defaults => {:followable => 'npb_applicant_participants'}
    end

    resources :npc_applicant_participants do
      resources :npc_followups, :defaults => {:followable => 'npc_applicant_participants'}
    end

    resources :npc_participants do
      resources :npc_followups, :defaults => {:followable => 'npc_participants'}
    end
  end


  # The priority is based upon order of creation: first created -> highest priority.
  # See how all your routes lay out with "rake routes".

  # You can have the root of your site routed with "root"
  # root 'welcome#index'

  # Example of regular route:
  #   get 'products/:id' => 'catalog#view'

  # Example of named route that can be invoked with purchase_url(id: product.id)
  #   get 'products/:id/purchase' => 'catalog#purchase', as: :purchase

  # Example resource route (maps HTTP verbs to controller actions automatically):
  #   resources :products

  # Example resource route with options:
  #   resources :products do
  #     member do
  #       get 'short'
  #       post 'toggle'
  #     end
  #
  #     collection do
  #       get 'sold'
  #     end
  #   end

  # Example resource route with sub-resources:
  #   resources :products do
  #     resources :comments, :sales
  #     resource :seller
  #   end

  # Example resource route with more complex sub-resources:
  #   resources :products do
  #     resources :comments
  #     resources :sales do
  #       get 'recent', on: :collection
  #     end
  #   end

  # Example resource route with concerns:
  #   concern :toggleable do
  #     post 'toggle'
  #   end
  #   resources :posts, concerns: :toggleable
  #   resources :photos, concerns: :toggleable

  # Example resource route within a namespace:
  #   namespace :admin do
  #     # Directs /admin/products/* to Admin::ProductsController
  #     # (app/controllers/admin/products_controller.rb)
  #     resources :products
  #   end

end
