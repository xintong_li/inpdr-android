# encoding: UTF-8
# This file is auto-generated from the current state of the database. Instead
# of editing this file, please use the migrations feature of Active Record to
# incrementally modify your database, and then regenerate this schema definition.
#
# Note that this schema.rb definition is the authoritative source for your
# database schema. If you need to create the application database on another
# system, you should be using db:schema:load, not running all the migrations
# from scratch. The latter is a flawed and unsustainable approach (the more migrations
# you'll amass, the slower it'll run and the greater likelihood for issues).
#
# It's strongly recommended that you check this file into your version control system.

ActiveRecord::Schema.define(version: 20160519035830) do

  create_table "addresses", force: :cascade do |t|
    t.string   "line_1"
    t.string   "line_2"
    t.string   "town"
    t.string   "county"
    t.string   "post_code"
    t.string   "country"
    t.datetime "created_at",  null: false
    t.datetime "updated_at",  null: false
    t.integer  "person_id"
    t.string   "person_type"
  end

  add_index "addresses", ["person_type", "person_id"], name: "index_addresses_on_person_type_and_person_id"

  create_table "npa_applicant_participants", force: :cascade do |t|
    t.datetime "created_at",                                   null: false
    t.datetime "updated_at",                                   null: false
    t.boolean  "age_diagnosed_known"
    t.integer  "age_diagnosed_year"
    t.integer  "age_diagnosed_month"
    t.integer  "family_diagnosed"
    t.string   "family_diagnosed_relation"
    t.integer  "applicant_relation"
    t.string   "applicant_relation_specify"
    t.boolean  "gene_test_performed"
    t.integer  "symptom_age_year"
    t.integer  "symptom_age_month"
    t.boolean  "symptom_age_unknown"
    t.boolean  "symptom_age_na"
    t.string   "hospital_clinician_name"
    t.string   "gene_diagnosis"
    t.boolean  "unable_to_contact_specialist", default: false
    t.boolean  "not_finish",                   default: true
  end

  create_table "npb_applicant_participants", force: :cascade do |t|
    t.datetime "created_at",                                   null: false
    t.datetime "updated_at",                                   null: false
    t.boolean  "age_diagnosed_known"
    t.integer  "age_diagnosed_year"
    t.integer  "age_diagnosed_month"
    t.integer  "family_diagnosed"
    t.string   "family_diagnosed_relation"
    t.integer  "applicant_relation"
    t.string   "applicant_relation_specify"
    t.boolean  "gene_test_performed"
    t.integer  "symptom_age_year"
    t.integer  "symptom_age_month"
    t.boolean  "symptom_age_unknown"
    t.boolean  "symptom_age_na"
    t.boolean  "symptom_enlarged_liver"
    t.boolean  "symptom_breathing"
    t.boolean  "symptom_anaemia"
    t.boolean  "symptom_bleeding"
    t.boolean  "symptom_growth_delay"
    t.boolean  "symptom_developmental_delay"
    t.boolean  "symptom_bone_pain"
    t.boolean  "symptom_abdominal_pain"
    t.boolean  "symptom_other"
    t.text     "symptom_other_specify"
    t.string   "hospital_clinician_name"
    t.string   "gene_diagnosis"
    t.boolean  "unable_to_contact_specialist", default: false
    t.boolean  "not_finish",                   default: true
  end

  create_table "npb_followups", force: :cascade do |t|
    t.integer  "symptom_bone",                   null: false
    t.integer  "symptom_abdominal",              null: false
    t.integer  "symptom_pain",                   null: false
    t.integer  "symptom_breathlessness_rate",    null: false
    t.integer  "symptom_breathlessness_impact",  null: false
    t.integer  "symptom_bleeding_rate",          null: false
    t.integer  "symptom_bleeding_impact",        null: false
    t.integer  "symptom_fatigue_rate",           null: false
    t.integer  "symptom_fatigue_impact",         null: false
    t.integer  "symptom_enlarge_organ",          null: false
    t.integer  "symptom_enlarge_organ_impact",   null: false
    t.integer  "symptom_slow_growth",            null: false
    t.integer  "symptom_slow_growth_impact",     null: false
    t.integer  "symptom_infection",              null: false
    t.integer  "symptom_fracture",               null: false
    t.integer  "symptom_cognition",              null: false
    t.integer  "impact_family_disappointed",     null: false
    t.integer  "impact_family_give_up",          null: false
    t.integer  "impact_family_worry_future",     null: false
    t.integer  "impact_family_closer",           null: false
    t.integer  "impact_wider_self_miss_career",  null: false
    t.integer  "impact_wider_carer_miss_career", null: false
    t.integer  "impact_wider_emergency",         null: false
    t.integer  "impact_wider_appointment",       null: false
    t.datetime "created_at",                     null: false
    t.datetime "updated_at",                     null: false
    t.integer  "followable_id"
    t.string   "followable_type"
  end

  add_index "npb_followups", ["followable_type", "followable_id"], name: "index_npb_followups_on_followable_type_and_followable_id"

  create_table "npb_participants", force: :cascade do |t|
    t.datetime "created_at",                                   null: false
    t.datetime "updated_at",                                   null: false
    t.boolean  "age_diagnosed_known"
    t.integer  "age_diagnosed_year"
    t.integer  "age_diagnosed_month"
    t.integer  "family_diagnosed"
    t.string   "family_diagnosed_relation"
    t.boolean  "gene_test_performed"
    t.integer  "symptom_age_year"
    t.integer  "symptom_age_month"
    t.boolean  "symptom_age_unknown"
    t.boolean  "symptom_age_na"
    t.boolean  "symptom_enlarged_liver"
    t.boolean  "symptom_breathing"
    t.boolean  "symptom_anaemia"
    t.boolean  "symptom_bleeding"
    t.boolean  "symptom_growth_delay"
    t.boolean  "symptom_developmental_delay"
    t.boolean  "symptom_bone_pain"
    t.boolean  "symptom_abdominal_pain"
    t.boolean  "symptom_other"
    t.text     "symptom_other_specify"
    t.string   "hospital_clinician_name"
    t.string   "gene_diagnosis"
    t.boolean  "unable_to_contact_specialist", default: false
    t.boolean  "not_finish",                   default: true
  end

  create_table "npc_applicant_participants", force: :cascade do |t|
    t.datetime "created_at",                                   null: false
    t.datetime "updated_at",                                   null: false
    t.boolean  "age_diagnosed_known"
    t.integer  "age_diagnosed_year"
    t.integer  "age_diagnosed_month"
    t.integer  "family_diagnosed"
    t.string   "family_diagnosed_relation"
    t.integer  "applicant_relation"
    t.string   "applicant_relation_specify"
    t.boolean  "gene_test_performed"
    t.integer  "baby_symptoms"
    t.integer  "symptom_age_year"
    t.integer  "symptom_age_month"
    t.boolean  "symptom_age_unknown"
    t.boolean  "symptom_age_na"
    t.boolean  "symptom_delayed_milestone"
    t.boolean  "symptom_ppers"
    t.boolean  "symptom_coordication"
    t.boolean  "symptom_eye"
    t.boolean  "symptom_behavioural"
    t.boolean  "symptom_seizure"
    t.boolean  "symptom_psychiatric"
    t.boolean  "symptom_other"
    t.text     "symptom_other_specify"
    t.string   "hospital_clinician_name"
    t.string   "gene_diagnosis"
    t.boolean  "unable_to_contact_specialist", default: false
    t.boolean  "not_finish",                   default: true
  end

  create_table "npc_followups", force: :cascade do |t|
    t.integer  "impact_ambulation",               null: false
    t.integer  "impact_manipulation",             null: false
    t.integer  "impact_speech",                   null: false
    t.integer  "impact_swallowing",               null: false
    t.integer  "impact_eye_movement",             null: false
    t.integer  "impact_seizure",                  null: false
    t.integer  "impact_cognitive_impaired",       null: false
    t.integer  "impact_family_disappointed",      null: false
    t.integer  "impact_family_give_up",           null: false
    t.integer  "impact_family_worry_future",      null: false
    t.integer  "impact_family_closer_family",     null: false
    t.integer  "impact_wider_self_miss_career",   null: false
    t.integer  "impact_wider_family_miss_career", null: false
    t.integer  "impact_wider_emergency",          null: false
    t.integer  "impact_wider_appointment",        null: false
    t.datetime "created_at",                      null: false
    t.datetime "updated_at",                      null: false
    t.integer  "followable_id"
    t.string   "followable_type"
  end

  add_index "npc_followups", ["followable_type", "followable_id"], name: "index_npc_followups_on_followable_type_and_followable_id"

  create_table "npc_participants", force: :cascade do |t|
    t.datetime "created_at",                                   null: false
    t.datetime "updated_at",                                   null: false
    t.boolean  "age_diagnosed_known"
    t.integer  "age_diagnosed_year"
    t.integer  "age_diagnosed_month"
    t.integer  "family_diagnosed"
    t.string   "family_diagnosed_relation"
    t.boolean  "gene_test_performed"
    t.integer  "baby_symptoms"
    t.integer  "symptom_age_year"
    t.integer  "symptom_age_month"
    t.boolean  "symptom_age_unknown"
    t.boolean  "symptom_age_na"
    t.boolean  "symptom_delayed_milestone"
    t.boolean  "symptom_ppers"
    t.boolean  "symptom_coordication"
    t.boolean  "symptom_eye"
    t.boolean  "symptom_behavioural"
    t.boolean  "symptom_seizure"
    t.boolean  "symptom_psychiatric"
    t.boolean  "symptom_other"
    t.text     "symptom_other_specify"
    t.string   "hospital_clinician_name"
    t.string   "gene_diagnosis"
    t.boolean  "unable_to_contact_specialist", default: false
    t.boolean  "not_finish",                   default: true
  end

  create_table "patients", force: :cascade do |t|
    t.string   "registrant_first_name"
    t.string   "registrant_last_name"
    t.integer  "relation_to_patient"
    t.integer  "addresses_id"
    t.string   "patient_first_name"
    t.string   "patient_last_name"
    t.integer  "gender"
    t.date     "date_of_birth"
    t.datetime "created_at",                           null: false
    t.datetime "updated_at",                           null: false
    t.integer  "user_id"
    t.boolean  "consent"
    t.string   "consent_name"
    t.boolean  "verified"
    t.string   "unique_id"
    t.integer  "enrollable_id"
    t.string   "enrollable_type"
    t.boolean  "not_finish",            default: true
  end

  add_index "patients", ["addresses_id"], name: "index_patients_on_addresses_id"
  add_index "patients", ["enrollable_type", "enrollable_id"], name: "index_patients_on_enrollable_type_and_enrollable_id"
  add_index "patients", ["user_id"], name: "index_patients_on_user_id"

  create_table "specialists", force: :cascade do |t|
    t.string   "name"
    t.string   "email"
    t.integer  "patient_id"
    t.string   "patient_type"
    t.datetime "created_at",    null: false
    t.datetime "updated_at",    null: false
    t.string   "hospital_name"
    t.string   "number"
  end

  add_index "specialists", ["patient_id"], name: "index_specialists_on_patient_id"

  create_table "users", force: :cascade do |t|
    t.string   "email",                  default: "",    null: false
    t.string   "encrypted_password",     default: "",    null: false
    t.string   "reset_password_token"
    t.datetime "reset_password_sent_at"
    t.datetime "remember_created_at"
    t.integer  "sign_in_count",          default: 0,     null: false
    t.datetime "current_sign_in_at"
    t.datetime "last_sign_in_at"
    t.string   "current_sign_in_ip"
    t.string   "last_sign_in_ip"
    t.string   "confirmation_token"
    t.datetime "confirmed_at"
    t.datetime "confirmation_sent_at"
    t.string   "unconfirmed_email"
    t.integer  "failed_attempts",        default: 0,     null: false
    t.string   "unlock_token"
    t.datetime "locked_at"
    t.datetime "created_at",                             null: false
    t.datetime "updated_at",                             null: false
    t.boolean  "admin",                  default: false
    t.boolean  "study_team",             default: false
    t.string   "authentication_token"
  end

  add_index "users", ["authentication_token"], name: "index_users_on_authentication_token", unique: true
  add_index "users", ["confirmation_token"], name: "index_users_on_confirmation_token", unique: true
  add_index "users", ["email"], name: "index_users_on_email", unique: true
  add_index "users", ["reset_password_token"], name: "index_users_on_reset_password_token", unique: true
  add_index "users", ["unlock_token"], name: "index_users_on_unlock_token", unique: true

end
