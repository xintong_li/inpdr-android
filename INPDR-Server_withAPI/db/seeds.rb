# This file should contain all the record creation needed to seed the database with its default values.
# The data can then be loaded with the rake db:seed (or created alongside the db with db:setup).
#
# Examples:
#
#   cities = City.create([{ name: 'Chicago' }, { name: 'Copenhagen' }])
#   Mayor.create(name: 'Emanuel', city: cities.first)

User.create([
                {id: 1,
                 email: 'william.hu@unimelb.edu.au',
                 password: 'password1',
                 password_confirmation: 'password1',
                 study_team: '1',
                 confirmed_at: '2015-07-09 06:49:56.004702',
                 confirmation_token: '57ad4034aff3df5a8e09d872b4c05c97943dda839b2efd6e41fc29d5988285ab',
                 confirmation_sent_at: '2015-07-09 06:49:18.277034',
                 created_at: '2015-07-09 06:49:18.275584',
                 updated_at: '2015-07-09 06:49:56.005467'
                },
                {id: 2,
                 email: 'rsinnott@unimelb.edu.au',
                 password: 'password1',
                 password_confirmation: 'password1',
                 study_team: '1',
                 confirmed_at: '2015-07-09 06:58:32.091334',
                 confirmation_token: '742881c1f1b6e9359db32635772a24ab477e577aec917a11ae2a00aa62146d79',
                 confirmation_sent_at: '2015-07-09 06:58:22.989680',
                 created_at: '2015-07-09 06:49:18.275584',
                 updated_at: '2015-07-09 06:49:56.005467'
                }
            ]
)