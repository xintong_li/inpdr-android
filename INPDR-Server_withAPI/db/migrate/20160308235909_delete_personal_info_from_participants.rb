class DeletePersonalInfoFromParticipants < ActiveRecord::Migration
  def change
    remove_column :npc_participants, :date_of_birth, :date
    remove_column :npc_participants, :gender, :integer

    remove_column :npc_applicant_participants, :date_of_birth, :date
    remove_column :npc_applicant_participants, :gender, :integer
    remove_column :npc_applicant_participants, :first_name, :string
    remove_column :npc_applicant_participants, :middle_name, :string
    remove_column :npc_applicant_participants, :surname, :string

    remove_column :npb_participants, :date_of_birth, :date
    remove_column :npb_participants, :gender, :integer

    remove_column :npb_applicant_participants, :date_of_birth, :date
    remove_column :npb_applicant_participants, :gender, :integer
    remove_column :npb_applicant_participants, :first_name, :string
    remove_column :npb_applicant_participants, :middle_name, :string
    remove_column :npb_applicant_participants, :surname, :string

    remove_column :npa_applicant_participants, :date_of_birth, :date
    remove_column :npb_applicant_participants, :gender, :integer
    remove_column :npb_applicant_participants, :first_name, :string
    remove_column :npb_applicant_participants, :middle_name, :string
    remove_column :npb_applicant_participants, :surname, :string
  end
end
