class CreateParticipantsUsers < ActiveRecord::Migration
  def change
    create_table :parti_users do |t|
      t.belongs_to :user, index: true
      t.references :participant, polymorphic: true, index: true
      t.timestamps null: false
    end
  end
end
