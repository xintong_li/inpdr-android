class AddRoleToUsers < ActiveRecord::Migration
  def change
    add_column :users, :admin, :boolean, :default => false
    add_column :users, :study_team, :boolean, :default => false
    add_column :users, :unique_id, :string, unique: true

    add_index :users, :unique_id, unique: true
  end
end
