class RemoveParticipantsFromUser < ActiveRecord::Migration
  def change
    remove_reference :users, :participant, polymorphic: true, index: true
  end
end
