class AddDetailsToSpecialists < ActiveRecord::Migration
  def change
    add_column :specialists, :hospital_name, :string
    add_column :specialists, :number, :string
  end
end
