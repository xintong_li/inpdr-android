class AddGeneReportToNpbParticipant < ActiveRecord::Migration
  def change
    add_column :npb_participants, :hospital_name, :string
    add_column :npb_participants, :hospital_address, :string
    add_column :npb_participants, :hospital_email, :string
    add_column :npb_participants, :gene_test_performed, :boolean
  end
end
