class UpdateAddressCountryToString < ActiveRecord::Migration
  def change
    change_column :addresses, :country, :string
  end
end
