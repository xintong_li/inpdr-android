class AddCarrierwaveGeneDiagnosisToParticipants < ActiveRecord::Migration
  def change
    add_column :npb_applicant_participants, :gene_diagnosis, :string


    add_column :npc_applicant_participants, :gene_diagnosis, :string


    add_column :npb_participants, :gene_diagnosis, :string


    add_column :npc_participants, :gene_diagnosis, :string


  end
end
