class AddGeneDiagnosisToNpaApplicantParticipants < ActiveRecord::Migration
  def change
    add_column :npa_applicant_participants, :gene_diagnosis, :string

  end
end
