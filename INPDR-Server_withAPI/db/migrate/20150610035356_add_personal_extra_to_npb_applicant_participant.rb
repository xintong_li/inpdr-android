class AddPersonalExtraToNpbApplicantParticipant < ActiveRecord::Migration
  def change
    add_column :npb_applicant_participants, :age_diagnosed_known, :boolean
    add_column :npb_applicant_participants, :age_diagnosed_year, :integer
    add_column :npb_applicant_participants, :age_diagnosed_month, :integer
    add_column :npb_applicant_participants, :family_diagnosed, :integer
    add_column :npb_applicant_participants, :family_diagnosed_relation, :string
    add_column :npb_applicant_participants, :applicant_relation, :integer
    add_column :npb_applicant_participants, :applicant_relation_specify, :string
  end
end
