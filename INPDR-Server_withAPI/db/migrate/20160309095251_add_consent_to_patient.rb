class AddConsentToPatient < ActiveRecord::Migration
  def change
    add_column :patients, :consent, :boolean
    add_column :patients, :consent_name, :string

    remove_column :npa_applicant_participants, :consent, :boolean
    remove_column :npa_applicant_participants, :consent_name, :string

    remove_column :npb_applicant_participants, :consent, :boolean
    remove_column :npb_applicant_participants, :consent_name, :string

    remove_column :npc_applicant_participants, :consent, :boolean
    remove_column :npc_applicant_participants, :consent_name, :string

    remove_column :npb_participants, :consent, :boolean
    remove_column :npb_participants, :consent_name, :string

    remove_column :npc_participants, :consent, :boolean
    remove_column :npc_participants, :consent_name, :string
  end
end
