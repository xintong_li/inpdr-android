class AddExtraInfoToPatient < ActiveRecord::Migration
  def change
    add_column :patients, :verified, :boolean
    add_column :patients, :unique_id, :string

    remove_column :npa_applicant_participants, :verified, :boolean
    remove_column :npb_participants, :verified, :boolean
    remove_column :npb_applicant_participants, :verified, :boolean
    remove_column :npc_participants, :verified, :boolean
    remove_column :npc_applicant_participants, :verified, :boolean

    remove_column :npa_applicant_participants, :unique_id, :string
    remove_column :npb_participants, :unique_id, :string
    remove_column :npb_applicant_participants, :unique_id, :string
    remove_column :npc_participants, :unique_id, :string
    remove_column :npc_applicant_participants, :unique_id, :string
  end
end
