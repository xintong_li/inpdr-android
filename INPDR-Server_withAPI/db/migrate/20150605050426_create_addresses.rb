class CreateAddresses < ActiveRecord::Migration
  def change
    create_table :addresses do |t|
      t.string :line_1
      t.string :line_2
      t.string :town
      t.string :county
      t.string :post_code
      t.integer :country
      t.string :country_specify
      t.integer :participant_id

      t.timestamps null: false
    end
  end
end
