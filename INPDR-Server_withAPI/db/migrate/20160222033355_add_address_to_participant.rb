class AddAddressToParticipant < ActiveRecord::Migration
  def change
    add_reference :npc_participants, :hospital_address, references: :addresses, index: true, foreign_key: true
    add_reference :npc_applicant_participants, :hospital_address, references: :addresses, index: true, foreign_key: true
    add_reference :npb_participants, :hospital_address, references: :addresses, index: true, foreign_key: true
    add_reference :npb_applicant_participants, :hospital_address, references: :addresses, index: true, foreign_key: true
    add_reference :npa_applicant_participants, :hospital_address, references: :addresses, index: true, foreign_key: true

    remove_column :npc_participants, :hospital_address
    remove_column :npc_applicant_participants, :hospital_address
    remove_column :npb_participants, :hospital_address
    remove_column :npb_applicant_participants, :hospital_address
    remove_column :npa_applicant_participants, :hospital_address
  end
end
