class AddUniqueIdToParticipants < ActiveRecord::Migration
  def change
    add_column :npc_participants, :unique_id, :string
    add_column :npc_applicant_participants, :unique_id, :string
    add_column :npb_participants, :unique_id, :string
    add_column :npb_applicant_participants, :unique_id, :string
    add_column :npa_applicant_participants, :unique_id, :string

    remove_column :users, :unique_id
  end
end
