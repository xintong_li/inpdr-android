class CreateNpcFollowups < ActiveRecord::Migration
  def change
    create_table :npc_followups do |t|
      t.integer :impact_ambulation, null: false
      t.integer :impact_manipulation, null: false
      t.integer :impact_speech, null: false
      t.integer :impact_swallowing, null: false
      t.integer :impact_eye_movement, null: false
      t.integer :impact_seizure, null: false
      t.integer :impact_cognitive_impaired, null: false

      t.integer :impact_family_disappointed, null: false
      t.integer :impact_family_give_up, null: false
      t.integer :impact_family_worry_future, null: false
      t.integer :impact_family_closer_family, null: false

      t.integer :impact_wider_self_miss_career, null: false
      t.integer :impact_wider_family_miss_career, null: false
      t.integer :impact_wider_emergency, null: false
      t.integer :impact_wider_appointment, null: false


      t.timestamps null: false
    end
  end
end
