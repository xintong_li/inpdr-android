class AddDetailsToParticipants < ActiveRecord::Migration
  def change
    add_column :npc_participants, :baby_symptoms, :integer
    add_column :npc_participants, :symptom_age_year, :integer
    add_column :npc_participants, :symptom_age_month, :integer
    add_column :npc_participants, :symptom_age_unknown, :boolean
    add_column :npc_participants, :symptom_age_na, :boolean
    add_column :npc_participants, :symptom_delayed_milestone, :boolean
    add_column :npc_participants, :symptom_ppers, :boolean
    add_column :npc_participants, :symptom_coordication, :boolean
    add_column :npc_participants, :symptom_eye, :boolean
    add_column :npc_participants, :symptom_behavioural, :boolean
    add_column :npc_participants, :symptom_seizure, :boolean
    add_column :npc_participants, :symptom_psychiatric, :boolean
    add_column :npc_participants, :symptom_other, :boolean
    add_column :npc_participants, :symptom_other_specify, :text
    add_column :npc_participants, :hospital_clinician_name, :string
    add_column :npc_participants, :hospital_phone, :string

    add_column :npc_applicant_participants, :baby_symptoms, :integer
    add_column :npc_applicant_participants, :symptom_age_year, :integer
    add_column :npc_applicant_participants, :symptom_age_month, :integer
    add_column :npc_applicant_participants, :symptom_age_unknown, :boolean
    add_column :npc_applicant_participants, :symptom_age_na, :boolean
    add_column :npc_applicant_participants, :symptom_delayed_milestone, :boolean
    add_column :npc_applicant_participants, :symptom_ppers, :boolean
    add_column :npc_applicant_participants, :symptom_coordication, :boolean
    add_column :npc_applicant_participants, :symptom_eye, :boolean
    add_column :npc_applicant_participants, :symptom_behavioural, :boolean
    add_column :npc_applicant_participants, :symptom_seizure, :boolean
    add_column :npc_applicant_participants, :symptom_psychiatric, :boolean
    add_column :npc_applicant_participants, :symptom_other, :boolean
    add_column :npc_applicant_participants, :symptom_other_specify, :text
    add_column :npc_applicant_participants, :hospital_clinician_name, :string
    add_column :npc_applicant_participants, :hospital_phone, :string

    add_column :npb_applicant_participants, :symptom_age_year, :integer
    add_column :npb_applicant_participants, :symptom_age_month, :integer
    add_column :npb_applicant_participants, :symptom_age_unknown, :boolean
    add_column :npb_applicant_participants, :symptom_age_na, :boolean    
    add_column :npb_applicant_participants, :symptom_enlarged_liver, :boolean
    add_column :npb_applicant_participants, :symptom_breathing, :boolean
    add_column :npb_applicant_participants, :symptom_anaemia, :boolean
    add_column :npb_applicant_participants, :symptom_bleeding, :boolean
    add_column :npb_applicant_participants, :symptom_growth_delay, :boolean
    add_column :npb_applicant_participants, :symptom_developmental_delay, :boolean
    add_column :npb_applicant_participants, :symptom_bone_pain, :boolean
    add_column :npb_applicant_participants, :symptom_abdominal_pain, :boolean
    add_column :npb_applicant_participants, :symptom_other, :boolean
    add_column :npb_applicant_participants, :symptom_other_specify, :text
    add_column :npb_applicant_participants, :hospital_clinician_name, :string
    add_column :npb_applicant_participants, :hospital_phone, :string

    add_column :npb_participants, :symptom_age_year, :integer
    add_column :npb_participants, :symptom_age_month, :integer
    add_column :npb_participants, :symptom_age_unknown, :boolean
    add_column :npb_participants, :symptom_age_na, :boolean
    add_column :npb_participants, :symptom_enlarged_liver, :boolean
    add_column :npb_participants, :symptom_breathing, :boolean
    add_column :npb_participants, :symptom_anaemia, :boolean
    add_column :npb_participants, :symptom_bleeding, :boolean
    add_column :npb_participants, :symptom_growth_delay, :boolean
    add_column :npb_participants, :symptom_developmental_delay, :boolean
    add_column :npb_participants, :symptom_bone_pain, :boolean
    add_column :npb_participants, :symptom_abdominal_pain, :boolean
    add_column :npb_participants, :symptom_other, :boolean
    add_column :npb_participants, :symptom_other_specify, :text
    add_column :npb_participants, :hospital_clinician_name, :string
    add_column :npb_participants, :hospital_phone, :string

    add_column :npa_applicant_participants, :symptom_age_year, :integer
    add_column :npa_applicant_participants, :symptom_age_month, :integer
    add_column :npa_applicant_participants, :symptom_age_unknown, :boolean
    add_column :npa_applicant_participants, :symptom_age_na, :boolean
    add_column :npa_applicant_participants, :hospital_clinician_name, :string
    add_column :npa_applicant_participants, :hospital_phone, :string
  end
end
