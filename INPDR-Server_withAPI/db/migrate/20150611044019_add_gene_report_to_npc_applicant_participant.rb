class AddGeneReportToNpcApplicantParticipant < ActiveRecord::Migration
  def change
    add_column :npc_applicant_participants, :hospital_name, :string
    add_column :npc_applicant_participants, :hospital_address, :string
    add_column :npc_applicant_participants, :hospital_email, :string
    add_column :npc_applicant_participants, :gene_test_performed, :boolean
  end
end
