class AddUnableContactSpecialistToParticipants < ActiveRecord::Migration
  def change
    add_column :npc_participants, :unable_to_contact_specialist, :boolean, :default => false
    add_column :npc_applicant_participants, :unable_to_contact_specialist, :boolean, :default => false
    add_column :npb_participants, :unable_to_contact_specialist, :boolean, :default => false
    add_column :npb_applicant_participants, :unable_to_contact_specialist, :boolean, :default => false
    add_column :npa_applicant_participants, :unable_to_contact_specialist, :boolean, :default => false
  end
end
