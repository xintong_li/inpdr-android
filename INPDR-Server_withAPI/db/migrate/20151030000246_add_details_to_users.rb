class AddDetailsToUsers < ActiveRecord::Migration
  def change
    remove_column :users, :name

    add_column :users, :first_name, :string
    add_column :users, :last_name, :string

    remove_reference :addresses, :participant, polymorphic: true, index: true
    add_reference :addresses, :user, index: true, foreign_key: true
  end
end
