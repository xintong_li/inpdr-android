class AddPersonalExtraToNpcParticipant < ActiveRecord::Migration
  def change
    add_column :npc_participants, :age_diagnosed_known, :boolean
    add_column :npc_participants, :age_diagnosed_year, :integer
    add_column :npc_participants, :age_diagnosed_month, :integer
    add_column :npc_participants, :family_diagnosed, :integer
    add_column :npc_participants, :family_diagnosed_relation, :string
  end
end
