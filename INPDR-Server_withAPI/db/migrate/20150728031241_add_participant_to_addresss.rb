class AddParticipantToAddresss < ActiveRecord::Migration
  def change
    remove_column :addresses, :participant_id

    add_reference :addresses, :participant, polymorphic: true, index: true
  end
end
