class AddConsentToParticipants < ActiveRecord::Migration
  def change
    add_column :npa_applicant_participants, :consent, :boolean
    add_column :npa_applicant_participants, :consent_name, :string

    add_column :npb_applicant_participants, :consent, :boolean
    add_column :npb_applicant_participants, :consent_name, :string

    add_column :npc_applicant_participants, :consent, :boolean
    add_column :npc_applicant_participants, :consent_name, :string

    add_column :npb_participants, :consent, :boolean
    add_column :npb_participants, :consent_name, :string

    add_column :npc_participants, :consent, :boolean
    add_column :npc_participants, :consent_name, :string

  end
end
