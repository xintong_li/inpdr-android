class CreateSpecialists < ActiveRecord::Migration
  def change
    create_table :specialists do |t|
      t.string :name
      t.string :address
      t.string :email
      t.integer :patient_id
      t.string :patient_type

      t.timestamps null: false
    end

    add_index :specialists, :patient_id
  end
end
