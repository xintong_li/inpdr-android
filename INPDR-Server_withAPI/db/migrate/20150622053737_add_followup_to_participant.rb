class AddFollowupToParticipant < ActiveRecord::Migration
  def change
    add_reference :npc_followups, :followable, polymorphic: true, index: true
    add_reference :npb_followups, :followable, polymorphic: true, index: true
  end
end
