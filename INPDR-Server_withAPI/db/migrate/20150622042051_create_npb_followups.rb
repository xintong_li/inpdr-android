class CreateNpbFollowups < ActiveRecord::Migration
  def change
    create_table :npb_followups do |t|
      t.integer :symptom_bone, null: false
      t.integer :symptom_abdominal, null: false
      t.integer :symptom_pain, null: false
      t.integer :symptom_breathlessness_rate, null: false
      t.integer :symptom_breathlessness_impact, null: false
      t.integer :symptom_bleeding_rate, null: false
      t.integer :symptom_bleeding_impact, null: false
      t.integer :symptom_fatigue_rate, null: false
      t.integer :symptom_fatigue_impact, null: false
      t.integer :symptom_enlarge_organ, null: false
      t.integer :symptom_enlarge_organ_impact, null: false
      t.integer :symptom_slow_growth, null: false
      t.integer :symptom_slow_growth_impact, null: false
      t.integer :symptom_infection, null: false
      t.integer :symptom_fracture, null: false
      t.integer :symptom_cognition, null: false

      t.integer :impact_family_disappointed, null: false
      t.integer :impact_family_give_up, null: false
      t.integer :impact_family_worry_future, null: false
      t.integer :impact_family_closer, null: false

      t.integer :impact_wider_self_miss_career, null: false
      t.integer :impact_wider_carer_miss_career, null: false
      t.integer :impact_wider_emergency, null: false
      t.integer :impact_wider_appointment, null: false

      t.timestamps null: false
    end
  end
end
