class AddAddressToSpecialists < ActiveRecord::Migration
  def change
    add_reference :specialists, :hospital_address, references: :addresses, index: true, foreign_key: true

    remove_column :specialists, :address
  end
end
