class AddNotCompleteToParticipants < ActiveRecord::Migration
  def change
    add_column :npa_applicant_participants, :not_finish, :boolean, :default => true
    add_column :npb_applicant_participants, :not_finish, :boolean, :default => true
    add_column :npc_applicant_participants, :not_finish, :boolean, :default => true
    add_column :npb_participants, :not_finish, :boolean, :default => true
    add_column :npc_participants, :not_finish, :boolean, :default => true
    add_column :patients, :not_finish, :boolean, :default => true
  end
end
