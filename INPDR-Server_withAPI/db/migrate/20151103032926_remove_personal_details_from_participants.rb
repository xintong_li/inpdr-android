class RemovePersonalDetailsFromParticipants < ActiveRecord::Migration
  def change
    remove_column :npc_participants, :email
    remove_column :npc_participants, :first_name
    remove_column :npc_participants, :middle_name
    remove_column :npc_participants, :surname
    remove_column :npc_participants, :previous_surname

    remove_column :npc_applicant_participants, :email
    remove_column :npc_applicant_participants, :applicant_first_name
    remove_column :npc_applicant_participants, :applicant_surname
    remove_column :npc_applicant_participants, :previous_surname

    remove_column :npb_applicant_participants, :email
    remove_column :npb_applicant_participants, :applicant_first_name
    remove_column :npb_applicant_participants, :applicant_surname
    remove_column :npb_applicant_participants, :previous_surname

    remove_column :npb_participants, :email
    remove_column :npb_participants, :first_name
    remove_column :npb_participants, :middle_name
    remove_column :npb_participants, :surname
    remove_column :npb_participants, :previous_surname

    remove_column :npa_applicant_participants, :email
    remove_column :npa_applicant_participants, :applicant_first_name
    remove_column :npa_applicant_participants, :applicant_surname
    remove_column :npa_applicant_participants, :previous_surname
  end
end
