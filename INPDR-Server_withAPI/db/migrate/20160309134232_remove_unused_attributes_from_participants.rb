class RemoveUnusedAttributesFromParticipants < ActiveRecord::Migration
  def change
    remove_column :npa_applicant_participants, :gender, :integer
    remove_column :npa_applicant_participants, :first_name, :string
    remove_column :npa_applicant_participants, :middle_name, :string
    remove_column :npa_applicant_participants, :surname, :string
    remove_column :npa_applicant_participants, :hospital_name, :string
    remove_column :npa_applicant_participants, :hospital_email, :string
    remove_column :npa_applicant_participants, :hospital_clinical_name, :string
    remove_column :npa_applicant_participants, :hospital_phone, :string

    remove_column :npc_applicant_participants, :gender, :integer
    remove_column :npc_applicant_participants, :first_name, :string
    remove_column :npc_applicant_participants, :middle_name, :string
    remove_column :npc_applicant_participants, :surname, :string
    remove_column :npc_applicant_participants, :hospital_name, :string
    remove_column :npc_applicant_participants, :hospital_email, :string
    remove_column :npc_applicant_participants, :hospital_clinical_name, :string
    remove_column :npc_applicant_participants, :hospital_phone, :string

    remove_column :npc_participants, :gender, :integer    
    remove_column :npc_participants, :hospital_name, :string
    remove_column :npc_participants, :hospital_email, :string
    remove_column :npc_participants, :hospital_clinical_name, :string
    remove_column :npc_participants, :hospital_phone, :string

    remove_column :npb_applicant_participants, :gender, :integer
    remove_column :npb_applicant_participants, :first_name, :string
    remove_column :npb_applicant_participants, :middle_name, :string
    remove_column :npb_applicant_participants, :surname, :string
    remove_column :npb_applicant_participants, :hospital_name, :string
    remove_column :npb_applicant_participants, :hospital_email, :string
    remove_column :npb_applicant_participants, :hospital_clinical_name, :string
    remove_column :npb_applicant_participants, :hospital_phone, :string

    remove_column :npb_participants, :gender, :integer
    remove_column :npb_participants, :hospital_name, :string
    remove_column :npb_participants, :hospital_email, :string
    remove_column :npb_participants, :hospital_clinical_name, :string
    remove_column :npb_participants, :hospital_phone, :string
  end
end
