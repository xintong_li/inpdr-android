class UpdatePolymorphicAddress < ActiveRecord::Migration
  def change
    remove_column :addresses, :user_id
    remove_column :npc_participants, :hospital_address_id
    remove_column :npc_applicant_participants, :hospital_address_id
    remove_column :npb_participants, :hospital_address_id
    remove_column :npb_applicant_participants, :hospital_address_id
    remove_column :npa_applicant_participants, :hospital_address_id
    remove_column :specialists, :hospital_address_id

    add_reference :addresses, :person, polymorphic: true, index: true
  end
end
