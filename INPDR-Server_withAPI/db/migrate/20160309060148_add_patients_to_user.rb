class AddPatientsToUser < ActiveRecord::Migration
  def change
    add_reference :patients, :user, index: true, foreign_key: true
    add_reference :npc_participants, :patient, index: true, foreign_key: true
    add_reference :npc_applicant_participants, :patient, index: true, foreign_key: true
    add_reference :npb_participants, :patient, index: true, foreign_key: true
    add_reference :npb_applicant_participants, :patient, index: true, foreign_key: true
    add_reference :npa_applicant_participants, :patient, index: true, foreign_key: true

    drop_table :parti_users
  end
end
