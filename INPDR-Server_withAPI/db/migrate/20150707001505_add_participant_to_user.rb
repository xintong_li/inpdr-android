class AddParticipantToUser < ActiveRecord::Migration
  def change
    add_reference :users, :participant, polymorphic: true, index: true
  end
end
