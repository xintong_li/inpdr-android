class CreateNpaApplicantParticipants < ActiveRecord::Migration
  def change
    create_table :npa_applicant_participants do |t|
      t.string :email, null: false
      t.integer :gender
      t.date :date_of_birth
      t.string :first_name, null: false
      t.string :middle_name
      t.string :surname, null:false
      t.string :previous_surname
      t.string :applicant_first_name, null: false
      t.string :applicant_surname, null: false

      t.timestamps null: false
    end
  end
end
