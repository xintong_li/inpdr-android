class CreatePatients < ActiveRecord::Migration
  def change
    create_table :patients do |t|
      t.string :registrant_first_name
      t.string :registrant_last_name
      t.integer :relation_to_patient
      t.references :addresses, index: true, foreign_key: true
      t.string :patient_first_name
      t.string :patient_last_name
      t.integer :gender
      t.date :date_of_birth

      t.timestamps null: false
    end
  end
end
