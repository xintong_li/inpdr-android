class AddVerifiedToParticipants < ActiveRecord::Migration
  def change
    add_column :npc_participants, :verified, :boolean, default: false
    add_column :npc_applicant_participants, :verified, :boolean, default: false
    add_column :npb_participants, :verified, :boolean, default: false
    add_column :npb_applicant_participants, :verified, :boolean, default: false
    add_column :npa_applicant_participants, :verified, :boolean, default: false
  end
end
