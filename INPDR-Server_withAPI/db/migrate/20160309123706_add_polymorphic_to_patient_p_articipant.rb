class AddPolymorphicToPatientPArticipant < ActiveRecord::Migration
  def change
    add_reference :patients, :enrollable, :polymorphic => true, :index => true

    remove_reference :npc_participants, :patient, index: true
    remove_reference :npb_participants, :patient, index: true
    remove_reference :npc_applicant_participants, :patient, index: true
    remove_reference :npb_applicant_participants, :patient, index: true
    remove_reference :npa_applicant_participants, :patient, index: true

  end
end
